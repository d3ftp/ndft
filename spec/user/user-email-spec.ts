import { TestUserDispatcher } from "../helpers/test-user-dispatcher";
import { UserEmail } from "../../app/server/user/user-email";
import { GetUser } from "../../app/server/user/get-user";

import emailChange = require('../../app/server/database/models/email-change.model');

let verificationCode: string;
let newEmail = 'lnewmail@test.p'
describe('User Email', () => {
    beforeAll((done) => {
        TestUserDispatcher.createDefaultUser().then(() => {
            done();
        })
    });

    describe('get', () => {
        it('should return email', async (done) => {
            let user = await new GetUser().by({ name: TestUserDispatcher.defaultUser.name });
            let email = await new UserEmail().getEmail(user._id);

            expect(email).toEqual(TestUserDispatcher.defaultUser.email);
            done();
        })

        it('should throw error with wrong id', (done) => {
            new UserEmail().getEmail('fffffffffffffff').then(() => {
                expect(false).toBeTruthy('Should dont invoke');
                done();
            }).catch(err => {
                expect(true).toBeTruthy();
                done();
            });
        });

        it('should throw error with null', (done) => {
            new UserEmail().getEmail(null)
                .then(() => {
                    expect(false).toBeTruthy('Should dont invoke');
                    done();
                })
                .catch(err => {
                    expect(true).toBeTruthy();
                    done();
                });
        });

        it('should throw error with undefined', (done) => {
            let user = {};
            new UserEmail().getEmail((user as any).id).then(() => {
                expect(false).toBeTruthy('Should dont invoke');
                done();
            }).catch(err => {
                expect(true).toBeTruthy();
                done();
            });
        });
    })

    describe('set verification', () => {
        it('should set verification code', async (done) => {
            let doc = await new GetUser().by({ name: TestUserDispatcher.defaultUser.name });
            await new UserEmail().createVerificationCodeModel(doc._id, newEmail);
            let changeEmailDoc = await emailChange.model.findOne({ user: doc._id });
            verificationCode = changeEmailDoc.verificationCode;

            expect(changeEmailDoc).toBeDefined();
            expect(changeEmailDoc.email).toEqual(newEmail);
            expect(changeEmailDoc.expires).toBeDefined();

            done();
        });
    })

    describe('change', () => {
        it('should dont return error', async (done) => {
            let doc = await new GetUser().by({ name: TestUserDispatcher.defaultUser.name });
            await new UserEmail().changeEmail(verificationCode);
            doc = await new GetUser().by({ name: TestUserDispatcher.defaultUser.name });
            expect(doc.email).toBe(newEmail)
            done();
        });

        it('should return error no exist id', (done) => {
            new UserEmail().changeEmail('5acbcc1ea13607309092aaf6').then(() => {
                expect(false).toBeTruthy('Should dont invoke');
                done();
            }).catch(err => {
                expect(true).toBeTruthy();
                done();
            });
        });

        it('should return error wrong id', (done) => {
            new UserEmail().changeEmail('ffffffffffff').then((doc) => {
                expect(false).toBeTruthy('Should dont invoke');
                done();
            }).catch(err => {
                expect(true).toBeTruthy();
                done();
            });
        });

        it('should return error null id', (done) => {
            new UserEmail().changeEmail(null).then(() => {
                expect(false).toBeTruthy('Should dont invoke');
                done();
            }).catch(err => {
                expect(true).toBeTruthy();
                done();
            });
        });

        it('should return error undefined ', (done) => {
            new UserEmail().changeEmail(undefined).then(() => {
                expect(false).toBeTruthy('Should dont invoke');
                done();
            }).catch(err => {
                expect(true).toBeTruthy();
                done();
            });
        });

    });

    describe('', () => {
        it('should dont with wrong verification code', (done) => {
            new UserEmail().removeVerificationCode('ffffff').then(() => {
                expect(false).toBeTruthy('Should dont invoke');
                done();
            }).catch(err => {
                expect(true).toBeTruthy();
                done();
            });
        })

        it('should dont with null parametr', (done) => {
            new UserEmail().removeVerificationCode(null).then(() => {
                expect(false).toBeTruthy('Should dont invoke');
                done();
            }).catch(err => {
                expect(true).toBeTruthy();
                done();
            });
        })

        it('should dont with undefined parametr', (done) => {
            new UserEmail().removeVerificationCode(undefined).then(() => {
                expect(false).toBeTruthy('Should dont invoke');
                done();
            }).catch(err => {
                expect(true).toBeTruthy();
                done();
            });
        })

        it('should remove verification code', (done) => {
            new UserEmail().removeVerificationCode(verificationCode).then((res) => {
                expect(res).toBeTruthy();
                done();
            }).catch(err => {
                expect(false).toBeTruthy('Should dont invoke');
                done();
            });
        })

        describe('', () => {
            it('email should be available', (done) => {
                new UserEmail().isEmailAvailable('super-available-email@os.pl').then(res => {
                    expect(res).toBeTruthy();
                    done();
                })
            });

            it('emailshould not be available', (done) => {
                new UserEmail().isEmailAvailable(newEmail).then(res => {
                    expect(res).toBeFalsy();
                    done();
                })
            });
        });
    });

    afterAll((done) => {
        TestUserDispatcher.deleteDefaultUser().then(() => {
            done();
        });
    });
});