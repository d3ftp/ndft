import { UserProfileImageDispatcher } from '../../app/server/user/user-profile-image-dispatcher';
import { GetUser } from '../../app/server/user/get-user';
import userModel = require('../../app/server/database/models/user.model');

describe('user Profile image dispatcher', () => {
    const fileName = 'testFile.jpg';

    it('should add file name', (done) => {
        new GetUser().by({ name: 'john' }).then(doc => {
            let id = doc._id;
            new UserProfileImageDispatcher().setImage(id, fileName).then((d) => {
                userModel.model.findById({ _id: id }).then(doc => {
                    expect(doc.profileImage).toEqual(fileName);
                    done();
                })
            })
        })

    });

    it('should get file name', (done) => {
        new GetUser().by({ name: 'john' }).then(doc => {
            let id = doc._id;
            new UserProfileImageDispatcher().getImage(id).then((_fileName) => {
                expect(_fileName).toEqual(fileName);
                done();
            })
        })

    });
});