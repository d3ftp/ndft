import { GetUser } from '../../app/server/user/get-user';
import { PopulateField } from '../../app/server/user/populate-fields';

describe('GetUser', () => {
    it('should get John account by name', (done) => {
        new GetUser().by({ name: 'john' }).then((resolve) => {
            expect(resolve).not.toBeNull();
            done();
        });
    });

    it('should get John account by email', (done) => {
        new GetUser().by({ email: 'john@email.com' }).then((resolve) => {
            expect(resolve).not.toBeNull();
            done();
        });
    });

    it('should get John account as user object', (done) => {
        new GetUser().asObject({ name: 'john' }).then((user) => {
            expect(user.groups).toBeDefined();
            expect(user.name).toEqual('john')
            done();
        });
    });

    it('should get John and populate field', (done) => {
        new GetUser().populate({ name: 'john' }, PopulateField.USER_DEFAULT_GROUP).then(doc => {
           expect(doc.groups).toBeDefined();
           done();
        })
    });
})