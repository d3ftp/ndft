import { ActiveUser } from '../../app/server/user/active-user';
import userModel = require('../../app/server/database/models/user.model');
import { GetUser } from '../../app/server/user/get-user';
import waitsForAndRun = require('../helpers/waits-for-and-run');
import { WaitsForAndRun } from '../helpers/waits-for-and-run';

describe("active user", () => {
    var document: userModel.I_UserModel;
    it('should active account', (done) => {
        new GetUser().by({ name: 'john' }).then(user => {
            new ActiveUser().by(user.VerificationCode).then(res => {
                done();
            });
        })
    });

    it('should get document', (done) => {
        userModel.model.findOne({ name: 'john' }).then(doc => {
            expect(doc).not.toBeNull();

            document = doc;
            done();
        });
    });

    it('should remove verificationCode value from document', (done) => {
        let wait = new WaitsForAndRun();
        var flag = null;
        wait.setEscapeTime(9000);
        wait.forFunction(() => {

            new GetUser().by({ name: 'john' }).then(doc => {
                flag = doc.VerificationCode;
            });
            if(flag === undefined) return true;
        });

        wait.after(() => {
            expect(flag).toBeUndefined();
            done();
        });
    });

    //
    // it('should remove expires value from document', (done) => {
    //     let wait = new WaitsForAndRun();
    //     var flag = null;
    //     wait.setEscapeTime(9000);
    //     wait.forFunction(() => {

    //         new GetUser().by({ name: 'john' }).then(doc => {
    //             flag = doc.expires;
    //         });
    //         if(flag === undefined) return true;
    //     });

    //     wait.after(() => {
    //         expect(flag).toBeUndefined();
    //         done();
    //     });
    // });

    it('should set isActibe to true', (done) => {
        new GetUser().by({ name: 'john' }).then(doc => {
            expect(doc.isActive).toBeTruthy();
            done();
        });
    });


});