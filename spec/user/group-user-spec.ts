import { GroupDispatcher } from "../../app/server/user/group-dispatcher";
import { GetUser } from "../../app/server/user/get-user";
import { GroupUser } from "../../app/server/user/group-user";
import { SystemConfig } from "../../app/server/system-config";
import { PopulateField } from "../../app/server/user/populate-fields";


describe("Group user", () => {
    it(' - user should dont have group', (done) => {
        new GetUser().by({ name: 'admin' }).then((doc) => {
            expect(doc.groups.length).toEqual(0);
            done();
        });
    });

    it('should set group', (done) => {
        SystemConfig.get().getDefaultUserGroup().then(defaultUserGroup => {
            new GroupUser().set('admin', [defaultUserGroup]).then(() => {
                new GetUser().populate({ name: 'admin' }, PopulateField.USER_DEFAULT_GROUP).then(user => {
                    expect(user.groups[0].name).toEqual(defaultUserGroup.name);
                    done();
                })
            });
        })
    });
});