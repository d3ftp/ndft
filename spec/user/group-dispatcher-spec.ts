import UserGroupModel = require('../../app/server/database/models/user-group.model');
import { I_UserGroup } from "../../app/server/database/models/i_user-group";
import { GroupDispatcher } from '../../app/server/user/group-dispatcher';
import { RouteDispatcher, QueueSync } from '../../app/server/permission/route-dispatcher';
import { Shell } from '../helpers/shell-Id';

var groupDispatcher: GroupDispatcher;
var id;
var userGroups: I_UserGroup[];
var shell: Shell = new Shell();

describe('Group Disptacher', () => {
    beforeAll(() => {
        groupDispatcher = GroupDispatcher.get();
    });

    it('schould create new group and return true', (done) => {
        groupDispatcher.create('testGroup', [
            RouteDispatcher.routePathToID('/test/name/:data', 'post'),
            RouteDispatcher.routePathToID('/test/name/:data', 'get'),
            RouteDispatcher.routePathToID('/test/page/:id/:value', 'get')
        ]).then((res) => {
            expect(res).toBeTruthy();
            done();
        });
    });

    it('should return all groups', (done => {
        groupDispatcher.getAll().then(docs => {
            userGroups = docs;
            expect(userGroups.length).not.toEqual(0);
            done();
        })
    }))

    it('schould return true when group exist in database', (done) => {
        groupDispatcher.isExist('testGroup').then((res) => {
            expect(res).toBeTruthy();
            done();
        });
    });

    it('schould return user group document', (done) => {
        groupDispatcher.get(shell.byName('testGroup', userGroups)).then((res) => {
            expect(res).not.toBeNull();
            done();
        });
    });

    it('schould not return user group document', (done) => { 
        groupDispatcher.get('NOT_EXISTING_GROUP').then((res) => {
            expect(res).toBeNull();
            done();
        }).catch(err => {
            console.log(err);
        })
    });

    it('schould return false when group not esist in database', (done) => {
        groupDispatcher.isExist('NOT_EXISTING_GROUP').then((res) => {
            expect(res).toBeFalsy();
            done();
        });
    });

    it('should update group permission and return true', (done => {//
        groupDispatcher.updatePermission(shell.byName('testGroup', userGroups), [
            RouteDispatcher.routePathToID('/test/name/:data', 'put'),
        ]).then(result => {
            expect(result).toBeTruthy();
            groupDispatcher.get(shell.byName('testGroup', userGroups)).then(resultUserGroup => {
                expect(resultUserGroup.routes[0]._id).toEqual(RouteDispatcher.routePathToID('/test/name/:data', 'put'));
                done();
            });
        });
    }));

    it('should update group Name and return true', (done) => {
        groupDispatcher.updateGroupName(shell.byName('testGroup', userGroups), 'UpdatedTestGroup').then((res) => {
            expect(res).toBeTruthy();
            done();
        })
    });

    it('should not update group name and return false', (done) => {
        groupDispatcher.updateGroupName(shell.byName('testGroup', userGroups), 'UpdatedTestGroup').then((res) => {
            expect(res).toBeFalsy();
            done();
        })
    });

    it('should update group name and routes', (done) => {
        groupDispatcher.update(shell.byName('testGroup', userGroups), "SuperUpdatedTestGroup", []).then(res => {
            expect(res).toBeTruthy();
            done();
        })
    });

    it('should dont update group name and routes', (done) => {
        groupDispatcher.update("", "SuperUpdatedTestGroup", []).then(res => {
            expect(res).toBeFalsy();
            done();
        })
    });

    it('should dont update group name and routes - no name given', (done) => {
        groupDispatcher.update(shell.byName('testGroup', userGroups), "", []).then(res => {
            expect(res).toBeFalsy();
            done();
        })
    });

    it('should return 5 groups', (done) => {
        groupDispatcher.create('MyNewTestGroup', [
            RouteDispatcher.routePathToID('/test/name/:data', 'post'),
            RouteDispatcher.routePathToID('/test/name/:data', 'get')
        ]).then((val) => {
            groupDispatcher.getAll().then((array) => {
                expect(array.length).toBeGreaterThanOrEqual(5);
                userGroups = userGroups.concat(array);
                done();
            });
        });
    });

    it('should return all groups with populate short form', (done) => { // Moze robić problemy z zwracaniem wartosci
        groupDispatcher.getAllAndPopulate().then(result => {
            expect(result[4].name).toBe("MyNewTestGroup");

            expect(result[4].routes[0].path).toBe('/test/name/:data');
            expect(result[4].routes[0].method).toBe('post');

            expect(result[4].routes[1].path).toBe('/test/name/:data');
            expect(result[4].routes[1].method).toBe('get');
            done();
        });
    });

    it('should remove group and return null when operation was end successful', (done) => {
        groupDispatcher.remove(shell.byName('testGroup', userGroups)).then((err) => {
            expect(err).toBeNull();
            groupDispatcher.getAll().then(array => {
                expect(array.length).toBe(4);
                expect(array[3].name).toBe('MyNewTestGroup');
                groupDispatcher.get(shell.byName(userGroups[0].name, userGroups)).then(result => {
                    expect(result).not.toBeNull();
                    done();
                })
            });
        });
    });
})
