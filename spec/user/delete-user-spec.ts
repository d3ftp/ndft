import { TestUserDispatcher } from "../helpers/test-user-dispatcher";
import { DeleteUser } from "../../app/server/user/delete-user";
import { GetUser } from "../../app/server/user/get-user";

describe('delete user', () => {
    beforeAll(async (done) => {
        await TestUserDispatcher.createUser('todelete', '1234', 'todelte@tt.pl', '333333')
        await TestUserDispatcher.ActiveUser('todelete');

        await TestUserDispatcher.createUser('todelete1', '123x4', 'todexlte@tt.pl', '33x3333')
        await TestUserDispatcher.ActiveUser('todelete1');

        done();
    });

    describe('by ID', () => {
        it('should dont delete user with wrong id, and should reject', (done) => {
            new DeleteUser().delete('fffffffffffffff').then(() => {
                expect(true).toBeFalsy('Should dont invoke!');
                done();
            }).catch(() => {
                expect(true).toBeTruthy();
                done();
            })

        });

        it('should dont delete user with wrong id as null, and should reject', (done) => {
            new DeleteUser().delete(null).then(() => {
                expect(true).toBeFalsy('Should dont invoke!');
                done();
            }).catch(() => {
                expect(true).toBeTruthy();
                done();
            })

        });

        it('should delete user by id', (done) => {
            new GetUser().by({ name: 'todelete' }).then((user) => {
                new DeleteUser().delete(user._id).then(() => {
                    new GetUser().by({ name: 'todelete' }).then(res => {
                        expect(res).toBeNull();
                        done();
                    })
                });
            })
        });

    });


    describe('by Name', () => {
        it('should dont delete user with wrong name, and should reject', (done) => {
            new DeleteUser().deleteByName('xcxcxc').then(() => {
                expect(true).toBeFalsy('Should dont invoke!');
                done();
            }).catch(() => {
                expect(true).toBeTruthy();
                done();
            })

        });

        it('should dont delete user with wrong id as null, and should reject', (done) => {
            new DeleteUser().deleteByName(null).then(() => {
                expect(true).toBeFalsy('Should dont invoke!');
                done();
            }).catch(() => {
                expect(true).toBeTruthy();
                done();
            })

        });

        it('should delete user by id', (done) => {
            new DeleteUser().deleteByName('todelete1').then(() => {
                new GetUser().by({ name: 'todelete1' }).then(res => {
                    expect(res).toBeNull();
                    done();
                })
            }).catch((err) => {
                expect(false).toBeTruthy(err);
                done();
            });
        });
    });
});