import { SystemConfig } from "../app/server/system-config";
import { GroupDispatcher } from "../app/server/user/group-dispatcher";
import systemConfigModel = require("../app/server/database/models/system-config.model");

var config: SystemConfig;
describe('system config', () => {
    let defaultUserGroupName = 'user';
    let defaultGuestGroupName = 'superuser';

    it('instance should be defined', () => {
        expect(SystemConfig.get()).toBeDefined();
        expect(SystemConfig.get()).not.toBeNull();
        config = SystemConfig.get();
    });

    it('should set default user group', (done) => {
        GroupDispatcher.get().getByName(defaultUserGroupName).then(group => {
            config.setDafultUserGroup(group._id).then(res => {
                expect(res).toBeTruthy();
                systemConfigModel.model.findOne({}).populate('defaultUserGroup').then((doc) => {
                    expect(doc.defaultUserGroup.name).toBe(defaultUserGroupName);
                    done();
                });
            })
        })

    });

    it('should get populated default user group', (done) => {
        SystemConfig.get().getDefaultUserGroup().then(group => {
            expect(group.name).toEqual(defaultUserGroupName);
            done();
        });
    });

    it('should set default guest group', (done) => {
        GroupDispatcher.get().getByName(defaultGuestGroupName).then(group => {
            config.setDefaultGuestGroup(group._id).then(res => {
                expect(res).toBeTruthy();
                systemConfigModel.model.findOne({}).populate('defaultGuestGroup').then((doc) => {
                    expect(doc.defaultGuestGroup.name).toBe(defaultGuestGroupName);
                    done();
                });
            })
        })
    });

    it('should get populated default guest group', (done) => {
        SystemConfig.get().getDefaultGuestGroup().then(group => {
            expect(group.name).toEqual(defaultGuestGroupName);
            done();
        });
    });

    let title = 'test Title'
    it('should change title', (done) => {
        let title = 'test Title'
        config.setWebsiteTitle(title).then(result => {
            expect(result).toBeTruthy();
            systemConfigModel.model.findOne({}).then(doc => {
                expect(doc.websiteTitle).toBe(title);
                done();
            })
        })
    })

    it('should get title', (done) => {
        config.getWebsiteTitle().then(websiteTitle => {
            expect(websiteTitle).toBe(title);
            done();
        })
    })

    it('should get emailConfirmTime', (done) => {
        config.getEmailConfirmTime().then(time => {
            expect(time).toBeDefined();
            done();
        })
    });

    it('should set emailConfirmTime', (done) => {
        let emailConfirmTime = 48;

        config.setEmailConfirmTime(emailConfirmTime).then(() => {
            systemConfigModel.model.findOne({}).then(doc => {
                expect(doc.emailConfirmTime).toBe(emailConfirmTime);
                done();
            })
        })
    });

    it('should get emailBlockedDomains', (done) => {
        config.getEmailBlockedDomains().then(blockedDomains => {
            expect(blockedDomains).toBeDefined();
            done();
        })
    });

    it('should set emailBlockedDomains', (done) => {
        let blockedDomains = ["a", "b", "c", "d"];

        config.setEmailBlockedDomains(blockedDomains).then(() => {
            systemConfigModel.model.findOne({}).then(doc => {

                let originalArray = JSON.stringify(doc.emailBlockedDomains);
                let expectedArray = JSON.stringify(blockedDomains);

                expect(originalArray).toEqual(expectedArray);
                done();
            });
        })
    });
});