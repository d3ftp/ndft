

interface I_Mock {
    getTxt(): string;
    getObject(): I_Mock;
    initialize();
}

abstract class Mock implements I_Mock {
    protected static mock: I_Mock;

    abstract getTxt();
    abstract initialize();

     getObject() {
        return this;
    }

    constructor() {
        if (Mock.mock == null) Mock.mock = this.getObject();
        return Mock.mock;
    }

}

class Class1 extends Mock{
    public msg  = 'Siema From class 1';
    public secret = 'Hola Hola kurwo :d';
    getTxt() {
        return this.msg;
    }
    initialize() {
        throw new Error("Method not implemented.");
    }

    public getSecretText(txt) {
        return txt;
    }
}


xdescribe('test zone', () => {
    it('test', () => {

        var a = null;

        var pr = new Promise((res, reject) => {
            if(a === null) reject('na');
            a.init();
        });

        pr.then(() => {
            console.log("BRO");
            
        }).catch((r) => {
            console.log('KURWA');
        })


        function resolvePromise() {
            return rejectPromise();
          }
          
          function rejectPromise() {
            return Promise.reject('HUJ')
          }
          
          resolvePromise().then(() => {
            console.log('resolved');
          }).catch((err) => {
            console.log('errored');
          });

    });

    function* get() {
        for(var el of ['ala', 'kot', 'mlotek']) {
            yield el;
        }
    }
    
});
