import { RegisterMail } from '../../../app/server/nodemailer-module/components/register-mail';
import { I_SystemMailerMessage } from '../../../app/server/nodemailer-module/i_system-mailer-message';
import { SystemMailerMessage } from '../../../app/server/nodemailer-module/system-mailer-message';

describe('RegisterTxTComponent', () => {
    var registerTxtComponet: RegisterMail;

    beforeAll(() => {
        registerTxtComponet = RegisterMail.getInstance();
    });

    it('should has implemented SystemMailerMessage', () => {
        expect(isMailerMessage(registerTxtComponet)).toBeTruthy();
    });

    it('should throw error when link is not defined', () => {
        expect(()=>{registerTxtComponet.getHtmlMsg()}).toThrowError('Message link must be defined before send!');
    });

    it('should throw error when object is not initialized', () => {
        expect(()=>{registerTxtComponet.setLink('testlink.pl')}).toThrowError('Link can be set only, before initialization object in Email Dispatcher!');
    });

    it('should dont throw error when object was initialized', () => {
        registerTxtComponet.initialize('txtMSG', 'this is your register link: ##system_LINK## ', 'registerSubject');
        expect(()=>{registerTxtComponet.setLink('testlink.pl')}).not.toThrow();
    });

    it('should return testlink.pl', () => {
        expect(registerTxtComponet.getLink()).toBe('testlink.pl');
    });

    it('should return converted htmlMsg', () => {
        expect(registerTxtComponet.getHtmlMsg()).toBe('this is your register link: testlink.pl ');
    });



});

function isMailerMessage(object: any): object is SystemMailerMessage {
    return 'getSubject' in object;
}
