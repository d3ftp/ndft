import { Nodemailer } from '../../app/server/nodemailer-module/nodemailer';
import { EmailDispatcher } from '../../app/server/nodemailer-module/email-dispatcher';
import { SystemMailerMessage } from "../../app/server/nodemailer-module/system-mailer-message";

import nodemailerConfig = require('../../app/server/database/models/nodemailer-config.model');
import systemConfigModel = require('../../app/server/database/models/system-config.model');
import systemMessageModel = require('../../app/server/database/models/system-message.model');
import { I_NodemailerConfig } from '../../app/server/database/models/i_nodemailer-config.model';

class MockSystemMessageA extends SystemMailerMessage {
    protected static instance: MockSystemMessageA = null;
    public className: string = 'MockSystemMessageA';

    constructor() {
        super();
        if (MockSystemMessageA.instance == null) MockSystemMessageA.instance = this;
        return MockSystemMessageA.instance;
    }

    public static getInstance(): MockSystemMessageA {
        if (MockSystemMessageA.instance == null) throw new Error('Object dont have static instance. Do you forget initialize it in Email Dispacher? ');
        return MockSystemMessageA.instance;
    }
}

class MockSystemMessageB extends SystemMailerMessage {
    protected static instance: MockSystemMessageB = null;
    public className: string = 'MockSystemMessageB';

    constructor() {
        super();
        if (MockSystemMessageB.instance == null) MockSystemMessageB.instance = this;
        return MockSystemMessageB.instance;
    }

    public static getInstance(): MockSystemMessageB {
        if (MockSystemMessageB.instance == null) throw new Error('Object dont have static instance. Do you forget initialize it in Email Dispacher? ');
        return MockSystemMessageB.instance;
    }
}

class MockSystemMessageC extends SystemMailerMessage {
    protected static instance: MockSystemMessageB = null;
    public className: string = 'MockSystemMessageC';

    constructor() {
        super();
        if (MockSystemMessageC.instance == null) MockSystemMessageC.instance = this;
        return MockSystemMessageC.instance;
    }

    public static getInstance(): MockSystemMessageC {
        if (MockSystemMessageC.instance == null) throw new Error('Object dont have static instance. Do you forget initialize it in Email Dispacher? ');
        return MockSystemMessageC.instance;
    }
}

describe('Email Dispatcher', () => {
    var emailDispatcher: EmailDispatcher;

    var A: MockSystemMessageA;
    var B: MockSystemMessageB;
    var C: MockSystemMessageC;

    beforeAll(() => {
        EmailDispatcher.JASMINE_RESET_NODEMAILER();
        A = new MockSystemMessageA();
        B = new MockSystemMessageB();
        C = new MockSystemMessageC();

        A.initialize('txtmsg-A', 'htmlMSG-A', 'testSubject-A');
        B.initialize('txtmsg-B', 'htmlMSG-B', 'testSubject-B');
        C.initialize('txtmsg-C', 'htmlMSG-C', 'testSubject-C');
    })

    // it('Nodemailer should be undefined before bootstrap', () => {
    //     expect(EmailDispatcher.getNodeMailer()).toBeNull();
    // });

    // it('Nodemailer schema should be undefined', (done) => {
    //     systemConfigModel.model.findOne({}).then(result => {
    //         expect(result.nodemailer.from).toBeUndefined();
    //         done();
    //     });
    // });

    it('Nodemailer should be defined after bootstrap', (done) => {
        EmailDispatcher.bootstrap().then(res => {
            emailDispatcher = new EmailDispatcher();
            expect(EmailDispatcher.getNodeMailer()).toBeDefined();

            done();
        })
    });

    // it('Nodemailer schema should be defined', (done) => {
    //     EmailDispatcher.bootstrap().then(res => {
    //         expect(res).not.toBeNull();
    //         systemConfigModel.model.findOne({}).then(result => {
    //             expect(result.nodemailer.from).toBeDefined();
    //             done();
    //         });
    //     });
    // });

    it('should update Nodemailer in database', (done) => {
        emailDispatcher.UpdateMailerConfig('test@server.pl', 'Gmail', {user: 'test', pass: '123456'}).then(() => {
            
            systemConfigModel.model.findOne({}).then((doc) => {

                expect(doc.nodemailer.auth.user).toBe('test');
                expect(doc.nodemailer.auth.pass).toBe('123456');
                expect(doc.nodemailer.from).toBe('test@server.pl');
                expect(doc.nodemailer.service).toBe('Gmail');
                done();
            })
        })
    });

    it('Mock system Mailers should dont exist in database', (done) => {
        var promise = [];

        [A.getClassName(), B.getClassName(), C.getClassName()].forEach(el => {
            var res = systemMessageModel.model.findOne({ className: el }).exec();
            promise.push(res);
        });

        Promise.all(promise).then(val => {
            val.forEach(el => {
                expect(el).toBeNull();
            })
            done();
        });
    });

    it('Email dispacher should add mailers to database', (done) => {
        EmailDispatcher.initializeSystemMessage([
            MockSystemMessageA.getInstance(),
            MockSystemMessageB.getInstance(),
            MockSystemMessageC.getInstance()
        ]).then(val => {
            done();
        })
    });

    it('Mock system Mailers should exist in database', (done) => {
        var promise = [];

        [A.getClassName(), B.getClassName(), C.getClassName()].forEach(el => {
            var res = systemMessageModel.model.findOne({ className: el }).exec();
            promise.push(res);
        });

        Promise.all(promise).then(val => {
            val.forEach(el => {
                expect(el).not.toBeNull();
            })
            done();
        });
    });

    it('should update Mail Msg', (done) => {
        emailDispatcher.updateMailMessage(MockSystemMessageA.getInstance(), 'Register Mock', '<h1>Hello World</h1>', 'Alfa').then(() => {
            systemMessageModel.model.findOne({ className: MockSystemMessageA.getInstance().className }).then((doc: systemMessageModel.SystemMessageModel) => {
                expect(doc).not.toBeNull();
                expect(doc.subject).toBe('Register Mock');
                expect(doc.html).toBe('<h1>Hello World</h1>');
                expect(doc.text).toBe('Alfa');

                done();
            })
        });
    });

    it('should update RegisterMail object too ', () => {
        expect(MockSystemMessageA.getInstance().getHtmlMsg()).toBe('<h1>Hello World</h1>');
        expect(MockSystemMessageA.getInstance().getSubject()).toBe('Register Mock');
        expect(MockSystemMessageA.getInstance().getTxtMsg()).toBe('Alfa');
    });
});
