import { I_SystemMailerMessage } from '../../app/server/nodemailer-module/i_system-mailer-message';
import { SystemMailerMessage } from '../../app/server/nodemailer-module/system-mailer-message';


class MockSystemMessage extends SystemMailerMessage {
    protected static instance: MockSystemMessage = null;
    public className: string = 'MockSystemMessage';

    constructor() {
        super();
        if (MockSystemMessage.instance == null) MockSystemMessage.instance = this;
        return  MockSystemMessage.instance;
    }

    public static getInstance(): SystemMailerMessage {
        if(MockSystemMessage.instance == null) throw new Error('Object dont have static instance. Do you forget initialize it in Email Dispacher? '); 
        return MockSystemMessage.instance;
    }
}

describe('System Mailer Message', () => {
    var mockSystemMessage: MockSystemMessage;

    it('should throw error when object was never create before', () => {
        expect(() => {MockSystemMessage.getInstance()}).toThrow();
    });

    it('should dont throw error when object was create', () => {
        mockSystemMessage = new MockSystemMessage()
        expect(() => {MockSystemMessage.getInstance()}).not.toThrow();
    });


    it('should has the same instance as other objects', () => {
        var ob1:MockSystemMessage = new MockSystemMessage();
        var ob2:MockSystemMessage = new MockSystemMessage();

        expect(ob1).toBe(mockSystemMessage);
        expect(ob2).toBe(mockSystemMessage);
    });

    it('should has implemented I_SystemMailerMessage', () => {
        expect(isMailerMessage(mockSystemMessage)).toBeTruthy();
    });


    it('should initialize variables and return it', () => {
        mockSystemMessage.initialize('txtMSG', 'htmlMSG', 'subject');
        
        expect(mockSystemMessage.getTxtMsg()).toBe('txtMSG');
        expect(mockSystemMessage.getHtmlMsg()).toBe('htmlMSG');
        expect(mockSystemMessage.getSubject()).toBe('subject');
    });

    it('should initialize variables and return it through interface', () => {
        mockSystemMessage.initialize('txtMSG', 'htmlMSG', 'subject');
        var i_mockSystem: I_SystemMailerMessage = mockSystemMessage;

        expect(i_mockSystem.getTxtMsg()).toBe('txtMSG');
        expect(i_mockSystem.getHtmlMsg()).toBe('htmlMSG');
        expect(i_mockSystem.getSubject()).toBe('subject');
    });


});

function isMailerMessage(object: any): object is I_SystemMailerMessage {
    return 'getSubject' in object;
}
