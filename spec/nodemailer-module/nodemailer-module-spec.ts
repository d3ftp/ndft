import {Nodemailer} from '../../app/server/nodemailer-module/nodemailer';
import { SystemMailerMessage } from "../../app/server/nodemailer-module/system-mailer-message";
import { JasmineNodemailer } from './jasmine-nodemailer';

class MockMessage extends SystemMailerMessage{
    protected className: string = 'MockMessage123'
}

describe('Nodemailer module', () => {
    var nodemailer: Nodemailer;
    var mockMsg = new MockMessage();

    beforeAll(() => {
        nodemailer = new Nodemailer();
        mockMsg.initialize('txtmsg', 'htmlMSG', 'testSubject')
        new JasmineNodemailer().init(nodemailer);
    });

    it('should send message', (done) => {
        var result = nodemailer.sendMail(new MockMessage, 'lukasz199312@gmail.com');
        result.then( val => {
            expect(val.accepted).toBeDefined();
            expect(val.rejected.length).toEqual(0);
            done();
        }, res => {
            done();
        })
    });
})