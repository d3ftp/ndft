import { Server } from '../server';
import { IndexRoute } from '../../app/server/routes/index.route';
import { User } from '../../app/server/user/user';
import { Router, Express } from "express";
import * as express from "express";
import request = require('supertest');
import { RoutesRoute } from '../../app/server/routes/routes.route';
import { GroupDispatcher } from '../../app/server/user/group-dispatcher';
import { AccessControlList } from '../../app/server/permission/access-control-list';
var bodyParser = require('body-parser');

var server: Server;
describe('Share user session', () => {

    beforeAll((done) => {
        server = Server.bootstrap();
        server.app.use(bodyParser.json());
        server.app.use(bodyParser.urlencoded({ extended: false }));
        let router = express.Router();
        router.get('/test/init', (req, res) => {
            GroupDispatcher.get().getByName('user').then( group => {
                let user: User = new User('','test_session', [group]);
                req.session.user = user;
                res.send({ complete: true });
            })
        });

        router.get('/test/get-session', (req, res) => {
            GroupDispatcher.get().getByName('user').then( group => {
                res.send(req.session.user);
            })
        });

        server.app.use(router);
        done();
    })

    it('should init session', (done) => {
        server.agent.get('/test/init').expect(200, (err, res) => {
            expect(res.body.complete).toBeTruthy();
            done();
        });
    })

    it('user should be defined', (done) => {
        server.agent.get('/test/get-session').expect(200, (err, res) => {
            expect(res.body.name).toEqual('test_session');
            done();
        });
    });

    it('user should confirm access', (done) => {
        server.agent.get('/test/get-session').expect(200, (err, res) => {
            let user: User = res.body;
            expect(AccessControlList.hasAccess( user.accessControlList, '/test/name/ala', 'post' )).toBeTruthy();
            expect(AccessControlList.hasAccess( user.accessControlList, '/test/name/ala', 'get' )).toBeTruthy();
            done();
        });
    });

    it('user should reject access', (done) => {
        server.agent.get('/test/get-session').expect(200, (err, res) => {
            let user: User = res.body;
            AccessControlList.hasAccess( user.accessControlList, '/test/usera', 'delete' );
            AccessControlList.hasAccess( user.accessControlList, '/test/user', 'get' );
            done();
        });
    });

});
