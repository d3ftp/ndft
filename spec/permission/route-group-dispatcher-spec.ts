import groupRouteModel = require("../../app/server/database/models/route-group.model")
import { RouteGroupDispatcher } from "../../app/server/permission/route-group-dispatcher";
import { RouteDispatcher } from "../../app/server/permission/route-dispatcher";
import { Error } from "mongoose";
import { Shell } from "../helpers/shell-Id";
import { I_UserGroup } from "../../app/server/database/models/i_user-group";
import { GroupDispatcher } from "../../app/server/user/group-dispatcher";

describe("Group route dispatcher", () => {
    var routeGroupDispatcher: RouteGroupDispatcher;
    var routeName: string;
    var shell: Shell = new Shell();
    var groups: I_UserGroup[] = [];
    var userGroups: I_UserGroup[] = [];

    beforeAll((done) => {
        routeGroupDispatcher = new RouteGroupDispatcher();

        groupRouteModel.model.remove({}, (err => {
            done();
        }))
    })

    afterAll((done) => {
        groupRouteModel.model.remove({}, (err => {
            done();
        }))
    })

    it("should be empty", (done) => {
        groupRouteModel.model.find({}, (err, docs) => {
            expect((docs.length)).toBe(0);
            done();
        })
    });

    it("should get all usergroup", (done) => {
        GroupDispatcher.get().getAll().then(docs => {
            for(let doc of docs) {
                userGroups.push(doc);
            }
            expect(()=> {shell.byName("moderator", userGroups)}).not.toThrowError();
            expect(()=> {shell.byName("moderator123", userGroups)}).toThrowError();
            done();
        })
    })

    it("should reject promise when there is no group ", (done) => {
        routeGroupDispatcher.get("test").then(res => {
            expect(true).toBeFalsy("Should reject promise!");
            done();
        }).catch(err => {
            expect(true).toBeTruthy();
            done();
        })
    });

    it("should add new routes group ", (done) => {
        new RouteDispatcher().getAll().then(docs => {
            routeName = docs[0].path;
            
            routeGroupDispatcher.add("register", docs[0]._id).then(res => {
                routeGroupDispatcher.add("userProfile", [docs[1]._id, docs[2]._id]).then(res => {
                    expect(res).toBeTruthy();
                    done();
                })
            })
        })
    });

    it('should get all groups', (done) => {
        routeGroupDispatcher.getAll().then((docs) => {
            groups = docs;
            expect(groups.length).toBeGreaterThan(0);
            done();
        })
    });

    it("should dont add group when group exist in db ", (done) => {
        new RouteDispatcher().getAll().then(docs => {
            routeGroupDispatcher.add("register", []).then(res => {
                done();
            }).catch(err => {
                expect(err).toBeDefined();
                done();
            })
        })
    });

    it("should get group by specified id", (done) => {
        routeGroupDispatcher.get(shell.byName("register",groups)).then(res => {
            expect(res).toBeDefined();
            expect(res.name).toBe("register");
            done();
        }).catch(err => {
            expect(false).toBeTruthy("Should dont reject!")
            done();
        })
    });

    it("should get all groups", (done) => {
        routeGroupDispatcher.getAll().then((docs) => {
            expect(docs.length).toBe(2);
            done();
        })
    });

    it("should get all groups with populate routes field", (done) => {
        routeGroupDispatcher.getAllPopulated().then((docs) => {
            expect(docs[0].routes[0].path).toBe(routeName);
            done();
        })
    });

    it("should reject promise when group dont exist after delete try", (done) => {
        routeGroupDispatcher.delete("registerx").then((res) => {
            expect(true).toBeFalsy("Error it should reject test!");
            done();
        }).catch(err => {
            expect(true).toBeTruthy();
            done();
        })
    });

    it("should delete group register", (done) => {
        routeGroupDispatcher.delete(shell.byName("register",groups)).then((res) => {
            expect(res).toBeTruthy();
            done();
        }).catch(err => {
            expect(true).toBeFalsy("Error it should reject test!");
            done();
        })
    });

    it("should update group", (done) => {
        new RouteDispatcher().getAll().then(docs => {
            routeGroupDispatcher.update(shell.byName("userProfile", groups), "newGroupName", [docs[3]._id]).then(res => {
                expect(res).toBeTruthy();
                done();
            }).catch(err => {
                expect(true).toBeFalsy("Error it should dont reject test!");
                done();
            })
        })
    });
    
    it("should dont update group", (done) => {
        new RouteDispatcher().getAll().then(docs => {
            routeGroupDispatcher.update("1a4be367891b8a2f9cb99894", "userProfile", [docs[3]._id]).then(res => {
                expect(true).toBeFalsy("Error it should reject test!");
                done();
            }).catch(err => {
                expect(true).toBeTruthy();
                done();
            })
        })
    });

    it("should dont update group 1", (done) => {
        new RouteDispatcher().getAll().then(docs => {
            routeGroupDispatcher.update("newGroupName", "userProfile", [docs[3]._id]).then(res => {
                expect(true).toBeFalsy("Error it should reject test!");
                done();
            }).catch(err => {
                expect(true).toBeTruthy();
                done();
            })
        })
    });

    it("should update group route 2", (done) => {
        new RouteDispatcher().getAll().then(docs => {
            routeGroupDispatcher.update(shell.byName("userProfile", groups), "newGroupName", [docs[1]._id]).then(res => {
                expect(true).toBeTruthy();
                done();
            }).catch(err => {
                expect(true).toBeFalsy("Error it should reject test!");
                done();
            })
        })
    });

    it("should add User group to routeGroup", (done) => {
        GroupDispatcher.get().get(shell.byName("moderator", userGroups)).then(res => {
            routeGroupDispatcher.appendUserGroup(shell.byName("userProfile", groups), [shell.byName("moderator", userGroups)]).then(res => {
                expect(res).toBeTruthy();
                done();
            }).catch(reject => {
                expect(true).toBeFalsy("Should dont reject");
                done();
            })
        })
    });
    
});