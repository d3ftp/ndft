import { AccessControlList } from "../../app/server/permission/access-control-list";

describe('access control list', () => {
    let accessControlList: AccessControlList = new AccessControlList();

    beforeAll(() => {
        accessControlList.initializeRoutesList([
            { id: '1', method: 'post', route: '/api/test', regRoute: makeRegExp('/api/test') },
            { id: '2', method: 'post', route: '/api/test/:ala', regRoute: makeRegExp('/api/test/:ala') },
            { id: '3', method: 'get', route: '/api/test', regRoute: makeRegExp('/api/test') },
            { id: '4', method: 'put', route: '/api/test', regRoute: makeRegExp('/api/test') }
        ])

        let test = makeRegExp('/api/test').toString();
    })

    it('should reject access ', () => {
        expect(AccessControlList.hasAccess(accessControlList, '/api/testc', 'get')).toBeFalsy();
        expect(AccessControlList.hasAccess(accessControlList, '/api/test/:ala', 'get')).toBeFalsy();
        expect(AccessControlList.hasAccess(accessControlList, '5', 'delete')).toBeFalsy();
    });

    it('should confirm access', () => {
        expect(AccessControlList.hasAccess(accessControlList, '/api/test', 'post')).toBeTruthy();
        expect(AccessControlList.hasAccess(accessControlList, '/api/test/ala', 'post')).toBeTruthy();
        expect(AccessControlList.hasAccess(accessControlList, '/api/test', 'get')).toBeTruthy();
        expect(AccessControlList.hasAccess(accessControlList, '/api/test', 'put')).toBeTruthy();
    });
});

function makeRegExp(value: string): RegExp {
    value = value.replace(/:[^\/]+/g, '[^\\/]+');
    value = value.replace(/\/(?!])/g, '\\/');

    return new RegExp('^' + value + '$');
}