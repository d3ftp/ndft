import { IsImage } from "../../app/server/src/is-image";

describe('isImage', () => {
    it('should return true when extension is jpg : sampleName.jpg', () => {
        expect(IsImage.is('sampleName.jpg')).toBeTruthy();
    });

    it('should return true when extension is jpg : sampleN-ame.x.jpg', () => {
        expect(IsImage.is('sampleN-ame.x.jpg')).toBeTruthy();
    });

    it('should return true when extension is jpeg : sampleN-ame.x.jpeg', () => {
        expect(IsImage.is(' sampleN-ame.x.jpeg')).toBeTruthy();
    });

    it('should return true when extension is png : sampleN-ame.txt.png', () => {
        expect(IsImage.is('sampleN-ame.txt.png')).toBeTruthy();
    });

    it('should return false when extension is not image extension : sampleN-ame.x.png.txt', () => {
        expect(IsImage.is('sampleN-ame.x.png.txt')).toBeFalsy();
    });

    it('should return false when extension is not image extension : sampleN-ame.x.png.jpg.txt', () => {
        expect(IsImage.is('sampleN-ame.x.png.jpg.txt')).toBeFalsy();
    });
});