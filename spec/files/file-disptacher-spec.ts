import { FileDispatcher } from "../../app/server/files/file-dispatcher";

import * as fs from 'fs';
import * as path from "path";
import * as uuidv4 from 'uuid/v4';

describe('File-Disptacher', () => {
    let fileDispatcher = new FileDispatcher();
    let fileName = 'sample.txt'
    let filePath = path.join(__dirname, fileName);

    it('should check does file extension is image ', () => {
        expect(fileDispatcher.isImage('testfile.jpg')).toBeTruthy();
        expect(fileDispatcher.isImage('testfile.txt.png')).toBeTruthy();
        expect(fileDispatcher.isImage('testfile.jpg.txt')).toBeFalsy();
    });

    it('should create new file', () => {
        let file = createFile(filePath);
        expect(fs.existsSync(filePath)).toBeTruthy();
    });

    it('should generate random name with extension', () => {
        let name = fileDispatcher.generateRandomName('jpg');
        expect(fileDispatcher.getExtension(name)).toEqual('jpg');
    });

    it('should generate random name without extension', () => {
        let name = fileDispatcher.generateRandomName();
        expect(fileDispatcher.getExtension(name)).toBeUndefined();
    });

    it('should get file extension', () => {
        expect(fileDispatcher.getExtension('testaa.txt.jpg')).toEqual('jpg');
    });

});


function createFile(name) {
    try {
        fs.writeFileSync(name, '');
    } catch (e) {
        throw Error('cant write file!: ' + e);
    }
}