import { TestUserDispatcher } from "../helpers/test-user-dispatcher";
import { BasicTvDisaptcher } from "../../app/server/cinema/basic-tv-dispatcher";
import { TvShowBackupDispatcher } from "../../app/server/cinema/backup/tv-show-backup-dispatcher";
import { model } from "../../app/server/database/models/cinema/tv-show-backup.model";
import { model as tvShowModel, I_TvShowModel } from "../../app/server/database/models/cinema/tv-show.model";
import { I_BasicTv } from "../../app/server/cinema/i_basic-tv";

describe('Tv Show backup dispatcher', () => {
    let userID;
    let orginalTvDocID;

    beforeAll(async (done) => {
        await TestUserDispatcher.createDefaultUser();
        await BasicTvDisaptcher.create({ name: 'testFilm', tvID: 12 } as any);
        orginalTvDocID = (await tvShowModel.findOne({}))._id;
        userID = await TestUserDispatcher.getUserID(TestUserDispatcher.defaultUser.name)
        done();
    });

    describe('create', () => {
        it('should create copy', async (done) => {
            await TvShowBackupDispatcher.create(userID, { name: 'testFilm', tvID: 12, _id: orginalTvDocID } as any);
            let doc = await model.findOne({ "previousData.tvID": 12 });

            expect(doc.previousData.name).toEqual('testFilm');
            expect(doc.expires).toBeDefined();
            done();
        })

        it('should create second copy', async (done) => {
            await TvShowBackupDispatcher.create(userID, { name: 'testFilm2', tvID: 12, _id: orginalTvDocID } as any);
            let doc = await model.findOne({ "previousData.name": 'testFilm2' });

            expect(doc.previousData.name).toEqual('testFilm2');
            expect(doc.expires).toBeDefined();
            done();
        })

        it('should create third copy', async (done) => {
            await TvShowBackupDispatcher.create(userID, { name: 'testFilm3', tvID: 12, _id: orginalTvDocID } as any);
            let doc = await model.findOne({ "previousData.name": 'testFilm3' });

            expect(doc.previousData.name).toEqual('testFilm3');
            expect(doc.expires).toBeDefined();
            done();
        })

        it('should contains three copy', async (done) => {
            let doc = await model.find({});
            let id = doc[0].previousData._id;
            let tvID = doc[0].previousData.tvID;

            expect(doc.length).toEqual(3);

            expect(doc[1].previousData.tvID).toEqual(tvID);
            expect(doc[1].previousData._id).toEqual(id);
            expect(doc[1].editor).toEqual(userID);

            expect(doc[2].previousData.tvID).toEqual(tvID);
            expect(doc[2].previousData._id).toEqual(id);
            expect(doc[2].editor).toEqual(userID);

            done();
        })

        it('should dont create fourth copy (no tvID)', (done) => {
            TvShowBackupDispatcher.create(userID, { name: 'testFilm4' } as any)
                .then(() => {
                    expect(false).toBeTruthy('Should throw error');
                    done();
                })
                .catch(err => {
                    expect(true).toBeTruthy();
                    done();
                })
        })
    });

    describe('restore data', () => {
        it('document should contains testFilm name', async (done) => {
            let tvDoc = await tvShowModel.findOne({});
            expect(tvDoc.name).toEqual('testFilm');
            done();
        });

        it('should restore document to testFilm3', async (done) => {
            let doc = await model.findOne({ "previousData.name": 'testFilm3' });
            await TvShowBackupDispatcher.resotre(doc._id);

            let tvDoc = await tvShowModel.findOne({});
            expect(tvDoc.name).toEqual('testFilm3');
            done();
        });

        it('should restore document to testFilm', async (done) => {
            let doc = await model.findOne({ "previousData.name": 'testFilm' });
            await TvShowBackupDispatcher.resotre(doc._id);

            let tvDoc = await tvShowModel.findOne({});
            expect(tvDoc.name).toEqual('testFilm');
            done();
        });

        it('should dont restore document without previusData._id', async (done) => {
            await TvShowBackupDispatcher.create(userID, { name: 'testFilm4', tvID: 12 } as any);
            let doc = await model.findOne({ "previousData.name": 'testFilm4' });
            TvShowBackupDispatcher.resotre(doc._id).then(() => {
                expect(true).toBeFalsy('Should throw error');
                done();
            }).catch(() => {
                expect(true).toBeTruthy();
                done();
            })
        });
    });

    describe("get all changes for doc", () => {
        it('should return all changes', async (done) => {
            let results = await TvShowBackupDispatcher.getAllChangesForDocByID(orginalTvDocID);
            expect(results.length).toEqual(3);
            done();
        })

        it('should return 0 if id is invalid', async (done) => {
            let results = await TvShowBackupDispatcher.getAllChangesForDocByID('')
            expect(results.length).toEqual(0);
            done();
        })
    });

    afterAll(async (done) => {
        await TestUserDispatcher.deleteDefaultUser();
        await model.remove({});
        await tvShowModel.remove({});
        done();
    })
})