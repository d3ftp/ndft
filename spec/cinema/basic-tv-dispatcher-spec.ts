import { model } from "../../app/server/database/models/cinema/tv-show.model";
import { I_BasicTv } from "../../app/server/cinema/i_basic-tv";
import { BasicTvDisaptcher } from "../../app/server/cinema/basic-tv-dispatcher";

describe('Basic Tv Dispatcher', () => {
    var id: any;
    let tv = {
        backdrop_path: '1',
        first_air_date: "2",
        last_air_date: '3',
        name: 'xxx',
        number_of_episodes: 5,
        number_of_seasons: 4,
        original_name: 'xxx',
        overview: 'xxxxxv',
        poster_path: 'xxxvvvb',
        seasons: [{ air_date: 'x' }],
        tvID: 1111,
        vote_average: 145
    } as I_BasicTv;


    beforeAll(async (done) => {
        try {
            let doc = await new model({tvID: 1111}).save();
            
            id = doc._id;
        } catch (error) {
            throw new Error(error);
        }
        done();
    });

    it('document should exist', async (done) => {     
        expect(await model.findById(id)).toBeDefined();
        done();
    })

    it('should update document by all fields', async (done) => {
        let c = await BasicTvDisaptcher.update(tv).catch(err => {
            
        })
        let doc = await model.findById(id);

        expect(doc.backdrop_path).toEqual(tv.backdrop_path);
        expect(doc.first_air_date).toEqual(tv.first_air_date);
        expect(doc.first_air_date).toEqual(tv.first_air_date);
        expect(doc.last_air_date).toEqual(tv.last_air_date);
        expect(doc.name).toEqual(tv.name);
        expect(doc.number_of_episodes).toEqual(tv.number_of_episodes);
        expect(doc.number_of_seasons).toEqual(tv.number_of_seasons);
        expect(doc.original_name).toEqual(tv.original_name);
        expect(doc.overview).toEqual(tv.overview);
        expect(doc.poster_path).toEqual(tv.poster_path);
        expect(doc.seasons[0].air_date).toEqual(tv.seasons[0].air_date);
        expect(doc.tvID).toEqual(tv.tvID);
        expect(doc.vote_average).toEqual(tv.vote_average);

        done();
    });

    it('should update document by few field', async (done) => {
        await BasicTvDisaptcher.update({ tvID: 1111, name: 'SuperTimm' } as I_BasicTv);
        let doc = await model.findById(id);

        expect(doc.name).toEqual("SuperTimm");
        expect(doc.seasons[0].air_date).toEqual(tv.seasons[0].air_date);
        done();
    })

    it('should throw error when invalid id', async (done) => {
        try {
            await BasicTvDisaptcher.update({name: 'SuperTimm' } as I_BasicTv)
        } catch (error) {
            
        } finally {
            done();
        }
    })

    it('should check is doument exist and return true', (done) => {
        BasicTvDisaptcher.isExist(tv).then(res => {
            expect(res).toBeTruthy();
            done();
        })
    });

    it('should check is doument exist and return false', (done) => {
        BasicTvDisaptcher.isExist({ tvID: 1110 } as I_BasicTv).then(res => {
            expect(res).toBeFalsy();
            done();
        })
    });

    it('should create new document', (done) => {
        BasicTvDisaptcher.create({name: 'test', tvID: 24} as any).then(() => {
            model.findOne({tvID: 24}).then(data => {
                expect(data).toBeDefined();
                expect(data.tvID).toEqual(24);
                done();
            })
        })
    })

    afterAll(async (done) => {
        await model.remove({});
        done();
    })
})