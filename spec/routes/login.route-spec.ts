import { Server } from '../server';
import { Router, Express } from "express";
import * as express from "express";
import * as path from "path";
import { LoginRoute } from '../../app/server/routes/login.route';
import { User } from '../../app/server/user/user';
import request = require('supertest');

const uploadsPath = path.join(__dirname, 'uploads');
const profileImagePath = path.join(uploadsPath, 'profile-image');
const tmpPath = path.join(uploadsPath, 'tmp');


var server: Server;
describe('login route', () => {

    beforeAll(() => {
        let route = express.Router();

        route.get('/test/session', (req, res, next) => {
            res.send(req.session);
        });

        route.get('/test/delete-session', (req, res, next) => {
            req.session.destroy(() => {
                res.send('OK');
            });

        });

        server = Server.bootstrap();
        server.app.use(new LoginRoute({ tmp: tmpPath, profileImgPath: profileImagePath, uploadPath: uploadsPath, serverProfileImgPath: '', tvShowHeaderPath: '' }).router);
        server.app.use(route);
    });

    it('should not login wrong login', (done) => {
        server.agent.get('/api/login/activetom123/123456')
            .expect(200, (err, res) => {
                expect(res.body.complete).toBeFalsy();
                done();
            });
    });

    it('should not login wrong password', (done) => {
        server.agent.get('/api/login/activetom/1234567')
            .expect(200, (err, res) => {
                expect(res.body.complete).toBeFalsy();
                done();
            });
    });


    it('should login ', (done) => {
        server.agent.get('/api/login/activetom/123456')
            .expect(200, (err, res) => {
                expect(res.body.complete).toBeTruthy();
                done();
            });
    });

    it('should get session', (done) => {
        server.agent.get('/test/session')
            .expect(200, (err, res) => {  
                expect(res.body.currentUser.name).toBe('activetom')
                done();
            });
    });

    it('Delete session', (done) => {
        server.agent.get('/test/delete-session')
            .expect(200, (err, res) => {
                server.agent.get('/test/session')
                .expect(200, (err, res) => {
                    expect(res.body.currentUser).toBeUndefined();
                    done();
                });
            });
    });

    it('should login with email and create session', (done) => {
        server.agent.get('/api/login/activethomas@email.com/123456')
        .expect(200, (err, res) => {
            expect(res.body.complete).toBeTruthy();
            done();
        });
    });

    it('should login and create session', (done) => {
        server.agent.get('/test/session')
            .expect(200, (err, res) => {
                expect(res.body.currentUser.name).toBe('activetom')
                done();
            });
    });
});

