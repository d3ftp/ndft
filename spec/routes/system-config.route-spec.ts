import { Server } from '../server';
import { IndexRoute } from '../../app/server/routes/index.route';
import { User } from '../../app/server/user/user';
import request = require('supertest');
import { SystemConfigRoute } from '../../app/server/routes/system-config.route';
import { GroupDispatcher } from '../../app/server/user/group-dispatcher';
import { SystemConfig } from '../../app/server/system-config';
import { I_NodemailerConfig } from '../../app/server/database/models/i_nodemailer-config.model';

var bodyParser = require('body-parser');

var server: Server;
describe('System Config Route: ', () => {
    let title = 'Test Ttitle'
    beforeAll(() => {
        server = Server.bootstrap();
        server.app.use(bodyParser.json());
        server.app.use(bodyParser.urlencoded({ extended: false }));

        server.app.use(new SystemConfigRoute().router);
    })

    describe('set title - ', () => {
        it('should fail when websiteTitle is undefined', (done) => {
            server.agent.put('/api/system-config/layout').send({})
                .expect(200, (res, req, next) => {
                    expect(req.body.complete).toBeFalsy();
                    done();
                })
        });

        it('should set title when websiteTitle is defined', (done) => {
            server.agent.put('/api/system-config/layout').send({ websiteTitle: title })
                .expect(200, (res, req, next) => {
                    expect(req.body.complete).toBeTruthy();
                    done();
                })
        });
    });

    describe('get Title - ', () => {
        it('should return title', (done) => {
            server.agent.get('/api/system-config/layout')
                .expect(200, (res, req) => {
                    expect(req.body.data).toBe(title);
                    done();
                })
        });
    });

    describe('email confirm time', () => {
        let emailConfirmTime = 72;

        it('should dont set time when emailConfirmTime is undefined', (done) => {
            server.agent.put('/api/system-config/email-confirm-time')
                .send({})
                .expect(200, (res, req) => {
                    expect(req.body.complete).toBeFalsy()
                    done();
                })
        })

        it('should dont set time when emailConfirmTime is not number', (done) => {
            server.agent.put('/api/system-config/email-confirm-time')
                .send({ emailConfirmTime: 'noNoNo' })
                .expect(200, (res, req) => {
                    expect(req.body.complete).toBeFalsy()
                    done();
                })
        })

        it('should set time', (done) => {
            server.agent.put('/api/system-config/email-confirm-time')
                .send({ emailConfirmTime: emailConfirmTime })
                .expect(200, (res, req) => {
                    expect(req.body.complete).toBeTruthy()
                    done();
                })
        })

        it('should get time', (done) => {
            server.agent.get('/api/system-config/email-confirm-time')
                .expect(200, (res, req) => {
                    expect(req.body.complete).toBeTruthy();
                    expect(req.body.data).toEqual(emailConfirmTime);
                    done();
                })
        })
    });

    describe('blocked email domains', () => {
        let blockedDomains = ['a', 'b', 'c', 'd'];

        it('should dont set blocked domains when blockedDomains is undefined', (done) => {
            server.agent.put('/api/system-config/blocked-domains')
                .send({})
                .expect(200, (res, req) => {
                    expect(req.body.complete).toBeFalsy();
                    done();
                })
        });

        it('should set blocked domains', (done) => {
            server.agent.put('/api/system-config/blocked-domains')
                .send({ blockedDomains: blockedDomains })
                .expect(200, (res, req) => {
                    expect(req.body.complete).toBeTruthy();
                    done();
                })
        });

        it('should get blocked domains array', (done) => {
            server.agent.get('/api/system-config/blocked-domains')
                .expect(200, (res, req) => {
                    expect(req.body.complete).toBeTruthy()

                    let oryginalArray = JSON.stringify(req.body.data);
                    let expectedArray = JSON.stringify(blockedDomains);

                    expect(oryginalArray).toEqual(expectedArray);
                    done();
                })
        });
    });

    describe('default user group', () => {
        let groupId;
        it('should set group', (done) => {
            GroupDispatcher.get().getAll().then((groups) => {
                groupId = groups[1]._id;

                server.agent.put('/api/system-config/default-user-group')
                    .send({ defaultUserGroupId: groupId })
                    .expect(200, (res, req) => {
                        expect(req.body.complete).toBeTruthy();
                        done();
                    });
            })

        });

        it('should dont set default userGroup when defaultUserGroupId is undefined', (done) => {
            server.agent.put('/api/system-config/default-user-group')
                .send({})
                .expect(200, (res, req) => {
                    expect(req.body.complete).toBeFalsy();
                    done();
                });

        });

        it('should dont set default userGroup when defaultUserGroupId does not match to any group id', (done) => {
            server.agent.put('/api/system-config/default-user-group')
                .send({ defaultUserGroupId: '5a7339dda92d5d084808824b' })
                .expect(200, (res, req) => {
                    expect(req.body.complete).toBeFalsy();
                    done();
                });

        });

        it('should get group', (done) => {
            server.agent.get('/api/system-config/default-user-group')
                .expect(200, (res, req) => {
                    expect(req.body.complete).toBeTruthy();
                    expect(req.body.data._id.toString()).toEqual(groupId.toString());

                    done();
                });
        });
    });

    describe('default guest group', () => {
        let groupId;
        it('should set group', (done) => {
            GroupDispatcher.get().getAll().then((groups) => {
                groupId = groups[1]._id;

                server.agent.put('/api/system-config/default-guest-group')
                    .send({ defaultGuestGroupId: groupId })
                    .expect(200, (res, req) => {
                        expect(req.body.complete).toBeTruthy();
                        done();
                    });
            })

        });

        it('should dont set default guestGroup when defaultGuestGroupId is undefined', (done) => {
            server.agent.put('/api/system-config/default-guest-group')
                .send({})
                .expect(200, (res, req) => {
                    expect(req.body.complete).toBeFalsy();
                    done();
                });

        });

        it('should dont set default guestGroup when defaultGuestGroupId does not match to any group id', (done) => {
            server.agent.put('/api/system-config/default-guest-group')
                .send({ defaultGuestGroupId: '5a7339dda92d5d084808824b' })
                .expect(200, (res, req) => {
                    expect(req.body.complete).toBeFalsy();
                    done();
                });

        });

        it('should get group', (done) => {
            server.agent.get('/api/system-config/default-guest-group')
                .expect(200, (res, req) => {
                    expect(req.body.complete).toBeTruthy();
                    expect(req.body.data._id.toString()).toEqual(groupId.toString());
                    done();
                });
        });
    })

    describe('nodemailer config', () => {

        it('should get', (done) => {
            server.agent.get('/api/system-config/nodemailer-config')
                .expect(200, (res, req) => {
                    let config = req.body.data as I_NodemailerConfig;

                    expect(req.body.complete).toBeTruthy();
                    expect(config.auth).toBeDefined();
                    expect(config.from).toBeDefined();
                    expect(config.service).toBeDefined();
                    done();
                });
        });

        it('should put config', (done) => {
            let cfg = { from: 'test', service: 'none', auth: { user: 'ad', pass: 'xxx' } };
            server.agent.put('/api/system-config/nodemailer-config')
                .send(cfg)
                .expect(200, (res, req) => {
                    expect(req.body.complete).toBeTruthy();
                    SystemConfig.get().getNodemailerConfig().then(config => {
                        expect(config.from).toEqual(cfg.from);
                        expect(config.service).toEqual(cfg.service);
                        expect(config.auth.pass).toEqual(cfg.auth.pass);
                        expect(config.auth.user).toEqual(cfg.auth.user);
                        done();
                    })
                });
        });

        it('should dont put config- No service', (done) => {
            let cfg = { from: 'test', auth: { user: 'ad', pass: 'xxx' } };
            server.agent.put('/api/system-config/nodemailer-config')
                .send(cfg)
                .expect(200, (res, req) => {
                    expect(req.body.complete).toBeFalsy();
                    done();
                });
        });

        it('should dont put config- No from', (done) => {
            let cfg = { service: 'none', auth: { user: 'ad', pass: 'xxx' } };
            server.agent.put('/api/system-config/nodemailer-config')
                .send(cfg)
                .expect(200, (res, req) => {
                    expect(req.body.complete).toBeFalsy();
                    done();
                });
        });


        it('should dont put config- No auth', (done) => {
            let cfg = { from: 'test', service: 'xxx' };
            server.agent.put('/api/system-config/nodemailer-config')
                .send(cfg)
                .expect(200, (res, req) => {
                    expect(req.body.complete).toBeFalsy();
                    done();
                });
        });
    });

});
