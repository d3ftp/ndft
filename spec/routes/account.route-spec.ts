import { Server } from '../server';
import { Router, Express } from "express";
import * as express from "express";
import request = require('supertest');
import { User } from '../../app/server/user/user';
import { GroupDispatcher } from '../../app/server/user/group-dispatcher';
import { ActiveUser } from '../../app/server/user/active-user';
import { AddUser } from '../../app/server/user/add-user';
import { GetUser } from '../../app/server/user/get-user';
import { TestUserDispatcher } from '../helpers/test-user-dispatcher';
import { AccountRoute } from '../../app/server/routes/account.route';
import { UserEmail } from '../../app/server/user/user-email';
import { RegisterMail } from '../../app/server/nodemailer-module/components/register-mail';
import { EmailDispatcher } from '../../app/server/nodemailer-module/email-dispatcher';
import { EmailChange } from '../../app/server/nodemailer-module/components/email-change';
import { JasmineNodemailer } from '../nodemailer-module/jasmine-nodemailer';
var bodyParser = require('body-parser');


let verificationCode: any;
let newEmail: string = 'test@new.pl'
describe('Account Route', () => {
    var server: Server;
    let user = { name: 'accountuser', password: '1234567', email: 'account-user@test.pl', verificationCode: 'ffffffffff' }

    beforeAll(async (done) => {
        let route = express.Router();
        await TestUserDispatcher.createUser(user.name, user.password, user.email, user.verificationCode);
        let doc = await new GetUser().by({ name: user.name });

        route.get('/test/session-init', (req, res, next) => {   
            let user = new User(doc._id, doc.name, []);
            req.session.currentUser = user;
            res.send(req.session);
        });

        route.get('/test/session-destroy', (req, res, next) => {
            req.session.destroy(() => { });
            res.send(true);
        });

        //await new UserEmail().createVerificationCodeModel(doc._id, 'test@os.pl');
        //verificationCode = await new UserEmail().getVerificationEmailCode(doc._id);

        server = Server.bootstrap();
        server.app.use(route);
        server.app.use(bodyParser.json());
        server.app.use(bodyParser.urlencoded({ extended: false }));
        server.app.use(new AccountRoute().router);

        server.app.use((err, req, res, next) => {
            res.status(500).send({ complete: false })
        })
        await initializeNodeMailer();
        done();
    });

    describe('Email', () => {
        describe('get', () => {

            it('should initialize session', (done) => { // CREATE SESSION
                server.agent.get('/test/session-init').expect(200, (err, res) => {
                    let currentUser = res.body.currentUser;

                    expect(currentUser).toBeDefined();
                    expect(currentUser.name).toEqual(user.name);

                    done();
                });
            });

            it('should return email address', (done) => {
                server.agent.get('/api/account/email').expect(200, (err, res) => {
                    expect(res.body.complete).toBeTruthy();
                    expect(res.body.data).toBe('account-user@test.pl');
                    done();
                });
            });

            it('should destroy session', (done) => {    // DESTROY SESISON
                server.agent.get('/test/session-destroy').expect(200, (err, res) => {
                    expect(res.body).toBeTruthy();
                    done();
                });
            });

            it('should dont return email address instead of this return 500 HTML Code', (done) => {
                server.agent.get('/api/account/email').expect(500, (err, res) => {
                    expect(res.body.complete).toBeFalsy();
                    done();
                });
            });

        });

        describe('prepare', () => {
            it('should dont when session not exist', (done) => {
                server.agent.post('/api/account/prepare-email-change').expect(500, (err, res) => {
                    expect(res.body.complete).toBeFalsy();
                    done();
                })
            })

            it('should initialize session', (done) => { // CREATE SESSION
                server.agent.get('/test/session-init').expect(200, (err, res) => {
                    let currentUser = res.body.currentUser;

                    expect(currentUser).toBeDefined();
                    expect(currentUser.name).toEqual(user.name);

                    done();
                });
            });

            it('should dont when necessary parametr not exist', (done) => {
                server.agent.post('/api/account/prepare-email-change').send({}).expect(500, (err, res) => {
                    expect(res.body.complete).toBeFalsy();
                    done();
                })
            })

            it('should dont when email is busy', (done) => {
                server.agent.post('/api/account/prepare-email-change').send({ newEmail: TestUserDispatcher.defaultUser.email }).expect(500, (err, res) => {
                    expect(res.body.complete).toBeFalsy();
                    done();
                })
            })

            it('should dont when email syntax is wrong', (done) => {
                server.agent.post('/api/account/prepare-email-change').send({ newEmail: 'wrongemailsytaxX' }).expect(500, (err, res) => {
                    expect(res.body.complete).toBeFalsy();
                    done();
                })
            })

            it('should create model and send email', (done) => {
                server.agent.post('/api/account/prepare-email-change').send({ newEmail: newEmail }).expect(200, (err, res) => {

                    expect(res.body.complete).toBeTruthy();
                    new GetUser().by({ name: TestUserDispatcher.defaultUser.name }).then((doc) => {
                        new UserEmail().getVerificationEmailCode(doc._id).then((emailCodeDoc) => {
                            expect(emailCodeDoc).toBeDefined();
                            expect(emailCodeDoc).not.toBeNull();

                            done();
                        })
                    })
                })
            })

            it('should destroy session', (done) => {    // DESTROY SESISON
                server.agent.get('/test/session-destroy').expect(200, (err, res) => {
                    expect(res.body).toBeTruthy();
                    done();
                });
            });

        });

        describe('change', () => {
            it('should dont change email without necessary parametr', (done) => {
                server.agent.post('/api/account/email').expect(500, (err, res) => {
                    expect(res.body.complete).toBeFalsy();
                    done();
                })
            })

            it('should change email', async (done) => {
                let user = await new GetUser().by({name: TestUserDispatcher.defaultUser.name});
                verificationCode = await new UserEmail().getVerificationEmailCode(user._id);
                server.agent.post('/api/account/email').send({ verificationCode: verificationCode }).expect(200, (err, res) => {
                    expect(res.body.complete).toBeTruthy();
                    done();
                })
            })

            it('should initialize session', (done) => { // CREATE SESSION
                server.agent.get('/test/session-init').expect(200, (err, res) => {
                    let currentUser = res.body.currentUser;

                    expect(currentUser).toBeDefined();
                    expect(currentUser.name).toEqual(user.name);

                    done();
                });
            });

            it('should return changed email', (done) => {
                server.agent.get('/api/account/email').expect(200, (err, res) => {
                    expect(res.body.complete).toBeTruthy();
                    expect(res.body.data).toBe(newEmail);
                    done();
                });
            });

            it('should removeVerfication code [delete model]', async (done) => {
                let user = await new GetUser().by({ name: TestUserDispatcher.defaultUser.name })
                new UserEmail().getVerificationEmailCode(user._id).then(res => {
                    expect(true).toBeFalsy('Should dont invoke!');
                    done();
                }).catch(err => {
                    expect(true).toBeTruthy();
                    done();
                })
            });

        });


    });

    afterAll((done) => {
        TestUserDispatcher.deleteUser(user.name).then(() => {
            done();
        })
    });

});


function initializeNodeMailer() {
    EmailChange.JASMINE_RESET_INSTANCE();
    return EmailDispatcher.bootstrap().then(() => {
        EmailDispatcher.initializeSystemMessage([EmailChange.getInstance()]).then(() => {
            new EmailDispatcher().updateMailMessage(EmailChange.getInstance(), 'Register', `<div style="color:red"> ##system_LINK## </div>`).then(() => {
                EmailChange.getInstance().setLink('4567fd1TEST_LINK');;
                new JasmineNodemailer().init(EmailDispatcher.getNodeMailer());
            });
        });
    })
}