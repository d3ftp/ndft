import { Server } from '../server';
import { IndexRoute } from '../../app/server/routes/index.route';
import request = require('supertest');
import { RoutesGroupRoute } from '../../app/server/routes/routes-group.route';
import { RouteDispatcher } from '../../app/server/permission/route-dispatcher';
import { Shell } from '../helpers/shell-Id';
import { I_RouteGroup } from '../../app/server/database/models/i_route-group';
import { GroupDispatcher } from '../../app/server/user/group-dispatcher';
import { FilesManagerRoute } from '../../app/server/routes/files-manager.route';
import * as fs from 'fs';
import * as path from "path";
import * as express from "express";
import { GetUser } from '../../app/server/user/get-user';
import { LoginRoute } from '../../app/server/routes/login.route';

var bodyParser = require('body-parser');

var server: Server;

const uploadsPath = path.join(__dirname, 'uploads');
const profileImagePath = path.join(uploadsPath, 'profile-image');
const tmpPath = path.join(uploadsPath, 'tmp');

const file1 = path.join(tmpPath, 'test.png');
let file1RandomName: string;

xdescribe('Files manager route:', () => {
    beforeAll(() => {
        server = Server.bootstrap();
        
        if (!fs.existsSync(uploadsPath)) {
            fs.mkdirSync(uploadsPath);
            fs.mkdirSync(profileImagePath);
            fs.mkdirSync(tmpPath);
        }
        createFile(file1);

        server.app.use(bodyParser.json());
        server.app.use(bodyParser.urlencoded({ extended: false }));
        server.app.use(new LoginRoute({ tmp: tmpPath, profileImgPath: profileImagePath, uploadPath: uploadsPath, serverProfileImgPath: '', tvShowHeaderPath: '' }).router);
        server.app.use(new FilesManagerRoute({ tmp: tmpPath, profileImgPath: profileImagePath, uploadPath: uploadsPath, serverProfileImgPath: '', tvShowHeaderPath: '' }).router);

        let route = express.Router();

    });

    it('should login ', (done) => {
        server.agent.get('/api/login/activetom/123456')
            .expect(200, (err, res) => {
                expect(res.body.complete).toBeTruthy();
                done();
            });
    });

    it('should upload file', (done) => {
        server.agent.post('/api/file/user-profile')
            .set('Content-Type', 'multipart/form-data')
            .attach('image', file1)
            .expect(200, (err, res) => {
                expect(res.body.complete).toBeTruthy();
                new GetUser().by({ name: 'activetom' }).then(user => {
                    expect(user.profileImage).toBeDefined();
                    expect(fs.existsSync(profileImagePath + '/' + user.profileImage)).toBeTruthy();
                    file1RandomName = user.profileImage;
                    done();
                })
            });
    });

    it('should upload file nad remove old', (done) => {
        server.agent.post('/api/file/user-profile')
            .set('Content-Type', 'multipart/form-data')
            .attach('image', file1)
            .expect(200, (err, res) => {
                expect(res.body.complete).toBeTruthy();
                new GetUser().by({ name: 'activetom' }).then(user => {
                    expect(user.profileImage).toBeDefined();
                    expect(fs.existsSync(profileImagePath + '/' + user.profileImage)).toBeTruthy();
                    expect(fs.existsSync((profileImagePath + '/' + file1RandomName))).toBeFalsy();
                    done();
                })
            });
    });


    afterAll(() => {
        fs.readdir(tmpPath, (err, files) => {
            if (err) throw err;

            for (const file of files) {
                fs.unlink(path.join(tmpPath, file), err => {
                    if (err) throw err;
                });
            }
        });

        fs.readdir(profileImagePath, (err, files) => {
            if (err) throw err;

            for (const file of files) {
                fs.unlink(path.join(profileImagePath, file), err => {
                    if (err) throw err;
                });
            }
        });
    })

});

function createFile(name) {
    try {
        return fs.writeFileSync(name, 'asdasdasdaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
    } catch (e) {
        throw Error('cant write file!: ' + e);
    }
}