import { Server } from '../server';
import { IndexRoute } from '../../app/server/routes/index.route';
import request = require('supertest');
import { GroupRoute } from '../../app/server/routes/group.route';
import { Response } from 'supertest';
import { Shell } from '../helpers/shell-Id';
import { GroupDispatcher } from '../../app/server/user/group-dispatcher';
import { RouteDispatcher } from '../../app/server/permission/route-dispatcher';

import UserGroupModel = require('../../app/server/database/models/user-group.model');
import { I_UserGroup } from '../../app/server/database/models/i_user-group';
var bodyParser = require('body-parser');

var server: Server;
var shell: Shell = new Shell();
var groups: I_UserGroup[] = [];

describe('Group route', () => {
    beforeAll((done) => {
        server = Server.bootstrap();
        server.app.use(bodyParser.json());
        server.app.use(bodyParser.urlencoded({ extended: false }));
        server.app.use(new GroupRoute().router);

        var promise = GroupDispatcher.get().create('testgroup1', [
            RouteDispatcher.routePathToID('/test/name/:data', 'post'),
            RouteDispatcher.routePathToID('/test/name/:data', 'get'),
            RouteDispatcher.routePathToID('/test/user', 'get'),
            RouteDispatcher.routePathToID('/test/user', 'delete')
        ]);

        promise.then(res => {
            done();
        })
    })

    it('should return all populated groups as short form', (done) => {
        server.agent.get('/api/group').expect(200, (err, res: Response, next) => {

            expect(res.body.length).toBe(4);
            expect([res.body[0].name, res.body[1].name, res.body[2].name]).toContain("user")
            expect(res.body[0].routes[0]._id).toBeDefined("ID SHOULD BE DEFINED");
            groups = new Array().concat(res.body);

            for (let group of res.body) {
                if (group.name == "user") {
                    expect(group.routes.length).toBe(3);
                    done();
                }
            }
        });
    });

    it('should contains testgroup1', (done) => {
        server.agent.get('/api/group').expect(200, (err, res: Response, next) => {
            for (let group of res.body) {
                if (group.name == "testgroup1") {
                    expect(true).toBe(true);
                    done();
                    return;
                }
            }
            expect(true).toBe(false);
            done();
        });
    });

    it('should update group name', (done) => {
        server.agent.put('/api/group')
            .send({ id: shell.byName("testgroup1", groups), newName: "iLikeApples", routes: [] })
            .expect(200, (err, res: Response, next) => {
                expect(res.body.Complete).toBeTruthy();
                done();
            });
    });

    it('should dont update group name when new name is used', (done) => {
        server.agent.put('/api/group')
            .send({ id: shell.byName("testgroup1", groups), newName: "user", routes: [] })
            .expect(200, (err, res: Response, next) => {
                expect(res.body.Complete).toBeFalsy();
                done();
            });
    });

    it('should dont update group name when new name is empty', (done) => {
        server.agent.put('/api/group')
            .send({ id: shell.byName("testgroup1", groups), newName: "", routes: [] })
            .expect(200, (err, res: Response, next) => {
                expect(res.body.Complete).toBeFalsy();
                done();
            });
    });

    it('should dont update group name', (done) => {
        server.agent.put('/api/group')
            .send({ id: "", newName: "iLikeApples", routes: [] })
            .expect(200, (err, res: Response, next) => {
                expect(res.body.Complete).toBeFalsy();
                done();
            });
    });


    it('should dont remove group when does not exist', (done) => {
        server.agent.delete('/api/group/nonono')
            .expect(200, (err, res, next) => {
                expect(res.body.Complete).toBeFalsy();
                done();
            })
    });

    it('should dont remove group', (done) => {
        server.agent.delete('/api/group/' + shell.byName("testgroup1", groups))
            .expect(200, (err, res, next) => {
                expect(res.body.Complete).toBeTruthy();
                done();
            })
    });

});
