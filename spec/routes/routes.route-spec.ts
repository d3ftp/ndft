import { Server } from '../server';
import { IndexRoute } from '../../app/server/routes/index.route';
import { User } from '../../app/server/user/user';
import request = require('supertest');
import { RoutesRoute } from '../../app/server/routes/routes.route';

var server: Server;
describe('Routes route', () => {

    beforeAll(() => {
        server = Server.bootstrap();
        server.app.use(new RoutesRoute().router);
    })

    it('should return all routes from database', (done) => {
        server.agent.get('/api/routes-all').expect(200, (err, req) => {
            expect(req.body[0].path).toBe("/test/name/:data");
            expect(req.body[0].method).toBe("get");

            expect(req.body[1].path).toBe("/test/page/:id/:value");
            expect(req.body[1].method).toBe("get");

            expect(req.body[3].path).toBe("/test/name/:data");
            expect(req.body[3].method).toBe("post");

            expect(req.body[7].path).toBe("/test/user");
            expect(req.body[7].method).toBe("put");
            done();
        });
    });




});
