import { Server } from '../server';
import { IndexRoute } from '../../app/server/routes/index.route';
import request = require('supertest');
import { NodemailerRoute } from '../../app/server/routes/nodemailer.route';
import { RegisterMail } from '../../app/server/nodemailer-module/components/register-mail';
import { SystemMail } from '../../app/server/nodemailer-module/system-mail-factory';


var bodyParser = require('body-parser');
var server: Server;


describe('Nodemailer Route', () => {
    beforeAll(() => {
        server = Server.bootstrap();
        server.app.use(bodyParser.json());
        server.app.use(bodyParser.urlencoded({ extended: false }));
        server.app.use(new NodemailerRoute().router);
    });

    describe('register mail', () => {
        let html = '<h1>Hello</h1>';
        let subject = 'test';
        it('should update', (done) => {
            server.agent.put('/api/nodemailer/system-mail')
                .send({ html: html, subject: subject, mailKey: SystemMail.REGISTER })
                .expect(200, (err, res) => {
                    expect(res.body.complete).toBeTruthy();
                    done();
                });
        });

        it('should dont update without html value', (done) => {
            server.agent.put('/api/nodemailer/system-mail')
                .send({ subject:subject })
                .expect(200, (err, res) => {
                    expect(res.body.complete).toBeFalsy();
                    done();
                });
        });

        it('should dont update without subject value', (done) => {
            server.agent.put('/api/nodemailer/system-mail')
                .send({ html: html })
                .expect(200, (err, res) => {
                    expect(res.body.complete).toBeFalsy();
                    done();
                });
        });

        it('should dont update without subject and html value', (done) => {
            server.agent.put('/api/nodemailer/system-mail')
                .send({})
                .expect(200, (err, res) => {
                    expect(res.body.complete).toBeFalsy();
                    done();
                });
        });

        it('should get', (done) => {
            server.agent.get('/api/nodemailer/system-mail/' + SystemMail.REGISTER).expect(200, (err, res) => {
                expect(res.body.complete).toBeTruthy();
                expect(res.body.data.oryginalHtmlMsg).toEqual(html);
                expect(res.body.data.subject).toEqual(subject);
                done();
            });
        });

        it('should dont get when system mail not exist', (done) => {
            server.agent.get('/api/nodemailer/system-mail/' + 999).expect(200, (err, res) => {
                expect(res.body.complete).toBeFalsy();
                done();
            });
        });
    });
});