import { Server } from '../server';
import { IndexRoute } from '../../app/server/routes/index.route';
import request = require('supertest');
import { RoutesGroupRoute } from '../../app/server/routes/routes-group.route';
import { RouteDispatcher } from '../../app/server/permission/route-dispatcher';
import { Shell } from '../helpers/shell-Id';
import { I_RouteGroup } from '../../app/server/database/models/i_route-group';
import { GroupDispatcher } from '../../app/server/user/group-dispatcher';
var bodyParser = require('body-parser');

var server: Server;
var shell: Shell;
var groups: I_RouteGroup[] = [];

describe('Routes group route:', () => {

    beforeAll(() => {
        server = Server.bootstrap();
        server.app.use(bodyParser.json());
        server.app.use(bodyParser.urlencoded({ extended: false }));
        server.app.use(new RoutesGroupRoute().router);

        shell = new Shell();
    });

    it('should add "login" route to database', (done) => {
        new RouteDispatcher().getAll().then(docs => {
            server.agent.post('/api/routes-group')
                .send({ name: 'login', routes: [docs[0]._id, docs[1]._id] })
                .expect(200, (err, res) => {
                    expect(res.body.complete).toBeTruthy();
                    done();
                });
        })
    });

    it('should add "register" route to database', (done) => {
        new RouteDispatcher().getAll().then(docs => {
            server.agent.post('/api/routes-group')
                .send({ name: 'register', routes: [docs[2]._id, docs[3]._id] })
                .expect(200, (err, res) => {
                    expect(res.body.complete).toBeTruthy();
                    done();
                });
        })
    });

    it('should dont add "register" route to database when exist', (done) => {
        new RouteDispatcher().getAll().then(docs => {
            server.agent.post('/api/routes-group')
                .send({ name: 'register', routes: [docs[2]._id, docs[3]._id] })
                .expect(200, (err, res) => {
                    expect(res.body.complete).toBeFalsy();
                    done();
                });
        })
    });

    it('should dont add route to database when parametrs are not specified', (done) => {
        new RouteDispatcher().getAll().then(docs => {
            server.agent.post('/api/routes-group')
                .send({})
                .expect(200, (err, res) => {
                    expect(res.body.complete).toBeFalsy();
                    done();
                });
        })
    });

    it('should return all routes group from database', (done) => {
        server.agent.get('/api/routes-group-all/false').expect(200, (err, res) => {
            expect(res.body.data[0].name).toBe("login");
            expect(res.body.data[1].name).toBe("register")

            groups.push(res.body.data[0],res.body.data[1]);

            expect(res.body.data[0].routes[0].path).toBeUndefined();
            done();
        });
    });

    it('should return all populated routes group from database', (done) => {
        server.agent.get('/api/routes-group-all/true').expect(200, (err, res) => {
            expect(res.body.data[0].name).toBe("login");
            expect(res.body.data[1].name).toBe("register")

            expect(res.body.data[0].routes[0].path).toBeDefined();
            done();
        });
    });

    it('should dont update register group route', (done) => {
        new RouteDispatcher().getAll().then(docs => {
            server.agent.put('/api/routes-group')
                .send({ id: "newRegister", newName: "registerrrr", routes: [docs[0]._id] })
                .expect(200, (err, res) => {
                    expect(res.body.complete).toBeFalsy();
                    done();
                });
        })
    });

    it('should dont update register group route', (done) => {
        new RouteDispatcher().getAll().then(docs => {
            server.agent.put('/api/routes-group')
                .send({})
                .expect(200, (err, res) => {
                    expect(res.body.complete).toBeFalsy();
                    done();
                });
        })
    });

    it('should dont update register group route', (done) => {
        new RouteDispatcher().getAll().then(docs => {
            server.agent.put('/api/routes-group')
                .send({ id: "newRegister", routes: [docs[0]._id] })
                .expect(200, (err, res) => {
                    expect(res.body.complete).toBeFalsy();
                    done();
                });
        })
    });

    it('should dont update register group route', (done) => {
        new RouteDispatcher().getAll().then(docs => {
            server.agent.put('/api/routes-group')
                .send({ id: "login", newName: "register", routes: [docs[0]._id] })
                .expect(200, (err, res) => {
                    expect(res.body.complete).toBeFalsy();
                    done();
                });
        })
    });

    it('should dont update register group route', (done) => {
        new RouteDispatcher().getAll().then(docs => {
            server.agent.put('/api/routes-group')
                .send({ id: "newRegister", newName: "registerrrr" })
                .expect(200, (err, res) => {
                    expect(res.body.complete).toBeFalsy();
                    done();
                });
        })
    });

    it('should update register group route', (done) => {
        new RouteDispatcher().getAll().then(docs => {
            server.agent.put('/api/routes-group')
                .send({ id: shell.byName("register", groups), newName: "newRegister", routes: [docs[0]._id] })
                .expect(200, (err, res) => {
                    expect(res.body.complete).toBeTruthy();
                    done();
                });
        })
    });

    it('should update register group route', (done) => {
        new RouteDispatcher().getAll().then(docs => {
            server.agent.put('/api/routes-group')
                .send({ id: shell.byName("register", groups), newName: "register", routes: [docs[4]._id] })
                .expect(200, (err, res) => {
                    expect(res.body.complete).toBeTruthy();
                    done();
                });
        });
    });

    
    it('should dont add  userGroup without id', (done) => {
        GroupDispatcher.get().getAll().then(res => {
            expect(res.length).toBeGreaterThan(0);
            let group = res[0];
            server.agent.put('/api/routes-group-append-user-group')
                .send({userGroupId: group._id})
                .expect(200, (err, res) => {
                    expect(res.body.complete).toBeFalsy();
                    done();
                });
        })
    });


    
    it('should dont add userGroup without userGroupId', (done) => {
        GroupDispatcher.get().getAll().then(res => {
            expect(res.length).toBeGreaterThan(0);
            let group = res[0];
            server.agent.put('/api/routes-group-append-user-group')
                .send({id: shell.byName(groups[0].name, groups)})
                .expect(200, (err, res) => {
                    expect(res.body.complete).toBeFalsy();
                    done();
                });
        })
    });


    it('should add userGroup', (done) => {
        GroupDispatcher.get().getAll().then(res => {
            expect(res.length).toBeGreaterThan(0);
            let group = res[0];
            server.agent.put('/api/routes-group-append-user-group')
                .send({id: shell.byName(groups[0].name, groups), userGroupId: group._id})
                .expect(200, (err, res) => {
                    expect(res.body.complete).toBeTruthy();
                    done();
                });
        })
    });

    it('should delete group', (done) => {
        server.agent.delete('/api/routes-group/' + shell.byName("register", groups)).expect(200, (err, res) => {
            expect(res.body.complete).toBeTruthy();
            done();
        });
    });


});
