import { Server } from '../server';
import { UserRoute } from '../../app/server/routes/user.route';
import { User } from '../../app/server/user/user';
import { EmailDispatcher } from '../../app/server/nodemailer-module/email-dispatcher';
import { RegisterMail } from '../../app/server/nodemailer-module/components/register-mail';
import { JasmineNodemailer } from '../nodemailer-module/jasmine-nodemailer'

import request = require('supertest');
import { GetUser } from '../../app/server/user/get-user';
import { PopulateField } from '../../app/server/user/populate-fields';
import { SystemConfig } from '../../app/server/system-config';
var bodyParser = require('body-parser');

var server: Server;
describe('User route', () => {
    beforeAll((done) => {
        RegisterMail.JASMINE_RESET_INSTANCE();
        EmailDispatcher.bootstrap().then(() => {
            EmailDispatcher.initializeSystemMessage([RegisterMail.getInstance()]).then(() => {
                new EmailDispatcher().updateMailMessage(RegisterMail.getInstance(), 'Register', `<div style="color:red"> ##system_LINK## </div>`).then(() => {
                        server = Server.bootstrap();

                        server.app.use(bodyParser.json());
                        server.app.use(bodyParser.urlencoded({ extended: false }));
                        server.app.use(new UserRoute().router);
                        RegisterMail.getInstance().setLink('4567fd1TEST_LINK');;
                        new JasmineNodemailer().init(EmailDispatcher.getNodeMailer());
                        done();
                    });
            });
        })
    });

    afterAll(() => {
        RegisterMail.JASMINE_RESET_INSTANCE();
        EmailDispatcher.JASMINE_RESET_NODEMAILER();
    });

    it('/api/user : should return fail when email is undefined', (done) => {
        server.agent.post('/api/user')
            .send({ name: 'testName123', password: '121212121212', repeatPassword: '121212121212' })
            .expect(200, (err, res) => {
                expect(res.body.registerComplete).toBeFalsy();
                expect(res.body.message).toContain('Email is undefined');
                done();
            });

    });

    it('/api/user : should return fail when repeatPassword is undefined', (done) => {
        server.agent.post('/api/user')
            .send({ name: 'testName123', password: '121212121212' })
            .expect(200, (err, res) => {
                expect(res.body.registerComplete).toBeFalsy();
                expect(res.body.message).toContain('Repeat Password is undefined');
                done();
            });

    });

    it('/api/user : should return fail when password is undefined', (done) => {
        server.agent.post('/api/user')
            .send({ name: 'testName123' })
            .expect(200, (err, res) => {
                expect(res.body.registerComplete).toBeFalsy();
                expect(res.body.message).toContain('Password is undefined');
                done();
            });

    });

    it('/api/user : should return fail when name is undefined', (done) => {
        server.agent.post('/api/user')
            .send({ password: '121212121212', repeatPassword: '121212121212', email: 'daniel@os.pl' })
            .expect(200, (err, res) => {
                expect(res.body.registerComplete).toBeFalsy();
                expect(res.body.message).toContain('Name is undefined');
                done();
            });

    });

    it('/api/user : should return fail when password not equal to repeatPassword', (done) => {
        server.agent.post('/api/user')
            .send({ name: 'admin', password: '121212121212', repeatPassword: '12121212121233', email: 'daniel@os.pl' })
            .expect(200, (err, res) => {
                expect(res.body.registerComplete).toBeFalsy();
                expect(res.body.message).toContain('The password must be the same');
                done();
            });

    });

    it('/api/user : should return fail when email has wrong syntax', (done) => {
        server.agent.post('/api/user')
            .send({ name: 'admin', password: '121212121212', repeatPassword: '121212121212', email: 'daniell`' })
            .expect(200, (err, res) => {
                expect(res.body.registerComplete).toBeFalsy();
                expect(res.body.message).toContain('Invalid email address');
                done();
            });

    });

    it('/api/user : should return fail password has invalid length', (done) => {
        server.agent.post('/api/user')
            .send({ name: 'admin', password: '2212', repeatPassword: '2212', email: 'daniel@os.pl' })
            .expect(200, (err, res) => {
                expect(res.body.registerComplete).toBeFalsy();
                expect(res.body.message).toContain('Invalid password length');
                done();
            });

    });

    it('/api/user : should return fail when password has invalid syntax', (done) => {
        server.agent.post('/api/user')
            .send({ name: 'admin', password: '``2212``', repeatPassword: '``2212``', email: 'daniel@os.pl' })
            .expect(200, (err, res) => {
                expect(res.body.registerComplete).toBeFalsy();
                expect(res.body.message).toContain('Invalid password syntax');
                done();
            });

    });

    it('/api/user : should return fail when name exist in database', (done) => {
        server.agent.post('/api/user')
            .send({ name: 'admin', password: '121212121212', repeatPassword: '121212121212', email: 'daniel@os.pl' })
            .expect(200, (err, res) => {
                expect(res.body.registerComplete).toBeFalsy();
                expect(res.body.message).toContain('Username exist in database');
                done();
            });

    });

    it('/api/user : should return fail when name has invalid length (not enough characters)', (done) => {
        server.agent.post('/api/user')
            .send({ name: 'aw', password: '121212121212', repeatPassword: '121212121212', email: 'daniel@os.pl' })
            .expect(200, (err, res) => {
                expect(res.body.registerComplete).toBeFalsy();
                expect(res.body.message).toContain('Invalid name length');
                done();
            });

    });

    it('/api/user : should return fail when name has invalid length (too many characters)', (done) => {
        server.agent.post('/api/user')
            .send({ name: '1234567890123456789012345678901234567890', password: '121212121212', repeatPassword: '121212121212', email: 'daniel@os.pl' })
            .expect(200, (err, res) => {
                expect(res.body.registerComplete).toBeFalsy();
                expect(res.body.message).toContain('Invalid name length');
                done();
            });

    });

    it('/api/user : should return fail when name has invalid syntax - adam wolfram second', (done) => {
        server.agent.post('/api/user')
            .send({ name: 'adam wolfram second', password: '121212121212', repeatPassword: '121212121212', email: 'daniel@os.pl' })
            .expect(200, (err, res) => {
                expect(res.body.registerComplete).toBeFalsy();
                expect(res.body.message).toContain('Invalid name syntax');
                done();
            });

    });

    it('/api/user : should return fail when name has invalid syntax -moje456^&', (done) => {
        server.agent.post('/api/user')
            .send({ name: '-moje456^&', password: '121212121212', repeatPassword: '121212121212', email: 'daniel@os.pl' })
            .expect(200, (err, res) => {
                expect(res.body.registerComplete).toBeFalsy();
                expect(res.body.message).toContain('Invalid name syntax');
                done();
            });

    });

    it('/api/user : should return fail when email exist in database', (done) => {
        server.agent.post('/api/user')
            .send({ name: 'adminTest', password: '121212121212', repeatPassword: '121212121212', email: 'john@email.com' })
            .expect(200, (err, res) => {
                expect(res.body.registerComplete).toBeFalsy();
                expect(res.body.message).toContain('Email Address exist in database');
                done();
            });

    });

    it('/api/user : should return true after successful register', (done) => {
        server.agent.post('/api/user')
            .send({ name: 'adminTest', password: '121212121212', repeatPassword: '121212121212', email: 'lukasz199312@gmail.com' })
            .expect(200, (err, res) => {
                expect(res.body.registerComplete).toBeTruthy();
                done();
            });

    });


    it('/api/user-name-available/ : should return true when name does not exist in database', (done) => {
        server.agent.get('/api/user-name-available/NoexistName')
            .expect(200, (err, res) => {
                expect(res.body.isAvailable).toBeTruthy();
                done();
            });

    });

    it('/api/user-name-available/:name : should return false when name exist in database', (done) => {
        server.agent.get('/api/user-name-available/admin')
            .expect(200, (err, res) => {
                expect(res.body.isAvailable).toBeFalsy();
                done();
            });

    });

    it('/api/user-email-available/:email : should return false when email exist in database', (done) => {
        server.agent.get('/api/user-email-available/thomas@email.com')
            .expect(200, (err, res) => {
                expect(res.body.isAvailable).toBeFalsy();
                done();
            });

    });

    it('/api/user-email-available/:email : should return true  when email does not  exist in database', (done) => {
        server.agent.get('/api/user-email-available/super@test.pl')
            .expect(200, (err, res) => {
                expect(res.body.isAvailable).toBeTruthy();
                done();
            });

    });

    it('adminTest user should has user group', (done) => {
        new GetUser().populate({ name: 'admintest' }, PopulateField.USER_DEFAULT_GROUP).then((user) => {
            SystemConfig.get().getDefaultUserGroup().then(defaultUserGroup => {
                expect(user.groups[0].name).toBe(defaultUserGroup.name);
                done();
            })
        })
    });

});


describe('User route - Get', () => {
    it('server should be defined', () => {
        expect(server).toBeDefined();
    });
});