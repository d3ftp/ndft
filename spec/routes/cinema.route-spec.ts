import { Server } from '../server';
import { IndexRoute } from '../../app/server/routes/index.route';
import request = require('supertest');
import { Response } from 'supertest';
import { Shell } from '../helpers/shell-Id';

import UserGroupModel = require('../../app/server/database/models/user-group.model');
import { I_UserGroup } from '../../app/server/database/models/i_user-group';
import { CinemaRoute } from '../../app/server/routes/cinema.route';
import { model } from '../../app/server/database/models/cinema/tv-show.model';
import { I_BasicTv } from '../../app/server/cinema/i_basic-tv';
import { TestUserDispatcher } from '../helpers/test-user-dispatcher';
import { GetUser } from '../../app/server/user/get-user';
import * as express from "express";
import { User } from '../../app/server/user/user';
import { TvShowBackupDispatcher } from '../../app/server/cinema/backup/tv-show-backup-dispatcher';
import { model as backupModel } from '../../app/server/database/models/cinema/tv-show-backup.model';
var bodyParser = require('body-parser');

var server: Server;
var shell: Shell = new Shell();
var groups: I_UserGroup[] = [];
let user = { name: 'accountuser', password: '1234567', email: 'account-user@test.pl', verificationCode: 'ffffffffff' }
let tvId;

xdescribe('cinema route', () => {
    beforeAll(async (done) => {
        let route = express.Router();

        server = Server.bootstrap();
        server.app.use(bodyParser.json());
        server.app.use(bodyParser.urlencoded({ extended: false }));
        server.app.use(new CinemaRoute({} as any).router);

        await TestUserDispatcher.createUser(user.name, user.password, user.email, user.verificationCode);
        let doc = await new GetUser().by({ name: user.name });

        route.get('/test/session-init', (req, res, next) => {
            let user = new User(doc._id, doc.name, []);
            req.session.currentUser = user;
            res.send(req.session);
        });

        route.get('/test/session-destroy', (req, res, next) => {
            req.session.destroy(() => { });
            res.send(true);
        });

        server.app.use((err, req, res, next) => {
            res.status(500).send({ complete: false })
        })

        server.app.use(route);

        done();
    })

    describe('update - ', () => {
        beforeAll((done) => {
            done();
        });

        it('should return false if session does not exist', (done) => {
            server.agent.put('/api/cinema/tv/0')
                .send({ name: 'test', poster_path: 'test_path', tvID: 0 })
                .expect(200, (err, req) => {
                    expect(req.body.complete).toBeFalsy();
                    done();
                });
        });

        it('should initialize session', (done) => { // CREATE SESSION
            server.agent.get('/test/session-init').expect(200, (err, res) => {
                let currentUser = res.body.currentUser;

                expect(currentUser).toBeDefined();
                expect(currentUser.name).toEqual(user.name);

                done();
            });
        });

        it('should return false when id is wrong', (done) => {
            server.agent.put('/api/cinema/tv/xx1')
                .send({})
                .expect(200, (err, req) => {
                    expect(req.body.complete).toBeFalsy();
                    done();
                });
        })

        it('should return false when does not contains TvID', (done) => {
            server.agent.put('/api/cinema/tv/0')
                .send({})
                .expect(200, (err, req) => {
                    expect(req.body.complete).toBeFalsy();
                    done();
                });
        })

        it('should return false when TvID isNAN', (done) => {
            server.agent.put('/api/cinema/tv/0')
                .send({ tvID: 'xx' })
                .expect(200, (err, req) => {
                    expect(req.body.complete).toBeFalsy();
                    done();
                });
        })

        it('should return false when TvID is empty string', (done) => {
            server.agent.put('/api/cinema/tv/0')
                .send({ tvID: '' })
                .expect(200, (err, req) => {
                    expect(req.body.complete).toBeFalsy();
                    done();
                });
        })

        it('should create new document if does not exist', (done) => {
            server.agent.put('/api/cinema/tv/0')
                .send({ name: 'test', poster_path: 'test_path', tvID: 0 })
                .expect(200, (err, req) => {
                    expect(req.body.complete).toBeTruthy();
                    model.findOne({ tvID: 0 }).then(data => {
                        expect(data.name).toEqual('test');
                        expect(data._id).toBeDefined();
                        expect(data.poster_path).toEqual('test_path');
                        tvId = data._id;
                        done();
                    })
                });
        });

        it('should create second document ', (done) => {
            server.agent.put('/api/cinema/tv/1')
                .send({name: 'test', poster_path: 'test_path', tvID: 1 })
                .expect(200, (err, req) => {
                    expect(req.body.complete).toBeTruthy();
                    model.findOne({ tvID: 0 }).then(data => {
                        expect(data.name).toEqual('test');
                        expect(data.poster_path).toEqual('test_path');
                        done();
                    })
                });
        });


        it('should update first document ', (done) => {
            server.agent.put('/api/cinema/tv/0')
                .send({ name: 'testx', poster_path: 'test_path', tvID: 0, _id: tvId })
                .expect(200, (err, req) => {
                    expect(req.body.complete).toBeTruthy();
                    model.findOne({ tvID: 0 }).then(data => {
                        expect(data.name).toEqual('testx');
                        expect(data.poster_path).toEqual('test_path');
                        done();
                    })
                });
        });

        it('should dont update first document when id != tvID ', (done) => {
            server.agent.put('/api/cinema/tv/0')
                .send({ name: 'testx', poster_path: 'test_path', tvID: 1, _id: tvId })
                .expect(200, (err, req) => {
                    expect(req.body.complete).toBeFalsy();
                    done();
                });
        });

        it('should contains only one update', (done) => {
            TvShowBackupDispatcher.getAllChangesForDocByID(tvId.toString()).then(results => {
                expect(results.length).toEqual(1);
                done();
            })
        });
    });

    afterAll((done) => {
        model.remove({}).then(() => done());
    })

});
