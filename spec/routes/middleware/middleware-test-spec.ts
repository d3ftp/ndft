import * as express from "express";
import request = require('supertest');
import { Server } from '../../server';
import { Router, Express } from "express";
import { necessaryParam } from "../../../app/server/routes/middleware/necessary-param";
import { sessionUserExist } from "../../../app/server/routes/middleware/session-user-exist";
import { User } from "../../../app/server/user/user";
var bodyParser = require('body-parser');

var server: Server;
describe('Middleware Test', () => {
    beforeAll(() => {
        initializeServer();

        server.app.use(createNecessaryParamRoute());
        server.app.use(createNecessaryParamMultipleRoute());
        server.app.use(sessionUserExist());

        setErrorHandler();
    });

    describe('NecessaryParam', () => {
        it('should return 500 http response', () => {
            server.agent.get('/test/middle/necessary-param').expect(500, (err, res) => { });
        });

        it('should return 200 http response', () => {
            server.agent.get('/test/middle/necessary-param').send({ login: 'dad' }).expect(500, (err, res) => { });
        });

        it('should return 500 http response with multiple param', () => {
            server.agent.get('/test/middle/necessary-param').expect(500, (err, res) => { });
        });

        it('should return 200 http response  multiple param', () => {
            server.agent.get('/test/middle/necessary-param').send({ login: 'dad', password: 'ee' }).expect(500, (err, res) => { });
        });

        it('should return 500 http response when one or more params missing', () => {
            server.agent.get('/test/middle/necessary-param').send({ login: 'dad' }).expect(500, (err, res) => { });
        });
    });

    describe('session user', () => {
        it('should return 500 http response when session is not init', () => {
            server.agent.get('/test/middle/session-user').expect(500, (err, res) => { });
        });

        it('should init session', () => {
            server.agent.get('/test/session-init').expect(200, (err, res) => { });
        });

        it('should return 200 http response ', () => {
            server.agent.get('/test/middle/necessary-param').expect(500, (err, res) => { });
        });
    });

    
});

function initializeServer() {
    server = Server.bootstrap();
    let route = express.Router();
    server = Server.bootstrap();
    server.app.use(route);
    server.app.use(bodyParser.json());
    server.app.use(bodyParser.urlencoded({ extended: false }));
}

function setErrorHandler() {
    server.app.use((err, req, res, next) => {
        res.status(500).send('Something broke! ' + err)
    })
}

function createNecessaryParamRoute() {
    let route = express.Router();
    route.get('/test/middle/necessary-param', necessaryParam('login'), (req, res) => {
        res.sendStatus(200);
    });

    return route;
}


function createNecessaryParamMultipleRoute() {
    let route = express.Router();
    route.get('/test/middle/necessary-param', necessaryParam('login', 'password'), (req, res) => {
        res.sendStatus(200);
    });

    return route;
}

// Session User

function createSession() {
    let route = express.Router();
    route.get('/test/middle/session-user', sessionUserExist(), (req, res) => {
        res.sendStatus(200);
    });

    route.get('/test/session-init', (req, res, next) => {
        let user = new User('fffffffff', 'xxxxx', []);
        req.session.currentUser = user;
        res.send(req.session);
    });

    return route;
}