import { Server } from '../server';
import { IndexRoute } from "../../app/server/routes/index.route";
import { AccountVerificationRoute } from "../../app/server/routes/account-verification.route";
import { AddUser } from "../../app/server/user/add-user";
import { CryptoHash } from "../../app/server/crypto/crypto-hash";
import { I_Result } from '../../app/server/routes/i_result';

var server: Server;
var code: string;

describe('Active verifcation route', () => {
    beforeAll((done) => {
        server = Server.bootstrap();
        server.app.use(new AccountVerificationRoute().router);
        code = CryptoHash.randomHex(16);
        new AddUser().add('ActiveRouteUser Test', '123123', 'test@test.pl', code).then(() => done());
    });


    it('should dont active account with wrong code', (done) => {
        server.agent.get(`/api/verification-register-code/activerouteuser test/123456`).expect(200, (err, res) => {
            
            expect((<I_Result>res.body).complete).toBeFalsy();
            done();
        });
    });

    it('should active account', (done) => {
        server.agent.get(`/api/verification-register-code/activerouteuser test/${code}`).expect(200, (err, res) => {
            expect((<I_Result>res.body).complete).toBeTruthy();
            done();
        });
    });
})