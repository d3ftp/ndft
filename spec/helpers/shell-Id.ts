export class Shell {
    public byName<T extends { _id: string, name: string }>(name: string, arrays: T[]): string {
        if(arrays.length == 0) throw new Error("Array is empty");

        for (let array of arrays) {
            if (array.name == name) return array._id;
        }

        throw new Error("Given value does not exist in array");
    }
    
    public byProperty<T extends {_id: string}>(value: any, propertyName: string, arrays: T[]): string {
        if(arrays.length == 0) throw new Error("Array is empty");

        let array = arrays[0] as Object;
        if(array.hasOwnProperty(propertyName) == false) throw new Error(propertyName + " does not exist in class!");

        for (let array of arrays) {
            if (array[propertyName] == value) return array._id;
        }

        throw new Error("Given value does not exist in array");
    }
}