import { Server } from "../server";
import userModel = require('../../app/server/database/models/user.model');
import { CryptoHash } from '../../app/server/crypto/crypto-hash';
import { AddUser } from '../../app/server/user/add-user';
import { ActiveUser } from '../../app/server/user/active-user';
import UserGroupModel = require('../../app/server/database/models/user-group.model');
import { RouteDispatcher, QueueSync } from '../../app/server/permission/route-dispatcher';
import { GroupDispatcher } from "../../app/server/user/group-dispatcher";
import { SystemConfig } from "../../app/server/system-config";
import { GetUser } from "../../app/server/user/get-user";
import { PopulateField } from "../../app/server/user/populate-fields";

var mongoose = require('mongoose');
global.Promise = require('bluebird');

beforeAll((done) => {
    mongoose.connect('mongodb://localhost/test-ndft');

    var connection = mongoose.connection;
    mongoose.Promise = global.Promise;

    connection.on('error', console.error.bind(console, 'connection error: '));
    connection.once('open', () => {

        mongoose.connection.db.dropDatabase((er, result) => {
            console.log("Dropping Database");
            Server.bootstrap();

            var verificationCode = CryptoHash.randomHex(128);
            var user1 = new AddUser().add('admin', 'adminpass123', 'admin@email.com', CryptoHash.randomHex(128));
            var user2 = new AddUser().add('John', 'qwerty1235', 'john@email.com', CryptoHash.randomHex(128));
            var user3 = new AddUser().add('Thomas', '123456', 'thomas@email.com', CryptoHash.randomHex(128));
            var user4 = new AddUser().add('activeTom', '123456', 'activethomas@email.com', verificationCode);

            var groupDispatcher = GroupDispatcher.get();
            var routeDispatcher = new RouteDispatcher();

            routeDispatcher.add('/test/name/:data', makeRegExp('/test/name/:data'), "post");
            routeDispatcher.add('/test/name/:data', makeRegExp('/test/name/:data'), "get");
            routeDispatcher.add('/test/name/:data', makeRegExp('/test/name/:data'), "put");
            routeDispatcher.add('/test/page/:id/:value', makeRegExp('/test/page/:id/:value'), "get");
            routeDispatcher.add('/test/user', makeRegExp('/test/user'), "get");
            routeDispatcher.add('/test/user', makeRegExp('/test/user'), "post");
            routeDispatcher.add('/test/user', makeRegExp('/test/user'), "put");
            routeDispatcher.add('/test/user', makeRegExp('/test/user'), "delete");

            var resultRouter = routeDispatcher.registerAll();
            Promise.all([user1, user2, user3, user4, resultRouter]).then(data => {
                new ActiveUser().by(verificationCode);

                var groupPromise1 = groupDispatcher.create('user', [
                    RouteDispatcher.routePathToID('/test/name/:data', 'post'),
                    RouteDispatcher.routePathToID('/test/name/:data', 'get'),
                    RouteDispatcher.routePathToID('/test/page/:id/:value', 'get')
                ]);

                var groupPromise2 = groupDispatcher.create('moderator', [
                    RouteDispatcher.routePathToID('/test/name/:data', 'post'),
                    RouteDispatcher.routePathToID('/test/name/:data', 'get'),
                    RouteDispatcher.routePathToID('/test/user', 'get'),
                    RouteDispatcher.routePathToID('/test/user', 'delete')
                ]);

                var groupPromise3 = groupDispatcher.create('superuser', [
                    RouteDispatcher.routePathToID('/test/user', 'get'),
                    RouteDispatcher.routePathToID('/test/user', 'delete'),
                    RouteDispatcher.routePathToID('/test/user', 'post'),
                    RouteDispatcher.routePathToID('/test/user', 'put')
                ]);

                var systemConfig = SystemConfig.init();

                try {
                    Promise.all([groupPromise1, groupPromise2, groupPromise3, systemConfig]).then(() => {
                        groupDispatcher.getByName('user').then((doc) => {
                            SystemConfig.get().setDafultUserGroup(doc).then(() => {
                                SystemConfig.get().setNodemailerConfig('f', 'f', { user: 'xxx', pass: 'xxxx' })
                                done();
                            });
                        })
                    })
                } catch (error) {
                    throw Error(error);
                }
            });
        });
    });


});

function makeRegExp(value: string): RegExp {
    value = value.replace(/:[^\/]+/g, '[^\\/]+');
    value = value.replace(/\/(?!])/g, '\\/');

    return new RegExp('^' + value + '$');
}

// afterAll((done) => {
//     mongoose.connection.db.dropDatabase((er, result) => {
//         console.log("Dropping Database");
//         done();
//     });
// });

