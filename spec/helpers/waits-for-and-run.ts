export class WaitsForAndRun {
    private escapeTime: number = 1000;
    private ms: number = 200;
    private callback: () => void;
    private intervalTimer: NodeJS.Timer;
    private timeoutTimer: NodeJS.Timer;

    public for<T>(value: T, toBe: T) {
        this.intervalTimer = setInterval(() => {
            if (value == toBe) this.finish();
        }, this.ms);

        this.timeoutTimer = setTimeout(() => {
            this.finish();
            throw Error();

        }, this.escapeTime);
    }

    public forFunction(fc: () => true) {
        this.intervalTimer = setInterval(() => {
            if (fc()) this.finish();
        }, this.ms)

        this.timeoutTimer = setTimeout(() => {
            this.finish();
            throw Error();

        }, this.escapeTime);
    }

    public after(callback: () => void) {
        this.callback = callback;
    }

    public setMs(ms: number) {
        this.ms = ms;
    }

    public setEscapeTime(time: number) {
        this.escapeTime = time;
    }

    private finish() {
        clearTimeout(this.timeoutTimer);
        clearInterval(this.intervalTimer)
        this.callback();
    }
} 