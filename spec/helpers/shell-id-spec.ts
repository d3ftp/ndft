import { Shell } from "./shell-Id";
import { I_UserGroup } from "../../app/server/database/models/i_user-group";

var shell = new Shell();
var arraysOne: {_id, name, age}[] = [];
var arraysTwo: I_UserGroup[] = [];

describe("Shell", () => {
    beforeAll(() => {
        arraysOne.push({_id: "1", age: 22, name: "DarkField"});
        arraysOne.push({_id: "2", age: 2, name: "Omlet"});
        arraysOne.push({_id: "3", age: 11, name: "Apple"});

        arraysTwo.push({_id: "11", name: "users", routes: null})
        arraysTwo.push({_id: "12", name: "moderator", routes: null})
        arraysTwo.push({_id: "13", name: "admin", routes: null})
    });

    it('shell id should return right name', ()=> {
        expect(shell.byName("Omlet", arraysOne)).toBe("2");
        expect(shell.byName("Apple", arraysOne)).toBe("3");

        expect(shell.byName("users", arraysTwo)).toBe("11");
        expect(shell.byName("admin", arraysTwo)).toBe("13");
        expect(shell.byName("admin", arraysTwo)).toBe("13");
    });

    it('shell id should throw error when array is empty', ()=> {
        expect(() => shell.byName("Omlet", [])).toThrowError();
        expect(() =>shell.byProperty(2,"age" , [])).toThrowError();
    });

    it('shell id should throw error when array does not contains provided value', ()=> {
        expect(() => shell.byName("Egg", arraysOne)).toThrowError("Given value does not exist in array");
        expect(() => shell.byProperty(45,"age" ,arraysOne)).toThrowError("Given value does not exist in array");
    });

    it('shell id should throw error when property does not exist', ()=> {
        expect(() => shell.byProperty(22,"lastName" ,arraysOne)).toThrowError("lastName does not exist in class!");
    });

    it('shell id should return by property name', ()=> {
        expect(shell.byProperty(2,"age" ,arraysOne)).toBe("2");
        expect(shell.byProperty(11,"age" ,arraysOne)).toBe("3");
    });
    
});