import { GroupDispatcher } from "../../app/server/user/group-dispatcher";
import { SystemConfig } from "../../app/server/system-config";
import { ActiveUser } from "../../app/server/user/active-user";
import { GroupUser } from "../../app/server/user/group-user";
import { GetUser } from "../../app/server/user/get-user";
import { AddUser } from "../../app/server/user/add-user";
import { DeleteUser } from "../../app/server/user/delete-user";

export class TestUserDispatcher {
    public static defaultUser = { name: 'accountuser', password: '1234567', email: 'account-user@test.pl', verificationCode: 'ffffffffff' }

    public static createUserGroup(userGroup) {
        return Promise.all([
            GroupDispatcher.get().create(userGroup, null)
        ])
    }

    public static async ActiveUser(userName) {
        return Promise.all([
            new ActiveUser().by(await TestUserDispatcher.returnVerifcationCode(userName))
        ])
    }

    public static setGroup(userName, groupName) {
        GroupDispatcher.get().getByName(groupName).then((group) => {
            new GroupUser().set(userName, [group]).then(() => { })
        })
    }

    public static async returnVerifcationCode(userName: string) {
        let user = await new GetUser().by({ name: userName })
        return user.VerificationCode;
    }

    public static createUser(name: string, password: string, email: string, verificationCode: string) {
        return Promise.all([
            new AddUser().add(name, password, email, verificationCode)
        ])
    }

    public static async createDefaultUser() {
        return await TestUserDispatcher.createUser(TestUserDispatcher.defaultUser.name,
                                                   TestUserDispatcher.defaultUser.password,
                                                   TestUserDispatcher.defaultUser.email,
                                                   TestUserDispatcher.defaultUser.verificationCode);
    }

    public static deleteUser(name: string) {
        return new DeleteUser().deleteByName(name)
    }

    public static deleteDefaultUser() {
        return TestUserDispatcher.deleteUser(TestUserDispatcher.defaultUser.name);
    }

    public static async getUserID(name: string) {
        let user =  await new GetUser().by({name: name.toLowerCase()})
        return user._id;
    }

}