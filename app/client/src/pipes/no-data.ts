import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'noData' })
export class NoData implements PipeTransform {
    transform(value: any, ...args: any[]) {
        if (value == null || value == "") return "";
        else return `(${value})`;
    }
}