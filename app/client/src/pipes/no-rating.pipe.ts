import { Pipe, PipeTransform } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import * as Rx from 'rxjs';

@Pipe({ name: 'noRating' })
export class NoRatingPipe implements PipeTransform {

    constructor(private translate: TranslateService) {}

    transform(value: any, ...args: any[]) {
        if (value <= 0) return " -- / 10"
        else return (value + " / 10");
    }
} 