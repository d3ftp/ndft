import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import * as Rx from 'rxjs'

@Pipe({ name: 'noContent' })
export class NoContentPipe implements PipeTransform {

    constructor(private translate: TranslateService) { }

    transform(value: any, ...args: any[]) {
        if (value == null || value == "") return this.getTranslate(args[0]);
        else return Rx.Observable.of(value);

    }

    private getTranslate(position: string) {
        switch (position) {
            case 'NoDescription':
                return this.translate.get("Pipes.NoDescription");
            default:
                throw Error('Undefined Translate in No Content Pipe!!!')
        }
    }
}