import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FilmBoxComponent } from './filmbox.component';

const routes: Routes = [
    {
        path: '',
        component: FilmBoxComponent
    }
]

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class FilmBoxRoutingModule { }