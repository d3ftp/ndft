import { Component } from '@angular/core';

@Component({
    selector: "filmbox",
    templateUrl: "./filmbox.component.html",
    styleUrls: ["./filmbox.component.css"]
})
export class FilmBoxComponent {}