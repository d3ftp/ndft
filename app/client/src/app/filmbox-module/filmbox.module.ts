import { NgModule } from '@angular/core';
import { AppRoutingModule } from '../app-routing.module';
import { SelectivePreloadingStrategy } from '../selective-preloading-strategy';
import { FilmBoxComponent } from './filmbox.component';
import { FilmBoxRoutingModule } from './filmbox.routing.module';

@NgModule({
    declarations: [FilmBoxComponent],
    imports: [FilmBoxRoutingModule],
    exports: [FilmBoxComponent]
})
export class FilmBoxModule {}