import {I_UserGroup} from "../../../../server/database/models/i_user-group";

export class User {
    name: string;
    group: string[];
    profileImagePath: string = "";

    constructor(name: string, group: string[], imgPath: string) {
        this.group = group;
        this.name = name;
        this.profileImagePath = imgPath;
    }
}

