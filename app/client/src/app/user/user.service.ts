import { Injectable } from "@angular/core";
import { Http } from '@angular/http';

import { User } from "./user";
import { ReplaySubject } from "rxjs";
@Injectable()
export class UserService {
    private currentUser: User = null;
    public user$: ReplaySubject<User> = new ReplaySubject<User>(null);

    public constructor(private http: Http) { }

    public getSessionUser(): Promise<any> {
        //return Promise.resolve(new User("uknow", "Guest"));
        return this.http.get('/api/session/current').toPromise()
            .then((response) => {
                let responseData = response.json();
                if (responseData.complete == false) return null;

                this.currentUser = new User(responseData.data.session.user.name,
                    responseData.data.session.user.groups,
                    responseData.data.session.user.imgPath);

                this.user$.next(this.currentUser);
                return this.currentUser;
            });
    }

    public getLoggedUser(): User {
        return this.currentUser;
    }

    public setCurrentUser(user: User) {
        this.currentUser = user;
    }
}