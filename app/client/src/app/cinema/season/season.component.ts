import { Component, AfterViewInit } from "@angular/core";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { MoviedbService, FindTvResult, FindSeasonResult } from "../moviedb.service";
import * as Rx from 'rxjs'
import { debounceTime, distinctUntilChanged, switchMap, filter } from 'rxjs/operators';
import { I_InformationPanel } from "../components/information-panel/information-panel.component";

@Component({
    templateUrl: 'season.component.html',
    styleUrls: ['season.component.css']
})
export class SeasonComponent {
    private result$: Rx.Observable<FindTvResult>;
    private seasonResult$: Rx.Observable<FindSeasonResult>
    private headerData;
    private seriesData;
    private informationData: I_InformationPanel = {} as any;

    constructor(private route: ActivatedRoute, private movieDB: MoviedbService) {
    }

    ngAfterViewInit(): void {
        this.result$ = this.route.paramMap.pipe(
            switchMap((params: ParamMap) => {
                return this.movieDB.findTv(params.get("id"));
            })
        )

        this.seasonResult$ = this.route.paramMap.pipe(
            switchMap((params: ParamMap) => {
                return this.movieDB.FindSeason(params.get("id"), "1");
            })
        )

        this.result$.subscribe(data => {
            this.removeSpecials(data);
            this.headerData = data;
            this.seriesData = data.seasons;
            this.informationData.episodesCount = this.getAllEpisodesCount(data.seasons);
        },
        error => {
            console.log('KURWA MAC222');
        })

        this.seasonResult$.subscribe(data => {
            console.log(data);

        },
            error => {
                console.log('KURWA MAC');
            })
    }

    private removeSpecials(data: FindTvResult) {
        try {
            if (data.seasons[0].name == "Specials") {
                data.seasons.splice(0, 1);
                return data.seasons;
            }
        } catch (error) {
            return data.seasons;
        }
    }

    private getAllEpisodesCount(series: [{ episode_count: number }]) {
        let sum: number = 0;

        for (let serie of series) {
            sum += serie.episode_count;
        }

        return sum;
    }
}