import { Inject } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { I_Result } from "../../../../../server/routes/i_result";

@Inject({})
export class ListService {
    private httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': 'my-auth-token'
        })
      };

    constructor(private http: HttpClient) { }
    
    public putSeriesToList(tvID: number, seasonID:number ,episodeID: number) {
        return this.http.put<I_Result>(`/api/series-list/tv/${tvID}/season/${seasonID}/episode/${episodeID}`, {complete:true}, this.httpOptions);
    }
}