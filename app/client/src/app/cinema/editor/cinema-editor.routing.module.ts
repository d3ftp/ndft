import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SeasonEditorComponent } from './season-editor/season-editor.component';
import { EditorHeaderComponent } from './components/editor-header/editor-header.component';
import { EditorSeasonsComponent } from './components/editor-seasons/editor-seasons.component';


const routes: Routes = [
    {
        path: '',
        component: SeasonEditorComponent,
        children: [
            {

                path: "header",
                component: EditorHeaderComponent
            },
            {
                path: "seasons",
                component: EditorSeasonsComponent
            }
            // { path: '', redirectTo: '/account/profile', pathMatch: 'full' }
        ]
    },

]

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class CinemaEditorRoutingModule { }