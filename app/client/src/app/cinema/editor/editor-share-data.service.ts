import { Inject } from "@angular/core";
import { Subject, BehaviorSubject, ReplaySubject } from "rxjs";
import { I_TvData } from "./data.service";
import { I_BasicSeason } from "../../../../../server/cinema/I_basic-season";

@Inject({})
export class EditorShareDataService {
    public dataSource$: Subject<I_TvData> = new ReplaySubject<I_TvData>(1);
    public seasonDataSource$: Subject<I_BasicSeason> = new ReplaySubject<I_BasicSeason>(1);
    public data: I_TvData;
    public seasonData: I_BasicSeason[];
    private lastSeasonID: number;

    public setData(i_tvData: I_TvData) {
        this.data = i_tvData;
        this.resetSeasonData();
        this.dataSource$.next(this.data);
    }

    public setSeasonData(i_seasonData: I_BasicSeason) {
        let id = this.data.seasons.findIndex(element => element.id == i_seasonData.seasonID);
        if (id == -1) throw Error("EditorShareDataService on setSeasonData cant find ID");
        this.lastSeasonID = id;
        if (this.seasonData[id] != null) {
            this.seasonDataSource$.next(this.seasonData[id]);
        }
        else {
            this.seasonData[id] = i_seasonData;
            this.seasonDataSource$.next(i_seasonData);
        }
    }

    public resetSeasonData() {
        this.seasonData = [];

        for (let i = 0; i < this.data.seasons.length; i++) {
            this.seasonData.push(null);
        }
    }

    public getLastSeasonID() {
        return this.lastSeasonID;
    }
}