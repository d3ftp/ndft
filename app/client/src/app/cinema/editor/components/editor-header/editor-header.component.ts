import { Component, OnInit, Input, OnDestroy, OnChanges, SimpleChanges, ChangeDetectionStrategy } from '@angular/core';
import { FileUploadPath } from '../../../../images/socket.service';
import { I_TvData } from '../../data.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs';
import { EditorShareDataService } from '../../editor-share-data.service';
import { CinemaPaths } from '../../../cinema-paths';
import { StaticPaths } from '../../../../../../../server/public-paths';

@Component({
  selector: 'editor-header',
  templateUrl: './editor-header.component.html',
  styleUrls: ['./editor-header.component.css']
})
export class EditorHeaderComponent implements OnInit, OnDestroy {
  public profilePath = FileUploadPath.TV_SHOW_HEADER;
  public data: I_TvData = { name: '', overview: '' } as any;
  private shareDataSubscription: Subscription;
  private url = "https://image.tmdb.org/t/p/w1400_and_h450_face/";

  constructor(private editorShareDataService: EditorShareDataService) { }

  ngOnInit() {
    this.shareDataSubscription = this.editorShareDataService.dataSource$.subscribe(
      data => {
        this.data = data;
        if (this.data.modified_backdrop_path) this.url = StaticPaths.getTvShowHeader() + this.data.backdrop_path;
        else this.url = StaticPaths.getMovieDbHeader() + this.data.backdrop_path;
      },
      error => {
        throw new error(error);
      }
    )
  };

  ngOnDestroy() {
    if (this.shareDataSubscription != undefined)
      this.shareDataSubscription.unsubscribe();
  }

  public headerUpload(imagePath: string) {
    this.url = '';
    this.data.backdrop_path = imagePath;
    this.data.modified_backdrop_path = true;
  }

  public getBackdropUrl() {
    try {
      return this.url;
    } catch (error) {
      return ''
    }
  }
}
