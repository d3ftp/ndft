import { Component, Input, ElementRef, ChangeDetectorRef, ViewChild, Output, EventEmitter } from "@angular/core";

@Component({
    selector: "editor-input",
    templateUrl: "editor-input.component.html",
    styleUrls: ["editor-input.component.css"]
})
export class EditorInputComponent {
    @Input() value;
    @Output() sendData = new EventEmitter<string>()
    @ViewChild("inputElement") inputChild: ElementRef;
    private isFocused = false;

    constructor(private changeDetectorRef: ChangeDetectorRef) {}

    public onClick() {
        this.isFocused = true;
        this.changeDetectorRef.detectChanges();
        this.inputChild.nativeElement.focus();
    }

    public onFocusOut() {
        this.isFocused = false;
        this.sendData.emit(this.value);
    }
 }