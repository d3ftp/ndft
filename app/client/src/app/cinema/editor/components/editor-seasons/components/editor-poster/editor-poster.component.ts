import { Component, OnInit, ApplicationRef, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { I_TvData, DataService } from "../../../../data.service";
import { Subscription } from "rxjs/Subscription";
import { EditorShareDataService } from "../../../../editor-share-data.service";
import { StaticPaths } from "../../../../../../../../../server/public-paths";
import { FileUploadPath } from "../../../../../../images/socket.service";

@Component({
    selector: "editor-poster",
    templateUrl: "editor-poster.component.html",
    styleUrls: ["editor-poster.component.css"]
})
export class EditorPosterComponent implements OnInit {
    public data: I_TvData = null;
    public profilePath = FileUploadPath.TV_SHOW_HEADER;
    private url: string = "";
    private shareDataSubscription: Subscription;
    private name = '';
    private lastSelectedIndex = 0;
    private modifiedIndex = [];

    constructor(private editorShareDataService: EditorShareDataService, private dataService: DataService) {
    }

    ngOnInit() {
        this.shareDataSubscription = this.editorShareDataService.dataSource$.subscribe(
            recivedData => {
                this.data = recivedData;
                this.url = this.getPosterUrl(0);
                this.name = this.data.seasons[0].name;

                for (let i = 0; i < this.data.seasons.length; i++) {
                    this.modifiedIndex.push(false);
                }

                if (this.data.seasons[0].has_internal_data) {
                    this.loadInternalData(this.data.seasons[0].id);
                }
                else {
                    this.loadExternalData(0);
                }
            },
            error => {
                throw new error(error);
            }
        )
    };

    public onClick(index: number) {
        this.name = this.data.seasons[index].name;
        this.url = this.getPosterUrl(index);
        this.lastSelectedIndex = index;

        if (this.data.seasons[index].has_internal_data) {
            this.loadInternalData(this.data.seasons[index].id);
        }
        else {
            this.loadExternalData(index);
        }
    }

    public onInput(name) {
        this.data.seasons[this.lastSelectedIndex].name = name;
    }

    public headerUpload(imagePath: string) {
        this.url = "";
        this.data.seasons[this.lastSelectedIndex].poster_path = imagePath;
        this.data.seasons[this.lastSelectedIndex].modified_backdrop_path = true;
        this.modifiedIndex[this.lastSelectedIndex] = true;
    }

    public getPosterUrl(index: number) {

        if (this.data.seasons[index].modified_backdrop_path == true) {            
            if (this.data.seasons[index].poster_path == null) return  StaticPaths.getNoImgPath();   
            if (this.modifiedIndex[index]) return StaticPaths.getTmpPath() + this.data.seasons[index].poster_path;
            else return StaticPaths.getPosterPath() + this.data.seasons[index].poster_path;
        }
        if (this.data.seasons[index].poster_path == null) return StaticPaths.getNoImgPath();
        return StaticPaths.getMovieDbPostarPath() + this.data.seasons[index].poster_path;
    }

    private loadExternalData(index) {
        let subscription = this.dataService.getSeasonExternalData(
            this.data.tvID.toString(),
            this.data.seasons[index].season_number.toString()
        ).subscribe(data => {
            this.editorShareDataService.setSeasonData(data);
            subscription.unsubscribe();
        })
    }

    private loadInternalData(seasonID: any) {
        let subscription = this.dataService.getSeasonInternalData(seasonID)
        .subscribe(data => {
            this.editorShareDataService.setSeasonData(data.data);
            subscription.unsubscribe();
        })
    }

    // public error(element) {
    //     element.src = StaticPaths.getNoImgPath();        
    // }
}