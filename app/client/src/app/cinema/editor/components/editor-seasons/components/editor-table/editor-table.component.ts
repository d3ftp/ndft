import { Component, OnInit } from "@angular/core";
import { Subscription } from 'rxjs';
import { EditorShareDataService } from "../../../../editor-share-data.service";
import { I_BasicSeason } from "../../../../../../../../../server/cinema/I_basic-season";
import { I_TvData } from "../../../../data.service";

@Component({
  selector: 'editor-table',
  templateUrl: 'editor-table.component.html',
  styleUrls: ['editor-table.component.css']
})
export class EditorTableComponent implements OnInit {
  public data: I_BasicSeason = null;
  public tvShow: I_TvData = null;
  private shareDataSubscription: Subscription;
  public episodeName: string = "";
  public episodeDate: string = "";

  constructor(private editorShareDataService: EditorShareDataService) { }

  ngOnInit() {

    this.editorShareDataService.dataSource$.subscribe(data => {
      this.tvShow = data;
    });

    this.editorShareDataService.seasonDataSource$.subscribe(data => {
      this.data = data;
    });
  };

  ngOnDestroy() {
    if (this.shareDataSubscription != undefined)
      this.shareDataSubscription.unsubscribe();
  }

  public onDeleteClick(index) {
    this.tvShow.seasons[this.editorShareDataService.getLastSeasonID()].has_internal_data = true;
    this.data.episodes.splice(index, 1);

    if (this.data.episodes.length > index) {
      for (let i = index; i < this.data.episodes.length; i++) {
        this.data.episodes[i].episode_number = i + 1;
      }
    }
  }

  public onReciveData(newData: string, index) {
    this.data.episodes[index].name = newData;
  }

  public onReciveDateData(newData: string, index) {
    this.data.episodes[index].air_date = newData;
  }

  public onAddNewEpisode() {
    this.data.episodes.push({
      air_date: this.episodeDate,
      name: this.episodeName,
      episode_number: this.data.episodes.length + 1
    });

    this.episodeDate = "";
    this.episodeName = "";
  }
}