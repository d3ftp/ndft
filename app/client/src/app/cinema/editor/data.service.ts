import { Inject } from "@angular/core";
import { MoviedbService, FindTvResult } from "../moviedb.service";
import { Observable } from "rxjs/Observable";
import { I_BasicTv } from "../../../../../server/cinema/i_basic-tv";
import { debounceTime, distinctUntilChanged, switchMap, filter, map } from 'rxjs/operators';
import { I_Search } from "../../../../../server/cinema/i-search";
import { HttpClient } from "@angular/common/http";
import { Subject } from "rxjs/Subject";
import { I_Result } from "../../../../../server/routes/i_result";
import { I_BasicSeason } from "../../../../../server/cinema/I_basic-season";

@Inject({})
export class DataService {
    constructor(private moviedb: MoviedbService, private http: HttpClient) { };

    public getSeasonExternalData(tvID: string, seasonID: string): Observable<I_BasicSeason> {
        return this.moviedb.FindSeason(tvID, seasonID).pipe(
            map(this.mapSeasonExternalData)
        )
    }

    public getTvExternalData(id: string): Observable<I_TvData> {
        return this.moviedb.findTv(id).pipe(
            map(this.mapTvExternalData)
        );
    }

    public getTvInternalData(tvID: any) {
        return this.http.get<I_Result>("/api/cinema/tv/" + tvID);
    }

    public getSeasonInternalData(seasonID: any) {
        return this.http.get<I_Result>("/api/cinema/season/" + seasonID);
    }

    public updateTvData(data: I_TvData) {
        return this.http.put<I_Result>("/api/cinema/tv/" + data.tvID, this.mapToInternalBasicTV(data));
    }

    public updateSeasonData(seasons: I_BasicSeason[], data: I_TvData) {
        let array = [] as any;
        
        for (let i = 0; i <= seasons.length; i++) {
            if (seasons[i] == null) continue;
            if (data.seasons[i].has_internal_data != true) continue;
            array.push(this.mapToInternalBasicSeason(seasons[i]));
        }

        return this.http.put<I_Result>("/api/cinema/seasons", array);
    }

    private mapTvExternalData(data: FindTvResult): I_TvData {
        let adapterData: I_TvData;
        adapterData = data as any;
        adapterData._id = undefined;
        adapterData.tvID = data.id;

        return adapterData;
    }

    private mapSeasonExternalData(data: any) {
        data._id = undefined;
        data.seasonID = data.id;
        return data;
    }

    private mapToInternalBasicTV(data: I_TvData) {
        return {
            _id: data._id,
            backdrop_path: data.backdrop_path,
            modified_backdrop_path: data.modified_backdrop_path,
            first_air_date: data.first_air_date,
            last_air_date: data.last_air_date,
            name: data.name,
            number_of_episodes: data.number_of_episodes,
            number_of_seasons: data.number_of_seasons,
            original_name: data.original_name,
            overview: data.overview,
            poster_path: data.poster_path,
            seasons: data.seasons,
            tvID: data.tvID,
            vote_average: data.vote_average
        } as I_BasicTv
    }

    private mapToInternalBasicSeason(data: I_BasicSeason) {
        return {
            _id: data._id,
            air_date: data.air_date,
            episodes: this.mapToBasicSeasonEpisodes(data.episodes),
            seasonID: data.seasonID
        } as I_BasicSeason
    }

    private mapToBasicSeasonEpisodes(episodes: any): any {
        let array = [];
        for (let i = 0; i < episodes.length; i++) {
            array.push({
                air_date: episodes[i].air_date,
                episode_number: episodes[i].episode_number,
                name: episodes[i].name
            })
        }        
        return array;
    }
}


export interface I_TvData extends I_BasicTv {
    id: string;
}

