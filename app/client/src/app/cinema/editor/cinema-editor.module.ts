import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditorHeaderComponent } from './components/editor-header/editor-header.component';
import { SeasonEditorComponent } from './season-editor/season-editor.component';
import { FileTransmissionModule } from '../../file-transmission/file-transmission.module';
import { CinemaEditorRoutingModule } from './cinema-editor.routing.module';
import { EditorMenuComponent } from './components/editor-menu/editor-menu.component';
import { EditorShareDataService } from './editor-share-data.service';
import { FormsModule } from '@angular/forms';
import { EditorSeasonsComponent } from './components/editor-seasons/editor-seasons.component';
import { EditorTableComponent } from './components/editor-seasons/components/editor-table/editor-table.component';
import { EditorPosterComponent } from './components/editor-seasons/components/editor-poster/editor-poster.component';
import { ShareModule } from '../../share.module';
import { EditorInputComponent } from './components/editor-input/editor-input.component';

@NgModule({
  imports: [
    CommonModule,
    FileTransmissionModule,
    CinemaEditorRoutingModule,
    FormsModule,
    ShareModule
  ],
  declarations: [
    EditorHeaderComponent,
    SeasonEditorComponent,
    EditorMenuComponent,
    EditorSeasonsComponent,
    EditorTableComponent,
    EditorPosterComponent,
    EditorInputComponent
   ],
  providers: [EditorShareDataService],
  exports: [SeasonEditorComponent]
})
export class CinemaEditorModule { }
