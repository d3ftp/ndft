import { Component, OnInit, OnDestroy } from '@angular/core';
import { MoviedbService, FindTvResult } from '../../moviedb.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { delay } from 'rxjs/operators/delay';
import { of } from 'rxjs/observable/of';
import { zip } from 'rxjs/observable/zip';
import "rxjs/add/operator/toPromise"
import { switchMap } from 'rxjs/operators/switchMap';
import { map } from 'rxjs/operators/map';
import { Subscription } from 'rxjs';
import { mergeAll, concatAll, mergeMap, catchError, concat } from 'rxjs/operators';
import { DataService, I_TvData } from '../data.service';
import { EditorShareDataService } from '../editor-share-data.service';
import { SystemMessageService } from '../../../system-message/system-message.service';

@Component({
  templateUrl: './season-editor.component.html',
  styleUrls: ['./season-editor.component.css']
})
export class SeasonEditorComponent implements OnInit, OnDestroy {
  private tvResultSubscribion: Subscription;
  private subscription: Subscription;
  private data: I_TvData;


  constructor(private route: ActivatedRoute,
    private dataService: DataService,
    private editorShareDataService: EditorShareDataService,
    private systemMessage: SystemMessageService
  ) { }

  ngOnInit() {
    this.subscription = this.getSeasonID().pipe(
      switchMap(id => {
        return zip(this.dataService.getTvInternalData(id),
          this.dataService.getTvExternalData(id),
          (interlnalData, externalData) => {
            if (interlnalData.complete == true) return this.removeSpecials(interlnalData.data);
            else return this.removeSpecials(externalData);
          });
      })
    ).subscribe(data => {
      this.editorShareDataService.setData(data);
      this.editorShareDataService.data = data;
      this.data = data;
    }, error => {
      this.systemMessage.activeError(error.message);
    })
  }


  private getSeasonID(): Observable<string> {
    return this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        return Observable.of(params.get('id'));
      })
    )
  }


  public onSaveClick() {
    this.dataService.updateTvData(this.data)
      .pipe(
        mergeMap(result => {
          this.data._id = result.data;
          return this.dataService.updateSeasonData(this.editorShareDataService.seasonData, this.data)
        })
      )
      .subscribe(result => {
        for (let data of result.data) {
          let res = this.editorShareDataService.seasonData.find(el => el.seasonID == data.seasonID);
          res._id = data.id;
        }
        this.systemMessage.activeSuccess("Zmiany zostały zapisane!");
      }, err => {
        this.systemMessage.activeError(err.message);
      })
  }

  ngOnDestroy() {
    if (this.subscription != undefined)
      this.subscription.unsubscribe();
    if (this.tvResultSubscribion != undefined)
      this.tvResultSubscribion.unsubscribe();
  }

  private removeSpecials(data) {
    if (data.seasons[0].season_number == 0) {
      data.seasons.splice(0, 1);
      return data;
    }
    return data;
  }

  private test() {
    console.log(this.editorShareDataService.seasonData);

  }
}