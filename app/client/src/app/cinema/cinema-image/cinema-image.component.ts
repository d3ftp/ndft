import { Component, Input, OnChanges, SimpleChanges } from "@angular/core";

@Component({
    selector: 'cinema-image',
    templateUrl: 'cinema-image.component.html',
    styleUrls: ['cinema-image.component.css']
})
export class CinemaImageComponent implements OnChanges{
    @Input() path: string = null;
    public imageLoading = true;

    public onImageLoad() {
        this.imageLoading = false;
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes['data'] != undefined ) {
            this.imageLoading = true;
        }
    }
}