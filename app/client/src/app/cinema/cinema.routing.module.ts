import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CinemaComponent } from './cinema.component';
import { SeriesComponent } from './series/series.component';
import { SeasonComponent } from './season/season.component';
import { SeasonEditorComponent } from './editor/season-editor/season-editor.component';



const routes: Routes = [
    {
        path: '',
        component: CinemaComponent,
        children: [
            {
                path: "cinema/series/:id",
                component: SeriesComponent
            },
            {
                path: "cinema/series/:id/edit",
                loadChildren: "./editor/cinema-editor.module#CinemaEditorModule"
            },
            {
                path: "cinema/series/:id/season/:number",
                component: SeasonComponent
            },
            { path: '', redirectTo: '/cinema/series', pathMatch: 'full' }
        ]
    },

]

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class CinemaRoutingModule { }