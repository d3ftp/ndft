import { NgModule } from "@angular/core"
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MoviedbService } from "./moviedb.service";
import { CinemaRoutingModule } from "./cinema.routing.module";
import { CinemaComponent } from "./cinema.component";
import { AppTranslateModule } from "../app-translate.module";
import { SeriesComponent } from "./series/series.component";
import { HeaderContentComponent } from "./components/header-content/header-content.component";
import { SpinnerModule } from "../spinners/spinner.module";
import { NoData } from "../../pipes/no-data";
import { NoContentPipe } from "../../pipes/no-content";
import { NoRatingPipe } from "../../pipes/no-rating.pipe";
import { SeasonContentComponent } from "./components/seasons-content/seasons-content.component";
import { InformationPanelComponent } from "./components/information-panel/information-panel.component";
import { CinemaImageComponent } from "./cinema-image/cinema-image.component";
import { SeasonComponent } from "./season/season.component";
import { PosterComponent } from "./components/poster-component.ts/poster.component";
import { TableComponent } from "./components/table/table-component";
import { SimpleBarDirective } from "../directives/simplebar/simplebar.directive";
import { ImportanceDateDirective } from "../directives/importance-date-directive/importance-date.directive";
import { CinemaEditorModule } from "./editor/cinema-editor.module";
import { DataService } from "./editor/data.service";
import { ShareModule } from "../share.module";
import { ListService } from "./list/list.service";


@NgModule({
    declarations: [CinemaComponent, SeriesComponent, HeaderContentComponent, SeasonContentComponent,InformationPanelComponent
    ,NoContentPipe, NoData, NoRatingPipe,
    CinemaImageComponent, SeasonComponent, PosterComponent, TableComponent],
    imports: [CommonModule, FormsModule, CinemaRoutingModule, AppTranslateModule, SpinnerModule, CinemaEditorModule, ShareModule],
    providers: [MoviedbService, DataService, ListService],
    exports: []
})
export class CinemaModule { }