import { Inject } from "@angular/core";
import moviedb = require('moviedb')
import * as Rx from 'rxjs'
import { Subject } from 'rxjs/Subject';
import { debounceTime, distinctUntilChanged, switchMap, filter } from 'rxjs/operators';
import { HttpClient, HttpParams } from "@angular/common/http";
import { I_BasicTv } from "../../../../server/cinema/i_basic-tv";
import { I_BasicSeason } from "../../../../server/cinema/I_basic-season";

@Inject({})
export class MoviedbService {
  private url = "https://api.themoviedb.org/3";
  private api_key = "cce022c51dff9728053487e1a2bea2f9"
  private mdb: any;
  public searchResults$: Rx.Observable<SearchResult>
  public searchText$ = new Subject<string>();

  constructor(private http: HttpClient) {
    this.mdb = moviedb('cce022c51dff9728053487e1a2bea2f9');
    this.searchResults$ = this.searchText$.pipe(
      filter(val => val != ""),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(results => this.searchTv(results))
    )
  }

  public searchTv(value: string, language: string = "pl") {
    let params = new HttpParams();
    params = params.append('api_key', this.api_key);
    params = params.append('query', value);
    params = params.append('language', language);
    return this.http.get<SearchResult>(this.url + "/search/tv", { params: params });
  }

  public findTv(id: string, language: string = "pl") {
    let params = new HttpParams();
    params = params.append('api_key', this.api_key);
    params = params.append('language', language);
    return this.http.get<FindTvResult>(this.url + "/tv/" + id, { params: params });
  }

  public FindSeason(id: string, seasonNumber: any ) {
    let params = new HttpParams();
    params = params.append('api_key', this.api_key);
    return this.http.get<FindSeasonResult>(this.url + "/tv/" + id + "/season/"+ seasonNumber, { params: params });
  }

}

export class MovieDBConfig {
  public static Config = {
    images: {
      base_url: "http://image.tmdb.org/t/p/",
      secure_base_url: "https://image.tmdb.org/t/p/",
      backdrop_sizes: [
        "w300",
        "w780",
        "w1280",
        "original"
      ],
      logo_sizes: [
        "w45",
        "w92",
        "w154",
        "w185",
        "w300",
        "w500",
        "original"
      ],
      poster_sizes: [
        "w92",
        "w154",
        "w185",
        "w342",
        "w500",
        "w780",
        "original"
      ],
      profile_sizes: [
        "w45",
        "w185",
        "h632",
        "original"
      ],
      still_sizes: [
        "w92",
        "w185",
        "w300",
        "original"
      ]
    },
    change_keys: [
      "adult",
      "air_date",
      "also_known_as",
      "alternative_titles",
      "biography",
      "birthday",
      "budget",
      "cast",
      "certifications",
      "character_names",
      "created_by",
      "crew",
      "deathday",
      "episode",
      "episode_number",
      "episode_run_time",
      "freebase_id",
      "freebase_mid",
      "general",
      "genres",
      "guest_stars",
      "homepage",
      "images",
      "imdb_id",
      "languages",
      "name",
      "network",
      "origin_country",
      "original_name",
      "original_title",
      "overview",
      "parts",
      "place_of_birth",
      "plot_keywords",
      "production_code",
      "production_companies",
      "production_countries",
      "releases",
      "revenue",
      "runtime",
      "season",
      "season_number",
      "season_regular",
      "spoken_languages",
      "status",
      "tagline",
      "title",
      "translations",
      "tvdb_id",
      "tvrage_id",
      "type",
      "video",
      "videos"
    ]
  }
}

export interface FindSeasonResult extends I_BasicSeason {}



export interface SearchResult {
  page: number,
  results: [{
    id: number;
    name: string
  }]
}


export interface FindTvResult extends I_BasicTv {
  backdrop_path: string,
  created_by: [
    {
      id: number,
      name: string,
      gender: number,
      profile_path: string
    }
  ],
  episode_run_time: [number],
  first_air_date: string,
  genres: [
    {
      id: number,
      name: string
    }
  ],
  homepage: string,
  id: number,
  in_production: boolean,
  languages: [string],
  last_air_date: string,
  name: string,
  networks: [
    {
      id: number,
      name: string
    }
  ],
  number_of_episodes: number,
  number_of_seasons: number,
  origin_country: [string],
  original_language: string,
  original_name: string,
  overview: string,
  popularity: number,
  poster_path: string,
  production_companies: [
    {
      id: number,
      logo_path: string,
      name: string,
      origin_country: string
    }
  ],
  seasons: [
    {
      air_date: string,
      episode_count: number,
      id: number,
      poster_path: string,
      season_number: number,
      name: string,
      modified_backdrop_path: boolean,
      has_internal_data: boolean
    }
  ],
  status: string,
  type: string,
  vote_average: number,
  vote_count: number
}