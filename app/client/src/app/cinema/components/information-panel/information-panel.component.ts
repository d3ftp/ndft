import { Component, Input } from "@angular/core";

@Component({
    selector: 'information-panel',
    templateUrl: 'information-panel.component.html',
    styleUrls: ['information-panel.component.css']
})
export class InformationPanelComponent { 
    @Input() data: I_InformationPanel;
}


export interface I_InformationPanel {
    air_date: number;
    episodesCount: number;
}