import { Component, Input, ViewChild, OnChanges, SimpleChanges, AfterViewInit, AfterContentInit, OnDestroy } from "@angular/core";
import { SimpleBarDirective } from "../../../directives/simplebar/simplebar.directive";
import { UserService } from "../../../user/user.service";
import { User } from "../../../user/user";
import { Subscription } from "rxjs";
import { ListService } from "../../list/list.service";

@Component({
    selector: 'table-component',
    templateUrl: 'table-component.html',
    styleUrls: ['table-component.css']
})
export class TableComponent implements OnChanges, OnDestroy {
    @Input() data: I_TableData = null;
    @ViewChild(SimpleBarDirective) simpleBar: SimpleBarDirective;
    public user: User = null;
    private userSubscription: Subscription;
    
    constructor(private userService: UserService, private listService: ListService) {
        this.userSubscription = userService.user$.subscribe(value => {
            this.user = value;
        })
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['data'] !== undefined) {
            this.simpleBar.resetScrollPosition();
        }
    }

    ngOnDestroy(): void {
        if (this.userSubscription != undefined) {
            this.userSubscription.unsubscribe();
        }
    }

    getVisablityColor(index: number) {        
        if (index + this.data.episodesOffset < 50) return "green";
        return "";
    }

    public onClick(index) {
        console.log(this.data);
        this.listService.putSeriesToList(this.data.seriesID, this.data.seasonID, 1).subscribe()
    }

}


export interface I_TableData {
    episodesOffset;
    episodes: [any];
    seriesID: any;
    seasonID: any;
}