import { Component, Input, Output, EventEmitter } from "@angular/core";
import { StaticPaths } from "../../../../../../server/public-paths";

@Component({
    selector: 'season-content',
    templateUrl: 'seasons-content.component.html',
    styleUrls: ['seasons-content.component.css']
})
export class SeasonContentComponent {
    @Input() data: [I_SeasonContent];
    @Output() indexSend = new EventEmitter();

    public onClick(index: number) {
        this.indexSend.emit(index);
    }

    public getUrl(index: number) {
        if (this.data[index].modified_backdrop_path) {
            return StaticPaths.getPosterPath() + this.data[index].poster_path;
        }
        else if (this.data[index].poster_path == null) return StaticPaths.getNoImgPath();
        
        return StaticPaths.getMovieDbPostarPath() + this.data[index].poster_path;
    }
}

export interface I_SeasonContent {
    id: number,
    air_date: string,
    episode_count: string,
    name: string,
    poster_path: string,
    modified_backdrop_path: boolean
}