import { Component, Input, OnChanges, SimpleChanges } from "@angular/core";
import { StaticPaths } from "../../../../../../server/public-paths";

@Component({
    selector: 'poster-component',
    templateUrl: 'poster.component.html',
    styleUrls: ['poster.component.css']
})
export class PosterComponent implements OnChanges {
    @Input() path: string;

    public imageLoading = true;

    public onImageLoad() {
        this.imageLoading = false;
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (this.path == null) this.path = StaticPaths.getNoImgPath();  
        else if (this.path != undefined) {

            var res = this.path.split('/');
            
            if (res[res.length - 1] == "null") {
                this.path = StaticPaths.getNoImgPath();       
                this.imageLoading = false;
                
            }
            else
                this.imageLoading = true;
        }
    }
}