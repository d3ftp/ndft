import { Component, Input, OnChanges, SimpleChanges, OnInit } from "@angular/core";
import { StaticPaths } from "../../../../../../server/public-paths"

@Component({
    selector: 'header-content',
    templateUrl: "header-content.component.html",
    styleUrls: ['header-content.component.css']
})
export class HeaderContentComponent implements OnChanges, OnInit {
    @Input() data: I_HeaderContent = null;
    public imageLoading = true;
    public url = StaticPaths.getMovieDbHeader();

    ngOnInit(): void {
        this.url = StaticPaths.getNoImgPath();
    }

    public onImageLoad() {
        this.imageLoading = false;        
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes['data'] != undefined) {
            this.imageLoading = true;

            if (this.data != null)
                if (this.data.modified_backdrop_path == true)
                    this.url = StaticPaths.getTvShowHeader()
                    else this.url = StaticPaths.getMovieDbHeader();
        }
    }

}

export interface I_HeaderContent {
    name: string,
    overview: string,
    poster_path: string,
    status: string,
    vote_average: number,
    vote_count: number,
    first_air_date: string,
    backdrop_path: string,
    modified_backdrop_path: boolean
}