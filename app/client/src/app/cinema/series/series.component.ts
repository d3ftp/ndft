import { Component, AfterViewInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { MoviedbService, FindTvResult, FindSeasonResult } from "../moviedb.service";
import * as Rx from 'rxjs'
import { debounceTime, distinctUntilChanged, switchMap, filter, flatMap, catchError, map } from 'rxjs/operators';
import { I_InformationPanel } from "../components/information-panel/information-panel.component";
import { I_TableData } from "../components/table/table-component";
import { DataService, I_TvData } from "../editor/data.service";
import { I_Result } from "../../../../../server/routes/i_result";
import { zip } from "rxjs/observable/zip";
import { StaticPaths } from "../../../../../server/public-paths";
import { Subscription } from "rxjs";

@Component({
    templateUrl: "series.component.html",
    styleUrls: ["series.component.css"]
})
export class SeriesComponent implements AfterViewInit, OnDestroy {
    private result$: Rx.Observable<I_TvData>;
    private seasonResult$: Rx.Observable<FindSeasonResult>;
    private internalResult$: Rx.Observable<I_Result>;
    private seriesResult: I_TvData;
    private seasonResult: FindSeasonResult;
    private seasonID: string;
    private headerData;
    private seriesData;
    private posterPath;
    private tableData: I_TableData = {} as any;
    private informationData: I_InformationPanel = {} as any;
    private hasSpecials = false;
    private actualSeason: number = 1;
    private zipSeasonSubscriber: Subscription;

    constructor(private route: ActivatedRoute, private movieDB: MoviedbService, private dataService: DataService) { }

    ngAfterViewInit(): void {        
        this.result$ = this.route.paramMap.pipe(
            switchMap((params: ParamMap) => {
                return zip(this.dataService.getTvExternalData(params.get("id")),
                    this.dataService.getTvInternalData(params.get("id")),
                    (externalData, InternalData) => {
                        if (InternalData.complete == true) {
                            return InternalData.data as I_TvData;
                        }
                        return externalData;
                    })
            })
        )

        this.seasonResult$ = this.route.paramMap.pipe(
            switchMap((params: ParamMap) => {
                this.seasonID = params.get("id");
                return this.dataService.getSeasonExternalData(params.get("id"), "1").catch((e, o) => {
                    return this.getEmptySeasonResult();
                });
            })
        )

        this.zipSeasonSubscriber = Rx.Observable.zip(
            this.result$,
            this.seasonResult$,
            async (seriesResult, sesonResult) => {
               try {
                    this.seasonResult = this.seriesData;
                    this.seriesResult = seriesResult;

                    this.removeSpecials(seriesResult);
                    if (seriesResult.seasons.length > 0 && seriesResult.seasons[0].has_internal_data) {
                        let result = await this.dataService.getSeasonInternalData(seriesResult.seasons[0].id).toPromise();
                        sesonResult = result.data;
                    }

                    this.setTableDate(sesonResult, seriesResult);
                    this.setInformationDate(sesonResult);
                    this.setHeaderData(seriesResult);
                    this.setEpisodesCount(seriesResult);
                    this.setPoster(seriesResult);
                    this.setSeasons(seriesResult);


                } catch (error) {
                    this.seasonResult = null;
                    this.seriesResult = null;
                    this.tableData = null;

                }
            }
        ).subscribe()
    }

    ngOnDestroy() {
        if (this.zipSeasonSubscriber != undefined)
            this.zipSeasonSubscriber.unsubscribe();
    }

    private setPoster(seriesResult: I_TvData) {
        try {
            this.posterPath = this.getUrl(0);
        } catch (error) {
            this.posterPath = null;
        }
    }

    private setSeasons(seriesResult: I_TvData) {
        try {
            this.seriesData = seriesResult.seasons;
        } catch (error) {
            this.seriesData = null;
        }
    }

    private setTableDate(seasonResult: FindSeasonResult, seriesResult: I_TvData, index = 0) {
        try {
            this.tableData = {} as any;
            this.tableData.episodes = seasonResult.episodes;
            this.tableData.episodesOffset = this.calculateSeasonEpisodeOffset(index, seriesResult.seasons);
            this.tableData.seasonID = seasonResult.seasonID;
            this.tableData.seriesID = seriesResult.id;
        } catch (error) {
            this.tableData = {} as any;
        }
    }

    private setEpisodesCount(seriesResult: I_TvData) {
        try {
            this.informationData.episodesCount = this.getAllEpisodesCount(seriesResult.seasons);
        } catch (error) {
            this.informationData.episodesCount = null;
        }
    }

    private setInformationDate(sesonResult: FindSeasonResult) {
        try {
            this.informationData.air_date = sesonResult.air_date;
        } catch (error) {
            this.informationData.air_date = null;
        }
    }

    private removeSpecials(data: I_TvData) {
        try {
            if (data.seasons[0].name == "Specials") {
                data.seasons.splice(0, 1);
                this.hasSpecials = true;
            }
            return data.seasons
        } catch (error) {
            return data.seasons;
        }
    }

    private setHeaderData(seriesResult: I_TvData) {
        try {
            this.headerData = seriesResult;
        } catch (error) {
            this.headerData = null;
        }
    }

    private getAllEpisodesCount(series: [{ episode_count: number }]) {
        let sum: number = 0;

        for (let serie of series) {
            sum += serie.episode_count;
        }

        return sum;
    }

    public onIndexSend(index: number) {
        this.setActualDisplaySeasonNumber(index);
        this.posterPath = this.getUrl(index);

        if (this.seriesResult.seasons[index].has_internal_data) {
            let subscription =  this.dataService.getSeasonInternalData(this.seriesResult.seasons[index].id).subscribe(data => {
                this.setInformationDate(data.data);
                this.setTableDate(data.data, this.seriesResult, index);
                subscription.unsubscribe();
            })
        }
        else {
            let subscription = this.movieDB.FindSeason(this.seasonID, this.seriesResult.seasons[index].season_number).subscribe(seasonResult => {
                seasonResult.seasonID = (seasonResult as any).id;
                this.setInformationDate(seasonResult);
                this.setTableDate(seasonResult, this.seriesResult, index);
                subscription.unsubscribe();
            })
        }

    }

    private setActualDisplaySeasonNumber(index: number) {
        this.actualSeason = index + 1;
    }

    public calculateSeasonEpisodeOffset(selectedSeasonIndex: number, series: [{ episode_count: number }]) {
        let offsetSum = 0;
        for (let i = 0; i <= selectedSeasonIndex; i++) {
            offsetSum += series[i].episode_count;
        }
        return offsetSum - series[selectedSeasonIndex].episode_count + 1;
    }

    private getEmptySeasonResult() {
        return Rx.Observable.of<FindSeasonResult>({
            _id: null,
            seasonID: null,
            air_date: null,
            episodes: null
        });
    }

    private getUrl(index: number) {
        if (this.seriesResult.seasons[index].modified_backdrop_path) {
            return StaticPaths.getPosterPath() + this.seriesResult.seasons[index].poster_path;
        }
        return StaticPaths.getMovieDbPostarPath() + this.seriesResult.seasons[index].poster_path;
    }
}