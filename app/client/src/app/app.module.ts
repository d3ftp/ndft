import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';
import { UserModule } from './user/user-module';

import { AppComponent } from './app.component';
import { AppTranslateModule } from "./app-translate.module";
import { HttpLoaderFactory as HttpLoaderFactory } from "./app-translate.module";
import { TranslatePipe, TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TestComponent } from "./test.component";
import { AppRoutingModule } from './app-routing.module';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { NavBarNoLoggedModule } from './nav-bar/nav-bar.module';
import { SidebarModule } from './sidebar/sidebar.module';
import { FooterModule } from './footer/footer.module';
import { FilmBoxModule } from './filmbox-module/filmbox.module';
import { AccountModule } from './account/account.module';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TestModule } from './test/test.module';
import { SystemMessageService } from './system-message/system-message.service';
import { CinemaModule } from './cinema/cinema.module';
import { FileTransmissionModule } from './file-transmission/file-transmission.module';
import { SimpleBarDirective } from './directives/simplebar/simplebar.directive';
import { SystemMessageComponent } from './system-message/system-message.component';
import { ImportanceDateDirective } from './directives/importance-date-directive/importance-date.directive';


@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    SystemMessageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule, //To Do zamienic z HttpClientModule
    HttpClientModule,
    AppTranslateModule,
    NavBarNoLoggedModule,
    SidebarModule,
    FooterModule,
    FilmBoxModule,
    AppRoutingModule,
    AccountModule,
    TestModule,
    CinemaModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
    

  ],
  providers: [SystemMessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
