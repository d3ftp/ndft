import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AdminDashboardComponent } from './admin-views/dashboard/admin-dashboard.component';
import { AppSettingsComponents } from './admin-views/app-settings/app-settings.component';
import { UserGroupsComponent } from './admin-views/user-groups/user-groups.component';
import { RoutesGroupComponent } from './admin-views/routes-group/routes-group.component';
import { SystemConfigComponent } from './admin-views/system-config/system-config.component';
import { AdminConfigGuard } from '../guards/admin-config-guard';
import { NodeMailerComponent } from './admin-views/nodemailer/nodemailer.component';


const routes: Routes = [
    {
        path: '',
        canActivate: [AdminConfigGuard],
        component: AdminComponent,
        children: [
            {
                path: "",
                component: AdminDashboardComponent
            },
            {
                path: "settings",
                component: AppSettingsComponents
            },
            {
                path: "user-groups",
                component: UserGroupsComponent
            },
            {
                path: "routes-group",
                component: RoutesGroupComponent
            },
            {
                path: "system-config",
                component: SystemConfigComponent
            },
            {
                path: "nodemailer",
                component: NodeMailerComponent
            }
        ]
    }

]

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AdminRoutingModule { }