import { NgModule } from "@angular/core"
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AdminComponent } from "./admin.component";
import { AdminRoutingModule } from "./admin.routing.module";
import { AdminDashboardComponent } from "./admin-views/dashboard/admin-dashboard.component";
import { AppSettingsComponents } from "./admin-views/app-settings/app-settings.component";
import { UserGroupsComponent } from "./admin-views/user-groups/user-groups.component";
import { RoutesGroupComponent } from "./admin-views/routes-group/routes-group.component";
import { RoutesListComponent } from "./admin-views/routes-list/routes-list.component";
import { AppTranslateModule } from "../app-translate.module";
import { SelectionRoutesGroupComponent } from "./admin-views/routes-group/selection-routes-group.component";
import { SystemConfigModule } from "./admin-views/system-config/system-config.module";
import { AdminConfigGuard } from "../guards/admin-config-guard";
import { NodeMailerComponent } from "./admin-views/nodemailer/nodemailer.component";
import { SafePipe } from "../../pipes/safe-html.pipe";


@NgModule({
    declarations: [ AdminComponent, AdminDashboardComponent,
                    AppSettingsComponents, UserGroupsComponent, 
                    RoutesGroupComponent, RoutesListComponent,
                     SelectionRoutesGroupComponent, NodeMailerComponent, SafePipe ],
    providers: [AdminConfigGuard],
                    
    imports: [AdminRoutingModule, CommonModule, FormsModule, AppTranslateModule, SystemConfigModule],
    exports: [AdminComponent]
})
export class AdminModule {}