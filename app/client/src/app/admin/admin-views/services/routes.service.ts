import { Injectable } from "@angular/core";
import { Http } from '@angular/http';
import {I_PopulateGroup} from '../../../../../../server/user/i_populate-group';
import 'rxjs/add/operator/toPromise';
import { I_Route } from "../../../../../../server/database/models/i_route";

@Injectable()
export class RoutesService {
    public constructor(private http: Http) {}

    public getAll(): Promise<I_Route[]> {
        return this.http.get('/api/routes-all')
        .map(response => response.json() as I_Route[])
        .toPromise<I_Route[]>();
    }
}