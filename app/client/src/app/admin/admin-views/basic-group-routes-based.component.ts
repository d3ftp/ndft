import { Component, OnInit, ViewChild } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { RoutesGroupService } from "./routes-group/routes-group.service";
import { Groups, I_GroupData } from "./groups";
import { I_RouteGroup } from "../../../../../server/database/models/i_route-group";
import { RoutesListComponent } from "./routes-list/routes-list.component";
import { I_Route } from "../../../../../server/database/models/i_route";
import { I_CheckBox } from "../../html-class/i_checkbox";

@Component({})
export abstract class BasicGroupRoutesBasedComponent<T extends {_id, routes, }> extends Groups<T>{
    @ViewChild(RoutesListComponent) routeList: RoutesListComponent;

    constructor() {
        super();
        this.groups = null;
    }

    // ********************************** ABSTRACT METHODS *******************************************************
    public abstract afterRoutesListInit();
    // ********************************** PRIVATE HANDLES METHODS ************************************************
    /**
     * handle child route list
     * @param routes 
     */
    private handleRouteList(routes: I_NgRoute[]) { 
        for(let group of this.groups) {
            group.element.routes = this.routeList.parse(group.element.routes);
        }
        if(this.groups.length > 0) {
            this.selectedItem = this.getFirst();
            this.routeList.setRouteCheckBoxToTrue(this.selectedItem.element.routes);
        }
        this.afterRoutesListInit();
    }


    private handleRouteClick(route: I_NgRoute) {
        if(route.checked) this.addRoute(route);
        else this.removeRoute(route);

        this.update();
    }
     // ********************************** PRIVATE METHODS ***********************************************************


    // ********************************** PROTECTED METHODS ***********************************************************

    /**
     * initialize groups array to start init child component
     */
    protected letTheInitChild() {
        this.groups = [];
    }

    protected addRoute(route: I_NgRoute) {
        this.selectedItem.element.routes.push(route);
    }

    protected removeRoute(route: I_NgRoute) {
        let index = this.selectedItem.element.routes.findIndex(el => el._id == route._id);
        this.selectedItem.element.routes.splice(index, 1);
    }
}

interface I_NgRoute extends I_Route, I_CheckBox {}

interface I_NgRoutesGroup extends I_RouteGroup {
    edited: boolean;
    isNew: boolean;
    oldName: string;
}

/**
 * generateEmptyGroup
 * finedPreviusSelectedElement
 * 
 */