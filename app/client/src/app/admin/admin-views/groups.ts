import { Component, OnInit, group } from "@angular/core";


import { IterableChangeRecord_ } from "@angular/core/src/change_detection/differs/default_iterable_differ";
import { I_Route } from "../../../../../server/database/models/i_route";
import { SelectControlValueAccessor } from "@angular/forms/src/directives/select_control_value_accessor";

export abstract class Groups<T extends { _id: string }> {
    public groups: I_GroupData<T>[] = [];
    public groupsToDelete: I_GroupData<T>[] = [];
    public selectedItem: I_GroupData<T> = null;

    // ********************************** PUBLIC DOM METHODS **************************************

    /**
     * Create new group
     */
    public add() {
        this.groups.push(this.getEmptyGroup());
        this.afterAdd();
    }

    /**
     * Mark item as TO_UPDATE
     */
    public update() {
        if (this.selectedItem.action != GroupAction.TO_SAVE) this.selectedItem.action = GroupAction.TO_UPDATE;
    }

    /**
     * Remove items from list and set next selected item
     */
    public delete() {
        if(this.groups.length <= 0) return;
        let selectedIndex = this.groups.findIndex(el => el == this.selectedItem);
        this.groups.splice(selectedIndex, 1);

        if (this.selectedItem.action != GroupAction.TO_SAVE) this.groupsToDelete.push(this.selectedItem);
        this.setNewSelectedItemAfterDelete(selectedIndex);
    }

    /**
     * Save groups to databse
     */
    public save() {
        for (let group of this.groups) {
            switch (group.action) {
                case GroupAction.TO_SAVE: this.onAdd(group); break;
                case GroupAction.TO_UPDATE: this.onUpdate(group); break;
            }
            group.action = GroupAction.NO_CHANGE;
        }

        for (let group of this.groupsToDelete) {
            this.onDelete(group);
        }
        this.groupsToDelete = [];
    }

    /**
     * set selected item
     */

    public select(groupElement: I_GroupData<T>) {
        this.selectedItem = groupElement;
        this.afterSelect();
    }

    // ********************************** PUBLIC METHODS *****************************************

    /**
     * return first group element
     */
    public getFirst(): I_GroupData<T> {
        return this.groups[0];
    }

    /**
     * return last group element
     */
    public getLast(): I_GroupData<T> {
        return this.groups[this.groups.length - 1];
    }

    /**
     * return next item relative to the selectedItem
     */
    public getNext(): I_GroupData<T> {
        let index = this.groups.findIndex(el => el === this.selectedItem);
        return this.groups[index + 1];
    }

    /**
     * return previous item relative to the selectedItem
     */
    public getPrevious(): I_GroupData<T> {
        let index = this.groups.findIndex(el => el === this.selectedItem);
        return this.groups[index - 1];
    }

    /**
     * Add item from db to groups class
     */
    
    public addItemFromDatabase(element: T) {
        this.groups.push({action: GroupAction.NO_CHANGE, element: element});
    }

    // ********************************** ABSTRACT METHODS **************************************

    // ************************* GET 'T' ITEM *************************
    public abstract getEmpty(): T;

    // ************************* ON SAVE METHOD ***********************
    public abstract onAdd(item: I_GroupData<T>): void;
    public abstract onUpdate(item: I_GroupData<T>): void;
    public abstract onDelete(item: I_GroupData<T>): void;
     // ************************* AFTER METHOD ***********************
    public abstract afterAdd();
    public abstract afterSelect();
    // ********************************** PRIVATE METHODS *****************************************

    private getEmptyGroup() {
        return { action: GroupAction.TO_SAVE, element: this.getEmpty() } as I_GroupData<T>
    }

    private setNewSelectedItemAfterDelete(selectedIndex: number) {
        if(selectedIndex > this.groups.length - 1) {
            this.selectedItem = this.groups[selectedIndex - 1];
        }
        else {
            this.selectedItem = this.groups[selectedIndex];
        }
     }  

}


export interface I_GroupData<T extends { _id: string }> {
    element: T;
    action: GroupAction;
}

export enum GroupAction {
    TO_SAVE, // When New
    TO_DELETE,
    TO_UPDATE,
    NO_CHANGE
}