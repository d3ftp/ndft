import { Groups, I_GroupData, GroupAction } from './groups';

interface MockData {
    _id: string;
    name: string;
    numbers: number[];
}

class MockGroup extends Groups<MockData> {
    public afterSelect() {
        
    }
    public value = false;
    public id = -1;

    public afterAdd() {
        this.value = true;
    }

    public getEmpty(): MockData {
        this.id += 1;
        return { _id: this.id.toString(), name: "New Mock", numbers: [] } as MockData;
    }
    public onAdd(item: I_GroupData<MockData>): void { }
    public onUpdate(item: I_GroupData<MockData>): void { }
    public onDelete(item: I_GroupData<MockData>): void { }
}


describe('Abstract Groups class', () => {
    var mockGroup: MockGroup;
    var LENGTH = 5;

    beforeAll(() => {
        mockGroup = new MockGroup();
    })

    //  Add Method
    it('groups should be empty ', () => {
        expect(mockGroup.groups.length).toBe(0);
    });

    it('should not call afterAdd method', () => {
        expect(mockGroup.value).toBeFalsy();
    });

    it('should add empty item to groups ', () => {
        mockGroup.add();
        mockGroup.add();
        mockGroup.add();
        mockGroup.add();
        mockGroup.add();

        expect(mockGroup.groups.length).toBe(LENGTH);
        expect(mockGroup.groups[0].action).toBe(GroupAction.TO_SAVE);
    });

    it('should call afterAdd method', () => {
        expect(mockGroup.value).toBeTruthy();
    });

    it('should return last item', () => {
        expect(mockGroup.getLast()).toBe(mockGroup.groups[mockGroup.groups.length - 1]);
    });

    it('should return first item', () => {
        expect(mockGroup.getFirst()).toBe(mockGroup.groups[0]);
    });

    // Selected Items

    it('selectedItem should be null', () => {
        expect(mockGroup.selectedItem).toBeNull();
    })

    it('selectedItem should be null', () => {
        expect(mockGroup.selectedItem).toBeNull();
    });

    it("selectedItem should has first groups item", () => {
        mockGroup.select(mockGroup.groups[0]);
        mockGroup.groups[0].element._id = "fffff";
        mockGroup.groups[1].element._id = "ddddd";

        expect(mockGroup.selectedItem).toBe(mockGroup.groups[0]);
        expect(mockGroup.selectedItem).not.toBe(mockGroup.groups[1]);
    });

    it("select method() should change selectedItem", () => {
        mockGroup.select(mockGroup.groups[1]);

        expect(mockGroup.selectedItem).toBe(mockGroup.groups[1]);
        expect(mockGroup.selectedItem).not.toBe(mockGroup.groups[0]);
    });

    it("should get previous element", () => {
        mockGroup.select(mockGroup.groups[1]);
        expect(mockGroup.getPrevious()).toBe(mockGroup.groups[0]);
    });

    it("should get next element", () => {
        mockGroup.select(mockGroup.groups[1]);
        expect(mockGroup.getNext()).toBe(mockGroup.groups[2]);
    });

    it("should dont change selected group state to 'To_UPDATE' after Update method when item is new", () => {
        mockGroup.update();

        expect(mockGroup.selectedItem).toBe(mockGroup.groups[1]);
        expect(mockGroup.selectedItem.action).not.toBe(GroupAction.TO_UPDATE);
        expect(mockGroup.groups[1].action).not.toBe(GroupAction.TO_UPDATE);
        expect(mockGroup.groups[1].action).toBe(GroupAction.TO_SAVE);
    });

    it("should change selected group state to 'To_UPDATE' after Update method", () => {
        mockGroup.select(mockGroup.groups[0]);
        expect(mockGroup.selectedItem).toBe(mockGroup.groups[0]);
        mockGroup.selectedItem.action = GroupAction.NO_CHANGE;

        mockGroup.update();

        expect(mockGroup.selectedItem.action).toBe(GroupAction.TO_UPDATE);
        expect(mockGroup.groups[0].action).toBe(GroupAction.TO_UPDATE);
        expect(mockGroup.groups[0].action).not.toBe(GroupAction.TO_SAVE);
    });

    it("should dont add deleted group to groupsToDelete variable", () => {
        mockGroup.select(mockGroup.groups[4]);
        mockGroup.delete();

        expect(mockGroup.groupsToDelete.length).toBe(0);
        expect(mockGroup.groups.length).toBe(LENGTH - 1);
    });

    it("should set prevoius element after delete", () => {
        expect(mockGroup.selectedItem).toBe(mockGroup.groups[3]);
    });

    it("should add deleted group to groupsToDelete variable", () => {
        mockGroup.select(mockGroup.groups[0]);
        mockGroup.delete();

        expect(mockGroup.groupsToDelete.length).toBe(1);
        expect(mockGroup.groups.length).toBe(LENGTH - 2);
    });

    it("should set next element after delete", () => {
        expect(mockGroup.selectedItem).toBe(mockGroup.groups[0]);
    });



    it("should respectively save objects", () => {

        let spyAdd = spyOn(mockGroup, "onAdd");
        let spyUpdate = spyOn(mockGroup, "onUpdate");
        let spyDelete = spyOn(mockGroup, "onDelete");

        mockGroup.save();

        expect(mockGroup.groupsToDelete.length).toBe(0);

        expect(mockGroup.groups[0].action).toBe(GroupAction.NO_CHANGE);
        expect(mockGroup.groups[1].action).toBe(GroupAction.NO_CHANGE);
        expect(mockGroup.groups[2].action).toBe(GroupAction.NO_CHANGE);

        expect(spyAdd).toHaveBeenCalledTimes(3);
        expect(spyUpdate).not.toHaveBeenCalled();
        expect(spyDelete).toHaveBeenCalledTimes(1);

    });

    it("should update all Objects", () => {

        let spyAdd = spyOn(mockGroup, "onAdd");
        let spyUpdate = spyOn(mockGroup, "onUpdate");
        let spyDelete = spyOn(mockGroup, "onDelete");

        mockGroup.select(mockGroup.groups[0]);
        mockGroup.update();

        mockGroup.select(mockGroup.groups[1]);
        mockGroup.update();

        mockGroup.select(mockGroup.groups[2]);
        mockGroup.update();

        mockGroup.save();

        expect(spyAdd).not.toHaveBeenCalled();
        expect(spyUpdate).toHaveBeenCalledTimes(3);
        expect(spyDelete).not.toHaveBeenCalled();

    });


    it('should add existing item from db', () => {
        mockGroup.addItemFromDatabase({_id: "11111111", name:"Test", numbers:[]});
        expect(mockGroup.getLast().element._id).toBe("11111111");
    });
})
