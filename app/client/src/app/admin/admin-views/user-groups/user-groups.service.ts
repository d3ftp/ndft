import { Injectable } from "@angular/core";
import { Http, Headers } from '@angular/http';
import { I_PopulateGroup } from '../../../../../../server/user/i_populate-group';
import 'rxjs/add/operator/toPromise';
import { I_UserGroup } from "../../../../../../server/database/models/i_user-group";

@Injectable()
export class UserGroupService {
    public constructor(private http: Http) { }

    private headers = new Headers();
    public selectedItem: I_UserGroup;

    public getAllGroup(): Promise<I_UserGroup[]> {
        return this.http.get('/api/group')
            .map(response => response.json() as I_UserGroup[])
            .toPromise<I_UserGroup[]>();
    }

    public update(group: I_UserGroup, newName:string) {
        return this.http.put("/api/group",
            JSON.stringify({id: group._id, newName: newName, routes: group.routes}),
            {headers: new Headers({ 'Content-type': 'application/json' })}
        )
        .map(reponse => reponse.json())
        .toPromise();
    }

    public add(group: I_UserGroup) {
        return this.http.post('/api/group', {name: group.name, routes: group.routes}, {
            headers: new Headers({ 'Content-type': 'application/json' })
        }).
        map(val => val.json())
        .toPromise();
    }

    public delete(id: string) {
        return this.http.delete('/api/group/' + id, {
            headers: new Headers({ 'Content-type': 'application/json' })
        }).
        map(val => val.json())
        .toPromise();
    }

}