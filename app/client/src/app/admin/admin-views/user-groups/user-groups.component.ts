import { Component, OnInit, ViewChild, group } from "@angular/core";
import { UserGroupService } from "./user-groups.service";
import { I_PopulateGroup } from "../../../../../../server/user/i_populate-group";
import { I_Route } from "../../../../../../server/database/models/i_route";
import { RemoveFromArray } from "../../../src/helpers/remove-from-array";
import { IterableChangeRecord_ } from "@angular/core/src/change_detection/differs/default_iterable_differ";
import { Groups, I_GroupData, GroupAction } from "../groups";
import { I_UserGroup } from "../../../../../../server/database/models/i_user-group";
import { I_NgRoute } from "../routes-list/i-ng-route";
import { TranslateService } from "@ngx-translate/core";
import { RoutesListComponent } from "../routes-list/routes-list.component";
import { BasicGroupRoutesBasedComponent } from "../basic-group-routes-based.component";
import { I_RouteGroup } from "../../../../../../server/database/models/i_route-group";
import { RoutesGroupService } from "../routes-group/routes-group.service";
import { I_CheckBox } from "../../../html-class/i_checkbox";
import { SelectionRoutesGroupComponent } from "../routes-group/selection-routes-group.component";

@Component({
    templateUrl: "./user-groups.component.html",
    styleUrls: ["./user-groups.component.css"],
    providers: [UserGroupService, TranslateService, RoutesGroupService]
})
export class UserGroupsComponent extends BasicGroupRoutesBasedComponent<I_UserGroup> implements OnInit {
    @ViewChild(RoutesListComponent) routeList: RoutesListComponent;
    @ViewChild(SelectionRoutesGroupComponent) selectionRouteGroup: SelectionRoutesGroupComponent;

    private routeGroups: I_NgRouteGroup[];
    private isChildReady = false;

    constructor(private userGroupService: UserGroupService, private routeGroupsService: RoutesGroupService, private translate: TranslateService) {
        super();
    }


    // ********************************** Groups<T> INTHERITED METHODS **************************************

    public afterRoutesListInit() {
        this.isChildReady = true;
        this.initializeRouteGroups();
        console.log(this.selectedItem);
    }

    public getEmpty(): I_UserGroup {
        return { _id: "", name: "", routes: [] } as I_UserGroup;
    }

    public onAdd(item: I_GroupData<I_UserGroup>): void {
        this.userGroupService.add(item.element).catch(err => {
            console.error(err);
        })
    }

    public onUpdate(item: I_GroupData<I_UserGroup>): void {
        this.userGroupService.update(item.element, item.element.name).catch(err => {
            console.error(err);
        })
    }

    public onDelete(item: I_GroupData<I_UserGroup>): void {
        this.userGroupService.delete(item.element._id).catch(err => {
            console.error(err);
        })
    }

    public afterAdd() {
        this.selectedItem = this.getLast();
        this.translate.get("AdminPanel.UserGroups.New").toPromise().then((val) => {
            this.selectedItem.element.name = val;
        })
        this.routeList.setAllRouteCheckBoxTo(false);
    }

    public afterSelect() {
        this.routeList.setAllRouteCheckBoxTo(false);
        this.routeList.setRouteCheckBoxToTrue(this.selectedItem.element.routes)

        this.unSelectAllRouteGroups();
        this.userGroupService.selectedItem = this.selectedItem.element;
        this.selectionRouteGroup.setCheckedGroups();
    }

    // ********************************** ANGULAR INTHERITED METHODS **************************************

    ngOnInit(): void {
        this.selectedItem = { action: GroupAction.NO_CHANGE, element: null };
        this.selectedItem.element = this.getEmpty();
        this.userGroupService.getAllGroup()
            .then(groups => {
                this.letTheInitChild();
                for (let group of groups) {
                    this.groups.push({ action: GroupAction.NO_CHANGE, element: group });
                }
                if(this.groups.length > 0) this.userGroupService.selectedItem = this.getFirst().element;
            })
            .catch(err => {
                console.error(err);
            })
    }


    // // ********************************** PRIVATE DOM METHODS ************************************************

    private onSelectGroupRoute(group: I_NgRouteGroup) {
        group.checked = !group.checked;
        if (group.checked) {
            for (let route of group.routes) {
                this.addRouteIfNotExist(route);
            }
        }
        else
            for (let route of group.routes) {
                this.removeRoute(route);
                route.checked = false;
            }

        this.update();
    }

    private handleSelectGroupRoute(routeGroup: I_RouteGroup) {
        for(let route of routeGroup.routes) {
            let ngRoute: I_NgRoute = this.routeList.getInstanceOf(route);
            ngRoute.checked = true;

            this.addRouteIfNotExist(ngRoute)
        }
        this.update();
    }

    private handleUnselectGroupRoute(routeGroup: I_RouteGroup) {
        for(let route of routeGroup.routes) {
            let ngRoute: I_NgRoute = this.routeList.getInstanceOf(route);
            ngRoute.checked = false;

            this.removeRoute(ngRoute)
        }
        this.update();
    }

    //  // ********************************** PRIVATE METHODS *******************************************************

    private initializeRouteGroups() {
        this.routeGroupsService.getAllGroup(true).then(result => {
            this.routeGroups = result.data;
            for (let route of this.routeGroups) {
                route.routes = this.routeList.parse(route.routes);
            }
        }).catch(err => {
            console.error(err);
        })
    }

    private addRouteIfNotExist(route: I_NgRoute) {
        let index = this.selectedItem.element.routes.findIndex(el => el == route);
        if(index < 0) {
            this.addRoute(route);
            route.checked = true;
        }
    }

    private unSelectAllRouteGroups() {
        for( let group of this.routeGroups) {
            group.checked = false;
        }
    }

    public save() {
        super.save();
        this.selectionRouteGroup.save();
    }

}

interface I_NgRouteGroup extends I_CheckBox, I_RouteGroup { routes: I_NgRoute[] };

