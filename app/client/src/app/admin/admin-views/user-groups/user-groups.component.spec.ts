import { DebugElement, Component, Output, EventEmitter } from "@angular/core";
import { ComponentFixture } from "@angular/core/testing";
import { async } from "@angular/core/testing";
import { TestBed } from "@angular/core/testing";
import { TranslateLoader, TranslateModule, TranslateService } from "@ngx-translate/core";
import { Observable } from "rxjs/Observable";
import { FormsModule } from "@angular/forms";
import { By } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { UserGroupsComponent } from "./user-groups.component";
import { UserService } from "../../../user/user.service";
import { RoutesListComponent } from "../routes-list/routes-list.component";
import { UserGroupService } from "./user-groups.service";
import { RoutesGroupService } from "../routes-group/routes-group.service";
import { I_UserGroup } from "../../../../../../server/database/models/i_user-group";
import { I_Result } from "../../../../../../server/routes/i_result";
import { I_RouteGroup } from "../../../../../../server/database/models/i_route-group";

let translations: any = {
    "Auth": {
        "SignIn": "Sign In",
        "Email": "Email address",
        "Password": "Password",
        "RememberMe": "Remember me",
        "LogIn": "Log In",
        "ErrorSignIn": "<strong>Warning!</strong> Wrong email address or password",
        "FB": "Sing In with Facebook"
    }
}


class FakeLoader implements TranslateLoader {
    getTranslation(lang: string): Observable<any> {
        return Observable.of(translations);
    }

}


let userGroups: I_UserGroup[] = [
    { _id: '1', name: 't1', routes: [] },
    { _id: '2', name: 't2', routes: [] },
    { _id: '3', name: 't3', routes: [] },
    { _id: '4', name: 't4', routes: [] },
    { _id: '5', name: 't5', routes: [] },
]

class UserGroupServiceMock {
    public selectedItem: I_UserGroup;

    public getAllGroup(): Promise<I_UserGroup[]> {
        return Promise.resolve(userGroups);
    }

    public update(group: I_UserGroup, newName: string) {
        return Promise.resolve();
    }

    public add(group: I_UserGroup) {
        return Promise.resolve();
    }

    public delete(id: string) {
        return Promise.resolve();
    }

}

let routesGroup: I_RouteGroup[] = [
    { _id: '1', name: 't1', routes: [], userGroups: [] },
    { _id: '2', name: 't2', routes: [], userGroups: [] },
    { _id: '3', name: 't3', routes: [], userGroups: [] },
    { _id: '4', name: 't4', routes: [], userGroups: [] },
    { _id: '5', name: 't5', routes: [], userGroups: [] },
]

class RoutesGroupServiceMock {
    public getAllGroup(populate: boolean): Promise<I_Result> {
        return Promise.resolve({ complete: true, data: routesGroup } as I_Result);
    }

    public add(populateGroup: I_RouteGroup) {
        return Promise.resolve(null);
    }

    public update(populateGroup: I_RouteGroup, newName: string) {
        return Promise.resolve(null);
    }

    public updateUserGroups(id: string, groupId) {
        return Promise.resolve(null);
    }

    public delete(id: string) {
        return Promise.resolve(null);
    }
}

@Component({
    selector: 'routes-list',
    template: ''
})
class RouteListStub {
    @Output() routeList = new EventEmitter();

    public parse(value) {
        return value
    };

    public getInstanceOf(value) {
        return value;
    };

    setRouteCheckBoxToTrue = jasmine.createSpy('setRouteCheckBoxToTrue');
    setAllRouteCheckBoxTo = jasmine.createSpy('setAllRouteCheckBoxTo');
}

@Component({
    selector: 'SelectionRoutes',
    template: ''
})
class SelectionRouteStub {
    @Output() selectGroupRoute = new EventEmitter();
    @Output() unselectGroupRoute = new EventEmitter();

    setCheckedGroups = jasmine.createSpy('setCheckedGroups');
    save = jasmine.createSpy('save');
}

xdescribe('User Groups', () => {
    let comp: UserGroupsComponent;
    let fixture: ComponentFixture<UserGroupsComponent>


    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [UserGroupsComponent, SelectionRouteStub, RouteListStub],
            imports: [
                FormsModule,
                TranslateModule.forRoot({
                    loader: { provide: TranslateLoader, useClass: FakeLoader }
                })
            ],
            providers: [UserGroupService, UserGroupService]
        }).overrideComponent(UserGroupsComponent, {
            set: {
                providers: [
                    { provide: UserGroupService, useClass: UserGroupServiceMock },
                    { provide: RoutesGroupService, useClass: RoutesGroupServiceMock }
                ]
            }
        })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(UserGroupsComponent);
                comp = fixture.componentInstance;

                let selectionRoute = TestBed.createComponent(SelectionRouteStub);
                comp.selectionRouteGroup = selectionRoute.componentInstance as any;

                let routeList = TestBed.createComponent(RouteListStub);
                comp.routeList = routeList.componentInstance as any;
            });
    }));

    it('component should be defined', () => {
        expect(comp).toBeDefined();
    });
})