import { DebugElement } from "@angular/core";
import { ComponentFixture } from "@angular/core/testing";
import { async } from "@angular/core/testing";
import { TestBed } from "@angular/core/testing";
import { TranslateLoader, TranslateModule, TranslateService } from "@ngx-translate/core";
import { Observable } from "rxjs/Observable";
import { FormsModule } from "@angular/forms";
import { By } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { RoutesListComponent } from "./routes-list.component";
import { RoutesService } from "../services/routes.service";
import { I_Route } from "../../../../../../server/database/models/i_route";


let translations: any = {
    "AdminPanel": {
        "UserGroups": {
            "NoGroup": "No Group",
            "Name": "Name",
            "New": "New Group"
        },
        "RouteGroups": {
            "NoGroup": "No Group",
            "Name": "Name",
            "New": "New Group"
        },
        "Routes": {
            "Route": "Route"
        }
    }
}

let sampleRoutesData: I_Route[] = [
    {_id: "1", method: "post", path: "/api/test/1"},
    {_id: "2", method: "post", path: "/api/test/2"},
    {_id: "3", method: "get", path: "/api/test/2"},
    {_id: "4", method: "post", path: "/api/test/3"},
    {_id: "5", method: "delete", path: "/api/test/4"},
    {_id: "6", method: "get", path: "/api/test/5"},
    {_id: "7", method: "post", path: "/api/test/6"},
    {_id: "8", method: "delete", path: "/api/test/7"},
    {_id: "9", method: "put", path: "/api/test/8"},
]

let instanceToSearch:  I_Route[] = [
    {_id: "1", method: "post", path: "/api/test/1"},
    {_id: "2", method: "post", path: "/api/test/2"},
    {_id: "3", method: "get", path: "/api/test/2"},
]

class FakeLoader implements TranslateLoader {
    getTranslation(lang: string): Observable<any> {
        return Observable.of(translations);
    }

}

class RouteServiceStub {
    public getAll(): Promise<I_Route[]> {
        return new Promise(resolve => {
            resolve(sampleRoutesData);
        });
    }
}

describe("routes list: ", () => {
    let comp: RoutesListComponent;
    let fixture: ComponentFixture<RoutesListComponent>;
    let de: DebugElement;
    let el: HTMLElement;
    let routesService: RoutesService;
    let routerStub;
    let translate;

    beforeEach(async(() => {
        routerStub = {
            navigateByUrl: jasmine.createSpy('navigateByUrl')
        }
        TestBed.configureTestingModule({
            declarations: [RoutesListComponent],
            imports: [
                FormsModule,
                TranslateModule.forRoot({
                    loader: { provide: TranslateLoader, useClass: FakeLoader }
                })
            ]
        }).overrideComponent(RoutesListComponent, {
            set: {
                providers: [
                    {provide: RoutesService, useClass: RouteServiceStub}
                ]
            }
        }).compileComponents().then(() => {
            fixture = TestBed.createComponent(RoutesListComponent);
            routesService = fixture.debugElement.injector.get(RoutesService, RouteServiceStub);

            translate = TestBed.get(TranslateService);

            translate.setDefaultLang('en');
            comp = fixture.componentInstance;
        });
    }));

    it('fixture should be defined', () => {
        expect(fixture).toBeDefined();
    });

    it('routes service should be defined', () => {
        expect(routesService).toBeDefined();
    });

    it('should be defined', () => {
        expect(TranslateService).toBeDefined();
    });

    it('should translate text placeholder', () => {
        fixture.detectChanges();
        expect(fixture.debugElement.nativeElement.querySelector('h4').textContent).toEqual(translations.AdminPanel.Routes["Route"]);
    });

    it('should return routes', () => {
        fixture.detectChanges();
        //expect()
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            let nativeElement = fixture.debugElement.nativeElement.querySelector('div.row').textContent;
            expect(nativeElement).toContain("/api/test/1 - post");
            expect(nativeElement).toContain("/api/test/2 - post");
            expect(nativeElement).toContain("/api/test/2 - get");
            expect(nativeElement).toContain("/api/test/3 - post");
            expect(nativeElement).toContain("/api/test/4 - delete");
            expect(nativeElement).toContain("/api/test/8 - put");

            expect(nativeElement).not.toContain("/api/test/9 - post");
        });
    });

    it('should set all routes check to true', () => {
        fixture.detectChanges();

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            comp.setAllRouteCheckBoxTo(true);
            for(let route of comp.routes) {
                expect(route.checked).toBeTruthy();
            }
        })
    })

    it('should return routes instance', () => {
        fixture.detectChanges();

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            let routesInstance = comp.parse(instanceToSearch);

            expect(comp.routes[0]).toBe(routesInstance[0]);
            expect(comp.routes[1]).toBe(routesInstance[1]);
            expect(comp.routes[2]).toBe(routesInstance[2]);

        })
    })

    it('should return single route instance', () => {
        fixture.detectChanges();

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            let routesInstance = comp.getInstanceOf({_id: "9", method: "put", path: "/api/test/8"});

            expect(comp.routes[8]).toBe(routesInstance);
        })
    })

    it('should emit value after click', () => {
        fixture.detectChanges();

        fixture.whenStable().then(() => {
            let spy = spyOn(comp.routeClick, "emit");

            fixture.detectChanges();
            fixture.debugElement.queryAll(By.css("label.form-check-label"))[0].nativeElement.click() 
            expect(spy).toHaveBeenCalledWith(comp.routes[0]);

        })
    })

    
    it('should set check to true next set check to false after click', () => {
        fixture.detectChanges();

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            expect(comp.routes[0].checked).toBeFalsy();

            fixture.debugElement.queryAll(By.css("label.form-check-label"))[0].nativeElement.click() 
            expect(comp.routes[0].checked).toBeTruthy();
            fixture.debugElement.queryAll(By.css("label.form-check-label"))[0].nativeElement.click() 
            expect(comp.routes[0].checked).toBeFalsy();

        })
    })

    

});