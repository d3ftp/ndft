import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { I_PopulateGroup } from "../../../../../../server/user/i_populate-group";
import { RoutesService } from "../services/routes.service";
import { I_NgRoute } from "./i-ng-route";
import { I_Route } from "../../../../../../server/database/models/i_route";

@Component({
    selector: 'routes-list',
    templateUrl: "./routes-list.component.html",
    styleUrls: ["./routes-list.component.css"],
    providers: [RoutesService]
})
export class RoutesListComponent implements OnInit {
    @Output() routeList = new EventEmitter();
    @Output() routeClick = new EventEmitter();

    public routes: I_NgRoute[];
    public rows: number[];
    public itemsPerRow = 4;

    constructor(private routesService: RoutesService) { }

    // ********************************** ANGULAR INTHERITED METHODS **************************************

    ngOnInit(): void {
        this.getAllRoutes();
    }

    // ********************************** PUBLIC METHODS ************************************************

    /**
     * set state to all routes
     */
    public setAllRouteCheckBoxTo(value: boolean) {
        for (let route of this.routes) {
            route.checked = value;
        }
    }

    public setRouteCheckBoxToTrue(i_routes: I_Route[]) {
        for (let route of i_routes as I_NgRoute[]) {
            console.log(route);
            route.checked = true;
        }
    }

    public parse(i_routes: I_Route[]) {
        let routes: I_NgRoute[] = [];
        for (let i_route of i_routes)
            for (let route of this.routes) {
                if (i_route._id == route._id) {
                    routes.push(route);
                    break;
                }
            }
        return routes;
    }

    public getInstanceOf(element: I_Route) {
        for(let route of this.routes) {
            if (element._id == route._id) {
                return route;
            }
        }
    }
    // ********************************** PRIVATE METHODS ************************************************

    /**
     * return all routes in system
     */
    private getAllRoutes() {
        this.routesService.getAll().then(routes => {
            this.routes = routes as I_NgRoute[];
            this.rows = Array.from(Array(Math.ceil(this.routes.length / this.itemsPerRow)).keys());
            this.routes = this.routes.sort((a, b) => {
                if (a.path < b.path) return -1;
                else if (a.path > b.path) return 1;
                return 0;
            });
            this.setAllRouteCheckBoxTo(false);
            this.routeList.emit(this.routes);
        });
    }

    private onSelectCheckBox(route: I_NgRoute) {
        route.checked = !route.checked;
        this.routeClick.emit(route);
    }

    public reset() {
        for(let route of this.routes) {
            route.checked = false;
        }
    }

}
