import { I_Route } from "../../../../../../server/database/models/i_route";

export interface I_NgRoute extends I_Route {
    checked: boolean;
}