import { Component } from '@angular/core'
import { I_SaveNotification } from '../../../src/notification/i_save-notification';
import { ConfigWebsitePart } from './parts/config-website-part';
import { SystemConfigService } from './system-config.service';

@Component({
    templateUrl: 'system-config.component.html',
    styleUrls: ['system-config.component.css'],
    providers: [SystemConfigService]
})
export class SystemConfigComponent  {
   private partsQuery: ConfigWebsitePart[] = [];

    onSave() {
        this.partsQuery.forEach(el => {
            if (el.isModified()) 
                el.save();
        })
    }


 public addChild(child: ConfigWebsitePart) {
     this.partsQuery.push(child);
 }

}