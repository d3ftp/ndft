import { DebugElement, Component, Output, EventEmitter, QueryList } from "@angular/core";
import { ComponentFixture } from "@angular/core/testing";
import { async } from "@angular/core/testing";
import { TestBed } from "@angular/core/testing";
import { TranslateLoader, TranslateModule, TranslateService } from "@ngx-translate/core";
import { Observable } from "rxjs/Observable";
import { FormsModule } from "@angular/forms";
import { By } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { SystemConfigComponent } from "./system-config.component";
import { MainWebsiteConfigComponent } from "./parts/main-website-config/main-website-config.component";
import { ConfigWebsitePart } from "./parts/config-website-part";

let translations: any = {

}

class FakeLoader implements TranslateLoader {
    getTranslation(lang: string): Observable<any> {
        return Observable.of(translations);
    }

}

class PartWebsite extends ConfigWebsitePart {
    public save() {
        console.log('ss');
    }

    public setModifyFlagToTrue() {
        super.setModifyFlagToTrue();
    }
    
}

@Component({
    selector: 'main-website-config',
    template: ''
})
class MainWebsiteConfigMock {

}

@Component({
    selector: 'blocked-mail-domain',
    template: ''
})
class BlockedConfigMock {

}


@Component({
    selector: 'nodemailer-config-component',
    template: ''
})
class NodemailerConfigComponent {

}



@Component({
    selector: 'register-option-config',
    template: ''
})
class RegisterConfigComponentMock {

}

describe('System Config Component', () => {
    let comp: SystemConfigComponent;
    let fixture: ComponentFixture<SystemConfigComponent>

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SystemConfigComponent, MainWebsiteConfigMock, RegisterConfigComponentMock, BlockedConfigMock, NodemailerConfigComponent],
            imports: [
                FormsModule,
                TranslateModule.forRoot({
                    loader: { provide: TranslateLoader, useClass: FakeLoader }
                })
            ]
        }).compileComponents()
        .then(() => {
            fixture = TestBed.createComponent(SystemConfigComponent);
            comp = fixture.componentInstance;
        })
    }));

    it('component should be defined', () => {
        expect(comp).toBeDefined();
    });

    it('should contains main website component', () => {
        fixture.detectChanges();
        expect(fixture.debugElement.injector.get(MainWebsiteConfigComponent, MainWebsiteConfigMock)).toBeDefined();
    })

    it('should call onSave method', () => {
        let spy = spyOn(comp, 'onSave')
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            let element = fixture.debugElement.query(By.css('#save_btn')).nativeElement;
            element.click();
            fixture.whenStable().then(() => {
                fixture.detectChanges();
            });
        });
    });

    
})