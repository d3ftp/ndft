import { DebugElement, Component, Output, EventEmitter } from "@angular/core";
import { ComponentFixture } from "@angular/core/testing";
import { async } from "@angular/core/testing";
import { TestBed } from "@angular/core/testing";
import { TranslateLoader, TranslateModule, TranslateService } from "@ngx-translate/core";
import { Observable } from "rxjs/Observable";
import { FormsModule } from "@angular/forms";
import { By } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { NodemailerConfigComponent } from "./nodemailer-config.component";
import { SystemConfigService } from "../../system-config.service";

let translations: any = {

}

class FakeLoader implements TranslateLoader {
    getTranslation(lang: string): Observable<any> {
        return Observable.of(translations);
    }

}

class SystemConfigServiceMock {
    getNodemailerConfig() {
        return Promise.resolve(sampleConfig);
    }

    setNodemailerConfig() { }
}

let sampleConfig = {
    from: 'test',
    service: 'testService',
    auth: {
        user: 'tester',
        pass: 'tetet'
    }
}

describe('Nodemailer config', () => {
    let comp: NodemailerConfigComponent;
    let fixture: ComponentFixture<NodemailerConfigComponent>

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [NodemailerConfigComponent],
            providers: [SystemConfigService],
            imports: [
                FormsModule,
                TranslateModule.forRoot({
                    loader: { provide: TranslateLoader, useClass: FakeLoader }
                })
            ]
        })
            .overrideComponent(NodemailerConfigComponent, {
                set: {
                    providers: [
                        { provide: SystemConfigService, useClass: SystemConfigServiceMock }
                    ]
                }
            })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(NodemailerConfigComponent);
                comp = fixture.componentInstance;
            })
    }));

    it('component should be defined', () => {
        expect(comp).toBeDefined();
    });

    it('should init config', () => {
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            expect(comp.getFrom()).toBeDefined();
            expect(comp.getPassword()).toBeDefined();
            expect(comp.getService()).toBeDefined();
            expect(comp.getUserName()).toBeDefined();
        });
    });

    it('should set modify flag to true after change name input', (done) => {
        let value = 'test_123';
        let spy = spyOn(comp, 'onInput').and.callThrough();
        inputValueToFieldZone(value, '#nodemailer-user', fixture).then(() => {
            expect(comp.isModified()).toBeTruthy();
            expect(spy).toHaveBeenCalled();
            done();
        });
    });

    it('should set modify flag to true after change password input', (done) => {
        let value = 'test_123xxx';
        let spy = spyOn(comp, 'onInput').and.callThrough();
        inputValueToFieldZone(value, '#nodemailer-password', fixture).then(() => {
            expect(comp.isModified()).toBeTruthy();
            expect(spy).toHaveBeenCalled();
            done();
        });
    });

    it('should set modify flag to true after change from input', (done) => {
        let value = 'from-test';
        let spy = spyOn(comp, 'onInput').and.callThrough();
        inputValueToFieldZone(value, '#nodemailer-from', fixture).then(() => {
            expect(comp.isModified()).toBeTruthy();
            expect(spy).toHaveBeenCalled();
            done();
        });
    });

    it('should set modify flag to true after change service input', (done) => {
        let value = 'service-test';
        let spy = spyOn(comp, 'onInput').and.callThrough();
        inputValueToFieldZone(value, '#nodemailer-service', fixture).then(() => {
            expect(comp.isModified()).toBeTruthy();
            expect(spy).toHaveBeenCalled();
            done();
        });
    });

    it('should change input value', () => {
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            let el1 = inputValueToField('1', '#nodemailer-user', fixture);
            let el2 = inputValueToField('2', '#nodemailer-password', fixture);
            let el3 = inputValueToField('3', '#nodemailer-from', fixture);
            let el4 = inputValueToField('4', '#nodemailer-service', fixture);
            fixture.detectChanges();
            fixture.whenStable().then(() => {
                expect(el1.value).toEqual('1');
                expect(comp.getUserName()).toEqual('1');

                expect(comp.getPassword()).toEqual('2');
                expect(el2.value).toEqual('2');

                expect(comp.getFrom()).toEqual('3');
                expect(el3.value).toEqual('3');

                expect(comp.getService()).toEqual('4');
                expect(el4.value).toEqual('4');
            }); 
        });
    });
})

function inputValueToFieldZone(value, css, fixture) {
    return new Promise(resolve => {
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            inputValueToField(value, css, fixture);
            fixture.detectChanges();
    
            fixture.whenStable().then(() => {
                fixture.detectChanges();
                resolve();
            });
        });
    })
}

function inputValueToField(value, css, fixture) {
    let element = fixture.debugElement.query(By.css(css)).nativeElement;
    element.value = value;
    element.dispatchEvent(new Event('input'));
    return element;
}

