import { Component, OnInit, Host, Optional } from '@angular/core';
import { ConfigWebsitePart } from "../config-website-part";
import { SystemConfigService } from '../../system-config.service';
import { SystemConfigComponent } from '../../system-config.component';

@Component({
    selector: 'nodemailer-config-component',
    templateUrl: 'nodemailer-config.component.html',
    styleUrls: ['nodemailer-config.component.css']
})
export class NodemailerConfigComponent extends ConfigWebsitePart implements OnInit {

    private userName: string;
    private password: string;
    private from: string;
    private service: string;

    constructor(private systemConfigService: SystemConfigService, @Optional() @Host() public parent: SystemConfigComponent) {
        super();
        this.setParentIfDefined(parent);
    }

    ngOnInit() {
        this.systemConfigService.getNodemailerConfig().then(cfg => {
            this.userName = cfg.auth.user;
            this.password = cfg.auth.pass;

            this.from = cfg.from;
            this.service = cfg.service;
        })
    }

    save() {
        this.systemConfigService.setNodemailerConfig(this.getUserName(), this.getPassword(), this.getFrom(), this.getService());
    }


    public getUserName() {
        return this.userName;
    }

    public getPassword() {
        return this.password;
    }

    public getFrom() {
        return this.from;
    }

    public getService() {
        return this.service;
    }


    public onInput() {
        this.setModifyFlagToTrue();
    }

} 