import { DebugElement, Component, Output, EventEmitter } from "@angular/core";
import { ComponentFixture } from "@angular/core/testing";
import { async } from "@angular/core/testing";
import { TestBed } from "@angular/core/testing";
import { TranslateLoader, TranslateModule, TranslateService } from "@ngx-translate/core";
import { Observable } from "rxjs/Observable";
import { FormsModule } from "@angular/forms";
import { By } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MainWebsiteConfigComponent } from "./main-website-config.component";
import { SystemConfigService } from "../../system-config.service";

let translations: any = {

}

class FakeLoader implements TranslateLoader {
    getTranslation(lang: string): Observable<any> {
        return Observable.of(translations);
    }
}


class SystemConfigServiceStub {
    setWebsiteTitle = jasmine.createSpy('setWebsiteTitle');
    getWebsiteTitle() {
        return Promise.resolve('Test Title');
    }
}


describe('main website component', () => {
    let comp: MainWebsiteConfigComponent;
    let fixture: ComponentFixture<MainWebsiteConfigComponent>

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [MainWebsiteConfigComponent],
            imports: [
                FormsModule,
                TranslateModule.forRoot({
                    loader: { provide: TranslateLoader, useClass: FakeLoader }
                })
            ]
        })
            .overrideComponent(MainWebsiteConfigComponent, {
                set: {
                    providers: [
                        { provide: SystemConfigService, useClass: SystemConfigServiceStub }
                    ]
                }
            })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(MainWebsiteConfigComponent);
                comp = fixture.componentInstance;
            })
    }));

    it('component should be defined', () => {
        fixture.detectChanges();
        expect(comp).toBeDefined();
    });

    it('should inherited ConfigWebsitePart', () => {
        expect(comp.save).toBeDefined();
    });

    describe('change title', () => {
        let newValue = 'New Test Title';
        let spy;

        it('isModified flag should be false', () => {
            expect(comp.isModified()).toBeFalsy();
        });

        it('should change website title', () => {
            fixture.detectChanges();
            fixture.whenStable().then(() => {
                let input = fixture.debugElement.query(By.css('#website-title'));
                let el = input.nativeElement;
                spy = spyOn(comp, 'onInputChange').and.callThrough();

                el.value = newValue;
                el.dispatchEvent(new Event('input'));

                fixture.detectChanges();
                fixture.whenStable().then(() => {
                    expect(el.value).toEqual(newValue);
                    expect(comp.websiteTitle).toEqual(newValue);
                    expect(spy).toHaveBeenCalled();
                    expect(comp.isModified()).toBeTruthy();
                });
            });
        });

    })

})