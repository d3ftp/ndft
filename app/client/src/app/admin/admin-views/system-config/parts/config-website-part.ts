import { I_SaveNotification } from "../../../../src/notification/i_save-notification";

export abstract class ConfigWebsitePart {
    protected _isModified = false;

    abstract save();
    
    public isModified(): boolean {
        return this._isModified;
    }

    protected setModifyFlagToTrue() {
        this._isModified = true;
    }

    protected setParentIfDefined(parent: { addChild(child: ConfigWebsitePart) }) {
        if (parent !=  undefined)
            parent.addChild(this);
    }
}