import { Component, OnInit, Host, Optional } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ConfigWebsitePart } from '../config-website-part';
import { SystemConfigService } from '../../system-config.service';
import { SystemConfigComponent } from '../../system-config.component';

@Component({
    selector: 'main-website-config',
    templateUrl: 'main-website-config.component.html',
    styleUrls: ['main-website-config.component.css'],
    providers: [TranslateService]
})
export class MainWebsiteConfigComponent extends ConfigWebsitePart implements OnInit {
    public websiteTitle: string = '';

    constructor(private systemConfigService: SystemConfigService, @Optional() @Host() public parent: SystemConfigComponent) {
        super();
        this.setParentIfDefined(parent);
    }

    save() {
        this.systemConfigService.setWebsiteTitle(this.websiteTitle).catch(err => {
            console.error(err);
        })
    }

    ngOnInit() {
        this.systemConfigService.getWebsiteTitle()
            .then(title => {
                this.websiteTitle = title;
            })
    }

    public onInputChange() {
        this.setModifyFlagToTrue();
    }

}