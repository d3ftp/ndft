import { DebugElement, Component, Output, EventEmitter, group } from "@angular/core";
import { ComponentFixture } from "@angular/core/testing";
import { async } from "@angular/core/testing";
import { TestBed } from "@angular/core/testing";
import { TranslateLoader, TranslateModule, TranslateService } from "@ngx-translate/core";
import { Observable } from "rxjs/Observable";
import { FormsModule } from "@angular/forms";
import { By } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { BlockedMailDomain } from "./blocked-mail-domain.component";
import { SystemConfigService } from "../../system-config.service";

let translations: any = {

}

class FakeLoader implements TranslateLoader {
    getTranslation(lang: string): Observable<any> {
        return Observable.of(translations);
    }

}

let blockedDomains = [
    'mail.ru',
    'test.mail.com',
    'spam.eu',
    '123mail.pl',
    'ooo.pl',
    'abv.kr'
]
class SystemConfigServiceMock {
    public setBlockedDomains(blockedDomainsArray: string[]) { }

    public getBlockedDomains() {
        return Promise.resolve(blockedDomains)
    }
}

describe('Blocked Main Domain Component', () => {
    let comp: BlockedMailDomain;
    let fixture: ComponentFixture<BlockedMailDomain>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [BlockedMailDomain],
            imports: [FormsModule,
                TranslateModule.forRoot({
                    loader: { provide: TranslateLoader, useClass: FakeLoader }
                })],
            providers: [SystemConfigService]
        })
            .overrideComponent(BlockedMailDomain, {
                set: {
                    providers: [
                        { provide: SystemConfigService, useClass: SystemConfigServiceMock }
                    ]
                }
            })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(BlockedMailDomain);
                comp = fixture.componentInstance;
            })
    }));

    it('should definec comp', () => {
        fixture.detectChanges();
        expect(comp).toBeDefined();
    })

    it('should load item', (done) => {
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            let originalArray = JSON.stringify(blockedDomains);
            let expectedArray;
            let tmpArray = []

            for(let domain of comp.getBlockedEmailDomains()) {
                tmpArray.push(domain.name);
            }
            expectedArray = JSON.stringify(tmpArray);
            
            expect(originalArray).toEqual(expectedArray);
            done();
        });
    });

    it('should set selected domain', (done) => {
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            expect(comp.getSelectedDomain().name).toBe(blockedDomains[0]);
            done();
        })
    });

    it('should change selected item after click', (done) => {
        let selectedIndex = 3;
        let spy = spyOn(comp, 'onSelectGroup').and.callThrough();

        fixture.detectChanges();
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            let items = fixture.debugElement.queryAll(By.css('li.list-group-item'));
            items[selectedIndex].nativeElement.click();
            fixture.detectChanges();

            fixture.whenStable().then(() => {
                fixture.detectChanges();

                let inputElement = fixture.debugElement.query(By.css('input.form-control')).nativeElement;

                expect(inputElement.value).toEqual(comp.getSelectedDomain().name);

                expect(spy).toHaveBeenCalled();
                expect(comp.getSelectedDomain().name).toBe(blockedDomains[selectedIndex]);
                done();
            })
        })
    });


    it('should change itemn name and call onUpdateValue', (done) => {
        let newValue = 'test.com';
        let spy = spyOn(comp, 'onUpdateValue').and.callThrough();

        fixture.detectChanges();
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            let inputElement = fixture.debugElement.query(By.css('input.form-control')).nativeElement;
            inputElement.value = newValue;
            inputElement.dispatchEvent(new Event('input'));
            
            fixture.detectChanges();
            fixture.whenStable().then(() => {
                fixture.detectChanges();
                expect(comp.isModified()).toBeTruthy();
                expect(comp.getSelectedDomain().name).toEqual(newValue);
                expect(comp.getBlockedEmailDomains()[0].name).toEqual(newValue);
                done(); 
            })
        })
    });

    it('should dont display warning', (done) => {
        let newValue = 'test.com';
        let spy = spyOn(comp, 'onUpdateValue').and.callThrough();

        fixture.detectChanges();
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            let inputElement = fixture.debugElement.query(By.css('input.form-control')).nativeElement;
            inputElement.value = newValue;
            inputElement.dispatchEvent(new Event('input'));
            
            fixture.detectChanges();
            fixture.whenStable().then(() => {
                fixture.detectChanges();
                expect(comp.getSelectedDomain().name).toEqual(newValue);
                expect(comp.getBlockedEmailDomains()[0].name).toEqual(newValue);
                expect(spy).toHaveBeenCalled();

                let element = fixture.debugElement.query(By.css('.alert'));
                expect(comp.isError()).toBeFalsy();
                expect(element).toEqual(null);
                done(); 
            })
        })
    });

    it('should display warning', (done) => {
        let newValue =  blockedDomains[3];
        let spy = spyOn(comp, 'onUpdateValue').and.callThrough();

        fixture.detectChanges();
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            let inputElement = fixture.debugElement.query(By.css('input.form-control')).nativeElement;
            inputElement.value = newValue;
            inputElement.dispatchEvent(new Event('input'));
            
            fixture.detectChanges();
            fixture.whenStable().then(() => {
                fixture.detectChanges();
                expect(comp.getSelectedDomain().name).toEqual(newValue);
                expect(comp.getBlockedEmailDomains()[0].name).toEqual(newValue);
                expect(spy).toHaveBeenCalled();

                let element = fixture.debugElement.query(By.css('.alert'));
                expect(comp.isError()).toBeTruthy();
                expect(element).toBeDefined();
                done(); 
            })
        })
    });

    it('should add new elemnt', (done) => {
        let newValue =  blockedDomains[3];
        let spy = spyOn(comp, 'addDomain').and.callThrough();
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            let itemCount = fixture.debugElement.queryAll(By.css('li')).length;

            let inputElement = fixture.debugElement.query(By.css('#btn_add')).nativeElement;
            inputElement.click();
            fixture.detectChanges();

            fixture.whenStable().then(() => {
                fixture.detectChanges();
                expect(comp.isModified()).toBeTruthy();
                expect(spy).toHaveBeenCalled();
                expect(itemCount).toBeLessThan(fixture.debugElement.queryAll(By.css('li')).length);
                done(); 
            })
        })
    });

    it('should remove elemnt', (done) => {
        let newValue =  blockedDomains[3];
        let spy = spyOn(comp, 'removeDomain').and.callThrough();
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            let itemCount = fixture.debugElement.queryAll(By.css('li')).length;

            let inputElement = fixture.debugElement.query(By.css('#btn_remove')).nativeElement;
            inputElement.click();
            fixture.detectChanges();

            fixture.whenStable().then(() => {
                fixture.detectChanges();
                expect(comp.isModified()).toBeTruthy();
                expect(spy).toHaveBeenCalled();
                expect(itemCount).toBeGreaterThan(fixture.debugElement.queryAll(By.css('li')).length);
                done(); 
            })
        })
    });

    

});