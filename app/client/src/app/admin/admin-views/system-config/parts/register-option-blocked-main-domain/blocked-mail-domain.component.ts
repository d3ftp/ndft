import { Component, OnInit, Host, Optional } from '@angular/core'
import { SystemConfigService } from '../../system-config.service';
import { ConfigWebsitePart } from '../config-website-part';
import { SystemConfigComponent } from '../../system-config.component';

@Component({
    selector: 'blocked-mail-domain',
    templateUrl: 'blocked-mail-domain.component.html',
    styleUrls: ['blocked-mail-domain.component.css']
})
export class BlockedMailDomain extends ConfigWebsitePart implements OnInit {
    private blockedEmailDomains: I_BlockedDomains[];
    private selectedDomain: I_BlockedDomains = { name: '' };
    private error = false;

    save() {
        let items = this.getBlockedEmailDomains().filter(el => el.name != '');
        let blockedDomainsNames = items.map(el => el.name);

        this.systemConfigService.setBlockedDomains(blockedDomainsNames);
    }

    constructor(private systemConfigService: SystemConfigService, @Optional() @Host() public parent: SystemConfigComponent) {
        super();
        this.blockedEmailDomains = [];
        this.setParentIfDefined(parent);
    }

    public ngOnInit() {
        this.setBlockedDomains().then(() => {
            this.setSelectedDomain(this.blockedEmailDomains[0]);
        })
    }

    private setBlockedDomains() {
        return new Promise(resolve => {
            this.systemConfigService.getBlockedDomains().then(domains => {
                for (let domainName of domains) {
                    this.blockedEmailDomains.push({ name: domainName })
                }
                resolve();
            })
        });
    }

    public onSelectGroup(domain: I_BlockedDomains) {
        this.ifWorningSetRestoreNormalState();
        this.setSelectedDomain(domain);
    }

    private ifWorningSetRestoreNormalState() {
        if (this.isError()) {
            this.removeDomain();
            this.changeWarningStatusToFalse();
        }
    }

    private changeWarningStatusToFalse() {
        this.error = false;
    }

    private setSelectedDomain(selectedDomain: I_BlockedDomains) {
        this.selectedDomain = selectedDomain
    }

    public onUpdateValue() {
        this.setModifyFlagToTrue();
        this.error = false;
        for (let domain of this.blockedEmailDomains) {
            if (domain.name == this.getSelectedDomain().name && domain !== this.getSelectedDomain()) {
                this.error = true;
                break;
            }
        }
    }

    public getBlockedEmailDomains() {
        return this.blockedEmailDomains;
    }

    public getSelectedDomain() {
        return this.selectedDomain;
    }

    public isError() {
        return this.error;
    }

    public removeDomain() {
        this.setModifyFlagToTrue();
        this.blockedEmailDomains = this.getBlockedEmailDomains().filter(el => el !== this.getSelectedDomain());
        this.selectedDomain = { name: '' };
    }

    public addDomain() {
        this.setModifyFlagToTrue();
        this.getBlockedEmailDomains().push({ name: '' });
        this.selectedDomain = this.getBlockedEmailDomains()[this.getBlockedEmailDomains().length - 1]
    }

}

class I_BlockedDomains {
    name: string;
}