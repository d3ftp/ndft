import { Component, OnInit, Host, Optional } from '@angular/core';
import { ConfigWebsitePart } from '../config-website-part';
import { UserGroupService } from '../../../user-groups/user-groups.service';
import { I_UserGroup } from '../../../../../../../../server/database/models/i_user-group';
import { group } from '@angular/core/src/animation/dsl';
import { SystemConfigService } from '../../system-config.service';
import { SystemConfigComponent } from '../../system-config.component';

@Component({
    selector: 'register-option-config',
    templateUrl: 'register-config.component.html',
    styleUrls: ['register-config.component.css'],
    providers: [UserGroupService]
})
export class RegisterConfigComponent extends ConfigWebsitePart implements OnInit {

    private defaultGroup: I_UserGroup = { _id: '', name: '', routes: [] };
    private guestGroup: I_UserGroup = { _id: '', name: '', routes: [] };
    private userGroups: I_UserGroup[] = [];
    private registerConfirmTime: string;
    private defaultTime: number;

    constructor(private userGroupService: UserGroupService, private systemConfigService: SystemConfigService,
        @Optional() @Host() public parent: SystemConfigComponent) {
        super();
        this.setParentIfDefined(parent)
    }

    public save() {
        this.systemConfigService.setEmailConfirmTime(Number.parseInt(this.registerConfirmTime));
        this.systemConfigService.setDefaultUserGroup(this.defaultGroup._id);
        this.systemConfigService.setDefaultGuestGroup(this.guestGroup._id);
    }

    public ngOnInit() {
        this.setDefaultConfirmTime();
        this.setAllGroups().then(() => {
            this.setDefaultGroup();
            this.setDefaultGuestGroup();
        })
    }

    private setDefaultConfirmTime() {
        this.systemConfigService.getEmailConfirmTime().then(time => {
            this.defaultTime = time;
            this.registerConfirmTime = time.toString();
        })
    }

    private setAllGroups() {
        return new Promise(resolve => {
            this.userGroupService.getAllGroup().then(groups => {
                this.userGroups = groups;
                resolve();
            });
        });
    }

    private setDefaultGroup() {
        this.systemConfigService.getDefaultUserGroup().then(defaultUserGroup => {
            if (defaultUserGroup == undefined || this.userGroups.length <= 0) return;

            for (let group of this.userGroups) {
                if (group._id == defaultUserGroup._id) {
                    this.defaultGroup = defaultUserGroup;
                    break;
                }
            }
        });
    }

    private setDefaultGuestGroup() {
        this.systemConfigService.getDefaultGuestGroup().then(defaultGuestGroup => {
            if (defaultGuestGroup == undefined ) return;
            this.guestGroup = defaultGuestGroup;
        })
    }

    onSelectChangeGuestGroup(index: string) {
        this.guestGroup = this.userGroups[index];
        this.setModifyFlagToTrue();
    }

    public onSelectChange(index: string) {
        this.defaultGroup = this.userGroups[index];
        this.setModifyFlagToTrue();
    }

    public onInputChange() {
        this.setModifyFlagToTrue();
    }

    public getGroups() {
        return this.userGroups;
    }

    public getSelectedGroup() {
        return this.defaultGroup;
    }

    public getDefaultGuestGroup() {
        return this.guestGroup
    }

    public getRegisterConfirmTime(): number {
        let time = Number.parseInt(this.registerConfirmTime);
        if (isNaN(time)) {
            this.registerConfirmTime = this.defaultTime.toString();
            return this.defaultTime;
        }
        return time;
    }

}