import { DebugElement, Component, Output, EventEmitter, group } from "@angular/core";
import { ComponentFixture } from "@angular/core/testing";
import { async } from "@angular/core/testing";
import { TestBed } from "@angular/core/testing";
import { TranslateLoader, TranslateModule, TranslateService } from "@ngx-translate/core";
import { Observable } from "rxjs/Observable";
import { FormsModule } from "@angular/forms";
import { By } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { RegisterConfigComponent } from "./register-config.component";
import { UserGroupService } from "../../../user-groups/user-groups.service";
import { UserService } from "../../../../../user/user.service";
import { I_UserGroup } from "../../../../../../../../server/database/models/i_user-group";
import { exec } from "child_process";
import { SystemConfigService } from "../../system-config.service";

let translations: any = {

}

class FakeLoader implements TranslateLoader {
    getTranslation(lang: string): Observable<any> {
        return Observable.of(translations);
    }

}

let groups = [
    { _id: '1', name: 't1', routes: [] },
    { _id: '2', name: 't2', routes: [] },
    { _id: '3', name: 't3', routes: [] },
    { _id: '4', name: 't4', routes: [] }
]

let defaultGroupsIndex = 2;
let defaultGuestIndex = 1;
let defaultGroup = groups[defaultGroupsIndex];
let defaultGuestGroup = groups[defaultGuestIndex];

class UserGroupServiceMock {
    getAllGroup() {
        return Promise.resolve(groups)
    }
}

let defaultConfirmTinme = 69;

class SystemConfigServiceMock {
    getDefaultUserGroup() {
        return Promise.resolve(defaultGroup);
    }

    getDefaultGuestGroup() {
        return Promise.resolve(defaultGuestGroup);
    }

    getEmailConfirmTime() {
        return Promise.resolve(defaultConfirmTinme);
    }
}

describe('Register Config Component', () => {
    let comp: RegisterConfigComponent;
    let fixture: ComponentFixture<RegisterConfigComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RegisterConfigComponent],
            imports: [
                FormsModule,
                TranslateModule.forRoot({
                    loader: { provide: TranslateLoader, useClass: FakeLoader }
                })
            ],
            providers: [UserGroupService, SystemConfigService]
        })
            .overrideComponent(RegisterConfigComponent, {
                set: {
                    providers: [
                        { provide: UserGroupService, useClass: UserGroupServiceMock },
                        { provide: SystemConfigService, useClass: SystemConfigServiceMock }
                    ]
                }
            })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(RegisterConfigComponent);
                comp = fixture.componentInstance;
            });
    }));

    it('component should be defined', () => {
        expect(comp).toBeDefined();
    });

    it('should get list from server', () => {
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            let oryginalArray = JSON.stringify(comp.getGroups());
            let expectedArray = JSON.stringify(groups)

            expect(oryginalArray).toEqual(expectedArray);
        });
    });

    it('should get default group', (done) => {
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            expect(comp.getSelectedGroup()).toBe(groups[defaultGroupsIndex]);
            done();
        });
    })

    it('should get default guest group', (done) => {
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            expect(comp.getDefaultGuestGroup()).toBe(groups[defaultGuestIndex]);
            done();
        });
    })

    it('should change default group after change item', (done) => {
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            let userGroupBeforeChange = comp.getSelectedGroup();
            fixture.detectChanges();
            let select = fixture.debugElement.query(By.css('#groupSelect'));

            select.nativeElement.selectedIndex = 0;
            select.nativeElement.dispatchEvent(new Event('change'));


            fixture.whenStable().then(() => {
                fixture.detectChanges();
                expect(userGroupBeforeChange).not.toBe(comp.getSelectedGroup());
                done();
            });
        })

    });

    it('should set modified flag to true', (done) => {
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            let userGroupBeforeChange = comp.getSelectedGroup();
            fixture.detectChanges();
            let select = fixture.debugElement.query(By.css('#groupSelect'));

            select.nativeElement.selectedIndex = 3;
            select.nativeElement.dispatchEvent(new Event('change'));


            fixture.whenStable().then(() => {
                fixture.detectChanges();
                expect(userGroupBeforeChange).not.toBe(comp.getSelectedGroup());
                expect(comp.isModified()).toBeTruthy();
                done();
            });
        })
    });

    it('should set modified flag to true - guest group', (done) => {
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            let userGroupBeforeChange = comp.getSelectedGroup();
            fixture.detectChanges();
            let select = fixture.debugElement.query(By.css('#guestGroupSelect'));

            select.nativeElement.selectedIndex = 3;
            select.nativeElement.dispatchEvent(new Event('change'));


            fixture.whenStable().then(() => {
                fixture.detectChanges();
                expect(comp.isModified()).toBeTruthy();
                done();
            });
        })
    });

    it('should set confirm time', (done) => {
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            expect(comp.getRegisterConfirmTime()).toEqual(defaultConfirmTinme);
            done();
        });
    })

    it('should set modified flag to true after input change', (done) => {
        let newConfirmTime = 72;

        fixture.detectChanges();
        fixture.whenStable().then(() => {
            expect(comp.isModified()).toBeFalsy();

            let inputElement = fixture.debugElement.query(By.css('input')).nativeElement;
            inputElement.value = newConfirmTime;
            inputElement.dispatchEvent(new Event('change'));
            inputElement.dispatchEvent(new Event('input'));

            fixture.whenStable().then(() => {
                fixture.detectChanges();
                expect(comp.getRegisterConfirmTime()).toEqual(newConfirmTime);
                expect(comp.isModified()).toBeTruthy();
                done();
            })
        });
    });

    it('should set defaultTime when input value is not number', (done) => {

        fixture.detectChanges();
        fixture.whenStable().then(() => {
            expect(comp.isModified()).toBeFalsy();

            let inputElement = fixture.debugElement.query(By.css('input')).nativeElement;
            inputElement.value = 'xxx';
            inputElement.dispatchEvent(new Event('change'));
            inputElement.dispatchEvent(new Event('input'));

            fixture.whenStable().then(() => {
                fixture.detectChanges();
                expect(comp.getRegisterConfirmTime()).toEqual(defaultConfirmTinme);
                expect(comp.isModified()).toBeTruthy();
                done();
            })
        });
    });
})