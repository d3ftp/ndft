import { Injectable } from "@angular/core";
import { Http, Headers } from '@angular/http';
import { I_Result } from "../../../../../../server/routes/i_result";
import { I_UserGroup } from "../../../../../../server/database/models/i_user-group";

@Injectable()
export class SystemConfigService {
    public constructor(private http: Http) { }

    public setWebsiteTitle(title: string) {
        return this.http.put('/api/system-config/layout', JSON.stringify({ websiteTitle: title }),
            { headers: new Headers({ 'Content-type': 'application/json' }) })
            .map(reponse => reponse.json())
            .toPromise()
    }

    public getWebsiteTitle(): Promise<string> {
        return this.http.get('/api/system-config/layout')
            .map(response => response.json() as I_Result)
            .map(res => res.data)
            .toPromise<string>();
    }

    public setBlockedDomains(blockedDomainsArray: string[]) {
        return this.http.put('/api/system-config/blocked-domains', JSON.stringify({ blockedDomains: blockedDomainsArray }),
            { headers: new Headers({ 'Content-type': 'application/json' }) })
            .map(reponse => reponse.json())
            .toPromise()
    }

    public getBlockedDomains() {
        return this.http.get('/api/system-config/blocked-domains')
            .map(response => response.json() as I_Result)
            .map(res => res.data as string[])
            .toPromise<string[]>();
    }

    public setDefaultUserGroup(id: string) {
        return this.http.put('/api/system-config/default-user-group', JSON.stringify({ defaultUserGroupId: id }),
            { headers: new Headers({ 'Content-type': 'application/json' }) })
            .map(reponse => reponse.json())
            .toPromise()
    }

    public getDefaultUserGroup() {
        return this.http.get('/api/system-config/default-user-group')
            .map(response => response.json() as I_Result)
            .map(res => res.data as I_UserGroup)
            .toPromise<I_UserGroup>();
    }

    public setDefaultGuestGroup(id: string) {
        return this.http.put('/api/system-config/default-guest-group', JSON.stringify({ defaultGuestGroupId: id }),
            { headers: new Headers({ 'Content-type': 'application/json' }) })
            .map(reponse => reponse.json())
            .toPromise()
    }

    public getDefaultGuestGroup() {
        return this.http.get('/api/system-config/default-guest-group')
            .map(response => response.json() as I_Result)
            .map(res => res.data as I_UserGroup)
            .toPromise<I_UserGroup>();
    }

    public setEmailConfirmTime(time: number) {
        return this.http.put('/api/system-config/email-confirm-time', JSON.stringify({ emailConfirmTime: time }),
            { headers: new Headers({ 'Content-type': 'application/json' }) })
            .map(reponse => reponse.json())
            .toPromise()
    }

    public getEmailConfirmTime() {
        return this.http.get('/api/system-config/email-confirm-time')
            .map(response => response.json() as I_Result)
            .map(res => res.data as number)
            .toPromise<number>();
    }

    public getNodemailerConfig() {
        return this.http.get('/api/system-config/nodemailer-config')
            .map(response => response.json() as I_Result)
            .map(res => res.data as Config)
            .toPromise<Config>();
    }

    public setNodemailerConfig(userName, password, from, service) {
        return this.http.put('/api/system-config/nodemailer-config',
            JSON.stringify({ from: from, service: service, auth: { user: userName, pass: password } }),
            { headers: new Headers({ 'Content-type': 'application/json' }) })
            .map(reponse => reponse.json())
            .toPromise()
    }
}

interface Config {
    from: string,
    service: string,
    auth: {
        user: string,
        pass: string
    }
}