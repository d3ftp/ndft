import { NgModule } from "@angular/core"
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppTranslateModule } from "../../../app-translate.module";
import { AdminRoutingModule } from "../../admin.routing.module";
import { MainWebsiteConfigComponent } from "./parts/main-website-config/main-website-config.component";
import { SystemConfigComponent } from "./system-config.component";
import { RegisterConfigComponent } from "./parts/register-option/register-config.component";
import { BlockedMailDomain } from "./parts/register-option-blocked-main-domain/blocked-mail-domain.component";
import { NodemailerConfigComponent } from "./parts/nodemailer-config/nodemailer-config.component";


@NgModule({
    declarations: [MainWebsiteConfigComponent, SystemConfigComponent,
        RegisterConfigComponent, BlockedMailDomain,
        NodemailerConfigComponent],

    imports: [AdminRoutingModule, CommonModule, FormsModule, AppTranslateModule],
    exports: [SystemConfigComponent]
})
export class SystemConfigModule { }