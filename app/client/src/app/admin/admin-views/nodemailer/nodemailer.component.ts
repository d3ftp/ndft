import { Component, OnInit } from '@angular/core';
import { NodemailerService } from './nodemailer.service';
import { SystemMail } from '../../../../../../server/nodemailer-module/system-mail-factory';

@Component({
    templateUrl: 'nodemailer.component.html',
    styleUrls: ['nodemailer.component.css'],
    providers: [NodemailerService]
})
export class NodeMailerComponent implements OnInit {
    public htmlToRender: string;
    public registerMail: MailView = new MailView(SystemMail.REGISTER);
    public changeMail: MailView = new MailView(SystemMail.CHANGE_EMAIL);
    public subject: string;

    constructor(private nodemailerService: NodemailerService) { }

    ngOnInit(): void {
        this.registerMail.initialize(this.nodemailerService);
        this.changeMail.initialize(this.nodemailerService);
    }

    public switchModalTo(mailView: MailView) {
        this.htmlToRender = mailView.getHtmlMsg();
        this.subject = mailView.subject;
    }

    public saveModal(mailView: MailView) {
        mailView.save(this.nodemailerService);
    }

}

class MailView {
    public html: string = ''
    public subject: string = ''
    private mailKey: SystemMail;

    public constructor(mailKey: SystemMail) {
        this.mailKey = mailKey;
    }

    public save(nodemailerService: NodemailerService) {
        nodemailerService.putMail(this.subject, this.html, this.mailKey);
    }

    public getHtmlMsg() {
        return this.html;
    }

    public initialize(nodemailerService: NodemailerService) {
        nodemailerService.getMail(this.mailKey).then((data) => {
            if (data.oryginalHtmlMsg != undefined)
                this.html = data.oryginalHtmlMsg;
            if (data.subject != undefined)
                this.subject = data.subject;
        });
    }

}