import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { SystemMail } from './../../../../../../server/nodemailer-module/system-mail-factory'

@Injectable()
export class NodemailerService {
    public constructor(private http: Http) { }

    public getMail(systemMail: SystemMail): Promise<{ subject: string, oryginalHtmlMsg: string }> {
        return this.http.get('/api/nodemailer/system-mail/' + systemMail)
            .map(response => response.json())
            .map(response => response.data)
            .toPromise<{ subject: string, oryginalHtmlMsg: string }>();
    }

    public putMail(subject: string, oryginalHtmlMsg: string, systemMail: SystemMail) {
        return this.http.put("/api/nodemailer/system-mail",
            JSON.stringify({ html: oryginalHtmlMsg, subject: subject, mailKey: systemMail }),
            { headers: new Headers({ 'Content-type': 'application/json' }) }
        )
            .map(reponse => reponse.json())
            .toPromise();
    }
}