import { DebugElement, Component, Output, EventEmitter } from "@angular/core";
import { ComponentFixture } from "@angular/core/testing";
import { async } from "@angular/core/testing";
import { TestBed } from "@angular/core/testing";
import { TranslateLoader, TranslateModule, TranslateService } from "@ngx-translate/core";
import { Observable } from "rxjs/Observable";
import { FormsModule } from "@angular/forms";
import { By } from "@angular/platform-browser";
import { NodeMailerComponent } from "./nodemailer.component";
import { NodemailerService } from "./nodemailer.service";
import { SafePipe } from "../../../../pipes/safe-html.pipe";


let translations;

class FakeLoader implements TranslateLoader {
    getTranslation(lang: string): Observable<any> {
        return Observable.of(translations);
    }

}

let registerMailHtml = '<h1>HelloWorld</h1>';
let registerSubject = 'This is nonsense';

class NodemailerServiceMock {
    public getMail(populate: boolean): Promise<{subject: string, oryginalHtmlMsg: string  }> {
        return Promise.resolve({ oryginalHtmlMsg: registerMailHtml, subject: registerSubject });
    }

    public putMail(html: string, subject: string) {
        return Promise.resolve();
    }
}

describe('Nodemailer: ', () => {
    let comp: NodeMailerComponent;
    let fixture: ComponentFixture<NodeMailerComponent>

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [NodeMailerComponent, SafePipe],
            imports: [
                FormsModule,
                TranslateModule.forRoot({
                    loader: { provide: TranslateLoader, useClass: FakeLoader }
                })
            ]
        })
            .overrideComponent(NodeMailerComponent, {
                set: {
                    providers: [
                        { provide: NodemailerService, useClass: NodemailerServiceMock }
                    ]
                }
            })
            .compileComponents().then(() => {
                fixture = TestBed.createComponent(NodeMailerComponent);
                comp = fixture.componentInstance;
            });
    }));

    describe('Jasmine environment:', () => {
        it('should defined fixture', () => {
            expect(fixture).toBeDefined();
        })

        it('should defined component', () => {
            expect(comp).toBeDefined();
        })
    });

    describe('onInit', () => {
        it('should init register mail', () => {
            fixture.detectChanges();
            fixture.whenStable().then(() => {
                expect(comp.registerMail.getHtmlMsg()).toEqual(registerMailHtml);
                expect(comp.registerMail.subject).toEqual(registerSubject);
            });
        });
    });

})
