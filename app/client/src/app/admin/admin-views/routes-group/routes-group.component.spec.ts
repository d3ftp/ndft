import { DebugElement, Component, Output, EventEmitter } from "@angular/core";
import { ComponentFixture } from "@angular/core/testing";
import { async } from "@angular/core/testing";
import { TestBed } from "@angular/core/testing";
import { TranslateLoader, TranslateModule, TranslateService } from "@ngx-translate/core";
import { Observable } from "rxjs/Observable";
import { FormsModule } from "@angular/forms";
import { By } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { RoutesGroupComponent } from "./routes-group.component";
import { RoutesGroupService } from "./routes-group.service";
import { I_Result } from "../../../../../../server/routes/i_result";
import { I_RouteGroup } from "../../../../../../server/database/models/i_route-group";
import { I_NgRoute } from "../routes-list/i-ng-route";
import { GroupAction } from "../groups";
import 'rxjs/add/observable/of';

let translations: any = {
    "Auth": {
        "SignIn": "Sign In",
        "Email": "Email address",
        "Password": "Password",
        "RememberMe": "Remember me",
        "LogIn": "Log In",
        "ErrorSignIn": "<strong>Warning!</strong> Wrong email address or password",
        "FB": "Sing In with Facebook"
    }
}

@Component({
    selector: 'routes-list',
    template: ''
})
class MockRoutesList {
    @Output() routeList = new EventEmitter();

    public parse(value) {
        return value
    };
    setRouteCheckBoxToTrue = jasmine.createSpy('setRouteCheckBoxToTrue');
}


class FakeLoader implements TranslateLoader {
    getTranslation(lang: string): Observable<any> {
        return Observable.of(translations);
    }

}

let routesGroup: I_RouteGroup[] = [
    { _id: '1', name: 't1', routes: [], userGroups: [] },
    { _id: '2', name: 't2', routes: [], userGroups: [] },
    { _id: '3', name: 't3', routes: [], userGroups: [] },
    { _id: '4', name: 't4', routes: [], userGroups: [] },
    { _id: '5', name: 't5', routes: [], userGroups: [] },
]

let routes: I_NgRoute[] = [
    { _id: "1", method: "post", path: "/api/test/1", checked: false },
    { _id: "2", method: "post", path: "/api/test/2", checked: false },
    { _id: "3", method: "get", path: "/api/test/2", checked: false },
    { _id: "4", method: "post", path: "/api/test/3", checked: false },
    { _id: "5", method: "delete", path: "/api/test/4", checked: false },
    { _id: "6", method: "get", path: "/api/test/5", checked: false },
    { _id: "7", method: "post", path: "/api/test/6", checked: false },
    { _id: "8", method: "delete", path: "/api/test/7", checked: false },
    { _id: "9", method: "put", path: "/api/test/8", checked: false },
]


class RoutesGroupServiceStub {
    public getAllGroup(populate: boolean): Promise<I_Result> {
        return Promise.resolve({ complete: true, data: routesGroup } as I_Result);
    }

    public add(populateGroup: I_RouteGroup) {
        return Promise.resolve(null);
    }


    public update(populateGroup: I_RouteGroup, newName: string) {
        return Promise.resolve(null);
    }

    public updateUserGroups(id: string, groupId) {
        return Promise.resolve(null);
    }

    public delete(id: string) {
        return Promise.resolve(null);
    }
}

xdescribe('routes group component', () => {
    let comp: RoutesGroupComponent;
    let fixture: ComponentFixture<RoutesGroupComponent>;
    let routesGroupService: RoutesGroupService;
    let routerStub;
    let routeListMock: MockRoutesList;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                RoutesGroupComponent, MockRoutesList
            ],
            imports: [
                FormsModule,
                TranslateModule.forRoot({
                    loader: { provide: TranslateLoader, useClass: FakeLoader }
                })
            ]
        })
            .overrideComponent(RoutesGroupComponent, {
                set: {
                    providers: [
                        { provide: RoutesGroupService, useClass: RoutesGroupServiceStub }
                    ]
                }
            })
            .compileComponents()
            .then(() => {
                let routeListFixture = TestBed.createComponent(MockRoutesList);
                routeListMock = routeListFixture.componentInstance;

                fixture = TestBed.createComponent(RoutesGroupComponent);
                routesGroupService = fixture.debugElement.injector.get(RoutesGroupService, RoutesGroupServiceStub);
                comp = fixture.componentInstance;
                comp.routeList = routeListFixture.componentInstance as any;

            });
    }));

    it('should add groups', () => {
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            expect(comp.groups.length).toBe(routesGroup.length);
        });
    })


    it('should emit routeList and add routes to groups', () => {
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            fixture.debugElement.query(By.directive(MockRoutesList)).injector.get(MockRoutesList).routeList.emit(routes);
            fixture.debugElement.query(By.directive(MockRoutesList)).injector.get(MockRoutesList).parse(routes)
            fixture.detectChanges();

            fixture.whenStable().then(() => {
                fixture.detectChanges();
            })
        });
    })

    it('should call onAdd  after save', () => {
        fixture.detectChanges();
        let spy = spyOn(comp,'onAdd');

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            comp.groups[0].action = GroupAction.TO_SAVE;
            comp.save();
            expect(spy).toHaveBeenCalled();
        });
    });

    it('should call onUpdate  after save', () => {
        fixture.detectChanges();
        let spy = spyOn(comp,'onUpdate');

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            comp.groups[0].action = GroupAction.TO_UPDATE;
            comp.save();
            expect(spy).toHaveBeenCalled();
        });
    });

    it('should call onDelete  after save', () => {
        fixture.detectChanges();
        let spy = spyOn(comp,'onDelete');

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            comp.groups[0].action = GroupAction.TO_DELETE;
            comp.selectedItem = comp.getFirst();
            comp.delete();
            comp.save();
            expect(spy).toHaveBeenCalled();
        });
    });

    it('should remove element from list after delete', () => {
        fixture.detectChanges();
        let spy = spyOn(comp,'onDelete');

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            comp.groups[0].action = GroupAction.TO_DELETE;
            comp.selectedItem = comp.getFirst();
            let itemToDelete = comp.selectedItem;
            comp.delete();
            expect(comp.groups).not.toContain(jasmine.objectContaining(itemToDelete));
        });
    });

    it('should dont call onDelete  without set TO_DeleTE action after save', () => {
        fixture.detectChanges();
        let spy = spyOn(comp,'onDelete');

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            comp.groups[0].action = GroupAction.TO_UPDATE;
            comp.save();
            expect(spy).not.toHaveBeenCalled();
        });
    });

    it('should dont call onUpdate  without set TO_UPDATE action after save', () => {
        fixture.detectChanges();
        let spy = spyOn(comp,'onUpdate');

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            comp.groups[0].action = GroupAction.TO_DELETE;
            comp.save();
            expect(spy).not.toHaveBeenCalled();
        });
    });

    it('should dont call onAdd  without set TO_SAVE action after save', () => {
        fixture.detectChanges();
        let spy = spyOn(comp,'onDelete');

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            comp.groups[0].action = GroupAction.TO_UPDATE;
            comp.save();
            expect(spy).not.toHaveBeenCalled();
        });
    });

});
