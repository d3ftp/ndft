import { Injectable } from "@angular/core";
import { Http, Headers } from '@angular/http';
import { I_RouteGroup } from '../../../../../../server/database/models/i_route-group';
import 'rxjs/add/operator/toPromise';
import { I_Result } from "../../../../../../server/routes/i_result";

@Injectable()
export class RoutesGroupService {
    public constructor(private http: Http) { }
    private headers = new Headers();

    public getAllGroup(populate: boolean): Promise<I_Result> {
        return this.http.get('/api/routes-group-all/'+ populate)
            .map(response => response.json() as I_Result)
            .toPromise<I_Result>();
    }

    public add(populateGroup: I_RouteGroup) {
        return this.http.post('/api/routes-group', {name: populateGroup.name, routes: populateGroup.routes}, {
            headers: new Headers({ 'Content-type': 'application/json' })
        }).
        map(val => val.json())
        .toPromise();
    }


    public update(populateGroup: I_RouteGroup, newName:string) {
        return this.http.put("/api/routes-group",
            JSON.stringify({id: populateGroup._id, newName: newName, routes: populateGroup.routes}),
            {headers: new Headers({ 'Content-type': 'application/json' })}
        )
        .map(reponse => reponse.json())
        .toPromise();
    }

    public updateUserGroups(id: string, groupId) {
        return this.http.put("/api/routes-group-append-user-group",
        JSON.stringify({id: id, userGroupId: groupId}),
        {headers: new Headers({ 'Content-type': 'application/json' })}
    )
    .map(reponse => reponse.json())
    .toPromise();
    }

    public delete(id: string) {
        return this.http.delete('/api/routes-group/' + id, {
            headers: new Headers({ 'Content-type': 'application/json' })
        }).
        map(val => val.json())
        .toPromise();
    }

}