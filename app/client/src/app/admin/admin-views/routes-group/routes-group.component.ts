import { Component, OnInit, ViewChild } from "@angular/core";
import { RoutesGroupService } from "./routes-group.service";
import { I_RouteGroup } from "../../../../../../server/database/models/i_route-group";
import { I_Route } from "../../../../../../server/database/models/i_route";
import { I_Result } from "../../../../../../server/routes/i_result";
import { Groups, I_GroupData } from "../groups";
import { TranslateService } from "@ngx-translate/core";
import { RoutesListComponent } from "../routes-list/routes-list.component";
import { BasicGroupRoutesBasedComponent } from "../basic-group-routes-based.component";

@Component({
    templateUrl: "./routes-group.component.html",
    styleUrls: ["./routes-group.component.css"],
    providers: [RoutesGroupService, TranslateService]

})
export class RoutesGroupComponent extends BasicGroupRoutesBasedComponent<I_RouteGroup> implements OnInit {
    @ViewChild(RoutesListComponent) routeList: RoutesListComponent;

    constructor(private routeGroupService: RoutesGroupService, private translate: TranslateService) {
        super();
        this.groups = null;
    }

    // ********************************** Groups<T> INTHERITED METHODS ******************************************

    public afterRoutesListInit() {
        
    }

    public getEmpty(): I_RouteGroup {
        return { _id: "", name: "", routes: [] } as I_RouteGroup;
    }

    public onAdd(item: I_GroupData<I_RouteGroup>): void {
        this.routeGroupService.add(item.element)
        .then(val => {
            if(val.complete) item.element._id = val.data;
        })
        .catch(err => console.error(err));
    }
    
    public onUpdate(item: I_GroupData<I_RouteGroup>): void {
        console.log('PIZDA MAC');
        
        this.routeGroupService.update(item.element, item.element.name).catch(err => console.error(err));
    }

    public onDelete(item: I_GroupData<I_RouteGroup>): void {
        this.routeGroupService.delete(item.element._id).catch(err => console.error(err));
    }

    public afterAdd() {
        this.selectedItem = this.getLast();
        this.translate.get("AdminPanel.RouteGroups.New").toPromise().then((val) => {
            this.selectedItem.element.name = val;
        })
        this.routeList.setAllRouteCheckBoxTo(false);
    }

    public afterSelect() {
        this.routeList.setAllRouteCheckBoxTo(false);
        this.routeList.setRouteCheckBoxToTrue(this.selectedItem.element.routes )
    }

    // ********************************** ANGULAR INTHERITED METHODS **********************************************
    ngOnInit(): void {
        this.groups = [];

        this.routeGroupService.getAllGroup(true).then(result => {
            if (result.complete == false) return;
            
            for( let group of result.data as I_RouteGroup[]) {
                this.addItemFromDatabase(group); 
            }
            
        }).catch(err => {
            console.error(err);
        })
    }

}
