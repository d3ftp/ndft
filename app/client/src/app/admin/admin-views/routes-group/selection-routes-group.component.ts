import { Component, Input, Output, OnInit, ViewChild, EventEmitter } from "@angular/core";
import { RoutesGroupService } from "./routes-group.service";
import { I_UserGroup } from "../../../../../../server/database/models/i_user-group";
import { I_CheckBox } from "../../../html-class/i_checkbox";
import { I_RouteGroup } from "../../../../../../server/database/models/i_route-group";
import { I_NgRoute } from "../routes-list/i-ng-route";
import { UserGroupService } from "../user-groups/user-groups.service";

@Component({
    selector: "SelectionRoutes",
    template: `
    <div *ngFor="let group of routeGroups">
    <label class="form-check-label">
        <input class="form-check-input" type="checkbox" value="" [(ngModel)]="group.checked" (click)="onSelect(group)">{{group.name}}
    </label>
</div>
    `,
    providers: [RoutesGroupService]
})
export class SelectionRoutesGroupComponent implements OnInit {
    @Output()selectGroupRoute = new EventEmitter();
    @Output()unselectGroupRoute = new EventEmitter();

    public routeGroups: I_NgRouteGroup[] = [];

    ngOnInit(): void {
        this.routeGroupService.getAllGroup(true).then(docs => {
            this.routeGroups = docs.data;
            let userGroupElementId = this.userGroupService.selectedItem._id;

            for(let routeGroup of this.routeGroups) {
                for(let userGroup of routeGroup.userGroups) {
                    if(userGroupElementId == userGroup._id) routeGroup.checked = true;
                }
            }
        })
    }

    constructor(private routeGroupService: RoutesGroupService, private userGroupService: UserGroupService) { }

    public save() {
        for(let group of this.routeGroups) {
            if(group.userGroups.length == 0) {
                this.routeGroupService.updateUserGroups(group._id, "");
                continue;
            }

            let userGroupsId = [];

            for(let userGroup of group.userGroups){
                userGroupsId.push(userGroup._id);
            }
            this.routeGroupService.updateUserGroups(group._id, userGroupsId);
        }
    }

    public setCheckedGroups() {
        let userGroupElementId = this.userGroupService.selectedItem._id;

        for(let routeGroup of this.routeGroups) {
            routeGroup.checked = false;
            for(let userGroup of routeGroup.userGroups) {
                if(userGroupElementId == userGroup._id) routeGroup.checked = true;
            }
        }
    } 

    public onSelect(group: I_NgRouteGroup) {
        if (!group.checked) {
            this.addUserGroup(group);
            this.selectGroupRoute.emit(group);
        }
        else {
            this.removeUserGroup(group);
            this.unselectGroupRoute.emit(group);
        }
    }

    private addUserGroup(group: I_NgRouteGroup) {
        group.userGroups.push(this.userGroupService.selectedItem);
    }

    private removeUserGroup(group: I_NgRouteGroup) {
        let index = group.userGroups.findIndex(el => el._id === this.userGroupService.selectedItem._id);
        if (index < 0) throw Error("Element not exist");
        group.userGroups.splice(index, 1);
        console.log(group);
    }

}

export interface I_NgRouteGroup extends I_CheckBox, I_RouteGroup { routes: I_NgRoute[] };