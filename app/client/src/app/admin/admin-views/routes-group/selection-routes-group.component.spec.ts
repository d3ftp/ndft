import { DebugElement } from "@angular/core";
import { ComponentFixture } from "@angular/core/testing";
import { async } from "@angular/core/testing";
import { TestBed } from "@angular/core/testing";
import { TranslateLoader, TranslateModule, TranslateService } from "@ngx-translate/core";
import { Observable } from "rxjs/Observable";
import { FormsModule } from "@angular/forms";
import { By } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { SelectionRoutesGroupComponent, I_NgRouteGroup } from "./selection-routes-group.component";
import { RoutesGroupService } from "./routes-group.service";
import { UserGroupService } from "../user-groups/user-groups.service";
import { I_Result } from "../../../../../../server/routes/i_result";
import { I_RouteGroup } from "../../../../../../server/database/models/i_route-group";
import { I_UserGroup } from "../../../../../../server/database/models/i_user-group";

let translations: any = {
    "Auth": {
        "SignIn": "Sign In",
        "Email": "Email address",
        "Password": "Password",
        "RememberMe": "Remember me",
        "LogIn": "Log In",
        "ErrorSignIn": "<strong>Warning!</strong> Wrong email address or password",
        "FB": "Sing In with Facebook"
    }
}


let ngRoutesGroup: I_NgRouteGroup[] = [
    { _id: "1", checked: false, name: 'test_1', routes: [], userGroups: [] },
    { _id: "2", checked: false, name: 'test_2', routes: [], userGroups: [] },
    { _id: "3", checked: false, name: 'test_3', routes: [], userGroups: [] },
    { _id: "4", checked: false, name: 'test_4', routes: [], userGroups: [] },
]

function resetngroutesGroup() {
    ngRoutesGroup = [
        { _id: "1", checked: false, name: 'test_1', routes: [], userGroups: [] },
        { _id: "2", checked: false, name: 'test_2', routes: [], userGroups: [] },
        { _id: "3", checked: false, name: 'test_3', routes: [], userGroups: [] },
        { _id: "4", checked: false, name: 'test_4', routes: [], userGroups: [] },
    ]

}

class RoutesGroupServiceStub {
    public getAllGroup(populate: boolean): Promise<I_Result> {
        return Promise.resolve({ complete: true, data: ngRoutesGroup } as I_Result)
    }

    public add(populateGroup: I_RouteGroup) {
        return Promise.resolve(null)
    }


    public update(populateGroup: I_RouteGroup, newName: string) {
        return Promise.resolve(null)
    }

    public updateUserGroups(id: string, groupId) {
        return Promise.resolve(null)
    }

    public delete(id: string) {
        return Promise.resolve(null)
    }
}

let selectedUserItem: I_UserGroup = { _id: '1', name: "dakan", routes: [] };

class UserGroupServiceStub {
    selectedItem;

    public getAllGroup(populate: boolean): Promise<I_Result> {
        return Promise.resolve(null)
    }

    public add(populateGroup: I_RouteGroup) {
        return Promise.resolve(null)
    }


    public update(populateGroup: I_RouteGroup, newName: string) {
        return Promise.resolve(null)
    }

    public updateUserGroups(id: string, groupId) {
        return Promise.resolve(null)
    }

    public delete(id: string) {
        return Promise.resolve(null)
    }
}

class FakeLoader implements TranslateLoader {
    getTranslation(lang: string): Observable<any> {
        return Observable.of(translations);
    }

}

xdescribe('Selection Routes Group', () => {
    let comp: SelectionRoutesGroupComponent;
    let fixture: ComponentFixture<SelectionRoutesGroupComponent>;
    let routesGroupService;
    let usersGroupService;
    let routerStub;

    beforeEach(async(() => {
        routerStub = {
            navigateByUrl: jasmine.createSpy('navigateByUrl')
        }
        TestBed.configureTestingModule({
            declarations: [SelectionRoutesGroupComponent],
            imports: [
                FormsModule,
                TranslateModule.forRoot({
                    loader: { provide: TranslateLoader, useClass: FakeLoader }
                })
            ]
        })
            .overrideComponent(SelectionRoutesGroupComponent, {
                set: {
                    providers: [
                        { provide: RoutesGroupService, useClass: RoutesGroupServiceStub },
                        { provide: UserGroupService, useClass: UserGroupServiceStub },
                    ]
                }
            })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(SelectionRoutesGroupComponent);
                routesGroupService = fixture.debugElement.injector.get(RoutesGroupService, RoutesGroupServiceStub);
                usersGroupService = fixture.debugElement.injector.get(UserGroupService, UserGroupServiceStub);
                comp = fixture.componentInstance;

                resetngroutesGroup();
                usersGroupService.selectedItem = selectedUserItem;
            });
    }));

    it('should display all groups', () => {
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            fixture.detectChanges();

            let implicitContext = fixture.debugElement.query(By.css('input.form-check-input')).context.ngForOf as I_NgRouteGroup[];

            let index = 0;
            for (let routeGroup of ngRoutesGroup) {
                expect(routeGroup).toBe(implicitContext[index]);
                index++;
            }
        })
    });

    it('should add userGroup after select', () => {
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            fixture.detectChanges();

            let implicitContext = fixture.debugElement.query(By.css('input.form-check-input')).nativeElement.click();
            fixture.detectChanges();

            fixture.whenStable().then(() => {
                let $implicitContext = fixture.debugElement.query(By.css('input.form-check-input')).context.ngForOf[0] as I_NgRouteGroup;
                expect($implicitContext.checked).toBeTruthy;
                expect($implicitContext.userGroups[0]).toBeDefined();
                expect($implicitContext.userGroups[0]).toBe(selectedUserItem);
            });


        })
    });

    it('should remove group after unselect', () => {
        fixture.detectChanges();

        fixture.whenStable().then(() => {
            fixture.detectChanges();

            comp.onSelect(ngRoutesGroup[0]);
            ngRoutesGroup[0].checked = true;

            expect(ngRoutesGroup[0].userGroups.length).toBeGreaterThan(0);

            comp.onSelect(ngRoutesGroup[0]);
            expect(ngRoutesGroup[0].userGroups.length).toBeLessThan(1);
        });

    });

    it('should call RoutesGroupService 4 times after click save', () => {
        
        let spy = spyOn(routesGroupService, "updateUserGroups");
        fixture.detectChanges();

        fixture.whenStable().then(() => {
            fixture.detectChanges();

            comp.save();
            expect(spy).toHaveBeenCalledTimes(4);
        });

    });


});