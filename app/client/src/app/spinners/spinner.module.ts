import { LoadSpinnerComponent } from "./load-spinner.component";
import { NgModule } from "@angular/core";
import { SpinnerComponent } from "./spinner.component";

@NgModule({
    declarations: [LoadSpinnerComponent, SpinnerComponent],
    exports: [LoadSpinnerComponent, SpinnerComponent],
})
export class SpinnerModule {

}