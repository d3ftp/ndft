import { Component } from '@angular/core';

@Component({
    selector: 'spinner',
    templateUrl: 'load-spinner.component.html',
    styleUrls: ['spinner.component.css']
})
export class SpinnerComponent { }