import { Component, OnDestroy, Input, OnInit, ViewChild, ElementRef } from "@angular/core";
import { SystemMessageService } from "./system-message.service";
import { Subscription } from "rxjs/Subscription";

@Component({
    selector: 'system-message',
    templateUrl: 'system-message.component.html',
    styleUrls: ['system-message.component.css']
})
export class SystemMessageComponent implements OnDestroy, OnInit {
    @Input() activeTime = 4000;
    public isActive: boolean = false;
    public isSuccess: boolean = false;
    public isError: boolean = false;
    public icon: string = '';
    public message: string = '';

    private successSubscription: Subscription;
    private errorSubscription: Subscription;
    private timeOut;

    constructor(private systemMessageService: SystemMessageService) { }

    ngOnInit(): void {
        this.systemMessageService.subscribeSuccess({
            next: value => this.successObserver(value)
        });

        this.systemMessageService.subscribeError({
            next: value => this.errorObserver(value)
        });
    }

    private successObserver(value) {
        this.activeSystemMessage(value);
        this.setSuccess();
    }

    private setSuccess() {
        this.isError = false;
        this.isSuccess = true;
        this.icon = "check";
    }

    private errorObserver(value) {
        this.activeSystemMessage(value);
        this.setError();
    }

    private setError() {
        this.isError = true;
        this.isSuccess = false;
        this.icon = "error_outline";
    }


    private activeSystemMessage(value) {
        this.clearDisplayedTimeout();
        this.chooseTimeout(value);

        this.message = value.message;
    }

    private chooseTimeout(value) {
        if (value.infinite) {
            this.isActive = true;
        }
        else {
            this.isActive = true;
            this.timeOut = setTimeout(() => {
                this.isActive = false;
            }, this.activeTime);
        
        }
    }

    ngOnDestroy(): void {
        this.successSubscription.unsubscribe();
        this.errorSubscription.unsubscribe();
        this.clearDisplayedTimeout();
    }

    public toggle() {
        this.isActive = false;
        this.clearDisplayedTimeout();
    }

    private clearDisplayedTimeout() {
        if (this.timeOut !== undefined) clearTimeout(this.timeOut);  
    }
}