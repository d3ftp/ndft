import { Injectable, Output } from "@angular/core";
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Subject } from "rxjs";
import { PartialObserver } from "rxjs/Observer";

@Injectable()
export class SystemMessageService {
    private toggleSuccessStatus: Subject<SystemMessage> = new Subject();
    private toggleErrorStatus: Subject<SystemMessage> = new Subject();

    public activeSuccess(message: string) {
        this.toggleSuccessStatus.next({ message: message, infinite: false } as SystemMessage);
    }

    public activeError(message: string) {
        this.toggleErrorStatus.next({ message: message, infinite: false } as SystemMessage);
    }

    public activeSuccessInfinity(message: string) {
        this.toggleSuccessStatus.next({ message: message, infinite: true } as SystemMessage);
    }

    public activeErrorInfinity(message: string) {
        this.toggleErrorStatus.next({ message: message, infinite: true } as SystemMessage);
    }

    public subscribeSuccess(observer: PartialObserver<SystemMessage>) {
        return this.toggleSuccessStatus.subscribe(observer);
    }

    public subscribeError(observer: PartialObserver<SystemMessage>) {
        return this.toggleErrorStatus.subscribe(observer);
    }
}

interface SystemMessage {
    message: string,
    infinite?: boolean;
}
