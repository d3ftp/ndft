import { TestBed, ComponentFixture, async } from "@angular/core/testing";
import { SystemConfigService } from "../admin/admin-views/system-config/system-config.service";
import { SystemMessageService } from "./system-message.service";
import { FormsModule } from "@angular/forms";
import { SystemMessageComponent } from "./system-message.component";
import { By } from "@angular/platform-browser";

describe('System-message', () => {
    let comp: SystemMessageComponent;
    let fixture: ComponentFixture<SystemMessageComponent>
    let service: SystemMessageService;
    let systemMessage: HTMLElement;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SystemMessageComponent],
            providers: [SystemMessageService],
        }).compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(SystemMessageComponent);
                comp = fixture.componentInstance;
                service = fixture.debugElement.injector.get(SystemMessageService);
                systemMessage = fixture.debugElement.query(By.css('#systemMessage')).nativeElement;
            });
    }));

    describe('initialization', () => {
        it('fixture should be defined', () => {
            expect(fixture).toBeDefined();
        });
    
        it('component should be defined', () => {
            expect(fixture).toBeDefined();
        });
    
        it('service should be defined', () => {
            expect(service).toBeDefined();
        });

        it('systemMessage should be defined', () => {
            expect(systemMessage).toBeDefined();
        });
    });

    describe('success subscribe', () => {
        it('should send message', (done) => {
            fixture.detectChanges();
            service.activeSuccess('Wola');
            fixture.detectChanges();
            fixture.whenStable().then(() => {
                fixture.detectChanges();
                expect(systemMessage.innerText).toContain('Wola');
                expect(systemMessage.classList.contains('success'));
                done();
            });
        });

        it('should send error message', (done) => {
            fixture.detectChanges();
            service.activeSuccess('Wola');
            fixture.detectChanges();
            fixture.whenStable().then(() => {
                fixture.detectChanges();
                expect(systemMessage.innerText).toContain('Wola');
                expect(systemMessage.classList.contains('error'));
                done();
            });
        })
    });
});