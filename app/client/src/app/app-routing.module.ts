import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { SelectivePreloadingStrategy } from './selective-preloading-strategy';

const routes: Routes = [
    // {path: "home",  loadChildren: './welcome-module/welcome.module#WelcomeModule'},
    //{path: "register-confirm", component: RegisterConfirmComponent},
    { path: "admin-panel", loadChildren: './admin/admin.module#AdminModule' },
    { path: "app", loadChildren: './filmbox-module/filmbox.module#FilmBoxModule' },
    { path: "account", loadChildren: './account/account.module#AccountModule' },
    { path: "test", loadChildren: './test/test.module#TestModule' },
    { path: "cinema", loadChildren: './cinema/cinema.module#CinemaModule' },
    { path: "series-list", loadChildren: "./list/list.module#ListModule" },
    { path: '', redirectTo: '/app', pathMatch: 'full' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { enableTracing: true })],
    exports: [RouterModule],
    providers: [
        SelectivePreloadingStrategy
    ]
})
export class AppRoutingModule { }