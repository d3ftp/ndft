import { Component, ViewChild, ElementRef, AfterViewInit } from "@angular/core";
import { UserService } from "../user/user.service";

@Component({
    selector: 'sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css'],
})
export class SidebarComponent implements AfterViewInit{
    @ViewChild('sidebar') sidebar: ElementRef;

    constructor(private userService: UserService) {
    }

    ngAfterViewInit(): void {
        this.sidebar.nativeElement.classList.toggle("shrinked");
    }
}