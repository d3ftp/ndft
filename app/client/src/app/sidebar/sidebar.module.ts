import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AppTranslateModule } from "../app-translate.module";
import { FormsModule } from "@angular/forms";
import { AppRoutingModule } from "../app-routing.module";
import { SidebarComponent } from "./sidebar.component";


@NgModule({
    imports:      [CommonModule, AppTranslateModule, FormsModule, AppRoutingModule],
    declarations: [SidebarComponent],
    exports:      [SidebarComponent]
})
export class SidebarModule { }