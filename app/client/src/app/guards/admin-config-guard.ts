import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Http, Headers } from '@angular/http';

@Injectable()
export class AdminConfigGuard implements CanActivate {

    public constructor(private http: Http) { }

    canActivate() {
        return this.http.get('/api/access-control-list-client/system_config')
            .map(reponse => reponse.json())
            .map(response => response.complete)
            .toPromise<boolean>();
    }
}