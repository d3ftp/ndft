import { Directive, EventEmitter, Output, AfterViewInit, ElementRef } from "@angular/core";

@Directive({
    selector: '[echo-directive]'
})
export class EchoDirective implements AfterViewInit{
    @Output() onReady = new EventEmitter();
    private hostElement: ElementRef;

    constructor(private el: ElementRef) {
        this.hostElement = el;
    }

    ngAfterViewInit(): void {
        this.onReady.emit(this.el);
    }
}