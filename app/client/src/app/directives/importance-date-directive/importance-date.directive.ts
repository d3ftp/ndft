import { Directive, HostBinding, ElementRef, Input, AfterViewInit, AfterContentChecked } from "@angular/core";

@Directive({
    selector: '[importance-date]'
})
export class ImportanceDateDirective implements AfterContentChecked {
    @HostBinding('style.color') color: string;
    @Input() date: string;

    constructor(el: ElementRef) {}

    ngAfterContentChecked(): void {  
        let dateNow = new Date(Date.now());
        let date = new Date(this.date);

        if (dateNow > date) this.color="#ffffff38";
    }
}