import { Directive, ElementRef, AfterViewInit } from "@angular/core";
import * as SimpleBar from 'simplebar'
import * as $ from 'jquery';
@Directive({
    selector: "[simplebar]"
})
export class SimpleBarDirective implements AfterViewInit {
    private simpleBar: SimpleBar = null;
    constructor(private el: ElementRef) { }

    ngAfterViewInit(): void {
        if (this.el.nativeElement == null) return;
        this.simpleBar = new SimpleBar(this.el.nativeElement);
    }

    public resetScrollPosition() {
        if (this.simpleBar != null)
        this.simpleBar.getScrollElement().scrollTop = 0;
    }

}