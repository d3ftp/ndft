import { Directive, HostListener, HostBinding, EventEmitter, Output, Input} from '@angular/core'

@Directive({
    selector: '[drag-and-drop-directive]',
    
})
export class DragAndDropDirective {
    @Input() private allowed_extension: Array<string> = [];
    @Output() private onValidFileDrop: EventEmitter<File[]> = new EventEmitter();
    @Output() private onInvalidFileDrop: EventEmitter<File[]> = new EventEmitter();

    @HostBinding('class.on-drag-over') isOnDragOver;

    public isDragOver = false;
    public isDragLeave = false;
    public isDrop = false;


    @HostListener('dragover', ['$event']) public onDragOver(evt) {    
        evt.preventDefault();
        evt.stopPropagation();
        this.isOnDragOver = true;
    }

    @HostListener('dragleave', ['$event']) public onDragLeave(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.isOnDragOver = false;
    }

    @HostListener('drop', ['$event']) public onDrop(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.isOnDragOver = false;
        
        let files = evt.dataTransfer.files;
        
        if (files.length == 1) {
            let extension = files[0].name.split('.')[files[0].name.split('.').length - 1];
            
            if (this.allowed_extension.indexOf(extension) != -1) {
                this.onValidFileDrop.emit(files[0]);     
            }
            else {
                this.onInvalidFileDrop.emit(files[0]);
            }
        }

    }
}