import { Component, DebugElement } from "@angular/core";
import { TestBed, ComponentFixture } from "@angular/core/testing";
import { DragAndDropDirective } from "./drag-and-drop.directive";
import { By } from "@angular/platform-browser";

@Component({
    template: `
     <div drag-and-drop-directive allowed_extension="['png','jpg','jpeg']" (onValidFileDrop)="onGetFile($event)"></div>
    `
})
class TestComponent {
    onGetFile = jasmine.createSpy('onGetFile');
}

class FakeEvent {
    constructor() {
        this.dataTransfer = { files: [] };
    }

    public dataTransfer: {
        files: any[];
    }

    preventDefault() { }
    stopPropagation() { }
}

let component: TestComponent;
let fixture: ComponentFixture<TestComponent>;
let debElement: DebugElement;

describe('Drag and drop directive', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [TestComponent, DragAndDropDirective]
        });
        fixture = TestBed.createComponent(TestComponent);
        component = fixture.componentInstance;

        debElement = fixture.debugElement.query(By.css('div'));
    })

    it('component should be defined', () => {
        expect(component).toBeDefined();
    });

    it('debElement should be defined', () => {
        expect(debElement).toBeDefined();
    });

    it('should capture dropped file', () => {
        let blob = new Blob([""], { type: 'image/jpeg' });
        blob["lastModifiedDate"] = "";
        blob["name"] = "filename.jpg";

        let evt = new FakeEvent();
        evt.dataTransfer.files.push(blob);

        debElement.triggerEventHandler('dragover', null);
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            debElement.triggerEventHandler('drop', evt);
            fixture.detectChanges();
            fixture.whenStable().then(() => {
                expect(component.onGetFile).toHaveBeenCalled();
            })
        })

    });

    it('should dont capture dropped file with invalid extension', () => {
        let blob = new Blob([""], { type: 'text/html' });
        blob["lastModifiedDate"] = "";
        blob["name"] = "filename.txt";

        let evt = new FakeEvent();
        evt.dataTransfer.files.push(blob);

        debElement.triggerEventHandler('dragover', null);
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            debElement.triggerEventHandler('drop', evt);
            fixture.detectChanges();
            fixture.whenStable().then(() => {
                expect(component.onGetFile).not.toHaveBeenCalled();
            })
        })

    })
})