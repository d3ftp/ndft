import { NgModule } from '@angular/core';
import { TestComponent } from './test.componen';
import { TestRoutingModle } from './test.routing.module';
import { CommonModule } from '@angular/common';
import { FileTransmissionModule } from '../file-transmission/file-transmission.module';
import { SystemMessageComponent } from '../system-message/system-message.component';
import { FormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill'
import Quill from "quill";


@NgModule({
    declarations: [
        TestComponent
    ],
    imports: [TestRoutingModle, CommonModule, FileTransmissionModule, FormsModule, QuillModule],
    exports: [TestComponent]
})
export class TestModule {}
