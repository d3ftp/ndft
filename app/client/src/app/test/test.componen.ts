import { Component } from "@angular/core";
import { SystemMessageService } from "../system-message/system-message.service";

@Component({
    templateUrl: 'test.component.html',
    styleUrls: ['test.component.css'],
})
export class TestComponent {
    myContext = { $implicit: 'World', localSk: 'Svet' };
    htmlContent: string;

    constructor(private systemMessageService: SystemMessageService) {}

    test() {
        this.systemMessageService.activeErrorInfinity("Looooo kurwa xD");
    }

    test2() {
        this.systemMessageService.activeErrorInfinity("Looooo kurwa xD");
    }
}


