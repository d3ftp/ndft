import { NgModule } from "@angular/core";
import { ImportanceDateDirective } from "./directives/importance-date-directive/importance-date.directive";
import { SimpleBarDirective } from "./directives/simplebar/simplebar.directive";

@NgModule({
    declarations: [
        ImportanceDateDirective,
        SimpleBarDirective
    ],
    exports: [
        ImportanceDateDirective,
        SimpleBarDirective
    ]
})
export class ShareModule {}