import { Component, ViewChild, OnInit, HostBinding, ElementRef, OnDestroy } from '@angular/core';
import { NgModel } from '@angular/forms';
import { ElementRoot } from '../../src/element-component/element-root';
import { I_ValueBoxAsync } from '../../src/element-component/i-value-box-async';
import { I_ValueBox } from '../../src/element-component/i-value-box';
import { IsAvailableService } from '../../src/is-available.service';
import { SyntaxService } from '../../src/syntax.service';
import { RegisterService } from '../../nav-bar/register-modal/register.service';
import { TranslateService } from '@ngx-translate/core';
import { MessengerSubject } from '../../src/messenger/messenger-subject';
import { FactoryFieldRules } from '../../src/element-component/factory-field-rules/factory-field-rules';
import { FieldRegisterElementAsync } from '../../src/element-component/field-register-element-async';
import { FieldRegisterElement } from '../../src/element-component/field-register-element';
import { Observable } from 'rxjs/observable';
import * as Rx from 'rxjs'
import { Base64ToBlob } from '../../src/base64-to-blob';
import { SocketService, FileUploadPath } from '../../images/socket.service';
import { ProfileService } from './profile.service';
import { UserService } from '../../user/user.service';

interface StyleClass {
    valid: boolean;
    invalid: boolean;
}

@Component({
    //templateUrl: 'profile.component.html',
    templateUrl: 'profile.component.html',
    styleUrls: ['profile.component.css'],
    providers: [IsAvailableService, SyntaxService, RegisterService]
})
export class ProfileComponent implements OnInit {
    @ViewChild('emailInput') emailInput: NgModel;
    @ViewChild('emailRepeatInput') emailRepeatInput: NgModel;
    @ViewChild('uploadImagePlace') uploadImagePlace: ElementRef;
    @ViewChild('profileImage') profileImage: ElementRef;
    @HostBinding('class.extended-image') extendedImageStyle: boolean = false;
    @HostBinding('class.after') afterStyle: boolean = false;

    public imagePath: string = "";
    public profilePath = FileUploadPath.USER_PROFILE;

    public emailValue: string;

    public emailStyleClass: StyleClass = this.createStyleObject();
    public emailRepeatStyleClass: StyleClass = this.createStyleObject();

    public emailElement: ElementRoot<I_ValueBoxAsync>;
    public emailRepeatElement: ElementRoot<I_ValueBox>;

    public emailMessenger: MessengerSubject = new MessengerSubject();

    private factoryFieldRules: FactoryFieldRules = new FactoryFieldRules();

    constructor(private isAvailableService: IsAvailableService,
        private translate: TranslateService,
        private registerService: RegisterService,
        private profileService: ProfileService,
        private socket: SocketService,
        private userService: UserService) { }

    private createStyleObject(): StyleClass {
        return { invalid: false, valid: false }
    }

    public checkEmail() {
        this.emailElement.check({ value: this.emailInput.value.toLocaleLowerCase() });
    }

    public checkRepeatEmail() {
        this.emailRepeatElement.check({ value: this.emailRepeatInput.value.toLocaleLowerCase(), optional: this.emailInput.value.toLocaleLowerCase() });
    }

    ngOnInit(): void {
        this.emailMessenger.setSelecter('. ');
        this.createRuleEmailField();
        this.createRepeatEmailField();

        this.profileService.getEmail().subscribe(res => {
          // this.emailInput.valueAccessor.writeValue(res.data);
        })

        this.userService.getSessionUser().then((data) => {
            this.imagePath = data.profileImagePath;
        })
        
    }




/**
 * Email Field
 */

    private createRuleEmailField() {
        var result = this.factoryFieldRules.getEmail(this.isAvailableService, this.translate);

        this.setAsyncConfirmCallback(result.root, this.emailStyleClass, this.emailMessenger);
        this.setAsyncInterruptCallback(result.root, this.emailStyleClass, this.emailMessenger);

        this.emailElement = result.element;
        this.emailMessenger.add(result.root);
    }

    /**
 * Email Repeat Field
 */

    private createRepeatEmailField() {
        var result = this.factoryFieldRules.getRepeatEmail(this.isAvailableService, this.translate);

        this.setConfirmCallback(result.root, this.emailRepeatStyleClass, this.emailMessenger);
        this.setInterruptCallback(result.root, this.emailRepeatStyleClass, this.emailMessenger);

        this.emailRepeatElement = result.element;
        this.emailMessenger.add(result.root);
    }

    /**
 * Async Callbacks for field
 */

    private setAsyncConfirmCallback(rootElement: FieldRegisterElementAsync<I_ValueBoxAsync>, styleClass: StyleClass, messenger: MessengerSubject) {
        rootElement.setConfirm(value => {
            messenger.update();
            styleClass.valid = true;
            styleClass.invalid = false;
        });
    }

    private setAsyncInterruptCallback(rootElement: FieldRegisterElementAsync<I_ValueBoxAsync>, styleClass: StyleClass, messenger: MessengerSubject) {
        rootElement.setInterrupt(() => {
            messenger.update();
            if (messenger.msg == '') {
                styleClass.valid = false;
                styleClass.invalid = false;
                return;
            }
            styleClass.valid = true;
            styleClass.invalid = true;
        });
    }

    /**
     * Normal Callback for field 
     */
    private setConfirmCallback(rootElement: FieldRegisterElement<I_ValueBox>, styleClass: StyleClass, messenger: MessengerSubject) {
        rootElement.setConfirm(value => {
            messenger.update();
            styleClass.valid = true;
            styleClass.invalid = false;
        });
    }

    private setInterruptCallback(rootElement: FieldRegisterElement<I_ValueBox>, styleClass: StyleClass, messenger: MessengerSubject) {
        rootElement.setInterrupt((msg) => {
            messenger.update();

            if (msg == '' || undefined) {
                styleClass.valid = false;
                styleClass.invalid = false;
                return;
            }
            styleClass.valid = true;
            styleClass.invalid = true;
        });
    }

}
