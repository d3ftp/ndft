import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { I_Result } from "../../../../../server/routes/i_result";

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
}

@Injectable()
export class ProfileService {

    constructor(private http: HttpClient) { }

    getEmail(): Observable<I_Result> {
        return this.http.get<I_Result>('api/account/email')
    }
}