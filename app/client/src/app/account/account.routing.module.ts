import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountComponent } from './account.component';
import { ProfileComponent } from './pages/profile.component';


const routes: Routes = [
    {
        path: '',
        component: AccountComponent,
        children: [
            {

                path: "profile",
                component: ProfileComponent
            },
            { path: '', redirectTo: '/account/profile', pathMatch: 'full' }
        ]
    },

]

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AccountRoutingModule { }