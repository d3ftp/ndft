import { NgModule } from "@angular/core"
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AccountComponent } from "./account.component";
import { AccountRoutingModule } from "./account.routing.module";
import { ProfileComponent } from "./pages/profile.component";
import { AppTranslateModule } from "../app-translate.module";
import { ScaleDirective } from "../directives/images-directive/scale-directive";
import { SocketService } from "../images/socket.service";
import { FileTransmissionModule } from "../file-transmission/file-transmission.module";
import { ProfileService } from "./pages/profile.service";



@NgModule({
    declarations: [AccountComponent, ProfileComponent, ScaleDirective],

    imports: [CommonModule, FormsModule, AccountRoutingModule, AppTranslateModule, FileTransmissionModule],
    providers: [SocketService, ProfileService],
    exports: []
})
export class AccountModule { }