import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { TransmissionButtonComponent } from "./transmission-button.component";
import { Component } from "@angular/core";
import { By } from "@angular/platform-browser";

@Component({
    template: `
        <transmission-button [fileUploadComponent]=instance  allowed_extension="['png','jpg','jpeg']"></transmission-button>
    `
})
class FileUploadComponent {
    instance = this;
    onGetFile = jasmine.createSpy('onGetFile');
}

class FakeEvent {
    constructor() {
        this.dataTransfer = { files: [] };
    }

    public dataTransfer: {
        files: any[];
    }

    preventDefault() { }
    stopPropagation() { }
}

describe('transmission button', () => {
    let componentButton: TransmissionButtonComponent;
    let fixtureButton: ComponentFixture<TransmissionButtonComponent>;

    let componentFile: FileUploadComponent;
    let fixtureFile: ComponentFixture<FileUploadComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TransmissionButtonComponent, FileUploadComponent]
        })
            .compileComponents()
            .then(() => {
                fixtureButton = TestBed.createComponent(TransmissionButtonComponent);
                componentButton = fixtureButton.componentInstance;

                fixtureFile = TestBed.createComponent(FileUploadComponent);
                componentFile = fixtureFile.componentInstance;
            })
    }))

    it('TransmissionButtonComponent should be defined', () => {
        expect(componentButton).toBeDefined();
        expect(fixtureButton).toBeDefined();
    });

    it('FileUploadComponent should be defined', () => {
        expect(fixtureFile).toBeDefined();
        expect(componentFile).toBeDefined();
    });

    it('FileUploadComponent should be defined', () => {
        expect(fixtureFile).toBeDefined();
        expect(componentFile).toBeDefined();
    });

    it('TransmissionButtonComponent should contains FileUploadComponent instance', () => {
        fixtureFile.detectChanges();
        expect(getTransButtonComponent(fixtureFile).fileUploadComponent).toBe(componentFile as any);
    })

    it('TransmissionButtonComponent should call onGetFile after button click', () => {
        fixtureFile.detectChanges();
        let buttonComp = getTransButtonComponent(fixtureFile);
        spyOn(buttonComp, 'onSelect').and.callFake((evt) => {
            buttonComp.fileUploadComponent.onGetFile(evt);
        });

        fixtureFile.debugElement.query(By.css('#file-upload')).triggerEventHandler('change', null)
        fixtureFile.detectChanges();
        fixtureFile.whenStable().then(() => {
          expect(componentFile.onGetFile).toHaveBeenCalled();
        })
    })

    
})


function getTransButtonComponent(fixtureFile): TransmissionButtonComponent {
    return fixtureFile.debugElement.query(By.directive(TransmissionButtonComponent)).injector.get(TransmissionButtonComponent);
}