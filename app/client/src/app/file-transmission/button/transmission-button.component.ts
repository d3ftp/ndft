import { Input, HostListener, Component, Directive } from "@angular/core";
import { FileUploadComponent } from "../file-upload.component";

@Component({
    selector: 'transmission-button',
    templateUrl: 'transmission-button.component.html'
})
export class TransmissionButtonComponent {
    @Input() fileUploadComponent: FileUploadComponent;
    @Input() private allowed_extension: Array<string> = [];

    public onSelect(htmlInputElement: HTMLInputElement) {
        if (this.fileUploadComponent === undefined) throw Error('You need set FileUploadComponent!!');
        
        if (this.isImage(htmlInputElement.files)) {
            this.fileUploadComponent.onGetFile(htmlInputElement.files[0]);
        }
    }

    private isImage(files: FileList) {
        if (files.length == 1) {
            let extension = files[0].name.split('.')[files[0].name.split('.').length - 1];

            if (this.allowed_extension.indexOf(extension) != -1) {
                return true;

            }
            else {
                return false;
            }
        }
    }

}