import { Component, Input, TemplateRef, Output, ViewChild, ElementRef, OnInit, AfterViewInit, OnChanges, SimpleChanges, EventEmitter, OnDestroy } from "@angular/core";
import * as Rx from 'rxjs';
import { Base64ToBlob } from "../src/base64-to-blob";
import { SocketService, FileUploadPath } from "../images/socket.service";
import { DomSanitizer } from "@angular/platform-browser";


@Component({
    selector: 'file-upload',
    templateUrl: 'file-upload.component.html',
    styleUrls: ['file-upload.component.scss']
})
export class FileUploadComponent implements OnInit, OnChanges, OnDestroy {
    @Input() image: string = ''
    @Input() imgWidth: number;
    @Input() imgHeight: number;
    @Input() maxCompresionWidth: number;
    @Input() maxCompresionHeight: number;
    @Input() path: FileUploadPath;
    @Input() defaultParams = null;
    @Output() onUploadComplete: EventEmitter<any> = new EventEmitter();
    @ViewChild('imgBuffor') imgBuffor: ElementRef;


    FileUploadStatus = FileUploadStatus;
    fileUploadStatus: FileUploadStatus = FileUploadStatus.NO_IMAGE;

    private subscription: Rx.Subscription;
    private lastImg;

    constructor(private socket: SocketService, private satanizer: DomSanitizer) { }

    ngOnInit(): void {
        if (this.image != undefined && this.image.length > 0) {
            this.fileUploadStatus = FileUploadStatus.DISPLAY_IMAGE;
        }
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['image'] != undefined && changes['image'].currentValue == '') {
            this.image = this.lastImg;
            this.fileUploadStatus = FileUploadStatus.DISPLAY_IMAGE;
        }
    }

    ngOnDestroy(): void {
        if (this.subscription != undefined) {
            this.subscription.unsubscribe();
        }
    }

    public onGetFile(file: File) {
        this.fileUploadStatus = FileUploadStatus.UPLOADING_IMAGE;
        this.convertToBase64(file);
    }

    public onBufforImgLoad(elementRef: HTMLInputElement) {
        let img = this.imgBuffor.nativeElement;
        new ScaleDirective({ maxHeight: this.maxCompresionHeight, maxWidth: this.maxCompresionWidth }).scaleImage(img).then(data => {
            img.src = '';
            this.image = data;
            this.lastImg = this.image;

            let block = data.toString().split(";");
            let contentType = block[0].split(":")[1];
            let base64Body = block[1].split(',')[1];

            let blob = Base64ToBlob.convert(base64Body, contentType)
            let file = new File([blob], 'upload.png');

            this.subscription = this.socket.uploadFile(file, SocketService.getPath(this.path) + this.addDefaultParam()).subscribe((x: any) => {
                this.fileUploadStatus = FileUploadStatus.DISPLAY_IMAGE;
                this.onUploadComplete.emit(x.body.data);
            })
        })
    }

    private convertToBase64(blob: Blob) {
        var reader = new FileReader();
        reader.readAsDataURL(blob);
        reader.onloadend = () => {
            this.imgBuffor.nativeElement.src = reader.result;
        }
    }

    public getSaveUrl() {
        return this.satanizer.bypassSecurityTrustUrl(this.image);
    }

    private addDefaultParam() {
        if (this.defaultParams == null) return "";
        return this.defaultParams;
    }

}

enum FileUploadStatus {
    UPLOADING_IMAGE,
    DISPLAY_IMAGE,
    NO_IMAGE
}

export class ScaleConfig {
    maxWidth: number;
    maxHeight: number;
}

export class ScaleDirective {
    private config: ScaleConfig;

    constructor(config: ScaleConfig) {
        this.config = config;
    }

    public scaleImage(img: HTMLImageElement): Promise<string> {
        return new Promise((resolve) => {
            var canvas = document.createElement('canvas');
            canvas.width = img.width;
            canvas.height = img.height;
            canvas.getContext('2d').drawImage(img, 0, 0, canvas.width, canvas.height);

            while (canvas.width >= (2 * this.config.maxWidth)) {
                canvas = this.getHalfScaleCanvas(canvas);
            }

            if (canvas.width > this.config.maxWidth) {
                canvas = this.scaleCanvasWithAlgorithm(canvas);
            }

            var imageData = canvas.toDataURL();
            resolve(imageData);
        })
    }

    private scaleCanvasWithAlgorithm(canvas: HTMLCanvasElement) {
        var scaledCanvas = document.createElement('canvas');

        var scale = this.config.maxWidth / canvas.width;

        scaledCanvas.width = canvas.width * scale;
        scaledCanvas.height = canvas.height * scale;

        var srcImgData = canvas.getContext('2d').getImageData(0, 0, canvas.width, canvas.height);
        var destImgData = scaledCanvas.getContext('2d').createImageData(scaledCanvas.width, scaledCanvas.height);

        this.applyBilinearInterpolation(srcImgData, destImgData, scale);

        scaledCanvas.getContext('2d').putImageData(destImgData, 0, 0);

        return scaledCanvas;
    }

    private getHalfScaleCanvas(canvas: HTMLCanvasElement) {
        var halfCanvas = document.createElement('canvas');
        halfCanvas.width = canvas.width / 2;
        halfCanvas.height = canvas.height / 2;

        halfCanvas.getContext('2d').drawImage(canvas, 0, 0, halfCanvas.width, halfCanvas.height);

        return halfCanvas;
    }

    private applyBilinearInterpolation(srcCanvasData: ImageData, destCanvasData: ImageData, scale) {
        function inner(f00, f10, f01, f11, x, y) {
            var un_x = 1.0 - x;
            var un_y = 1.0 - y;
            return (f00 * un_x * un_y + f10 * x * un_y + f01 * un_x * y + f11 * x * y);
        }
        var i, j;
        var iyv, iy0, iy1, ixv, ix0, ix1;
        var idxD, idxS00, idxS10, idxS01, idxS11;
        var dx, dy;
        var r, g, b, a;
        for (i = 0; i < destCanvasData.height; ++i) {
            iyv = i / scale;
            iy0 = Math.floor(iyv);
            // Math.ceil can go over bounds
            iy1 = (Math.ceil(iyv) > (srcCanvasData.height - 1) ? (srcCanvasData.height - 1) : Math.ceil(iyv));
            for (j = 0; j < destCanvasData.width; ++j) {
                ixv = j / scale;
                ix0 = Math.floor(ixv);
                // Math.ceil can go over bounds
                ix1 = (Math.ceil(ixv) > (srcCanvasData.width - 1) ? (srcCanvasData.width - 1) : Math.ceil(ixv));
                idxD = (j + destCanvasData.width * i) * 4;
                // matrix to vector indices
                idxS00 = (ix0 + srcCanvasData.width * iy0) * 4;
                idxS10 = (ix1 + srcCanvasData.width * iy0) * 4;
                idxS01 = (ix0 + srcCanvasData.width * iy1) * 4;
                idxS11 = (ix1 + srcCanvasData.width * iy1) * 4;
                // overall coordinates to unit square
                dx = ixv - ix0;
                dy = iyv - iy0;
                // I let the r, g, b, a on purpose for debugging
                r = inner(srcCanvasData.data[idxS00], srcCanvasData.data[idxS10], srcCanvasData.data[idxS01], srcCanvasData.data[idxS11], dx, dy);
                destCanvasData.data[idxD] = r;

                g = inner(srcCanvasData.data[idxS00 + 1], srcCanvasData.data[idxS10 + 1], srcCanvasData.data[idxS01 + 1], srcCanvasData.data[idxS11 + 1], dx, dy);
                destCanvasData.data[idxD + 1] = g;

                b = inner(srcCanvasData.data[idxS00 + 2], srcCanvasData.data[idxS10 + 2], srcCanvasData.data[idxS01 + 2], srcCanvasData.data[idxS11 + 2], dx, dy);
                destCanvasData.data[idxD + 2] = b;

                a = inner(srcCanvasData.data[idxS00 + 3], srcCanvasData.data[idxS10 + 3], srcCanvasData.data[idxS01 + 3], srcCanvasData.data[idxS11 + 3], dx, dy);
                destCanvasData.data[idxD + 3] = a;
            }
        }
    }
}