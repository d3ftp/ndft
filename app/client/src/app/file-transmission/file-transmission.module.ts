import { NgModule } from "@angular/core";
import { FileUploadComponent } from "./file-upload.component";
import { CommonModule } from "@angular/common";
import { DragAndDropDirective } from "../directives/drag-and-drop-directive/drag-and-drop.directive";
import { EchoDirective } from "../directives/echo.directive";
import { LoadSpinnerComponent } from "../spinners/load-spinner.component";
import { TransmissionButtonComponent } from "./button/transmission-button.component";
import { AppTranslateModule } from "../app-translate.module";
import { SpinnerModule } from "../spinners/spinner.module";

@NgModule({
    declarations: [FileUploadComponent, DragAndDropDirective, EchoDirective, TransmissionButtonComponent],
    imports: [CommonModule, AppTranslateModule, SpinnerModule],
    exports: [FileUploadComponent, TransmissionButtonComponent],
})
export class FileTransmissionModule {
    
}