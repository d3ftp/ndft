import { Injectable } from '@angular/core'
import { HttpClient, HttpRequest, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import * as Rx from 'rxjs';
import { of } from 'rxjs/observable/of';
import { catchError, last, map, tap } from 'rxjs/operators';



@Injectable()
export class SocketService {
    constructor(private http: HttpClient) { }
    public uploadFile(file: File, path: string) {
        if (!file) return;

        const formData = new FormData();
        formData.append('image', file);

        const req = new HttpRequest('POST', path, formData);


        return this.http.request(req).pipe(
            last(),
            catchError(this.handleError(file))
        )

        
    }

    private handleError(file: File) {
        return (error: HttpErrorResponse) => {
            return of(error)
        }
    }

    public static getPath(fileUploadPath: FileUploadPath) {
        switch(fileUploadPath) {
            case FileUploadPath.USER_PROFILE: return '/api/file/user-profile';
            case FileUploadPath.TV_SHOW_HEADER: return '/api/file/tv-show-header';
        }
    }
}

export enum FileUploadPath {
    USER_PROFILE,
    TV_SHOW_HEADER
}