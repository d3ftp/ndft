import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { RegisterFieldResetDirective } from "../directives/attribute-directive/register-field-reset-directive";
import { RegisterModalComponent } from "./register-modal/register-modal.component";
import { LoginFormComponent } from './login-form/login-form.component';
import { UserService } from '../user/user.service';
import { User } from '../user/user';

@Component({
    selector: 'nav-bar',
    templateUrl: './nav-bar.component.html',
    styleUrls: ['./nav-bar.component.css'],
})
export class NavBarComponent implements OnInit, AfterViewInit {
    @ViewChild('rfr') registerFieldResetDirective: RegisterFieldResetDirective;
    @ViewChild('loginForm') loginForm: LoginFormComponent;
    @ViewChild('rm') registerModalComponent: RegisterModalComponent;

    nav_bar_test = "Nav-bar work";
    public user: User;

    constructor(private userService: UserService,) {
    }

    public parse(ob: object) {
        console.log(ob);
    }

    ngOnInit(): void {
        this.userService.getSessionUser().then(user => {
            this.user = user;
        })        

    }

    public resetLoginForm() {
        this.loginForm.resetForm();
    }

    ngAfterViewInit(): void {
        this.registerFieldResetDirective.setRegisterModal(this.registerModalComponent);
    }

    public onUserLogged(user: User) {
        this.user = user;
    }
 }