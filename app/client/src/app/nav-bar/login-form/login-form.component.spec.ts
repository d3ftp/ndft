import { LoginFormComponent } from "./login-form.component";
import { LogInService } from "./login-in.service"
import { DebugElement } from "@angular/core";
import { ComponentFixture } from "@angular/core/testing";
import { async } from "@angular/core/testing";
import { TestBed } from "@angular/core/testing";
import { TranslateLoader, TranslateModule, TranslateService } from "@ngx-translate/core";
import { Observable } from "rxjs/Observable";
import { FormsModule } from "@angular/forms";
import { By } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from "../../app.component";
import { AppModule } from "../../app.module";
import { I_Result } from "../../../../../server/routes/i_result";
import { UserService } from "../../user/user.service";

let translations: any = {
    "Auth": {
        "SignIn": "Sign In",
        "Email": "Email address",
        "Password": "Password",
        "RememberMe": "Remember me",
        "LogIn": "Log In",
        "ErrorSignIn": "<strong>Warning!</strong> Wrong email address or password",
        "FB": "Sing In with Facebook"
    }
}

class LoginInServiceStub {
    complete: boolean;
    error: LoginError;

    verification() {
        return Promise.resolve({complete: this.complete, data:{error: this.error}});
    }
}

class userServiceStub { 
    public getSessionUser(): Promise<any> {
        return Promise.resolve({ complete: true } as I_Result)
    }

}

class FakeLoader implements TranslateLoader {
    getTranslation(lang: string): Observable<any> {
        return Observable.of(translations);
    }

}

export enum LoginError {
    WRONG_NAME_PASSWORD,
    ACCOUNT_NOT_ACTIVE
}

describe('Login Form Component', () => {
    let comp: LoginFormComponent;
    let fixture: ComponentFixture<LoginFormComponent>;
    let de: DebugElement;
    let el: HTMLElement;
    let loginService;
    let translate: TranslateService;

    let emailInput: HTMLInputElement;
    let passwordInput: HTMLInputElement;
    let checkBoox: HTMLDivElement;
   // let errorDivMsg: HTMLDivElement; // can only get when the error is displayed - It is not initialize!
    let facebookLogin: HTMLButtonElement;
    let loginButton: HTMLElement;
    let routerStub;

    beforeEach(async(() => {
        routerStub = {
            navigateByUrl: jasmine.createSpy('navigateByUrl')
        }
        TestBed.configureTestingModule({
            declarations: [LoginFormComponent],
            imports: [
                FormsModule,
                TranslateModule.forRoot({
                    loader: { provide: TranslateLoader, useClass: FakeLoader }
                })
            ]

        }).overrideComponent(LoginFormComponent, {
            set: {
                providers: [
                    { provide: LogInService, useClass: LoginInServiceStub },
                    { provide: Router, useValue: routerStub },
                    { provide: UserService, useClass: userServiceStub }
                ]
            }
        }).compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(LoginFormComponent);
                loginService = fixture.debugElement.injector.get(LogInService, LoginInServiceStub);
                translate = TestBed.get(TranslateService);

                translate.setDefaultLang('en');

                emailInput = fixture.debugElement.query(By.css("#login-email-input")).nativeElement;
                passwordInput = fixture.debugElement.query(By.css("#login-password-input")).nativeElement;
                checkBoox = fixture.debugElement.query(By.css("#login-checkbox-rememberme")).nativeElement;
                facebookLogin = fixture.debugElement.query(By.css("#login-facebook-login")).nativeElement;
                loginButton = fixture.debugElement.query(By.css('#login-button-login')).nativeElement;
            })
    }));

    it('fixture should be defined', () => {
        expect(fixture).toBeDefined();
    });

    it('loginService should be defined', () => {
        expect(loginService).toBeDefined();
    });

    it('is defined', () => {
        expect(TranslateService).toBeDefined();
        expect(translate).toBeDefined();
        expect(translate instanceof TranslateService).toBeTruthy();
    });

    it('should be able to get translations', () => {
        translate.get('Auth.Email').subscribe((res: string) => {
            expect(res).toEqual('Email address');
        });
    });

    it('should translate email placeholder', () => {
        fixture.detectChanges();
        expect(emailInput.getAttribute('placeholder')).toEqual(translations.Auth['Email']);
    });

    it('should translate password placeholder', () => {
        fixture.detectChanges();
        expect(passwordInput.getAttribute('placeholder')).toEqual(translations.Auth['Password']);
    });

    it('should translate checkbox innerHtml', () => {
        fixture.detectChanges();

        expect(checkBoox.innerHTML).toContain(translations.Auth['RememberMe']);
    });

    it('should translate buttton facebook value', () => {
        fixture.detectChanges();

        expect(facebookLogin.value).toContain(translations.Auth['FB']);
    });

    it('should dont navigate to home after fail login', () => {
        fixture.detectChanges();
        loginService.complete = false;
        loginButton.click();
        fixture.whenStable().then(() => {
            expect(routerStub.navigateByUrl).not.toHaveBeenCalled();
            expect(routerStub.navigateByUrl).not.toHaveBeenCalledWith('/home');
        });

        expect(facebookLogin.value).toContain(translations.Auth['FB']);
    });

    it('should dont show error message without clicked login button', () => {
        fixture.detectChanges();
        expect(() => {getErroMessage()}).toThrowError();
    });

    it('should show fail message after failed login', () => {
        fixture.detectChanges();
        loginService.complete = false;
        loginService.error = LoginError.WRONG_NAME_PASSWORD;
        loginButton.click();

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            expect(routerStub.navigateByUrl).not.toHaveBeenCalled();
            expect(routerStub.navigateByUrl).not.toHaveBeenCalledWith('/home');
            
            expect(getErroMessage()).toContain(translations.Auth['ErrorSignIn'])

        });

    });


    it('should navigate to home after confirm login', () => {
        fixture.detectChanges();
        loginService.complete = true;
        loginButton.click();

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            expect(routerStub.navigateByUrl).toHaveBeenCalledWith('/home');
        });
    });

    function getErroMessage() {
        return fixture.debugElement.query(By.css('#login-error-failed-message')).nativeElement.outerHTML;
    }

})
