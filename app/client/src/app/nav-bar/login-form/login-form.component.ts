import { Component, ApplicationRef, Output, EventEmitter } from '@angular/core';
import { LogInService } from "./login-in.service";
import { Router } from '@angular/router';
import { UserService } from '../../user/user.service';

@Component({
    //moduleId: module.id,
    selector: 'login-form',
    templateUrl: './login-form.component.html',
    providers: [LogInService]
})

export class LoginFormComponent {
    @Output() userLogged = new EventEmitter();
    public login: string;
    public password: string;
    public errorMSG: boolean = false;

    public constructor(private logInService: LogInService, private router: Router, private appRef: ApplicationRef
    , private userService: UserService) { }

    public LogIn() {
        this.logInService.verification(this.login, this.password).then((serverRespone) => {
            this.errorMSG = false;
            if (serverRespone.complete) {
                this.userLogged.emit(serverRespone.data.user);
                this.router.navigateByUrl('/home');
            }
            else
                switch (serverRespone.data.error) {
                    case LoginError.ACCOUNT_NOT_ACTIVE:
                        //do something
                        break;
                    case LoginError.WRONG_NAME_PASSWORD:
                        this.errorMSG = true;
                        break;
                    default:

                        break;
                }
        });
    }

    public resetForm() {
        this.login = '';
        this.password = '';
        this.errorMSG = false;
    }
}

//From login route
export enum LoginError {
    WRONG_NAME_PASSWORD,
    ACCOUNT_NOT_ACTIVE
}