import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { I_Result } from '../../../../../server/routes/i_result';

import { Observable } from 'rxjs/observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class LogInService {

    constructor(private http: Http) { }
    /**
     * function verify whether user exsist in database 
     */
    verification(login: string, password: string): Promise<I_Result> {
        return this.http.get(`/api/login/${login}/${password}`)
            .map(val => val.json())
            .toPromise<I_Result>()
    }
}