import { Component, ViewChild, ElementRef, Input } from "@angular/core";
import { MoviedbService, SearchResult } from "../../cinema/moviedb.service";
import * as Rx from 'rxjs'
import { DataService } from "../../cinema/editor/data.service";
import { I_Search } from "../../../../../server/cinema/i-search";
import { zip } from "rxjs/observable/zip";

@Component({
    selector: 'search-component',
    templateUrl: 'search.component.html',
    styleUrls: ['search.component.css']
})
export class SearchComponent {
    // @ViewChild('dropdownList') dropdown: ElementRef;
    public searchResults$: Rx.Observable<SearchResult>;
    public expand: boolean = false;
    public dropdown: boolean = false;

    constructor(private moviedbService: MoviedbService, private dataService: DataService) {
        this.searchResults$ = moviedbService.searchResults$;
    }

    onKeyUp(value) {
        this.moviedbService.searchText$.next(value);
        this.dropdown = true;
    }

    public onBlur(event) {
        this.expand = false;
        this.dropdown = false;
    }

    public onInputClick() {
        this.expand = true;
    }

    public onSelectClick(target, input: HTMLInputElement) {
        target.click();
        input.value = "";

    }
}