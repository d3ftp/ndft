import { browser, element, by, protractor, ElementFinder } from 'protractor';

export class TimerZone {
    public until = protractor.ExpectedConditions;
    public defaultWaitTime: number = 5000;

    public waitForAppear(ele: ElementFinder, wait = this.defaultWaitTime, message = "Element taking too long to appear in the DOM") {
        return browser.wait(this.until.visibilityOf(ele), wait, message)
    }

    public waitForDisappear(ele: ElementFinder, wait = this.defaultWaitTime, message = "Element taking too long to disappear in the DOM") {
        return browser.wait(this.until.invisibilityOf(ele), wait, message);
    }

    public waitForButtonBeClickable(ele: ElementFinder, wait = this.defaultWaitTime, message = "Element taking too long to disappear in the DOM") {
        return browser.wait(this.until.elementToBeClickable(ele), wait, message);
    }
}