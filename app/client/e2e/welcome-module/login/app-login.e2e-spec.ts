import { browser, element, by, protractor, ElementFinder } from 'protractor';
import { LoginElement } from './app-login.po';
import { WelcomeNavbar } from '../app-welcome-navbar.po';
import { Navigation } from '../../app-navigation.po';


describe('Login Element', () => {
    let loginElemnt: LoginElement;
    let welcomeNavbar: WelcomeNavbar;
    let navigation: Navigation;

    beforeAll(() => {
        loginElemnt = new LoginElement();
        welcomeNavbar = new WelcomeNavbar();
        navigation = new Navigation();

        navigation.toHome();
    });

    // Open Login Modal

    it('should open Sign in element', (done) => {
        welcomeNavbar.getLoginMenu().click().then(() => {
            waitForAppear(loginElemnt.getInputLogin()).then(visableResult => {
                expect(visableResult).toBeTruthy();
                done();
            });
        });
    });

    // Insert text to fields

    it('should send keys to login input', (done) => {
        loginElemnt.getInputLogin().sendKeys("MyLogin").then(() => {
            loginElemnt.getInputLogin().getAttribute("value").then((value) => {
                expect(value).toBe("MyLogin");
                done();
            });
        });
    });

    it('should send keys to password input', (done) => {
        loginElemnt.getInputPassword().sendKeys("password").then(() => {
            loginElemnt.getInputPassword().getAttribute("value").then((value) => {
                expect(value).toBe("password");
                done();
            });
        });
    });

    // close modal

    it('should close modal after click on Sign In element at navbar', (done) => {
        welcomeNavbar.getLoginMenu().click().then(() => {
            waitForDisappear(loginElemnt.getInputLogin()).then(visableResult => {
                expect(visableResult).toBeTruthy();
                done();
            });
        });
    });

    // Open Login Modal

    it('should open Sign in element', (done) => {
        welcomeNavbar.getLoginMenu().click().then(() => {
            waitForAppear(loginElemnt.getInputLogin()).then(visableResult => {
                expect(visableResult).toBeTruthy();
                done();
            });
        });
    });

     // test whelther fields are empty

    it('should reset all field after open' , (done) => {
        loginElemnt.getInputLogin().getAttribute("value").then((value) => {
            expect(value).toBe("");
            loginElemnt.getInputPassword().getAttribute("value").then((value) => {
                expect(value).toBe("");
                done();
            });
        });
    });
});

var until = protractor.ExpectedConditions;
var defaultWaitTime: number = 5000;

function waitForAppear(ele: ElementFinder, wait = defaultWaitTime, message = "Element taking too long to appear in the DOM") {
    return browser.wait(until.visibilityOf(ele), 5000, message)
}

function waitForDisappear(ele: ElementFinder, wait = defaultWaitTime, message = "Element taking too long to disappear in the DOM") {
    return browser.wait(until.invisibilityOf(ele), wait, message);
}