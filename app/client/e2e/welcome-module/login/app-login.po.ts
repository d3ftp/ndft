import { browser, element, by } from 'protractor';

export class LoginElement {

    getInputLogin() {
        return element(by.id("login-email-input"));
    }

    getInputPassword() {
        return element(by.id("login-password-input"));
    }

    getCheckBox() {
        return element(by.id("login-checkbox-rememberme"));
    }

    getErrorMessage() {
        return element(by.id("login-error-failed-message"));
    }

    getLoginButton() {
        return element(by.id("login-button-login"));
    }

    getFacebookLoginButton() {
        return element(by.id("login-facebook-login"));
    }
}