import { browser, element, by } from 'protractor';

export class WelcomeNavbar {
    getLoginMenu() {
        return element(by.id("dropdown-menu-log-in"));
    }

    getRegisterMenu() {
        return element(by.id("register-link-id"));
    }
}