import { browser, element, by } from 'protractor';

export class NodemailerPage {
    navigateTo() {
        return browser.get('/admin-panel/nodemailer');
    }

    getPageContainer() {
        return element(by.id('registered-users'));
    }

    getRegisterInput() {
        return element(by.id('registerMailInput'));
    }

    getRegisterArea() {
        return element(by.id('registerArea'));
    }

    getRegisterSaveBtn() {
        return element(by.id('registerSaveBtn'));
    }

    getRegisterPreviewBtn() {
        return element(by.id('registerPreviewBtn'));
    }

    getModal() {
        return element(by.id('modalPreView'));
    }

    getlabel() {
        return element(by.id('exampleModalLabel'));
    }
}