import { TimerZone } from "../../timer-zone";
import { NodemailerPage } from "./nodemailer.po";
import { ElementFinder } from "protractor";

describe('Nodemailer ', () => {
    let page = new NodemailerPage();
    let timer = new TimerZone();

    beforeAll(() => {
        page.navigateTo();
    });

    describe('Register system mail', () => {
        let regTitle = 'Register title';
        let regHtml = '<div style="color:red;">Sample Text ##system_LINK## </div>'

        it('should load page', (done) => {
            timer.waitForAppear(page.getPageContainer()).then(result => {
                expect(result);
                done();
            })
        });

        it('should send keys to subject', (done) => {
            sendTextTo(page.getRegisterInput(), regTitle, done);
        });

        it('should send keys to text area', (done) => {
            sendTextTo(page.getRegisterArea(), regHtml, done);
        });

        it('should save change', (done) => {
            page.getRegisterSaveBtn().click();
            done();
        });

        it('should refresh page', (done) => {
            page.navigateTo();
            done();
        })

        it('should load page', (done) => {
            timer.waitForAppear(page.getPageContainer()).then(result => {
                expect(result);
                done();
            })
        });

        it('should contains subject', (done) => {
            page.getRegisterInput().getAttribute('value').then(value => {
                expect(value).toEqual(regTitle);
                done();
            })
        });

        it('should contains area', (done) => {
            page.getRegisterArea().getAttribute('value').then(value => {
                expect(value).toEqual(regHtml);
                done();
            })
        });

        it('should open modal', (done) => {
            page.getRegisterPreviewBtn().click().then(() => {
                timer.waitForAppear(page.getModal()).then(value => {
                    expect(value).toBeTruthy();
                    done();
                })
            });
        });

        it('preview should contains subject', (done) => {
            page.getlabel().getText().then(value => {
                expect(value).toEqual(regTitle);
                done();
            })
        });


    });
})

var hasClass = function (element, cls) {
    return element.getAttribute('class').then(function (classes) {
        return classes.split(' ').indexOf(cls) !== -1;
    });
};

function sendTextTo(el: ElementFinder, title, done) {
    el.clear().then(() => {
        el.sendKeys(title).then(() => {
            done();
        })
    })
}
