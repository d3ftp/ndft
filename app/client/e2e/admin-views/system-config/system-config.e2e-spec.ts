import { TimerZone } from "../../timer-zone";
import { SystemConfigPage } from "./system-config.po";

describe('system config:', () => {
    let page = new SystemConfigPage();
    let timer = new TimerZone();
    let nodemailerConfig = {
        from: 'test',
        service: 'test2',
        auth: {
            user: 'ala_test',
            pass: '123TEST'
        }
    }
    beforeAll(() => {
        page.navigateTo();
    })

    describe('main website config', () => {
        it('shoul load main website config component', (done) => {
            timer.waitForAppear(page.getContainer()).then(res => {
                expect(res).toBeTruthy();
                done();
            })
        })

        it('websiteTitle should not be empty', (done) => {
            page.getWebsiteTitle().getAttribute('value').then(value => {
                expect(value).toBe('default Title');
                done();
            })
        });

        it('should input new websiteTitle', (done) => {
            page.getWebsiteTitle().clear().then(() => {
                page.getWebsiteTitle().sendKeys('newWbsiteTitle').then(() => {
                    done();;
                })
            })
        });

    })

    describe('email confirm time', () => {
        it('should set time', (done) => {
            let time = '45';
            page.getEmialConfirmTimeInput().clear().then(() => {
                page.getEmialConfirmTimeInput().sendKeys(time).then(() => {
                    page.getEmialConfirmTimeInput().getAttribute('value').then((value) => {
                        expect(value).toEqual(time)
                        done();
                    });
                })
            })
        })
    });

    describe('blocked domain', () => {
        it('should contains items', (done) => {
            page.getBlockedDomainListItems().count().then(count => {
                expect(count).toBeGreaterThan(0);
                done();
            })
        })

        it('should select first item', () => {
            expect(hasClass(page.getBlockedDomainListItems().get(0), 'active')).toBeTruthy();
        })

        it('should change select to second item', (done) => {
            page.getBlockedDomainListItems().get(1).click().then(() => {
                expect(hasClass(page.getBlockedDomainListItems().get(0), 'active')).toBeFalsy();
                expect(hasClass(page.getBlockedDomainListItems().get(1), 'active')).toBeTruthy();

                done();
            });
        });

        it('should change change name on list', (done) => {
            let name = 'super-spam-mail.pl';
            page.getBlockedDomainListItems().get(1).click().then(() => {
                page.getDomainInput().clear().then(() => {
                    page.getDomainInput().sendKeys(name).then(() => {
                        page.getBlockedDomainListItems().get(1).getText().then(val => {
                            expect(val).toEqual(name);
                            done();
                        })
                    })
                })
            });
        });

    });

    describe('nodemailer', () => {
        it('should load user name', (done) => {
            page.getNodemailerUserName().getAttribute('value').then((value) => {
                expect(value).toEqual('user-');
                done();
            })
        })

        it('should change user name value', (done) => {
            page.getNodemailerUserName().clear().then(() => {
                page.getNodemailerUserName().sendKeys(nodemailerConfig.auth.user).then(() => {
                    page.getNodemailerUserName().getAttribute('value').then((value) => {
                        expect(value).toEqual(nodemailerConfig.auth.user);
                        done();
                    })
                })
            })
        });

        it('should load user pass', (done) => {
            page.getNodemailerUserPassword().getAttribute('value').then((value) => {
                expect(value).toEqual('pass-');
                done();
            })
        })

        it('should change user pass value', (done) => {
            page.getNodemailerUserPassword().clear().then(() => {
                page.getNodemailerUserPassword().sendKeys(nodemailerConfig.auth.pass).then(() => {
                    page.getNodemailerUserPassword().getAttribute('value').then((value) => {
                        expect(value).toEqual(nodemailerConfig.auth.pass);
                        done();
                    })
                })
            })
        });

        it('should load from', (done) => {
            page.getNodemailerFrom().getAttribute('value').then((value) => {
                expect(value).toEqual('from-');
                done();
            })
        })

        it('should change user from', (done) => {
            page.getNodemailerFrom().clear().then(() => {
                page.getNodemailerFrom().sendKeys(nodemailerConfig.from).then(() => {
                    page.getNodemailerFrom().getAttribute('value').then((value) => {
                        expect(value).toEqual(nodemailerConfig.from);
                        done();
                    })
                })
            })
        });

        it('should load servce', (done) => {
            page.getNodemailerService().getAttribute('value').then((value) => {
                expect(value).toEqual('service-');
                done();
            })
        })

        it('should change user servce', (done) => {
            page.getNodemailerService().clear().then(() => {
                page.getNodemailerService().sendKeys(nodemailerConfig.service).then(() => {
                    page.getNodemailerService().getAttribute('value').then((value) => {
                        expect(value).toEqual(nodemailerConfig.service);
                        done();
                    })
                })
            })
        });

        it('should save', (done) => {
            page.getSaveButton().click().then(() => {
                setTimeout(() => {
                    done();
                }, 500);
            })
        });
    });

    describe('after reload', () => {
        it('should reload page', (done) => {
            page.navigateTo().then(() => {
                timer.waitForAppear(page.getContainer()).then(res => {
                    expect(res).toBeTruthy();
                    done();
                })
            })
        });

        it('should change user name value', (done) => {
            page.getNodemailerUserName().getAttribute('value').then((value) => {
                expect(value).toEqual(nodemailerConfig.auth.user);
                done();
            })
        });

        it('should change user pass value', (done) => {
            page.getNodemailerUserPassword().getAttribute('value').then((value) => {
                expect(value).toEqual(nodemailerConfig.auth.pass);
                done();
            })

        });


        it('should change user from', (done) => {
            page.getNodemailerFrom().getAttribute('value').then((value) => {
                expect(value).toEqual(nodemailerConfig.from);
                done();
            })
        });

        it('should change user servce', (done) => {
            page.getNodemailerService().getAttribute('value').then((value) => {
                expect(value).toEqual(nodemailerConfig.service);
                done();
            })
        });

    })

});

var hasClass = function (element, cls) {
    return element.getAttribute('class').then(function (classes) {
        return classes.split(' ').indexOf(cls) !== -1;
    });
};
