import { browser, element, by } from 'protractor';

export class SystemConfigPage {
    navigateTo() {
        return browser.get('/admin-panel/system-config');
    }

    getWebsiteTitle() {
        return element(by.id('website-title'));
    }

    getGroupSelect() {
        return element(by.id('groupSelect'));
    }

    getGroupOptions() {
        return element.all(by.css('option'));
    }

    getEmialConfirmTimeInput() {
        return element(by.id('email-confirm-time'));
    }

    getContainer() {
        return element(by.id('admin-container'));
    }

    getBlockedDomainListItems() {
        return element.all(by.css('li.list-group-item'));
    }

    getDomainInput() {
        return element(by.css('#email-domain-service'))
    }

    getNodemailerUserName() {
        return element(by.css('#nodemailer-user'));
    }

    getNodemailerUserPassword() {
        return element(by.css('#nodemailer-password'));
    }

    getNodemailerFrom() {
        return element(by.css('#nodemailer-from'));
    }
    
    getNodemailerService() {
        return element(by.css('#nodemailer-service'));
    }

    getSaveButton() {
        return element(by.css('#save_btn'));
    }
}