import { RouteGroupDispatcher } from "../../../../server/permission/route-group-dispatcher"
import { RoutesGroupPage } from "./routes-group.po";

import { TimerZone } from "../../timer-zone";

/**
 * This test require empty database
 */

describe('routes-group', () => {
    let page: RoutesGroupPage = new RoutesGroupPage();
    let timerZone = new TimerZone();
    const group_0_name = 'test_group_0';
    const grooup_1_name = 'test_group_1'

    beforeAll((done) => {
        page.navigateTo();
        done();
    });

    it('should load container', (done) => {
        timerZone.waitForAppear(page.getContainer()).then((result) => {
            expect(result).toBeTruthy();
            done();
        })
    });

    it('should display NoGroup label', (done) => {
        timerZone.waitForAppear(page.getNoGroupLabel()).then(res => {
            expect(res).toBeTruthy();
            done();
        })
    });

    it('should display text label', (done) => {
        timerZone.waitForAppear(page.getInputText()).then(res => {
            expect(res).toBeTruthy();
            done();
        })
    });

    it('should load routes content', (done) => {
        timerZone.waitForAppear(page.getRoutesContent()).then(res => {
            expect(res).toBeTruthy();
            done();
        })
    });

    describe('routes-group - Adding new group', () => {
        it('should add first new group after button click', (done) => {
            page.getNewButton().click().then(res => {
                page.getGroupLabels().count().then((number) => {
                    expect(number).toBeGreaterThan(0);
                    done();
                })
            });
        })

        it('added first group should be active', (done) => {
            page.getGroupLabels().then(res => {
                expect(hasClass(res[0], 'active')).toBeTruthy();
                done();
            })
        })

        it('should add second new group after button click', (done) => {
            page.getNewButton().click().then(res => {
                page.getGroupLabels().count().then((number) => {
                    expect(number).toBeGreaterThan(0);
                    done();
                })
            });
        })

        it('added second group should be active', (done) => {
            page.getGroupLabels().then(res => {
                expect(hasClass(res[1], 'active')).toBeTruthy();
                done();
            })
        })

        it('first groups should dont be active', (done) => {
            page.getGroupLabels().then(res => {
                expect(hasClass(res[0], 'active')).toBeFalsy();
                done();
            })
        })

    })

    describe('routes - group changing selected item to first', () => {
        it('should click to first item ', (done) => {
            page.getGroupLabels().then(res => {
                res[0].click().then(() => {
                    done();
                })
            })
        });

        it('first item should be selected', () => {
            page.getGroupLabels().then(res => {
                expect(hasClass(res[0], 'active')).toBeTruthy();
            })
        })


        it('second item should not be selected', () => {
            page.getGroupLabels().then(res => {
                expect(hasClass(res[1], 'active')).toBeFalsy();
            })
        })
    })

    describe('routes-group - Changing name', () => {

        it('should send keys to input Text', (done) => {
            page.getInputText().clear().then(() => {
                page.getInputText().sendKeys(group_0_name).then(() => {
                    page.getInputText().getAttribute('value').then(value => {
                        expect(value).toEqual(group_0_name);
                        done();
                    })
                })
            })
        });

        it('should change selected label', (done) => {
            page.getGroupLabels().then(res => {
                expect(hasClass(res[0], 'active')).toBeTruthy();
                res[0].getText().then((res_value) => {
                    expect(res_value).toEqual(group_0_name);
                    done();
                })
            })
        });

    })

    describe('routes-group - adding routes to selected item', () => {

        it('should select routes', (done) => {
            page.getRoutesCheckbox().then(res => {
                res[0].getAttribute('checked').then(ischecked => {
                    expect(ischecked).toBeFalsy();
                });

                let res_1 = res[0].click();
                let res_2 = res[1].click();
                let res_3 = res[3].click();

                Promise.all([res_1, res_2, res_3]).then(() => {
                    done();
                })
            })
        });

        it('second groups should dont have selected routes: [switch to second group]', (done) => {
            page.getGroupLabels().then(groups => {
                groups[1].click().then(() => {
                    page.getRoutesCheckbox().then(routes => {
                        let res_1 = routes[0].getAttribute('checked');
                        let res_2 = routes[1].getAttribute('checked');
                        let res_3 = routes[3].getAttribute('checked');

                        Promise.all([res_1, res_2, res_3]).then((isCheckedArray) => {
                            expect(isCheckedArray[0]).toBeFalsy();
                            expect(isCheckedArray[1]).toBeFalsy();
                            expect(isCheckedArray[2]).toBeFalsy();
                            done();
                        })
                    })
                });
            });
        })

    });

    describe('routes-group - deleting second item', () => {

        it('should delete item after click delete button: [switch to first Group]', (done) => {
            page.getDeleteButton().click().then(() => {
                page.getGroupLabels().count().then(number => {
                    expect(number).toEqual(1);
                    done();
                })
            })
        });

        it('should change selected item to first', (done) => {
            page.getGroupLabels().then(group => {
                expect(hasClass(group[0], 'active')).toBeTruthy();
                done();
            });
        });
    });

    describe('routes-group - Save group', () => {

        it('should save first group', (done) => {
            page.getSaveButton().click().then(() => {
                page.navigateTo();
                done();
            })
        });

        it('should load content', (done) => {
            timerZone.waitForAppear(page.getContainer()).then((waitForContainerRes) => {
                expect(waitForContainerRes).toBeTruthy();
                timerZone.waitForAppear(page.getRoutesContent()).then((waitForRoutesContentRes) => {
                    expect(waitForRoutesContentRes).toBeTruthy();
                    done();
                });
            })
        });

        it('should contains groups', (done) => {
            page.getGroupLabels().then(groups => {
                groups[0].getText().then(name => {
                    expect(name).toEqual(group_0_name);
                    expect(hasClass(groups[0], 'active')).toBeTruthy()
                    done();
                })
            })
        });

        
        it('should cointains selected routes', (done) => {
            page.getRoutesCheckbox().then(routes => {
                let res_1 = routes[0].getAttribute('checked');
                let res_2 = routes[1].getAttribute('checked');
                let res_3 = routes[3].getAttribute('checked');

                Promise.all([res_1, res_2, res_3]).then((isCheckedArray) => {
                    expect(isCheckedArray[0]).toBeTruthy();
                    expect(isCheckedArray[1]).toBeTruthy();
                    expect(isCheckedArray[2]).toBeTruthy();
                    done();
                })
            });
        });



    });


});

var hasClass = function (element, cls) {
    return element.getAttribute('class').then(function (classes) {
        return classes.split(' ').indexOf(cls) !== -1;
    });
};
