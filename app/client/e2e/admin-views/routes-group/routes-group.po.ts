import { browser, element, by } from 'protractor';

export class RoutesGroupPage {
    navigateTo() {
        return browser.get('/admin-panel/routes-group');
    }

    getContainer() {
        return element(by.id('registered-groups'));
    }

    getNoGroupLabel() {
        return element(by.id('routes-group'));
    }

    getInputText() {
        return element(by.id('inputGroupName'));
    }

    getRoutesContent() {
        return element(by.css('div.col div routes-list'));
    }

    getGroupLabels() {
        return element.all(by.id('_list_item'));
    }

    getNewButton() {
        return element(by.id('_new'));
    }

    getDeleteButton() {
        return element(by.id('_delete'));
    }

    getSaveButton() {
        return element.all(by.id('_save'));
    }

    getRoutesCheckbox() {
        return element.all(by.id('_checkbox_route'));
    }

}