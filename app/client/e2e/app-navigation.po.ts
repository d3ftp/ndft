import { browser } from "protractor";

export class Navigation {

    toHome() {
        return browser.get('/');
    }
}