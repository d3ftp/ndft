import { IndexRoute } from "./routes/index.route";
import { UserRoute } from './routes/user.route';
import { Database } from './database/database';
import { GroupRoute } from './routes/group.route';
import { Salt } from "./salt";

import * as bodyParser from "body-parser";
import * as cookieParser from "cookie-parser";
import * as express from "express";
import * as logger from "morgan";
import * as path from "path";
import * as ExpressSession from 'express-session';

import * as fs from 'fs';
// import * as formidable from 'express-formidable';

//ToDelete
import { LoginRoute } from "./routes/login.route";


import { AuthDatabaseConfig } from "./database/auth-database.config";
import { RouteDispatcher } from "./permission/route-dispatcher";
import { I_Route } from "./public/typescript/i_route";
import { SessionRoute } from "./routes/session.route";

import mongoose = require("mongoose");
import { EmailDispatcher } from "./nodemailer-module/email-dispatcher";
import { SystemConfig } from "./system-config";
import { AccountVerificationRoute } from "./routes/account-verification.route";
import { RoutesRoute } from "./routes/routes.route";
import { RoutesGroupRoute } from "./routes/routes-group.route";
import { SystemConfigRoute } from "./routes/system-config.route";
import { TestDatabaseHelper } from "./test-database-helper";
import { ProfileRoute } from "./routes/profile.route";
import { AccessControlList } from "./permission/access-control-list";
import { I_Result } from "./routes/i_result";
import { ClientRouteAccess } from "./routes/client-route-access.route";
import { NodemailerRoute } from "./routes/nodemailer.route";
import { FilesManagerRoute } from "./routes/files-manager.route";
import { AccountRoute } from "./routes/account.route";
import { EmailChangeRoute } from "./routes/email-change.route";
import { SystemMailFactory, SystemMail } from "./nodemailer-module/system-mail-factory";
import { CinemaRoute } from "./routes/cinema.route";
import { ListRoute } from "./routes/list.route";

global.Promise = require('bluebird');
mongoose.Promise = require('bluebird');
const url = require('url');

const uploadPath = path.join(__dirname, 'public', 'uploads');
const userProfileImgPath = path.join(uploadPath, 'profile-img');
const galleryPath = path.join(uploadPath, 'gallery');
const tvShowHeaderPath = path.join(uploadPath, 'tv-show-header');
const posterPath = path.join(uploadPath, 'poster');
const filmPath = path.join(galleryPath, 'film');
const actorsPath = path.join(galleryPath, 'actors');
const tmp = path.join(uploadPath, 'tmp');
const serverProfileImg = url.resolve("http://localhost:3000", 'uploads/profile-img/');
export class Server {
    public app: express.Application;

    isUploadDirExist() {
        return fs.existsSync(uploadPath);
    }

    createUpdatesDir() {
        fs.mkdir(uploadPath, () => {
            fs.mkdir(userProfileImgPath, () => { });
            fs.mkdir(tvShowHeaderPath, () => { });
            fs.mkdir(posterPath, () => { });
            fs.mkdir(galleryPath, () => {
                fs.mkdir(filmPath, () => { });
                fs.mkdir(actorsPath, () => { });
            });
            fs.mkdir(tmp, () => { });
        })
    }

    constructor() {
        this.app = express();
        //this.config();
        //this.db_Connect(this.routes());
        if (!this.isUploadDirExist()) {
            this.createUpdatesDir();
        }

        this.db_Connect().then(() => {
            SystemConfig.init().then(() => {
                EmailDispatcher.bootstrap().then(() => {
                    this.initializeSystemMailer();
                    this.config();
                    this.routes();

                    if (process.argv.length > 2 && process.argv[2] == "e2e")
                        TestDatabaseHelper.initializeDatabase();
                });
            });
        }).catch(() => {
            throw new Error('Cant set connection with database!');
        })

        // var res = new GroupDispatcher().create("Moderator", ['5952bf2a2e88e00eb4560099', '5952bf2a2e88e00eb4560099']);
        // res.then(res => {
        //     new GroupDispatcher().get('Moderator').then(res2 => {
        //         console.log("Value res: " + res2);
        //     })
        // })
    }

    public static bootstrap(): Server {
        return new Server();

    }

    public config() {
        // view engine setup
        this.app.set('views', path.join(__dirname, 'views'));
        this.app.set('view engine', 'pug');

        this.app.use(logger('dev'));
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(cookieParser());
        this.app.use(express.static(path.join(__dirname, 'public')));



        this.app.set("true proxy", 1);

        this.app.use(ExpressSession({
            secret: "zxSd98czw32SdzXfg441",
            resave: false,
            saveUninitialized: true,
        }));

        Salt.password = "ps3XczGron%$@1sdX";
        Salt.secretCode = "xk#$dsfd5T#@okyb#4x5*)!d,adx";

        this.app.get('/test', function (req, res, next) {
            let sess: any = req.session;

            if (sess.views) {
                sess.views++;
                res.setHeader('Content-Type', 'text/html');
                res.write('<p>views: ' + sess.views + '</p>');
                res.write('<p>expires in: ' + (sess.cookie.maxAge / 1000) + 's</p>' + "<br>");
                res.write(JSON.stringify(sess));
                res.end();
            } else {
                sess.views = 1;
                res.end('welcome to the session demo. refresh!: ' + sess.views);
            }

        });
    }

    public routes() {
        var routeDispatcher = new RouteDispatcher();

        this.app.use((req, res, next) => {
            var result = routeDispatcher.parse(req.originalUrl, req.method.toLowerCase());
            if (result == null) return res.send({ complete: false, message: "Unavailable Route" });
            next();
        });

        //this.setMiddlewareAccessControlList();
        let configPath = {
            uploadPath: uploadPath,
            profileImgPath: userProfileImgPath,
            tmp: tmp,
            serverProfileImgPath: serverProfileImg,
            tvShowHeaderPath: tvShowHeaderPath,
            posterPath: posterPath
        } as UploadPath;
        
        this.addRoute(new UserRoute(), routeDispatcher);
        this.addRoute(new GroupRoute(), routeDispatcher);
        this.addRoute(new SessionRoute(), routeDispatcher);
        this.addRoute(new LoginRoute(configPath), routeDispatcher);
        this.addRoute(new AccountVerificationRoute(), routeDispatcher);
        this.addRoute(new EmailChangeRoute(), routeDispatcher);
        this.addRoute(new RoutesRoute(), routeDispatcher);
        this.addRoute(new RoutesGroupRoute(), routeDispatcher);
        this.addRoute(new SystemConfigRoute(), routeDispatcher);
        this.addRoute(new ProfileRoute(), routeDispatcher);
        this.addRoute(new ClientRouteAccess(), routeDispatcher);
        this.addRoute(new NodemailerRoute(), routeDispatcher);
        this.addRoute(new FilesManagerRoute(configPath), routeDispatcher);
        this.addRoute(new AccountRoute(), routeDispatcher);
        this.addRoute(new ListRoute(), routeDispatcher);
        this.addRoute(new CinemaRoute(configPath), routeDispatcher);
        
        routeDispatcher.registerAll().then(() => {

        })

        this.app.use(function (err, req, res, next) {
            res.status(500).send({ complete: false, message: err.toString() } as I_Result);
        })
    }

    private setMiddlewareAccessControlList() {
        this.app.use((req, res, next) => {
            if (req.session.currentUser !== undefined) {
                let result = AccessControlList.hasAccess(req.session.currentUser.accessControlList, req.path, req.method);

                if (!result) {
                    this.sendAccessDenied(res);
                    return;
                }
            }
            else {
                //Generate Session for no logged
            }
            next();
        });
    }

    private sendAccessDenied(res) {
        res.send({ complete: false, message: 'access denied', data: { accessDenied: true } } as I_Result)
    }

    public db_Connect() { //TODO REFACTORY
        return new Promise((resolve, reject) => {
            if (process.argv.length > 2 && process.argv[2] == "e2e") AuthDatabaseConfig.switchToTestDatabase();
            console.log(AuthDatabaseConfig.getNoAuthorizationAdress());
            mongoose.connect(AuthDatabaseConfig.getNoAuthorizationAdress(), { useMongoClient: true });
            var connection = mongoose.connection;

            connection.on('error', console.error.bind(console, 'connection error: '));
            connection.once('open', () => {
                if (process.argv.length > 2 && process.argv[2] == "e2e") {
                    connection.db.dropDatabase().then(() => {
                        resolve();
                    });
                }
                else
                    resolve();
            });


        })
    }


    public addRoute(route: I_Route, routeDispatcher: RouteDispatcher) {
        this.app.use(route.router);
        route.router.stack.forEach(el => {
            routeDispatcher.add(el.route.path, el.regexp, el.route.stack[0].method);
        })
    }

    public initializeSystemMailer() {
        let factory = new SystemMailFactory();
        EmailDispatcher.initializeSystemMessage([
            factory.get(SystemMail.REGISTER),
            factory.get(SystemMail.CHANGE_EMAIL)
        ]);
    }
}

export interface UploadPath {
    uploadPath: string;
    profileImgPath: string;
    tmp: string;
    serverProfileImgPath: string;
    tvShowHeaderPath: string;
    posterPath;
}