export interface I_SetImageName {
    setImage(id: string, imageName: string): Promise<boolean>
}