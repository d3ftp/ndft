import { I_SetImageName } from "./i_set-image-name";
import { I_GetImageName } from "./i_get-image-name";

export interface I_ImageAction extends I_SetImageName, I_GetImageName {}