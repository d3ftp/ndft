import { IsImage } from "../src/is-image";
import * as fs from 'fs';
import * as path from "path";
import * as uuidv4 from 'uuid/v4';

export class FileDispatcher {
    isImage(name) {
        return IsImage.is(name);
    }

    generateRandomName(extension?: string) {
        return extension ? uuidv4() + '.' + extension : uuidv4();
    }

    public async moveTo(filePath: string, newFilePath: string) {
        await fs.rename(filePath, newFilePath, (err) => {
            if (err != null) throw Error(err.message);
        });
    }

    public remove(filePath) {
        fs.unlink(filePath, (err) => { })
    }

    public getExtension(name: string) {
        let splitedName = name.split('.');
        if (splitedName.length <= 1) return undefined;

        let extension = splitedName[splitedName.length - 1];
        return extension;
    }

    public appendNameToPath(filePath: string, name: string) {
        return path.join(filePath, name);
    }

    public createReadStream(filePath: string, fileName: string) {
        return fs.createReadStream(path.join(filePath, fileName));
    }

    public join(...paths: string[]) {
        return path.join(...paths);
    }
}