export interface I_GetImageName {
    getImage(id: string): Promise<string>;
}