import { I_RouteGroup } from "../database/models/i_route-group";
import routeGroupModel = require("../database/models/route-group.model");
import { I_RouteGroupModel } from "../database/models/route-group.model";

export class RouteGroupDispatcher {
    /**
     * Add route group
     */
    public add(name: string, routes: string[]) {
        return new Promise((resolve, reject) => {
            this.isExist(name).then(result => {
                if(result) {
                    reject("Duplicate");
                    return;
                }

                var routeGroup = new routeGroupModel.model({
                    name: name,
                    routes: routes
                });

                routeGroup.save((err, prod) => {
                    if (err) reject(err);
                    resolve(prod._id);
                });
            })
        })
    }

    /**
     * Return group route by name
     */
    public get(id: string): Promise<I_RouteGroupModel> {
        return new Promise((resolve, reject) => {
            routeGroupModel.model.findById({ _id: id }, (err, doc) => {
                if (doc == null) reject("document does not exist");
                resolve(doc);
            });
        });
    }

    /**
     * Return all route group in system
     */
    public getAll(): Promise<I_RouteGroupModel[]> {
        return new Promise((resolve, reject) => {
            routeGroupModel.model.find({}, (err, docs) => {
                if (docs.length == 0) reject("there is no item in database");
                resolve(docs);
            });
        });
    }

    /**
     * Return all populate route group in system 
     */
    public getAllPopulated(): Promise<I_RouteGroupModel[]> {
        return new Promise((resolve, reject) => {
            routeGroupModel.model.find({}, (err, docs) => {
                if (docs.length == 0) reject("there is no item in database");
                resolve(docs);
            }).populate("routes userGroups");
        });
    }

    /**
     * Delete specified group route from system
     */

    public delete(id: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            routeGroupModel.model.findByIdAndRemove({_id: id}).then(doc => {
                resolve(true);
            }).catch(err => {
                reject(false);
            })
        })
    }

    public update(id: string, newName: string, routes:string[]) {
        return new Promise((resolve, reject) => {
            routeGroupModel.model.findByIdAndUpdate({_id: id}, {$set: {name: newName, routes: routes}}).exec((err, res) => {
                if(err || res == null) reject(err);
                resolve(true);
            }).catch(err => {
                reject(err);
            })
        });
    }

    public isExist(name: string): Promise<boolean> {
        return new Promise(resolve => {
            routeGroupModel.model.findOne({name: name},(err, doc) => {
                if(doc == null) resolve(false);
                resolve(true);
            })
        })
    }

    public appendUserGroup(id: string ,userGroup_id: string[]): Promise<boolean> {
        
        return new Promise((resolve,reject) => {
            routeGroupModel.model.findByIdAndUpdate({_id: id}, {$set: {userGroups: userGroup_id}}).exec((err, res) => {
                if(err || res == null) reject(err);
                resolve(true);
            }).catch(err => {
                reject(err);
            })
        })
    }

    public unsetUserGroup(id: string ,userGroup_id: string): Promise<boolean> {
        return new Promise((resolve,reject) => {
            routeGroupModel.model.findByIdAndUpdate({_id: id}, {$set: {userGroups: []}}).exec((err, res) => {
                if(err || res == null) reject(err);
                resolve(true);
            }).catch(err => {
                reject(err);
            })
        })
    }
}