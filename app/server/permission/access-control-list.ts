import { RouteDispatcher, I_RegRoutes } from "../permission/route-dispatcher";

export class AccessControlList {
    private getRoutes: string[] = [];
    private postRoutes: string[] = [];
    private putRoutes: string[] = [];
    private deleteRoutes: string[] = [];

    public initializeRoutesList(routes: I_RegRoutes[]) {
        routes.forEach(route => this.addRouteToSpecifiedGroup(route));
    }

    private addRouteToSpecifiedGroup(route: I_RegRoutes) {
        AccessControlList.getSpecifiedGroupByMethod(this, route.method).push(this.deleteBorderRhomb(route.regRoute.toString()));
    }

    private deleteBorderRhomb(value: string) {
        return value.slice(1, value.length - 1);
    }

    public static hasAccess(accessControlList: AccessControlList, url: string, method: string, ) {
        let index = AccessControlList.getSpecifiedGroupByMethod(accessControlList, method.toLocaleLowerCase())
            .findIndex(el => RegExp(el).test(url));
        if (index < 0) return false;
        return true;
    }

    private static getSpecifiedGroupByMethod(accessControlList: AccessControlList, method: string) {
        switch (method) {
            case 'get': return accessControlList.getRoutes;
            case 'post': return accessControlList.postRoutes;
            case 'put': return accessControlList.putRoutes;
            case 'delete': return accessControlList.deleteRoutes;
            default: throw Error('No specified group');
        }
    }
}