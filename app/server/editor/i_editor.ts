import { I_User } from "../database/models/i_user";

export interface I_Editor {
    creator: I_User;
    modifications: {
        date: Date;
        editor: I_User
    }[]
}