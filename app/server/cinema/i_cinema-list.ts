import { I_User } from "../database/models/i_user";

export interface I_CinemaList {
    userOwner: I_User;
    series: [
        {
            seriesID: number;
            currentSeasonID: number;
            currentEpisodeID: number;
        }
    ]
}