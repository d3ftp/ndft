import { model } from "../database/models/cinema/tv-show.model";

export class TvShowImageDisptacher {
    public async setHeaderImage(tvID: number, filePath: string) {
        return model.findOneAndUpdate({ tvID: tvID }, { $set: { filePath: filePath, modified_backdrop_path: true } }).exec();
    }
}