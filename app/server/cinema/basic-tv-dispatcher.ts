import { I_BasicTv } from "./i_basic-tv";
import { model } from "../database/models/cinema/tv-show.model";

export class BasicTvDisaptcher {

    public static search(pattern: string, limit: number) {
        return new Promise(async (resolve, reject) => {
            model.find({ name: { $regex: pattern, $options: 'i' } }).limit(limit).then(docs => {
                if (docs.length > 0) resolve(docs);
                else resolve(null)
            }).catch(error => {
                reject(error);
            })
        })
    }


    public static findOne(tvID: number) {
        return new Promise(async (resolve, reject) => {
            model.findOne({tvID: tvID}).then(doc => {
                if (doc != null) resolve(doc);
                else resolve(null)
            }).catch(error => {
                reject(error);
            })
        })
    }

    public static update(tv: I_BasicTv) {
        return new Promise(async (resolve, reject) => {
            await model.findOneAndUpdate({ tvID: tv.tvID }, tv).then(async (res) => {
                if (res != null) resolve(res)
                else reject('Cant update tv model!');
            })
        })
    }

    public static create(tv: I_BasicTv) {
        return new Promise(async (resolve, reject) => {
            try {
                delete tv._id;
                let modelDoc = new model(tv);
                let doc = await modelDoc.save();
                
                resolve(doc._id);
            } catch (error) {
                reject(error);
            }
        });
    }

    public static async isExist(tv: I_BasicTv) {
        return new Promise((resolve, reject) => {
            model.findOne({ tvID: tv.tvID }).then((res) => {
                if (res != null) resolve(true)
                else resolve(false);
            }).catch(err => {
                reject(err);
            })
        })
    }
}