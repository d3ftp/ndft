import { model } from "../database/models/cinema/cinema-list.module";

export class CinemaListDispatcher {
    public createList(userID: string) {
        return new Promise(async (resolve, reject) => {
            try {
                let modelDoc = new model({
                    userOwner: userID
                });

                let doc = await modelDoc.save();
                
                resolve(doc);
            } catch (error) {
                reject(error);
            }
        });
    }

    public getList(userID: string) {
        return new Promise(async (resolve, reject) => {
            try {
                let doc = await model.findOne({ userOwner: userID });
                if (doc != null) resolve(doc);
                else reject(new Error("getList doc is null"))
            } catch (error) {
                reject(error);
            }
        });
    }

    public putItem(userID: string, seriesID: number, seasonID: number, episodeID: number) {
        return new Promise(async (resolve, reject) => {
            try {
                let doc = await model.findOne({ userOwner: userID, 'series.seriesID': seriesID });
                if (doc != null) {
                    await doc.update({$set: {"series.$.currentSeasonID": seasonID,  "series.$.currentEpisodeID": episodeID}});
                }
                else {
                    await model.update({userOwner: userID}, {$push: {series: {"currentSeasonID": seasonID,  "currentEpisodeID": episodeID}}});
                }

                model.findOneAndUpdate()

                resolve();
            } catch (error) {
                reject(error);
            }
        });
    }

}