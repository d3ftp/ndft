export interface I_BasicSeason {
  _id: any;
  air_date,
  seasonID: number,
  episodes: [{
    air_date: string,
    episode_number: number,
    name: string
  }]
}