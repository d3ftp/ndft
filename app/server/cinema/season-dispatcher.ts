import { model } from "../database/models/cinema/season.model";
import { I_BasicSeason } from "./I_basic-season";

export class SeasonDispatcher {
    public static findID(seasonID: number) {
        return new Promise(async (resolve, reject) => {
            model.findOne({seasonID: seasonID}).then(doc => {
                if (doc != null) resolve(doc._id);
                else resolve(null);
            }).catch(error => {
                reject(error);
            })
        });
    }

    public static find(seasonID: number) {
        return new Promise(async (resolve, reject) => {
            model.findOne({seasonID: seasonID}).then(doc => {
                if (doc != null) resolve(doc);
                else resolve(null);
            }).catch(error => {
                reject(error);
            })
        });
    }

    public static create(season: I_BasicSeason) {
        return new Promise(async (resolve, reject) => {
            try {
                delete season._id;
                let modelDoc = new model(season);
                let doc = await modelDoc.save();
                
                resolve(doc._id);
            } catch (error) {
                reject(error);
            }
        });
    }

    public static update(tv: I_BasicSeason) {
        return new Promise(async (resolve, reject) => {
            await model.findOneAndUpdate({seasonID : tv.seasonID }, tv).then(async (res) => {
                if (res != null) resolve(res)
                else reject('Cant update tv model!');
            })
        })
    }

    public static async isExist(season: I_BasicSeason) {
        return new Promise((resolve, reject) => {
            model.findOne({ seasonID: season.seasonID }).then((res) => {
                if (res != null) resolve(true)
                else resolve(false);
            }).catch(err => {
                reject(err);
            })
        })
    }
}