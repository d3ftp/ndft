import { model } from "../../database/models/cinema/tv-show-backup.model";
import { model as seasonBackupModel } from "../../database/models/cinema/season-backup.model";
import { I_BasicTv } from "../i_basic-tv";
import { I_TvShowBackup } from "./i_tv-show-backup";
import { model as tvshowModel } from "../../database/models/cinema/tv-show.model";
import { model as seasonModel } from "../../database/models/cinema/season.model";
import { I_BasicSeason } from "../I_basic-season";

export class TvShowBackupDispatcher {

    public static async createSeason(userId: any, data: I_BasicSeason) {  
        try {
            if (!Number.isInteger(data.seasonID)) throw Error('Season must be number');
            let doc = new seasonBackupModel({
                editor: userId,
                previousData: data
            });
            await doc.save();

        } catch (error) {
            throw Error(error);
        }
    }

    public static async create(userId: any, data: I_BasicTv) {
        try {            
            if (!Number.isInteger(data.tvID)) throw Error('Tv must be number');
            let doc = new model({
                editor: userId,
                previousData: data
            });
            await doc.save();

        } catch (error) {
            throw Error(error);
        }
    }

    public static async resotre(backupDataID: any) {
        try {
            let doc = await model.findById(backupDataID);
            let result = await tvshowModel.findByIdAndUpdate(doc.previousData._id, doc.previousData);
            if (!result) throw Error("Can't restore document!");
            
        } catch (error) {
            throw Error(error);
        }
    }

    public static async getAllChangesForDocByID(changeDocumentID: string) {
        try {
            let docs = await model.find({'previousData._id': changeDocumentID});
            if (!docs) throw Error("Can't restore document!");
            
            return docs;
        } catch (error) {
            throw Error(error);
        }
    }
}