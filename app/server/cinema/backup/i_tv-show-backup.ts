import { I_User } from "../../database/models/i_user";
import { I_BasicTv } from "../i_basic-tv";

export interface I_TvShowBackup<T> {
    _id: any,
    editor: I_User,
    previousData: T,
    expires: { type: Date, expires: number, default: Date },
}