export interface I_BasicTv {
    _id: any;
    backdrop_path: string,
    modified_backdrop_path: boolean,
    first_air_date: string,
    tvID: number,
    last_air_date: string,
    name: string,
    number_of_episodes: number,
    number_of_seasons: number,
    original_name: string,
    overview: string,
    poster_path: string,
    seasons: [
      {
        air_date: string,
        episode_count: number,
        modified_backdrop_path: boolean,
        has_internal_data: boolean,
        id: number,
        poster_path: string,
        season_number: number,
        name: string
      }
    ],
    vote_average: number,
}