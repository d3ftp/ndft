export class IsImage {
    private static extensions = ['png', 'jpg', 'jpeg'];

    public static is(fileName: string): boolean {
        let name = fileName.split('.');
        let extension = name[name.length - 1];
        
        return IsImage.isImageExtension(extension);
    }

    private static isImageExtension(extension) {
        let index = IsImage.extensions.findIndex(el => el == extension);
        let result = (index >=0) ? true : false;

        return result;
    }
}