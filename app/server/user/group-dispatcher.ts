import UserGroupModel = require('../database/models/user-group.model');
import RouteModel = require('../database/models/route-model');
import { I_UserGroup } from "../database/models/i_user-group";
import { I_PopulateGroup } from './i_populate-group';

export class GroupDispatcher {
    private static instance: GroupDispatcher = null;

    private constructor() {}

    /**
     * Create and add new group and add to database
     * @param grupName 
     * @param permission 
     * return true when operation end successful otherwise return false
     */

    public create(grupName: string, routes: string[]): Promise<boolean> {
        var userGroupModel = new UserGroupModel({
            name: grupName,
            routes: routes
        });

        return new Promise((resolve) => {
            userGroupModel.save((err) => {
                if (err == null) resolve(true);
                resolve(false);
            });
        });

    }

    /**
     * Check is Group exist in database
     * @param groupName 
     * return false when group exist in db otherwise return false
     */

    public isExist(groupName: string): Promise<Boolean> {
        return new Promise((resolve) => {
            UserGroupModel.findOne({ name: groupName }, (err, doc) => {
                if (doc == null) resolve(false);
                resolve(true);
            });
        });
    }


    /**
     * Update group permission, return true when operation end successful otherwise return false
     * @param groupName 
     * @param routes 
     */

    public updatePermission(id: string, routesId: string[]): Promise<boolean> {
        return new Promise((resolve) => {
            UserGroupModel.findByIdAndUpdate({ _id: id }, { $set: { routes: routesId } }, (err, raw) => {
                if (err != null || raw == null || raw == undefined) resolve(false)
                resolve(true);

            });
        });
    }

    /**
     * Update groupName, return true when operation end successful otherwise return false
     * @param groupName 
     * @param newGroupName 
     */

    public updateGroupName(id: string, newGroupName): Promise<boolean> {
        return new Promise((resolve) => {
            UserGroupModel.update({ _id: id }, { $set: { name: newGroupName } }, (err, raw) => {
                if (err != null || raw.nModified <= 0) resolve(false)
                resolve(true);
            });
        });
    }

    /**
     * update group name and routes
     * @param id 
     * @param newName 
     * @param routesId 
     */
    public update(id: string, newName: string, routesId: string[]): Promise<boolean> {
        return new Promise(resolve => {
            if(newName == "") resolve(false);
            
            UserGroupModel.findByIdAndUpdate({_id: id}, {$set: {name: newName, routes: routesId}}).exec((err, res) => {
                if(err || res == undefined) resolve(false);
                resolve(true);
            })
        });
    }

    /**
     * return all groups from database
     */

    public getAll(): Promise<I_UserGroup[]> {
        return new Promise((resolve) => {
            UserGroupModel.find({}).then((docs) => {
                resolve(docs);
            })
        });
    }

    /**
     * return all groups from database and populate
     */

    public getAllAndPopulate(): Promise<I_UserGroup[]> {
        return new Promise((resolve) => {
            UserGroupModel.find({}).populate("routes").then((docs) => {
                resolve(docs);
            })
        });
    }

    /**
     * remove group from database return error when operation end failure
     * @param groupName 
     */

    public remove(id: string): Promise<any> {
        return new Promise((resolve) => { 
            UserGroupModel.findByIdAndRemove({ _id: id }, (err) => {
                resolve(err);
            });
        });
    }



    public get(id: string): Promise<I_UserGroup> {
        return new Promise(resolve => {
            UserGroupModel.findById({ _id: id }).populate('routes').exec((err, doc) => {
                if (err) resolve(null)
                resolve(doc);
            }).catch(err => {
                resolve(null)
            })
        });
    }

    public getByName(name: string): Promise<I_UserGroup> {
        return new Promise(resolve => {
            UserGroupModel.findOne({ name: name }).populate('routes').exec((err, doc) => {
                if (err) throw err;
                resolve(doc);
            });
        });
    }

    public static get(): GroupDispatcher {
        if(GroupDispatcher.instance == null) GroupDispatcher.instance = new GroupDispatcher();
        return GroupDispatcher.instance;
    }

    // public static convertNameToId(name: string): string {

    //     for(let group of GroupDispatcher.groups) {
    //         if(group.name == name) return group._id;
    //     }
    //     throw Error('Cant convert group name to id');
    // }

    // public static convertNames(names: object[]): I_Group[] {
    //     let groups: I_Group[] = [];
    //     names.forEach(name => {
    //         var nameStr = name.toString();
    //         groups.push({name: nameStr, id : GroupDispatcher.convertNameToId(nameStr)});
    //     })
    //     if(names.length != groups.length) throw Error('Cant convert group names to ids');   
    //     return groups
    // }

    // public static convertIdToName(id: string): string {
    //     id = id.toString();
    //     for(let group of GroupDispatcher.groups) {
    //         var str:String = group._id;

    //         if(str == id) {
    //             return group.name;
    //         }
    //     }
    //    throw Error('Cant convert group name to id');

    // }

    // public static convertIds(ids: string[]): I_Group[] {
    //     let groups: I_Group[] = [];

    //     ids.forEach(id => {
    //         var idStr = id.toString();
    //         groups.push({id: idStr, name: GroupDispatcher.convertIdToName(idStr)});
    //     })

    //     if(ids.length != groups.length) throw Error('Cant convert ids to names');   
    //     return groups
    // }

    // public static checkPermission(group: I_UserGroup, route: string): boolean {
    //     if( group.permission.indexOf() )
    // }



}