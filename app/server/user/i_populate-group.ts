export interface I_PopulateGroup {
    name: string,
    routes: {
        _id: string;
        path: string;
        method: string;
    }[]
}