import { Express } from 'express';
import { I_UserGroup } from '../database/models/i_user-group';
import { RouteDispatcher, I_RegRoutes } from '../permission/route-dispatcher';
import { AccessControlList } from '../permission/access-control-list';
/**
 * It represents abstract user in system
 */
export class User {
    public name: string;
    public groups: I_UserGroup[];
    public accessControlList: AccessControlList = new AccessControlList();
    public id: string;
    public profileImagePath: string = "";

    constructor(id: string, name: string, groups: I_UserGroup[]) {
        try {
            this.id = id;
            this.name = name;
            this.groups = groups;
            let routes: I_RegRoutes[] = [];
            let group = this.groups[0];
            if (groups.length <= 0) return;
            if (group.routes == undefined) return;
            for (let route of group.routes) {
                let regExpRoute = RouteDispatcher.findProperRoute(route.path, route.method);
                routes.push(regExpRoute);
            }
    
            this.accessControlList.initializeRoutesList(routes);
        } catch (error) {
            return;
        }
    }
}

