import userModel = require('../database/models/user.model');
import { User } from './user';
import { GroupDispatcher } from './group-dispatcher';
import { DocumentQuery } from 'mongoose';
import { PopulateField } from './populate-fields';


export class GetUser {
    /**
     * get user from database by specified 'query' for example
     * {email: sample@email.com} return I_UserModel when email is equal to sample@email.com otherwise return null
     * @param query 
     */

    public by(query: {}): Promise<userModel.I_UserModel> {
        return new Promise((resolve) => {
            userModel.model.findOne(query, (err, doc) => {
                if (err) resolve(null);
                if (doc) resolve(doc);
                resolve(null);
            }).populate({path: 'groups', populate: {path: 'routes'}});
        })
    }

    /**
     * return user document as user class object
     * @param query 
     */
    public asObject(query: {}): Promise<User> {
        return new Promise(resolve => {
            this.by(query).then(doc => {
                let user = new User(doc._id ,doc.name, doc.groups);
                resolve(user);
            });
        })
    }

    public populate(query: {}, populateField: PopulateField): Promise<userModel.I_UserModel> {
        return new Promise(resolve => {
            userModel.model.findOne(query).populate(populateField).exec((err,doc) => {
                resolve(doc);
            })
        });
    }
}