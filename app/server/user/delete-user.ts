import userModel = require('../database/models/user.model');

export class DeleteUser {
    public delete(id: string) {
        return new Promise((resolve, reject) => {
            userModel.model.findByIdAndRemove({ _id: id }).then(() => {
                resolve(true);
            }).catch(err => {
                reject(err);
            })
        });
    }

    public deleteByName(name: string) {
        return new Promise((resolve, reject) => {
            userModel.model.findOneAndRemove({ name: name }).then((res) => { 
                if(res == null) reject(new Error('No item'));
                resolve(true);
            }).catch(err => {
                reject(err);
            })
        });
    }
}