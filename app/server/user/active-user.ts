import userModel = require('../database/models/user.model');

export class ActiveUser {
    public by(VerificationCode: string): Promise<boolean> {
        return new Promise(resolve => {
            userModel.model.findOneAndUpdate({ VerificationCode: VerificationCode }, { $set: { isActive: true }, $unset: { expires: "", VerificationCode: "" } }).then((doc) => {
                if (doc == null || doc == undefined) resolve(false);
                resolve(true);
            });
        })
    }
}