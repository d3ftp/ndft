import { GetUser } from "./get-user";
import userModel = require('../database/models/user.model');
import emailChange = require('../database/models/email-change.model');
import { CryptoHash } from "../crypto/crypto-hash";

export class UserEmail {
    public getEmail(id) {
        return new Promise((resolve, reject) => {
            new GetUser().by({ _id: id }).then((doc) => {
                if (doc == null) reject("cant get document by id: " + id);
                else resolve(doc.email);
            });
        });
    }


    public changeEmail(verificationCode: string) {
        return new Promise(async (resolve, reject) => {
            try {
                let emailDoc = await emailChange.model.findOne({ verificationCode: verificationCode });
                let updatedUserDoc = await userModel.model.findByIdAndUpdate({ _id: emailDoc.user }, { $set: { email: emailDoc.email } });

                if (updatedUserDoc == null) reject(new Error('cant update user!'));
                else resolve(true);
            } catch (error) {
                reject(error);
            }
        });
    }

    public removeVerificationCode(verificationCode: string) {
        return new Promise(async (resolve, reject) => {
            try {
                let emailDoc = await emailChange.model.findOneAndRemove({ verificationCode: verificationCode });
                if (emailDoc == null) reject('cant delete email change object user!');
                else resolve(true);
            } catch (error) {
                reject(error);
            }
        });
    }

    public getVerificationEmailCode(userID: string) {
        return new Promise(async (resolve, reject) => {
            try {
                let emailDoc = await emailChange.model.findOne({ user: userID });
                
                if (emailDoc == null) reject(new Error('cant find verification email model'));
                else resolve(emailDoc.verificationCode);
            } catch (error) {
                reject(error);
            }
        });
    }

    public createVerificationCodeModel(userID: string, newEmail: string) {
        return new Promise((resolve, reject) => {
            try {
                let model = new emailChange.model({
                    user: userID,
                    verificationCode: this.generateVerificationCode(),
                    email: newEmail
                });

                model.save(() => {
                    resolve(true);
                })

            } catch (error) {
                reject(error);
            }

        });
    }

    private generateVerificationCode() {
        return CryptoHash.randomHex(128);
    }

    public isEmailAvailable(email: string) {
        return new GetUser().by({ email: email }).then((resolve) => {
            if (resolve == null) return true;
            return false;
        })
    }


}