import userModel = require('../database/models/user.model');
import { I_ImageAction } from '../files/i_image-file-action';


export class UserProfileImageDispatcher implements I_ImageAction  {
    setImage(id: string, imageName: string): Promise<boolean> {
        return new Promise(resolve => {
            userModel.model.findByIdAndUpdate({ _id: id }, { $set: { profileImage: imageName } }).exec().then(doc => {
                resolve(true);
            }).catch(err => {
                resolve(false);
            })
        })
    }

    getImage(id: string) {
        try {
            return new Promise(resolve => {
                userModel.model.findById({ _id: id }).exec().then(doc => {
                    resolve(doc.profileImage);
                })
            })
        } catch (error) {
            return Promise.resolve(error);
        }
    }

}