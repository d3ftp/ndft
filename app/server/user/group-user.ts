import userModel = require('../database/models/user.model');
import { GroupDispatcher } from './group-dispatcher';
import { I_UserGroup } from '../database/models/i_user-group';

export class GroupUser {
    // public add(name: string) {
    //     userModel.model.findOneAndUpdate({name: name}).then()
    // }

    /**
     * set new groups
     */
    public set(userName: string, groups: I_UserGroup[]) {
        return userModel.model.findOneAndUpdate({ name: userName.toLocaleLowerCase() }, { $set: { groups: groups } });
    }
}