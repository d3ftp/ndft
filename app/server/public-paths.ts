import url = require('url');

export class StaticPaths {
    private static serverProfileImg: string = "http://localhost:3000";

    public static getMovieDbHeader() {
        return "https://image.tmdb.org/t/p/w1400_and_h450_face/";
    }

    public static getMovieDbPostarPath() {
        return "https://image.tmdb.org/t/p/w600_and_h900_bestv2/";
    }

    public static getTvShowHeader() {
        return url.resolve(this.serverProfileImg, 'uploads/tv-show-header/')
    }

    public static getPosterPath() {
        return url.resolve(this.serverProfileImg, 'uploads/poster/')
    }

    public static getTmpPath() {
        return url.resolve(this.serverProfileImg, 'uploads/tmp/')
    }

    public static getNoImgPath() {
        return url.resolve(this.serverProfileImg, '/no-img.png');
    }
}