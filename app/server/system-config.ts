import { I_SystemConfig } from "./database/models/i_system-config";
import systemConfigModel = require('./database/models/system-config.model');
import { I_UserGroup } from "./database/models/i_user-group";
import { I_NodemailerConfig } from "./database/models/i_nodemailer-config.model";
import { GroupDispatcher } from "./user/group-dispatcher";
export class SystemConfig {
    private static instance: SystemConfig = null;
    private config: I_SystemConfig;

    private constructor() { }

    public static init(): Promise<void> {
        return new Promise<void>(resolve => {
            SystemConfig.instance = new SystemConfig();
            systemConfigModel.model.findOne({}).populate('defaultGroupUser').then(doc => {
                if (doc == null) {
                    var model = new systemConfigModel.model({});
                    model.websiteTitle = 'default Title',
                    model.defaultUserGroup = null;
                    model.defaultGuestGroup = null;
                    model.emailConfirmTime = 0;
                    model.emailBlockedDomains = [];
                    model.save().then(() => {
                        SystemConfig.instance.config = model;
                        resolve();
                    })
                }
                else {
                    SystemConfig.instance.config = doc;
                    resolve();
                }
            })
        });
    }

    public static get(): SystemConfig {
        if (SystemConfig.instance.config == null) throw new Error("You need call SystemConfig.init() before get instance!");
        return SystemConfig.instance;
    }

    public setDafultUserGroup(id): Promise<I_SystemConfig> {
        return new Promise(resolve => {
            systemConfigModel.model.findOneAndUpdate({}, { $set: { defaultUserGroup: id } }).then((doc) => {
                resolve(doc);
            });
        })
    }

    public getDefaultUserGroup(): Promise<I_UserGroup> {
        return new Promise(resolve => {
            systemConfigModel.model.findOne({}).populate('defaultUserGroup').then(doc => {
                resolve(doc.defaultUserGroup);
            });
        })
    }

    public setDefaultGuestGroup(id: string): Promise<I_SystemConfig> {
        return new Promise(resolve => {
            systemConfigModel.model.findOneAndUpdate({}, { $set: { defaultGuestGroup: id } }).then((doc) => {
                resolve(doc);
            });
        })
    }

    public getDefaultGuestGroup(): Promise<I_UserGroup> {
        return new Promise(resolve => {
            systemConfigModel.model.findOne({}).populate('defaultGuestGroup').then(doc => {
                resolve(doc.defaultGuestGroup);
            });
        })
    }

    public getNodemailerConfig(): Promise<I_NodemailerConfig> {
        return new Promise(resolve => {
            systemConfigModel.model.findOne({}).then(doc => {
                resolve(doc.nodemailer);
            })
        });
    }


    public setNodemailerConfig(from: string, service: string, auth: { user: string, pass: string }): Promise<void> {
        this.config.nodemailer.auth = auth;
        this.config.nodemailer.from = from;
        this.config.nodemailer.service = service;
        return systemConfigModel.model.update({}, { $set: { nodemailer: { from: from, service: service, auth: auth } } }).exec();
    }

    public setWebsiteTitle(title: string) {
        return new Promise((resolve, reject) => {
            this.config.websiteTitle = title;
            systemConfigModel.model.findOneAndUpdate({}, { $set: { websiteTitle: title } }).then(res => {
                resolve(true);
            })
        })
    }

    public getWebsiteTitle() {
        return new Promise(resolve => {
            systemConfigModel.model.findOne({}).then(doc => {
                resolve(doc.websiteTitle);
            })
        })
    }

    public getEmailConfirmTime() {
        return new Promise(resolve => {
            systemConfigModel.model.findOne({}).then(doc => {
                resolve(doc.emailConfirmTime);
            })
        })
    }

    public setEmailConfirmTime(time: number) {
        return new Promise(resolve => {
            systemConfigModel.model.findOneAndUpdate({}, { $set: { emailConfirmTime: time } }).then(res => {
                resolve(true);
            })
        })
    }

    public getEmailBlockedDomains() {
        return new Promise(resolve => {
            systemConfigModel.model.findOne({}).then(doc => {
                resolve(doc.emailBlockedDomains);
            })
        })
    }

    public setEmailBlockedDomains(blockedDomainArray: string[]) {
        return new Promise(resolve => {
            systemConfigModel.model.findOneAndUpdate({}, { $set: { emailBlockedDomains: blockedDomainArray } }).then(res => {
                resolve(true);
            })
        })
    }

}
