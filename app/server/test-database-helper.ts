import { GroupDispatcher } from "./user/group-dispatcher";
import { SystemConfig } from "./system-config";
import { AddUser } from "./user/add-user";
import { GetUser } from "./user/get-user";
import { ActiveUser } from "./user/active-user";
import { GroupUser } from "./user/group-user";

export class TestDatabaseHelper {
    public static initializeDatabase() {
        return Promise.all([
            TestDatabaseHelper.createUserGroup().then(() => {
                TestDatabaseHelper.createUsers().then(() => {
                    TestDatabaseHelper.ActiveUsers();
                    TestDatabaseHelper.setGroup('test_0', 'user_');
                })
            }),
            TestDatabaseHelper.createEmailBlockedDomains(),
            TestDatabaseHelper.createNodemailerConfig()
        ]);
    }

    private static createUserGroup() {
        return Promise.all([
            GroupDispatcher.get().create('guest_', null),
            GroupDispatcher.get().create('user_', null),
            GroupDispatcher.get().create('superUser_', null),
            GroupDispatcher.get().create('admin_', null),
        ])
    }

    private static createEmailBlockedDomains() {
        return Promise.resolve(
            SystemConfig.get().setEmailBlockedDomains([
                'mail.ru',
                'cash.mail.com',
                'malinator.com',
                'spam.mail.pl',
                '123fast.ru'
            ])
        );
    }

    private static async ActiveUsers() {
        return Promise.all([
            new ActiveUser().by(await TestDatabaseHelper.returnVerifcationCode('test_0'))
        ])
    }

    private static setGroup(userName, groupName) {
        GroupDispatcher.get().getByName(groupName).then((group) => {
            new GroupUser().set(userName, [group]).then(() => {})
        })
    }

    private static async returnVerifcationCode(userName:string ) {
        let user = await new GetUser().by({ name: userName})
        return user.VerificationCode;
    }

    private static createUsers() {
        return Promise.all([
            new AddUser().add('test_0', 'test_0+++', 'test@test.pl', '2222222222')
        ])
    }

    private static createNodemailerConfig() {
        return Promise.all([
            SystemConfig.get().setNodemailerConfig('from-', 'service-', { pass: 'pass-', user: 'user-' })
        ])
    }
}