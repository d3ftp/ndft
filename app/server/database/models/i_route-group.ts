import { I_Route } from "./i_route";
import { I_UserGroup } from "./i_user-group";

export interface I_RouteGroup {
    _id:any,
    name: string,
    userGroups: I_UserGroup[],
    routes: I_Route[];
}