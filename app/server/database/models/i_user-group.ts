import { I_Route } from "./i_route";

export interface I_UserGroup {
    _id: any,
    name: string,
    routes: I_Route[]
}