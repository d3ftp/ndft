export interface I_SystemMessage {
    _id:any,
    className: string,
    subject: string,
    html: string,
    text: string
}