import mongoose = require("mongoose");
import { I_NodemailerConfig } from "./i_nodemailer-config.model";

export interface I_NodemailerConfigModel extends I_NodemailerConfig { }

var userSchema = new mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    from: String,
    service: String,
    auth: {
        user: String,
        pass: String
    }
});

var model = mongoose.model<I_NodemailerConfigModel & mongoose.Document >("NodemailerConfig", userSchema, "nodemailer_config");
export {model};