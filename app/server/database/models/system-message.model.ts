import mongoose = require("mongoose");
import { I_SystemMessage } from "./i_system-message";

export interface SystemMessageModel extends I_SystemMessage { }

var systemMessage = new mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    className: String,
    subject: String,
    html: String,
    text: String
});

var model = mongoose.model<I_SystemMessage & mongoose.Document>("SystemMessage", systemMessage, "system_message");

export {model};