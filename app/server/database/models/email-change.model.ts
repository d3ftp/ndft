import mongoose = require("mongoose");
import { I_EmailChange } from "./i_email-change";

export interface I_EmailChangeModel extends I_EmailChange, mongoose.Document { }
var userSchema = new mongoose.Schema({
        id: mongoose.Schema.Types.ObjectId,
        user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        email: String,
        verificationCode: String,
        expires: { type: Date, expires: 86400, default: Date.now }
});

var model = mongoose.model<I_EmailChangeModel & mongoose.Document >("EmailChange", userSchema, "email-change");
export {model};


