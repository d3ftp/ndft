import mongoose = require("mongoose");
export interface I_NodemailerConfig {
    from:string,
    service:string,
    auth: {
        user: string,
        pass: string
    }
}