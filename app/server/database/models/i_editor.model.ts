import { I_Editor } from "../../editor/i_editor";
import mongoose = require("mongoose");

export interface I_EditorModel extends I_Editor { }

var schema = new mongoose.Schema({
    creator: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
    modifications: [{
        date: Date,
        editor: { type: mongoose.Schema.Types.ObjectId, ref: "User" }
    }]
});

let model = mongoose.model<I_EditorModel & mongoose.Document>("Editor", schema, 'editor');
export { model }