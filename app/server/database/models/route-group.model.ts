import mongoose = require("mongoose");
import { I_UserGroup } from "./i_user-group";
import { I_RouteGroup } from "./i_route-group";

export interface I_RouteGroupModel extends mongoose.Document, I_RouteGroup {}

var schema = new mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    name: String,
    userGroups: [{ type: mongoose.Schema.Types.ObjectId, ref: 'UserGroup' }],
    routes: [{ type: mongoose.Schema.Types.ObjectId, ref: 'RouteModel' }]
});

var model = mongoose.model<I_RouteGroupModel>('RouteGroupModel', schema, 'route_group');
export {model}; 