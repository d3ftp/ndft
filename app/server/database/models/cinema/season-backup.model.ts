import mongoose = require("mongoose");
import { I_TvShowBackup } from "../../../cinema/backup/i_tv-show-backup";
import { I_BasicSeason } from "../../../cinema/I_basic-season";

export interface I_TvShowBackupModel extends I_TvShowBackup<I_BasicSeason>{ }

let schema = new mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    editor: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    previousData: {},
    expires: { type: Date, expires: 604800, default: Date },
});

let model = mongoose.model<I_TvShowBackup<I_BasicSeason> & mongoose.Document >("SeasonBackup", schema, "season_backup");
export { model }