import mongoose = require("mongoose");
import { I_CinemaList } from "../../../cinema/i_cinema-list";

let schema = new mongoose.Schema({
    userOwner: {type: mongoose.Schema.Types.ObjectId, ref: "User"},
    series: [{
        seriesID: Number,
        currentSeasonID: Number,
        currentEpisodeID: Number
    }]
});

var model = mongoose.model<I_CinemaList & mongoose.Document >("CinemaList", schema, "cinema-list");
export {model};