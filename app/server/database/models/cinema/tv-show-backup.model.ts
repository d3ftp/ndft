import mongoose = require("mongoose");
import { I_TvShowBackup } from "../../../cinema/backup/i_tv-show-backup";
import { I_BasicTv } from "../../../cinema/i_basic-tv";

export interface I_TvShowBackupModel extends I_TvShowBackup<I_BasicTv>{ }

let schema = new mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    editor: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    previousData: {},
    expires: { type: Date, expires: 604800, default: Date },
});

let model = mongoose.model<I_TvShowBackup<I_BasicTv> & mongoose.Document >("TvShowBackup", schema, "tv_show_backup");
export { model }