import mongoose = require("mongoose");
import { I_BasicSeason } from "../../../cinema/I_basic-season";

let schema = new mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    air_date: String,
    seasonID: Number,
    episodes: [{
      air_date: String,
      modified_backdrop_path: Boolean,
      episode_number: Number,
      name: String
    }]
});

let model = mongoose.model<I_BasicSeason & mongoose.Document>("Season", schema, "season");
export { model }