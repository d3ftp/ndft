import { I_BasicTv } from "../../../cinema/i_basic-tv";
import mongoose = require("mongoose");

export interface I_TvShowModel extends I_BasicTv { };


let schema = new mongoose.Schema({
  id: mongoose.Schema.Types.ObjectId,
  backdrop_path: String,
  modified_backdrop_path: { type: Boolean, default: false },
  first_air_date: String,
  tvID: Number,
  last_air_date: String,
  name: String,
  number_of_episodes: Number,
  number_of_seasons: Number,
  original_name: String,
  overview: String,
  poster_path: String,
  seasons: [
    {
      _id: false,
      air_date: String,
      episode_count: Number,
      id: Number,
      poster_path: String,
      modified_backdrop_path: { type: Boolean, default: false },
      has_internal_data: { type: Boolean, default: false },
      season_number: Number,
      name: String
    }
  ],
  vote_average: Number,
});

let model = mongoose.model<I_TvShowModel & mongoose.Document>("TvShow", schema, "tv_show");
export { model }