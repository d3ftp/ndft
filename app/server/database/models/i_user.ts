import mongoose = require("mongoose");
import { I_UserGroup } from "./i_user-group";
export interface I_User {
    _id: any,
    name: string,
    displayName: string,
    profileImage: string,
    password: string,
    salt: string,
    email: string,
    isActive: boolean,
    VerificationCode: string,
    groups: I_UserGroup[],
    expires: { type: Date, expires: 86400, default: Date }
}