import { I_User } from "./i_user";

export interface I_EmailChange {
    _id: any,
    user: I_User,
    email: string,
    verificationCode: string,
    expires: { type: Date, expires: 86400, default: Date }
}