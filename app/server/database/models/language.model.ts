import mongoose = require("mongoose");
import { I_Language } from "./i_language";

export interface I_LanguageModel extends I_Language { }

var userSchema = new mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    name: String
});

var model = mongoose.model<I_LanguageModel & mongoose.Document>("Language", userSchema, "language");
export { model }