import mongoose = require("mongoose");
import { I_SystemConfig } from "./i_system-config";

export interface I_SystemModel extends I_SystemConfig, mongoose.Document {}

var schema = new mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    defaultUserGroup: {type:mongoose.Schema.Types.ObjectId, ref: 'UserGroup'},
    defaultGuestGroup: {type:mongoose.Schema.Types.ObjectId, ref: 'UserGroup'},
    websiteTitle: String,
    nodemailer: {
        from:String,
        service:String,
        auth: {
            user: String,
            pass: String
        }
    },
    emailConfirmTime: Number,
    emailBlockedDomains: [String]
});

var model = mongoose.model<I_SystemModel>('SystemConfigModel', schema, 'system_config');

export {model};