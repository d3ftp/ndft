import { I_UserGroup } from "./i_user-group";
import { I_NodemailerConfig } from "./i_nodemailer-config.model";

export interface I_SystemConfig {
    _id: any,
    websiteTitle: string,
    defaultUserGroup: I_UserGroup,
    defaultGuestGroup: I_UserGroup,
    nodemailer: I_NodemailerConfig,
    emailConfirmTime: number,
    emailBlockedDomains: string[]
}