import mongoose = require("mongoose");
import { I_UserGroup } from "./i_user-group";
import { I_Route } from "./i_route";

export interface I_RouteModel extends mongoose.Document, I_Route {}

var schema = new mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    path: String,
    method: String
});

var model = mongoose.model<I_RouteModel>('RouteModel', schema, 'routes');
export {model};