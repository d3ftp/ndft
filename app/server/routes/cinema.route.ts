import { Router } from "express";
import { I_Route } from "../public/typescript/i_route";
import * as express from "express";
import * as fs from 'fs';
import { necessaryParam } from "./middleware/necessary-param";
import { I_Result } from "./i_result";
import { BasicTvDisaptcher } from "../cinema/basic-tv-dispatcher";
import { I_TvData } from "../../client/src/app/cinema/editor/data.service";
import { TvShowBackupDispatcher } from "../cinema/backup/tv-show-backup-dispatcher";
import { sessionUserExist } from "./middleware/session-user-exist";
import { UploadPath } from "../server";
import { I_BasicTv } from "../cinema/i_basic-tv";
import { FileDispatcher } from "../files/file-dispatcher";
import { I_Search } from "../cinema/i-search";
import { I_BasicSeason } from "../cinema/I_basic-season";
import { SeasonDispatcher } from "../cinema/season-dispatcher";

// TO DO:
// REad data
//* REFACTORY
//* change backup  session psuedo id to actual session
export class CinemaRoute implements I_Route {
    public router: Router;
    public uploadPath: UploadPath;
    public fileDispatcher: FileDispatcher;

    constructor(path: UploadPath) {
        this.router = express.Router();
        this.uploadPath = path;
        this.fileDispatcher = new FileDispatcher();
        this.create();
    }

    public create() {
        this.search();
        this.get();
        this.getSeason();
        this.update();
        this.updateSeason();
    }

    public get() {
        this.router.get("/api/cinema/tv/:tvID", async (req, res) => {
            try {
                let doc: any = await BasicTvDisaptcher.findOne(req.params.tvID);

                if (doc == null)
                    return res.send({ complete: false });

                res.send({ complete: true, data: doc });

            } catch (error) {
                res.status(500).send({ complete: false, message: error.message } as I_Result)
            }
        });
    }

    public getSeason() {
        this.router.get("/api/cinema/season/:seasonID", async (req, res) => {
            try {
                let doc: any = await SeasonDispatcher.find(req.params.seasonID);
                if (doc == null)
                    return res.status(404).send({ complete: false });

                res.send({ complete: true, data: doc });

            } catch (error) {
                res.status(500).send({ complete: false, message: error.message } as I_Result)
            }
        });
    }

    public updateSeason() {
        this.router.put("/api/cinema/seasons", async (req, res) => {
            try {
                console.log(req.body);
                let array: I_BasicSeason[] = req.body;
                let idArray = [];
                for (let season of array) {
                    
                    if (isNaN(season.seasonID)) throw Error('tvIDx must be number!');
                    if (season._id) {
                        let data = await SeasonDispatcher.find(season.seasonID);                       
                        await TvShowBackupDispatcher.createSeason("5b257b2909f6f56898643a0a", data as any);
                    }
    
                    let res = await SeasonDispatcher.isExist(season);

                    if (res) {
                        await SeasonDispatcher.update(season);
                    }
                    else {
                        //create 
                        let id = await SeasonDispatcher.create(season)
                        idArray.push({id:id, seasonID: season.seasonID});
                    }
                    
                }

                res.send({ complete: true, data: idArray});
            } catch (error) {
                res.status(500).send({ complete: false, message: error.message } as I_Result)
            }
        });
    }

    public search() {
        this.router.get("/api/cinema/search/tv/:name", async (req, res) => {
            try {
                let docs: any = await BasicTvDisaptcher.search(req.params.name, 10);
                let searchResults: I_Search[] = [];

                if (docs == null)
                    return res.send({ complete: true, data: searchResults });

                for (let doc of docs) {
                    searchResults.push({ id: doc.tvID, name: doc.name });
                }

                res.send({ complete: true, data: searchResults });

            } catch (error) {
                res.status(500).send({ complete: false, message: error.message } as I_Result)
            }
        })
    }

    public update() {
        this.router.put("/api/cinema/tv/:id", necessaryParam('tvID'), ((req, res, next) => {
            res.locals.routeClass = this;
            next();
        }),
            this._tvValidation,
            this._createTvBackup,
            this._updateTvDocOrCreate);
    }

    public _tvValidation(req, res, next) {
        try {
            let id: number = req.params.id;
            if (isNaN(id)) throw Error('id must be number!');

            let bodyData: I_TvData = req.body;
            if (isNaN(bodyData.tvID)) throw Error('tvID must be number!');
            if (bodyData.tvID != id) throw Error('TvID and Id conflict!');
            next();

        } catch (error) {
            console.log(error);

            res.status(500).send({ complete: false, message: error.message } as I_Result)
        }
    }

    public async _createTvBackup(req, res, next) {
        try {
            if (req.body._id) {
                let data = await BasicTvDisaptcher.findOne(req.params.id);
                await TvShowBackupDispatcher.create("5b257b2909f6f56898643a0a", data as any);
            }
            //await TvShowBackupDispatcher.create(req.session.currentUser.id, req.body);

            next();
        } catch (error) {
            console.log(error);
            res.status(500).send({ complete: false, message: error.message } as I_Result)
        }
    }

    public async _updateTvDocOrCreate(req, res) {
        try {
            let that: CinemaRoute = res.locals.routeClass;
            let bodyData: I_BasicTv = req.body;
            let id = null;
            let cinemaFileDispatcher = new CinemaFileDispatcher(that.uploadPath.tmp, that.uploadPath.tvShowHeaderPath);
            let cinemaFileDispatcherPoster = new CinemaFileDispatcher(that.uploadPath.tmp, that.uploadPath.posterPath);

            let result = await BasicTvDisaptcher.isExist(bodyData);

            if (result) {
                delete bodyData._id;
                let previousDoc: any = await BasicTvDisaptcher.update(bodyData);
                let oldFilePath = cinemaFileDispatcher.join(that.uploadPath.tvShowHeaderPath, previousDoc.backdrop_path);

                if (previousDoc.backdrop_path != bodyData.backdrop_path) {
                    that.moveToBackupDirectory(oldFilePath, cinemaFileDispatcher, previousDoc.backdrop_path);
                }

                for (let season of bodyData.seasons) {
                    let result = previousDoc.seasons.find(element => element.id == season.id);

                    if (result == undefined) continue;



                    if (season.poster_path != result.poster_path) {
                        let oldFilePosterPath = cinemaFileDispatcher.join(that.uploadPath.posterPath, result.poster_path);
                        that.moveToBackupDirectory(oldFilePosterPath, cinemaFileDispatcherPoster, result.poster_path);
                    }
                }

                id = bodyData._id;
            }
            else {
                id = await BasicTvDisaptcher.create(bodyData);
            }

            cinemaFileDispatcher.moveImageToAppropriatePath(bodyData.backdrop_path);
            for (let season of bodyData.seasons) {
                if (season.poster_path == null) continue;
                cinemaFileDispatcherPoster.moveImageToAppropriatePath(season.poster_path);
            }

            res.send({ complete: true, data: id } as I_Result)
        } catch (error) {
            console.log(error);
            res.status(500).send({ complete: false, message: error.message } as I_Result)
        }
    }

    private moveToBackupDirectory(oldFilePath: string, cinemaFileDispatcher: CinemaFileDispatcher, elementName: string) {
        if (fs.existsSync(oldFilePath)) {
            let dirName = cinemaFileDispatcher.createBackupDirectory();
            cinemaFileDispatcher.moveToBackupDirectory(dirName, elementName);
        }
    }

    private deleteFile(path: string, imageName: string) {
        let oldFileName = this.fileDispatcher.appendNameToPath(path, imageName as string)
        this.fileDispatcher.remove(oldFileName);
    }

}


export class CinemaFileDispatcher {
    private tmpDirPath: string;
    private imageDirPath: string;
    private fileDispatcher: FileDispatcher;

    constructor(tmpPath: string, imagePath: string) {
        this.tmpDirPath = tmpPath;
        this.imageDirPath = imagePath;

        this.fileDispatcher = new FileDispatcher();
    }

    public moveImageToAppropriatePath(imageName: string) {
        let currentPath = this.fileDispatcher.join(this.tmpDirPath, imageName);

        if (fs.existsSync(currentPath)) {
            let newPath = this.fileDispatcher.join(this.imageDirPath, imageName);
            this.fileDispatcher.moveTo(currentPath, newPath);

            return true;
        }

        return false;
    }

    public moveToBackupDirectory(dirName: string, imageName: string) {
        let oldfilePath = this.fileDispatcher.join(this.imageDirPath, imageName);
        let newFilePath = this.fileDispatcher.join(this.imageDirPath, dirName, imageName);

        this.fileDispatcher.moveTo(oldfilePath, newFilePath);
        return newFilePath;
    }

    /**
     * return dir Name
     */
    public createBackupDirectory() {
        let name = this.createDirDateName();
        let path = this.fileDispatcher.join(this.imageDirPath, name);

        if (!fs.existsSync(path)) {
            fs.mkdirSync(path);
        }
        return name;
    }

    private createDirDateName() {
        let date = new Date();

        let day = date.getDay();
        let month = date.getMonth();
        let year = date.getFullYear();
        let nameDir = day + '_' + month + '_' + year;

        return nameDir;
    }

    public join(...paths: string[]) {
        return this.fileDispatcher.join(...paths);
    }
}