import { I_Route } from "../public/typescript/i_route";
import { Router } from "express";
import * as express from "express";
import { I_Result } from "./i_result";
import { GetUser } from "../user/get-user";
import { sessionUserExist } from "./middleware/session-user-exist";
import { necessaryParam } from "./middleware/necessary-param";
import { UserEmail } from "../user/user-email";
import { SystemMailFactory, SystemMail } from "../nodemailer-module/system-mail-factory";
import { SystemLinks } from "../system-links";
import { EmailDispatcher } from "../nodemailer-module/email-dispatcher";
import { User } from "../user/user";
import { Syntax } from "../../client/src/app/src/helpers/syntax";


export class AccountRoute implements I_Route {
    public router: Router;

    public constructor() {
        this.router = express.Router();
        this.create();
    }

    public create() {
        this.getEmail();
        this.changeEmail();
        this.prepareEmailChange();
    }

    public getEmail() {
        this.router.get('/api/account/email', sessionUserExist(), async (req, res) => {
            try {
                let user = await this.getUserBasedOnSession(req);
                this.sendSuccessResponse(res, user.email)
            } catch (error) {
                this.sendFailStatusUnsupportedError(res, error);
            }
        });
    }

    public prepareEmailChange() {
        this.router.post('/api/account/prepare-email-change', [sessionUserExist(), necessaryParam('newEmail')], async (req, res) => {
            try {
                let newEmail: string =  req.body.newEmail;
                let user = this.getSessionUser(req);
                let userEmail = new UserEmail();

                if (!new Syntax().isEmailAddress(newEmail)) return this.sendFailStatusUnsupportedError(res, new Error('wrong email syntax'));
                
                await this.throwErrorWhenEmailBusy(newEmail);
                await userEmail.createVerificationCodeModel(user.id, newEmail)

                this.sendVerificationEmailChangeCode(newEmail);
                this.sendSimpleSuccessResponse(res);
            } catch (error) {
                this.sendFailStatusUnsupportedError(res, error);
            }
        });
    }

    private sendVerificationEmailChangeCode(newEmail: string) {
        let email = new SystemMailFactory().get(SystemMail.CHANGE_EMAIL);
        email.setLink(SystemLinks.verificationEmailChange);
        let emailDispatcher = new EmailDispatcher();
        emailDispatcher.sendMessage(email, newEmail)
    }

    private async throwErrorWhenEmailBusy(newEmail) {
        if (!await new UserEmail().isEmailAvailable(newEmail)) {
            throw Error('given email exist already');
        }
    }

    public changeEmail() {
        this.router.post('/api/account/email', [necessaryParam('verificationCode')], async (req, res) => {            
            try {
                let userEmail = new UserEmail();
                let verificationCode = req.body.verificationCode

                await userEmail.changeEmail(verificationCode);
                await userEmail.removeVerificationCode(verificationCode);
                
                this.sendSimpleSuccessResponse(res);
            } catch (error) {
                this.sendFailStatusUnsupportedError(res, error);
            }
        });
    }

    public getUserBasedOnSession(req) {
        return new GetUser().by({ _id: req.session.currentUser.id });
    }

    public getSessionUser(req) {
        return req.session.currentUser as User;
    }

    public sendSuccessResponse(res, data: any) {
        res.send({ complete: true, data: data } as I_Result)
    }

    public sendSimpleSuccessResponse(res) {
        res.send({ complete: true } as I_Result)
    }

    public sendFailStatusUnsupportedError(res, err: Error) {
        res.status(500).send({ complete: false, message: err.message } as I_Result)
    }



}