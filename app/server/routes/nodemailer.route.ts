import { I_Route } from "../public/typescript/i_route";
import { Router } from "express";
import * as express from "express";
import { I_Result } from "./i_result";
import { RegisterMail } from "../nodemailer-module/components/register-mail";
import { EmailDispatcher } from "../nodemailer-module/email-dispatcher";
import { SystemMailFactory, SystemMail } from "../nodemailer-module/system-mail-factory";


export class NodemailerRoute implements I_Route {
    public router: Router;
    public emailDispatcher: EmailDispatcher;
    public systemMailFactory: SystemMailFactory;

    public constructor() {
        this.router = express.Router();
        this.emailDispatcher = new EmailDispatcher();
        this.systemMailFactory = new SystemMailFactory();
        this.create();
    }

    public create() {
        this.getMail();
        this.putMail();
    }

    private getMail() {
        this.router.get('/api/nodemailer/system-mail/:mailKey', (req, res) => {
            let mailInstance = this.systemMailFactory.get(SystemMailFactory.toSystemMail(req.params.mailKey));
            if (mailInstance == null) return res.send({ complete: false, message: 'System type mesage is not defined' } as I_Result);
            res.send({ complete: true, data: mailInstance } as I_Result);
        });
    }

    private putMail() {
        this.router.put('/api/nodemailer/system-mail', (req, res) => {
            let subject = req.body.subject;
            let html = req.body.html;
            let mailKey = req.body.mailKey;

            if (this.sendErrorIfValueUndefined(subject, 'subject is undefined', res)) {
                return;
            }
            if (this.sendErrorIfValueUndefined(html, 'html is undefined', res)) {
                return;
            }
            if(this.sendErrorIfValueUndefined(mailKey, 'mailKey is undefined', res)) {
                return;
            }

            let mail = this.systemMailFactory.get(SystemMailFactory.toSystemMail(mailKey));

            this.emailDispatcher.updateMailMessage(mail, subject, html).then((doc) => {
                res.send({ complete: true } as I_Result);
            });
        });
    }

    private sendErrorIfValueUndefined(val, msg, res) {
        if (val == undefined) {
            res.send({ complete: false, message: msg} as I_Result)
            return true;
        }
        return false;
    }
}