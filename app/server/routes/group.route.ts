import { NextFunction, Request, Response, Router } from "express";
import { I_Route } from "../public/typescript/i_route";
import * as express from "express";
import { GroupDispatcher } from "../user/group-dispatcher";

export class GroupRoute implements I_Route {
    router: Router;

    constructor() {
        this.router = express.Router();
        this.create();
    }

    public create() {
        this.add();
        this.getAll();
        this.update();
        this.remove();

    }

    /**
     * add group with specified permission to database
     */

    public add() {
        this.router.post('/api/group', (req, res, next) => {
            var groupDispatcher = GroupDispatcher.get();

            var resposne = this.checkSyntax(req.body.name, req.body.routes);
            if (resposne) return res.send(resposne);

            groupDispatcher.isExist(req.body.name).then(resolve => {
                if (resolve == true) return res.send({ Complete: false, message: 'Name of this group exist in databse' });

                groupDispatcher.create(req.body.name, req.body.routes as string[]).then(result => {
                    if (result) return res.send({ Complete: true, message: '' });
                    return res.send({ Complete: false, message: 'Ops, something goes wrong :( !' });
                });
            });
        });
    }

    /**
     * return all available groups
     */

    public getAll() {
        this.router.get("/api/group", (req, res, next) => {
            var groupDispatcher = GroupDispatcher.get();

            groupDispatcher.getAllAndPopulate().then(result => {
                res.send(result);
            });
        });
    }

    /**
     * uppdate group, replace old permissions table with new
     */
    public update() {
        this.router.put('/api/group', (req, res, next) => {
            var groupDispatcher = GroupDispatcher.get();
            var id = req.body.id;
            var newName = req.body.newName;
            var permissions = req.body.routes;

            if (id === undefined) {
                return res.send({ Complete: false, message: 'id is undefined ' });
            }
            if (permissions === undefined && newName === undefined) {
                return res.send({ Complete: false, message: 'routes or newName for group must be defined' });
            }
            if (newName == "") {
                return res.send({ Complete: false, message: 'new name cant be empty' });
            }

            groupDispatcher.get(id).then((doc) => {
                if (doc.name == newName) {
                    doc.routes = permissions;
                    (<any>doc).save();
                    return res.send({ Complete: true });
                }
                else {
                    groupDispatcher.isExist(newName).then(result => {
                        if (result == false) {
                            doc.name = newName;
                            doc.routes = permissions;
                            (<any>doc).save();
                            return res.send({ Complete: true });
                        }
                        return res.send({ Complete: false, message: 'Name is used' });
                    })
                }
            }).catch(err => {
                return res.send({ Complete: false, message: 'cant update, err: ' + err });
            })
        });
    }

    /**
     * remove group
     */

    public remove() {
        this.router.delete('/api/group/:id', (req, res, next) => {
            var groupDispatcher = GroupDispatcher.get();
            var id = req.params.id;

            if (id === undefined) return { Complete: false, message: 'name is undefined ' };

            groupDispatcher.remove(id).then(resolve => {
                if (resolve) return res.send({ Complete: false, message: 'Ops, something goes wrong :( !' });
                return res.send({ Complete: true });
            });

        });
    }

    /**
     * check is request has defined in body name and permissions[]
     * @param name 
     * @param permissions 
     */

    private checkSyntax(name: string, permissions: any): { Complete: boolean, message: string } {
        try {
            permissions = permissions as string[];
        }
        catch (err) {
            return { Complete: false, message: err };
        }

        if (name === undefined) return { Complete: false, message: 'name is undefined ' };
        if (permissions === undefined) return { Complete: false, message: 'permission is undefined' };
        return null;
    }

}