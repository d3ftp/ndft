import { I_Route } from "../public/typescript/i_route";
import { Router } from "express";
import * as express from "express";
import { User } from "../user/user";
import { I_Result } from "./i_result";


export class ProfileRoute implements I_Route {
    public router: Router;

    public constructor() {
        this.router = express.Router();
        this.create();
    }

    public create() {
        this.getPermissions();
    }

    private getPermissions() {
        this.router.get('/api/profile/permissions', (req, res) => {
            
        });
    }

}