import { I_Route } from "../public/typescript/i_route";
import { Router } from "express";
import * as express from "express";
import { User } from "../user/user";
import { I_Result } from "./i_result";

interface SessionExist {
    exist: boolean;
    user: User;
}

export class SessionRoute implements I_Route {
    public router: Router;

    public constructor() {
        this.router = express.Router();
        this.create();
    }

    public create() {
        this.getSession();
    }

    /**
     * return current session or return new when not exist
     */

    private getSession() {
        this.router.get('/api/session/current', (req: any, res, next) => {
            let session = req.session;
            let resData: I_Result;
            if (session.currentUser === undefined) {
                resData = { complete: false, message: "No Logged" }
                return res.send(resData);
            }

            let groupsName: string[] = [];
            if (req.session.currentUser.groups.length <= 0) {
                resData = { complete: false, message: "System Error, account dont has assigned group!" }
                res.send(resData);
                throw Error("User Dont have Group!")
            }

            for (let group of req.session.currentUser.groups) {
                groupsName.push(group);
            }
            
            resData = {
                complete: true, data: {
                    session: {
                        user: {
                            name: session.currentUser.name,
                            groups: groupsName,
                            imgPath: session.currentUser.profileImagePath
                        }
                    }
                }
            }
            res.send(resData);
        });
    }


}