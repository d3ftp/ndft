import { Request } from "express";

export function necessaryParam(...params) {
    return (req: Request, res, next) => {
        for (let param of params) {
            if (req.body[param] === undefined) return callError(next)
        }
        next();
    }

    function callError(next) {
        next(new Error(`params: ${params.toString()} are requied!`))
    }
}
