import { Request } from "express";

export function sessionUserExist() {
    return (req: Request, res, next) => {
        if (req.session == undefined) return callError(next, 'Session is undefined!');
        if (req.session.currentUser == undefined) return callError(next, 'Session.User is undefined!');
        next();
    }

    function callError(next, msg) {
        next(new Error(msg))
    }
}
