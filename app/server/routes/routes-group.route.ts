import { NextFunction, Request, Response, Router } from "express";
import { I_Route } from "../public/typescript/i_route";
import * as express from "express";
import { RouteDispatcher } from "../permission/route-dispatcher";
import { RouteGroupDispatcher } from "../permission/route-group-dispatcher";
import { I_Result } from "./i_result";

export class RoutesGroupRoute implements I_Route {
    router: Router;
    private routeGroupDispatcher: RouteGroupDispatcher;

    constructor() {
        this.routeGroupDispatcher = new RouteGroupDispatcher();

        this.router = express.Router();
        this.create();
    }

    public create() {
        this.getAll();
        this.add();
        this.update();
        this.remove();
        this.updateUserGroup();
    }

    public getAll() {
        this.router.get("/api/routes-group-all/:populate", (req, res) => {
            var result: I_Result;
            if (req.params.populate == "true") {
                this.routeGroupDispatcher.getAllPopulated().then(docs => {
                    result = { complete: true, message: "Success get all data", data: docs }
                    return res.send(result);
                }).catch(err => {
                    result = { complete: false, message: err }
                    return res.send(result);
                });
            }
            else {
                this.routeGroupDispatcher.getAll().then(docs => {
                    result = { complete: true, message: "Success get all data", data: docs }
                    return res.send(result);
                }).catch(err => {
                    result = { complete: false, message: err }
                    return res.send(result);
                });
            }
        });
    }

    public add() {
        this.router.post("/api/routes-group", (req, res) => {
            var result: I_Result;
            if (req.body.name === undefined) {
                result = { complete: false, message: "Name must be defined" };
                return res.send(result);
            }
            if (req.body.routes === undefined) {
                result = { complete: false, message: "Routes must be defined" };
                return res.send(result);
            }

            this.routeGroupDispatcher.add(req.body.name, req.body.routes).then((result) => {
                result= { complete: true, message: "Success add data", data: result};
                return res.send(result);
            }).catch(err => {
                result = { complete: false, message: err };
                return res.send(result);
            })
        });
    }

    public update() {
        this.router.put("/api/routes-group", (req, res) => {
            var result: I_Result;
            if (req.body.id === undefined || req.body.newName === undefined) {
                result = { complete: false, message: "id and newName must be defined" };
                return res.send(result);
            }
            if (req.body.routes === undefined) {
                result = { complete: false, message: "Routes must be defined" };
                return res.send(result);
            }

            this.routeGroupDispatcher.update(req.body.id, req.body.newName, req.body.routes).then(() => {
                result = { complete: true, message: "Success update data" };
                return res.send(result);
            }).catch(err => {
                result = { complete: false, message: err };
                return res.send(result);
            })
        });
    }

    public updateUserGroup() {
        this.router.put("/api/routes-group-append-user-group", (req,res) => {
            var result: I_Result;
            if(req.body.id === undefined) {
                result = { complete: false, message: "id and must be defined" };
                return res.send(result);
            }
            if(req.body.userGroupId === undefined) {
                result = { complete: false, message: "userGroup be defined" };
                return res.send(result);
            }

            if(req.body.userGroupId == "") {
                this.routeGroupDispatcher.unsetUserGroup(req.body.id, req.body.userGroupId).then(() => {
                    result = { complete: true, message: "Success update data" };
                    return res.send(result);
                }).catch(err => {
                    result = { complete: false, message: err };
                    return res.send(result);
                })
            }

            this.routeGroupDispatcher.appendUserGroup(req.body.id, req.body.userGroupId).then(() => {
                result = { complete: true, message: "Success update data" };
                return res.send(result);
            }).catch(err => {
                result = { complete: false, message: err };
                return res.send(result);
            })
        });
    }


    public remove() {
        this.router.delete('/api/routes-group/:id', (req, res, next) => {
            var result: I_Result;

            var id = req.params.id;

            if (id === undefined) {
                result = { complete: false, message: "id must be defined" };
                return res.send(result);
            }

            this.routeGroupDispatcher.delete(id).then(() => {
                result = { complete: true, message: "Success delete data" };
                return res.send(result);
            }).catch((err) => {
                result = { complete: false, message: err };
                return res.send(result);
            });

        });
    }
}