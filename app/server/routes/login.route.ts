import { Router, NextFunction, Request, Response } from "express";
import { I_Route } from "../public/typescript/i_route";
import { SyntaxVerification } from "../syntax-verification/syntax-verification";
import * as express from "express";
import { User } from "../user/user";
import { GetUser } from "../user/get-user";
import { I_Result } from "./i_result";
import { CryptoHash } from "../crypto/crypto-hash";
import userModel = require('../database/models/user.model');
import { GroupDispatcher } from "../user/group-dispatcher";
import * as ExpressSession from 'express-session';
import { I_GetImageName } from "../files/i_get-image-name";
import { UserProfileImageDispatcher } from "../user/user-profile-image-dispatcher";
import * as path from "path";
import { UploadPath } from "../server";

export class LoginRoute implements I_Route {
    router: Router;

    constructor(private uploadPath: UploadPath) {
        this.router = express.Router();
        this.create();
    }

    public create() {
        this.router.get('/api/login/:login/:password', (req: any, res, next) => {
            let login = req.params.login.toLocaleLowerCase();
            let password = req.params.password;

            let query = this.createQuery(login);
            new GetUser().by(query).then((doc) => {
                if (doc == null || doc == undefined) return this.sendAfterFailLogin(res);

                CryptoHash.hashPassword(password, doc.salt).then(async result => {
                    if (result.derivedKey == doc.password) {
                        if (doc.isActive == false) {
                            return this.sendAfterNotActive(res);
                        }
                        let resultLogIn = await this.loginIn((<any>req).session, res, doc);

                        return res.send(resultLogIn);
                    }
                    return this.sendAfterFailLogin(res);
                });
            });
        });
    }

    private createQuery(value: string): any {
        let syntax = new SyntaxVerification();

        if (syntax.isEmailAddress(value)) {
            return { email: value };
        }
        else if (value != '' || value != undefined) {
            return { name: value };
        }
    }

    private sendAfterFailLogin(res: Response) {
        let result: I_Result = { complete: false, message: 'Wrong name or password', data: { error: LoginError.WRONG_NAME_PASSWORD } };
        res.send(result);
    }

    private sendAfterNotActive(res: Response) {
        let result: I_Result = { complete: false, message: 'Account not active', data: { error: LoginError.ACCOUNT_NOT_ACTIVE } };
        res.send(result);
    }

    private async loginIn(session: any, res: Response, doc: userModel.I_UserModel) {
        let user = new User(doc._id, doc.name, doc.groups);
        let imagePath: string;

        let imageName = await this.isImageDefined(user.id, new UserProfileImageDispatcher);

        if (imagePath == undefined) user.profileImagePath = "";
        else {
            imagePath = path.join(this.uploadPath.serverProfileImgPath, imageName)
            user.profileImagePath = imagePath;
        }
        session.currentUser = user;

        return { complete: true, data: { user: { name: user.name, groups: user.groups, profilePath: user.profileImagePath } } };
    }

    private async isImageDefined(id, imageGetter: I_GetImageName) {
        let result = await imageGetter.getImage(id);
        return (result == undefined) ? undefined : result;
    }

}

//use by login component
export enum LoginError {
    WRONG_NAME_PASSWORD,
    ACCOUNT_NOT_ACTIVE
}


