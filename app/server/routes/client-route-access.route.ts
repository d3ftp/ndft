import { I_Route } from "../public/typescript/i_route";
import { Router } from "express";
import * as express from "express";
import { I_Result } from "./i_result";


export class ClientRouteAccess implements I_Route {
    public router: Router;

    public constructor() {
        this.router = express.Router();
        this.create();
    }

    public create() {
        this.getAdminConfigRouteAccess();
    }

    private getAdminConfigRouteAccess() {
        this.router.get('/api/access-control-list-client/system_config', (req, res) => {
            res.send({complete: true} as I_Result);
        });
    }
}