import { Router } from "express";
import { I_Route } from "../public/typescript/i_route";
import * as express from "express";
import { sessionUserExist } from "./middleware/session-user-exist";
import { CinemaListDispatcher } from "../cinema/cinema-list-dispatcher";
import { I_Result } from "./i_result";
import { GetUser } from "../user/get-user";




export class ListRoute implements I_Route {
    public router: Router;
    private listDispatcher: CinemaListDispatcher;

    constructor() {
        this.router = express.Router();
        this.listDispatcher = new CinemaListDispatcher();
        this.create();
    }

    public create() {
        // this.getList();
        this.putItemToList();

    }

    // private getList() {
    //     this.router.get("/api/series-list", sessionUserExist, (async (req, res, next) => {
    //         try {
    //             let doc = await this.listDispatcher.getList(req.session.currentUser.id);
    //             res.send({ complete: true, data: doc } as I_Result);
    //         } catch (error) {
    //             res.status(404).send({ complete: false, message: error.message } as I_Result);
    //         }
    //     }))
    // }

    public putItemToList() {
        this.router.put("/api/series-list/tv/:tvID/season/:seasonID/episode/:episodeID",  async (req,res,next) => {
            try {  
                let tvID = req.params.tvID;
                let seasonID = req.params.seasonID;
                let episodeID = req.params.episodeID;
                
                let x = await new GetUser().by({name: 'test'});
                await this.listDispatcher.putItem(x._id, tvID, seasonID, episodeID);
                res.send({ complete: true } as I_Result);

            } catch (error) {
                res.status(404).send({ complete: false, message: error } as I_Result);
            }
        });
    }


}
