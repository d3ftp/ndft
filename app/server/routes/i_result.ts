export interface I_Result {
    complete: boolean,
    message?: string,
    data?: any;
}