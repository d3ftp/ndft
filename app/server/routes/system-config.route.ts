import { NextFunction, Request, Response, Router } from "express";
import { Database } from '../database/database';
import { I_Route } from "../public/typescript/i_route";
import * as express from "express";
import { RoutePermission } from "../permission/route-permission";
import { I_Result } from "./i_result";
import { SystemConfig } from "../system-config";
import { GroupDispatcher } from "../user/group-dispatcher";
import { serveStatic } from "serve-static";

export class SystemConfigRoute implements I_Route {
    public api: string = '/api/current-user';
    public router: Router;

    constructor() {
        this.router = express.Router();

        this.putWebsiteTitle();
        this.getTitle();

        this.putBlockedDomains();
        this.getBlockedDomains();

        this.getDefaultUserGroup();
        this.putDefaultUserGroup();

        this.getDefaultGuestGroup();
        this.putDefaultGuestGroup();

        this.putEmailConfirmTime();
        this.getEmailConfirmTime();

        this.putNodemailerConfig();
        this.getNodemailerConfig();
    }


    private putWebsiteTitle() {
        this.router.put('/api/system-config/layout', (req, res) => {
            let title = req.body.websiteTitle

            if (this.checkIsDefined(title, res)) {
                this.setWebsiteTitle(title, res);
            }
        });
    }

    private checkIsDefined(websiteTitle, res): boolean {
        if (websiteTitle === undefined) {
            res.send({ complete: false, message: 'websiteTitle is undefined' } as I_Result);
            return false;
        }
        return true;
    }

    private setWebsiteTitle(title, res) {
        let promise = SystemConfig.get().setWebsiteTitle(title);

        promise.then(() => {
            res.send({ complete: true } as I_Result);
        }).catch((err) => {
            res.send({ complete: false, message: err } as I_Result);
        })
    }

    private getTitle() {
        this.router.get('/api/system-config/layout', (req, res) => {
            SystemConfig.get().getWebsiteTitle().then(title => {
                res.send({ complete: true, data: title } as I_Result)
            })
        })
    }

    private getDefaultUserGroup() {
        this.router.get('/api/system-config/default-user-group', (req, res) => {
            SystemConfig.get().getDefaultUserGroup().then(group => {
               return res.send({ complete: true, data: group } as I_Result);
            })
        });
    }

    private putDefaultUserGroup() {
        this.router.put('/api/system-config/default-user-group', (req, res) => {
            let id = req.body.defaultUserGroupId;

            let isUndefined = this.sendFailValueIsUndefined(id, 'defaultUserGroupId is not defined', res);
            if (isUndefined) {
                return true;
            }

            GroupDispatcher.get().get(id).then(result => {
                if (result == null) {
                    res.send({ complete: false, message: 'Selected user does not exist' } as I_Result)
                    return;
                }
                SystemConfig.get().setDafultUserGroup(id).then(() => {
                    res.send({ complete: true } as I_Result)
                    return;
                })
            })

        });
    }

    private getDefaultGuestGroup() {
        this.router.get('/api/system-config/default-guest-group', (req, res) => {            
            SystemConfig.get().getDefaultGuestGroup().then(default_guest_group => {
                if(default_guest_group == null) return res.send({ complete: false, message: 'Default guest group is not set' } as I_Result);
                return res.send({ complete: true, data: default_guest_group } as I_Result)
            });
        });
    }


    private putDefaultGuestGroup() {
        this.router.put('/api/system-config/default-guest-group', (req, res) => {
            let id = req.body.defaultGuestGroupId;

            let isUndefined = this.sendFailValueIsUndefined(id, 'defaultGuestGroupId is not defined', res);
            if (isUndefined) {
                return true;
            }

            GroupDispatcher.get().get(id).then(result => {
                if (result == null) {
                    res.send({ complete: false, message: 'Selected group does not exist' } as I_Result)
                    return;
                }
                SystemConfig.get().setDefaultGuestGroup(id).then(() => {
                    res.send({ complete: true } as I_Result)
                    return;
                })
            })
        })
    }

    private putEmailConfirmTime() {
        this.router.put('/api/system-config/email-confirm-time', (req, res) => {
            let emailConfirmTime = req.body.emailConfirmTime;

            let isUndefined = this.sendFailValueIsUndefined(emailConfirmTime, 'emailConfirmTime is not defined', res);
            if (isUndefined) {
                return;
            }

            let isNotNumber = this.sendFailValueIsNotNumber(emailConfirmTime, 'emailConfirmTime must be number', res);
            if (isNotNumber) {
                return;
            }

            this.setEmailConfirmTimeAndSendRes(emailConfirmTime, res);
        })
    }

    private sendFailValueIsNotNumber(value, msg, res): boolean {
        if (!Number.isInteger(value)) {
            res.send({ complete: false, message: msg } as I_Result)
            return true
        }
    }

    private setEmailConfirmTimeAndSendRes(value, res) {
        SystemConfig.get().setEmailConfirmTime(value).then(() => {
            res.send({ complete: true } as I_Result)
            return;
        })
    }

    private getEmailConfirmTime() {
        this.router.get('/api/system-config/email-confirm-time', (req, res) => {
            SystemConfig.get().getEmailConfirmTime().then(time => {
                res.send({ complete: true, data: time })
                return;
            });
        });
    }

    private putBlockedDomains() {
        this.router.put('/api/system-config/blocked-domains', (req, res) => {
            let blockedDomains: string[] = req.body.blockedDomains;

            let isUndefined = this.sendFailValueIsUndefined(blockedDomains, 'blockedDomains is not defined', res);
            if (isUndefined) {
                return;
            }

            let letisLessThanOne = this.sendFailValueLessThanOne(blockedDomains, 'blockedDomains cant be empty', res);
            if (letisLessThanOne) {
                return;
            }

            this.setBlockedDomainsSendRes(blockedDomains, res);
        })
    }

    private sendFailValueLessThanOne(value: any[], msg, res): boolean {
        if (value.length < 1) {
            res.send({ complete: false, message: msg } as I_Result)
            return true;
        }
        return false;
    }

    private setBlockedDomainsSendRes(value, res) {
        SystemConfig.get().setEmailBlockedDomains(value).then(() => {
            res.send({ complete: true } as I_Result)
            return;
        });
    }

    private getBlockedDomains() {
        this.router.get('/api/system-config/blocked-domains', (req, res) => {
            SystemConfig.get().getEmailBlockedDomains().then(blockedDomains => {
                res.send({ complete: true, data: blockedDomains } as I_Result)
                return;
            })
        })
    }

    private sendFailValueIsUndefined(value, msg, res): boolean {
        if (value === undefined) {
            res.send({ complete: false, message: msg } as I_Result)
            return true;
        }
        return false;
    }

    public getNodemailerConfig() {
        this.router.get('/api/system-config/nodemailer-config', (req, res) => {
            SystemConfig.get().getNodemailerConfig().then(nodemailer => {
                res.send({ complete: true, data: nodemailer } as I_Result);
                return;
            });
        })
    }

    public putNodemailerConfig() {
        this.router.put('/api/system-config/nodemailer-config', (req, res) => {
            let from = req.body.from;
            let service = req.body.service;
            let auth = req.body.auth;

            if (this.sendFailValueIsUndefined(from, 'from value is undefined', res))
                return;
            if (this.sendFailValueIsUndefined(service, 'service value is undefined', res))
                return;
            if (this.sendFailValueIsUndefined(auth, 'auth value is undefined', res))
                return;

            SystemConfig.get().setNodemailerConfig(from, service, auth).then(() => {
                res.send({ complete: true } as I_Result);
                return;
            });
        });
    }

}