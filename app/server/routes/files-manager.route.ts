import { I_Route } from "../public/typescript/i_route";
import { Router } from "express";
import * as express from "express";
import * as formidable from 'formidable';
import { User } from "../user/user";
import { I_Result } from "./i_result";
import * as path from "path";
const url = require('url');
import * as fs from 'fs';
import { UploadPath } from "../server";
import { IsImage } from "../src/is-image";
import * as uuidv4 from 'uuid/v4';
import { FileDispatcher } from "../files/file-dispatcher";
import * as multer from 'multer'
import { UserProfileImageDispatcher } from "../user/user-profile-image-dispatcher";
import { GetUser } from "../user/get-user";
import { I_GetImageName } from "../files/i_get-image-name";

const imageFilter = function (req, file, cb) {
    // accept image only
    if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
        return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
};

export class FilesManagerRoute implements I_Route {
    public router: Router;
    private uploadPath: UploadPath;
    private multerProfileImage;
    private fileDispatcher: FileDispatcher;

    public constructor(uploadPath: UploadPath) {
        this.router = express.Router();
        this.fileDispatcher = new FileDispatcher();
        this.uploadPath = uploadPath;
        this.multerProfileImage = multer({ dest: this.uploadPath.tmp, limits: { fileSize: 2097152 }, fileFilter: imageFilter });
        this.create();
    }

    public create() {
        this.uploadUserProfile();
        this.downloadCurrentUserProfile();
        this.uploadTvShowHeader();
    }

    private uploadTvShowHeader() {
        this.router.post("/api/file/tv-show-header", this.multerProfileImage.single('image'), async (req, res) => {
            try {
                let newFileName = this.generateRandomNameForFile(req.file.originalname);
                let newPath = this.fileDispatcher.appendNameToPath(this.uploadPath.tmp, newFileName);
                await this.fileDispatcher.moveTo(req.file.path, newPath);
                this.getFile(newPath, 100, res, newFileName);
            } catch (error) {
                res.send({ complete: false, message: error.message } as I_Result);
            }
        });
    }

    // Wait for file to exist, checks every 2 seconds
public getFile(path, timeout, res, newFileName) {
    const timeoutx = setInterval(function() {

        const file = path;
        const fileExists = fs.existsSync(file);
        
        if (fileExists) {
            
            res.send({ complete: true, data: newFileName } as I_Result)
            clearInterval(timeoutx);
        }
    }, timeout);
};

    private uploadUserProfile() {
        this.router.post('/api/file/user-profile', this.multerProfileImage.single('image'), async (req, res) => {
            this.uploadProfileOrSendError(req, res);
        });
    }

    private async _uploadUserProfile(req, res) {
        let newFileName = this.generateRandomNameForFile(req.file.originalname);
        let newPath = this.fileDispatcher.appendNameToPath(this.uploadPath.profileImgPath, newFileName);
        this.fileDispatcher.moveTo(req.file.path, newPath);

        let user = await this.getCurrentUserDoc(req);
        req.session.currentUser.profileImagePath = url.resolve(this.uploadPath.serverProfileImgPath, newFileName);

        this.isImageDefined(user._id, new UserProfileImageDispatcher()).then(imageName => {
            if (imageName != undefined) {
                this.deleteFile(this.uploadPath.profileImgPath, imageName);
            }
            this.addImageToSessionUser(user._id, newFileName);

            res.send({ complete: true } as I_Result)
        })
    }

    private async uploadProfileOrSendError(req, res) {
        try {
            await this._uploadUserProfile(req, res);
        } catch (error) {
            res.status(400).send({ complete: false, message: error.toString() } as I_Result)
        }
    }


    private downloadCurrentUserProfile() {
        this.router.get('/api/file/user-profile', async (req, res) => {
            let user: User = req.session.currentUser;
            this.isImageDefined(user.id, new UserProfileImageDispatcher).then(imageName => {
                if (imageName == undefined) return res.status(404).send({ complete: false, message: 'No Image' } as I_Result)
                this.fileDispatcher.createReadStream(this.uploadPath.profileImgPath, imageName).pipe(res);
            })
        });
    }

    private generateRandomNameForFile(originalName: string) {
        let extension = this.fileDispatcher.getExtension(originalName)
        return this.fileDispatcher.generateRandomName(extension);
    }

    private deleteFile(path: string, imageName: string) {
        let oldFileName = this.fileDispatcher.appendNameToPath(path, imageName as string)
        this.fileDispatcher.remove(oldFileName);
    }

    private async getCurrentUserDoc(req) {
        let user: User = req.session.currentUser;
        return await new GetUser().by({ _id: user.id });
    }

    private async isImageDefined(id, imageGetter: I_GetImageName) {
        let result = await imageGetter.getImage(id);
        return (result == undefined) ? undefined : result;
    }


    private addImageToSessionUser(userID, imageName) {
        new UserProfileImageDispatcher().setImage(userID, imageName);
    }
}
