import { RoutePermission } from "../permission/route-permission";
import { I_Route } from "../public/typescript/i_route";
import { Router } from "express";
import * as express from "express";
import { ActiveUser } from "../user/active-user";
import { I_Result } from "./i_result";
import { GetUser } from "../user/get-user";

export class AccountVerificationRoute implements I_Route {
    router: Router;

    constructor() {
        this.router = express.Router();
        this.verifiationRegisterCode();
    }

    private verifiationRegisterCode() {
        this.router.get('/api/verification-register-code/:name/:code', (req, res, next) => {

            new GetUser().by({ name: req.params.name }).then(doc => {
                if (doc == null) return this.userDontExist(res);
                if (doc.isActive == true) return this.accountIsActive(res);

                new ActiveUser().by(req.params.code).then(result => {
                    if (result) return this.successfulVerification(res);
                    return this.codeDontExist(res);
                });
            });
        });
    }

    private userDontExist(res) {
        let iResult: I_Result = { complete: false, message: "Can't Active Account", data: RuteVerificationError.USER_NOT_EXIST };
        res.send(iResult);
    }

    private accountIsActive(res) {
        let iResult: I_Result = { complete: false, message: "Can't Active Account", data: RuteVerificationError.ACCOUNT_ACTIVE };
        res.send(iResult);
    }

    private successfulVerification(res) {
        let iResult: I_Result = { complete: true, message: "Account Active" };
        res.send(iResult);
    }

    private codeDontExist(res) {
        let iResult: I_Result = { complete: false, message: "Can't Active Account", data: RuteVerificationError.CODE_NOT_EXIST };
        res.send(iResult);
    }
}

export enum RuteVerificationError {
    ACCOUNT_ACTIVE,
    INVALID_VERIFICATION_CODE ,
    USER_NOT_EXIST,
    CODE_NOT_EXIST 

}