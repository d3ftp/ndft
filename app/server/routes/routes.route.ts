import { NextFunction, Request, Response, Router } from "express";
import { I_Route } from "../public/typescript/i_route";
import * as express from "express";
import { RouteDispatcher } from "../permission/route-dispatcher";

export class RoutesRoute implements I_Route {
    router: Router;

    constructor() {
        this.router = express.Router();
        this.create();
    }

    public create() {
        this.getAll();
    }

    public getAll() {
        this.router.get("/api/routes-all", (req, res) => {
            var routeDispatcher = new RouteDispatcher();
            routeDispatcher.getAll().then(docs => {
                res.send(docs)
            })
        })
    }
}