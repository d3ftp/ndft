import * as nodemailer from 'nodemailer';
import * as smtp from 'nodemailer-smtp-transport';
import { I_SystemMailerMessage } from "./i_system-mailer-message";

export class Nodemailer {
    private transporter: nodemailer.Transporter;
    public mailOptions;
    private auth = {
        user: '',
        pass : ''
    }

    constructor() {
        this.mailOptions = {
            from: '',
            to: '',
            subject: '',
            text: '',
            html: '',
            service: ''

        };
    }

    public sendMail(message: I_SystemMailerMessage, to: string) {
        this.mailOptions.to = to;
        this.mailOptions.subject = message.getSubject();
        this.mailOptions.html = message.getHtmlMsg();
        this.mailOptions.text = message.getTxtMsg();

        return this.transporter.sendMail({
            from: this.mailOptions.from,
            to: this.mailOptions.to,
            subject: this.mailOptions.service,
            html: this.mailOptions.html,
            text: this.mailOptions.text
        });
    }

    public initialize(from: string, service: string, auth: { user: string, pass: string }) {
        this.mailOptions.from = from;
        this.mailOptions.service = service;
        this.auth = auth;

        var smtpConfig = {
            host: 'smtp.gmail.com',
            port: 465,
            secure: true, // use SSL
            auth: {
                user: 'd3ftpcode@gmail.com',
                pass: 'olomTY93jjz'
            }
        };

        // this.transporter = nodemailer.createTransport(smtp({
        //     service: this.mailOptions.service,
        //     auth: this.auth
        // }));

        this.transporter = nodemailer.createTransport(smtpConfig);
        
    }
}