import { I_SystemMailerMessage } from "./i_system-mailer-message";

export abstract class SystemMailerMessage implements I_SystemMailerMessage {
    protected htmlMsg: string = '';
    protected txtMsg: string = '';
    protected subject: string = '';

    protected abstract className: string;

    public getSubject(): string {
        return this.subject
    }
    public getHtmlMsg(): string {
        return this.htmlMsg;
    }
    public getTxtMsg(): string {
        return this.txtMsg;
    }

    public getClassName(): string {
        return this.className;
    }

    initialize(txtMsg: string, htmlMsg: string, subject: string) {
        this.txtMsg = txtMsg;
        this.htmlMsg = htmlMsg;
        this.subject = subject;
    }

}