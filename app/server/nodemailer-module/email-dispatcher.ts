import { Nodemailer } from "./nodemailer";
import { I_SystemMailerMessage } from "./i_system-mailer-message";

//import nodemailerConfig = require('../database/models/nodemailer-config.model');
import systemMessageModel = require('../database/models/system-message.model');
import { SystemConfig } from "../system-config";

export class EmailDispatcher {
    private static nodemailer: Nodemailer = null;

    public sendMessage(message: I_SystemMailerMessage, to: string) {
        return EmailDispatcher.nodemailer.sendMail(message, to);
    }

    /**
     * Update System Mail
     * @param mail 
     * @param subject 
     * @param html 
     * @param txt 
     */
    public updateMailMessage(mail: I_SystemMailerMessage, subject: string, html: string, txt: string = '') {
        var promise = systemMessageModel.model.update({ className: mail.getClassName() }, { $set: { subject: subject, html: html, text: txt } }).exec();
        mail.initialize(txt, html, subject);

        return promise;
    }

    public UpdateMailerConfig(from: string, service: string, auth: { user: string, pass: string }) {
        var promise = SystemConfig.get().setNodemailerConfig(from, service, auth);
        EmailDispatcher.nodemailer.initialize(from, service, auth);

        return promise;
    }

    public static bootstrap() {
        return new Promise(resolve => {
            EmailDispatcher.nodemailer = new Nodemailer();
            new EmailDispatcher().InitializeNodemailer().then(() => {
                resolve(true);
            })
        })
    }

    /**
     * Initialize system messages
     * @param systemMessages 
     */

    public static initializeSystemMessage(systemMessages: I_SystemMailerMessage[]) {
        var foundResults: {
            promise: any,
            element: I_SystemMailerMessage;
        }[] = [];

        var results: any[] = [];

        systemMessages.forEach(el => {
            foundResults.push({ promise: systemMessageModel.model.findOne({ className: el.getClassName() }).exec(), element: el })
        });

        foundResults.forEach(el => {
            let promise = new Promise(resolve => {
                el.promise.then((doc: systemMessageModel.SystemMessageModel) => {
                    if (doc == null) resolve(EmailDispatcher.addSystemMailerToDatabase(el.element));
                    else {
                        el.element.initialize(doc.text, doc.html, doc.subject);
                        resolve(Promise.resolve(null));
                    }
                });
            });
            results.push(promise);
        });

        return Promise.all(results);

    }

    public static getNodeMailer(): Nodemailer {
        return EmailDispatcher.nodemailer;
    }

    private InitializeNodemailer() {
        return this.getNodemailerConfiguration().then(res => {
            EmailDispatcher.nodemailer.initialize(res.from, res.service, res.auth);
        });
    }

    private getNodemailerConfiguration() {
        return SystemConfig.get().getNodemailerConfig();
    }

    private static addSystemMailerToDatabase(message: I_SystemMailerMessage) {
        var msg = new systemMessageModel.model({
            className: message.getClassName()
        });
        return msg.save().then((res) => { return true }, reject => {
            throw new Error(`Something goes wrong with save System Mail to database ${reject}`);
        })
    }

    /**
     * Only for test! Dont use in real app
     */
    public static JASMINE_RESET_NODEMAILER() {
        EmailDispatcher.nodemailer = null;
    }

}