import { RegisterMail } from "./components/register-mail";
import { EmailChange } from "./components/email-change";

export class SystemMailFactory {
    public get(systemMail: SystemMail) {
        switch (systemMail) {
            case SystemMail.REGISTER:
                return RegisterMail.getInstance();
            case SystemMail.CHANGE_EMAIL:
                return EmailChange.getInstance();
            default: return null;

        }
    }

    public static toSystemMail(value: string) {
        return Number(value) as SystemMail
    }
}

export enum SystemMail {
    REGISTER,
    CHANGE_EMAIL
}