export interface I_SystemMailerMessage {
    getSubject(): string;
    getHtmlMsg(): string;
    getTxtMsg(): string;
    getClassName(): string;
    initialize(txtMsg: string, htmlMsg: string, subject: string);
}