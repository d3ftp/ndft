import { Component } from '../../basic-module/component';
import { Module } from '../../basic-module/module';
import { SyntaxVerification } from '../../syntax-verification/syntax-verification';
import { SystemMailerMessage } from "../system-mailer-message";

/**
 * Component adds to outData passwordVerificationMsg and passwordVerificationResult variables
 */

export class EmailChange extends SystemMailerMessage {
    protected static instance: EmailChange = null;
    protected className: string = 'EmailChangeComponent';
    private link: string;
    private oryginalHtmlMsg: string = '';

    private constructor() {
        super();
    }

    /**
     * set register link
     * @param link 
     */

    public setLink(link: string) {
        if (this.oryginalHtmlMsg == '') throw new Error('Link can be set only, before initialization object in Email Dispatcher!');
        this.link = link;
        this.htmlMsg = this.oryginalHtmlMsg;

        this.htmlMsg = this.htmlMsg.replace('##system_LINK##', this.link);
        if (this.htmlMsg == this.oryginalHtmlMsg) {
            throw new Error("Can't find ##system_LINK## !!!");
        }
    }

    initialize(txtMsg: string, htmlMsg: string, subject: string) {
        this.txtMsg = txtMsg;
        this.htmlMsg = htmlMsg;
        this.subject = subject;
        this.oryginalHtmlMsg = htmlMsg;
    }

    public getLink(): string {
        return this.link;
    }

    getHtmlMsg(): string {
        if (this.link == undefined || this.link == '' || this.link == null) throw new Error('Message link must be defined before send!');
        return this.htmlMsg;
    }

    public getOryginalMsg() { return this.oryginalHtmlMsg };

    public static getInstance(): EmailChange {
        if (EmailChange.instance == null) EmailChange.instance = new EmailChange();
        return EmailChange.instance;
    }

    /**
     * Only for test! Dont use in real app
     */
    public static JASMINE_RESET_INSTANCE() {
        EmailChange.instance = null;
    }

}